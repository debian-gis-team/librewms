/*
/ LayerTree.cpp
/ tree control to handle map layers
/
/ version 1.0, 2013 July 31
/
/ Author: Sandro Furieri a-furieri@lqt.it
/
/ Copyright (C) 2013  Alessandro Furieri
/
/    This program is free software: you can redistribute it and/or modify
/    it under the terms of the GNU General Public License as published by
/    the Free Software Foundation, either version 3 of the License, or
/    (at your option) any later version.
/
/    This program is distributed in the hope that it will be useful,
/    but WITHOUT ANY WARRANTY; without even the implied warranty of
/    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/    GNU General Public License for more details.
/
/    You should have received a copy of the GNU General Public License
/    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/
*/

#include "Classdef.h"

#include "wx/filename.h"
#include "wx/imaglist.h"

//
// ICONs in XPM format [universally portable]
//
#include "icons/wms.xpm"
#include "icons/on.xpm"
#include "icons/off.xpm"
#include "icons/about.xpm"
#include "icons/remove.xpm"
#include "icons/crs.xpm"
#include "icons/config.xpm"
#include "icons/group.xpm"
#include "icons/layer.xpm"

MyLayerTree::MyLayerTree(MyFrame * parent, wxWindowID id):wxTreeCtrl(parent, id)
{
//
// constructor: TREE control to manage Map Layers
//
  MainFrame = parent;
  Changed = true;
  CurrentItem = wxTreeItemId();
  DraggedItem = wxTreeItemId();
  Root = AddRoot(wxT("WMS Layers"));
// setting up icons 
  Images = new wxImageList(16, 16, true);
  wxIcon icons[3];
  icons[0] = wxIcon(wms_xpm);
  icons[1] = wxIcon(on_xpm);
  icons[2] = wxIcon(off_xpm);
  Images->Add(icons[0]);
  Images->Add(icons[1]);
  Images->Add(icons[2]);
  SetImageList(Images);
  SetItemImage(Root, 0);

// setting up event handlers 
  Connect(wxID_ANY, wxEVT_COMMAND_TREE_SEL_CHANGED,
          (wxObjectEventFunction) & MyLayerTree::OnSelChanged);
  Connect(wxID_ANY, wxEVT_COMMAND_TREE_ITEM_ACTIVATED,
          (wxObjectEventFunction) & MyLayerTree::OnItemActivated);
  Connect(wxID_ANY, wxEVT_COMMAND_TREE_ITEM_RIGHT_CLICK,
          (wxObjectEventFunction) & MyLayerTree::OnRightClick);
  Connect(Tree_CRS, wxEVT_COMMAND_MENU_SELECTED,
          (wxObjectEventFunction) & MyLayerTree::OnCmdMapCRS);
  Connect(Tree_RemoveAll, wxEVT_COMMAND_MENU_SELECTED,
          (wxObjectEventFunction) & MyLayerTree::OnCmdRemoveAll);
  Connect(Tree_ShowAll, wxEVT_COMMAND_MENU_SELECTED,
          (wxObjectEventFunction) & MyLayerTree::OnCmdShowAll);
  Connect(Tree_HideAll, wxEVT_COMMAND_MENU_SELECTED,
          (wxObjectEventFunction) & MyLayerTree::OnCmdHideAll);
  Connect(Tree_Visible, wxEVT_COMMAND_MENU_SELECTED,
          (wxObjectEventFunction) & MyLayerTree::OnCmdVisible);
  Connect(Tree_Configure, wxEVT_COMMAND_MENU_SELECTED,
          (wxObjectEventFunction) & MyLayerTree::OnCmdConfigure);
  Connect(Tree_LayerInfo, wxEVT_COMMAND_MENU_SELECTED,
          (wxObjectEventFunction) & MyLayerTree::OnCmdLayerInfo);
  Connect(Tree_RemoveLayer, wxEVT_COMMAND_MENU_SELECTED,
          (wxObjectEventFunction) & MyLayerTree::OnCmdRemoveLayer);
// Drag & Drop event handlers
  Connect(wxID_ANY, wxEVT_COMMAND_TREE_BEGIN_DRAG,
          (wxObjectEventFunction) & MyLayerTree::OnDragStart);
  Connect(wxID_ANY, wxEVT_COMMAND_TREE_BEGIN_RDRAG,
          (wxObjectEventFunction) & MyLayerTree::OnDragStart);
  Connect(wxID_ANY, wxEVT_COMMAND_TREE_END_DRAG,
          (wxObjectEventFunction) & MyLayerTree::OnDragEnd);
}

MyLayerTree::~MyLayerTree()
{
// destructor
  if (Images)
    delete Images;
}

void MyLayerTree::AddLayer(WmsLayer * layer)
{
//
// adds a layer to the TREE list
//
  wxTreeItemId item;
  wxString name;
  if (layer->GetTileServiceLayer() != NULL)
    name = wxString::FromUTF8(layer->GetTileServiceLayer()->GetTitle());
  else
    name = wxString::FromUTF8(layer->GetTitle());
  if (GetChildrenCount(Root) == 0)
    item = AppendItem(Root, name, 1);
  else
    item = InsertItem(Root, 0, name, 1);
  LayerObject *obj = new LayerObject(layer);
  SetItemData(item, (wxTreeItemData *) obj);
  Expand(Root);
}

void MyLayerTree::MarkCurrentItem()
{
//
// setting the Current Item as BOLD
//
  wxTreeItemId item;
  wxTreeItemIdValue cookie;
// unsetting the currently active WMS Layer
  MainFrame->SetActiveWmsLayer(NULL);
  item = GetFirstChild(GetRoot(), cookie);
  while (item.IsOk() == true)
    {
      if (item == CurrentItem)
        {
          SetItemBold(item, true);
          LayerObject *obj = (LayerObject *) GetItemData(item);
          if (obj != NULL)
            {
              WmsLayer *layer = obj->GetLayer();
              if (layer->IsVisible() == 1)
                {
                  // making this WMS Layer to be currently active
                  MainFrame->SetActiveWmsLayer(layer);
                }
            }
      } else
        SetItemBold(item, false);
      item = GetNextChild(GetRoot(), cookie);
    }
}

void MyLayerTree::OnSelChanged(wxTreeEvent & event)
{
//
// selecting some node [mouse action]
//
  wxTreeItemId item = event.GetItem();
  if (item == Root)
    {
      CurrentItem = wxTreeItemId();
      MarkCurrentItem();
      return;
    }
  CurrentItem = item;
  MarkCurrentItem();
}

void MyLayerTree::OnItemActivated(wxTreeEvent & event)
{
//
// activating some node [mouse action]
//
  int idx = 1;
  wxColour color(0, 0, 0);
  wxTreeItemId item = event.GetItem();
  if (item == Root)
    return;
  LayerObject *obj = (LayerObject *) GetItemData(item);
  if (obj == NULL)
    return;
  WmsLayer *layer = obj->GetLayer();
  if (layer->IsVisible() == 1)
    {
      layer->Hide();
      idx = 2;
      color = wxColour(192, 192, 192);
  } else
    layer->Show();
  SetItemImage(item, idx);
  SetItemTextColour(item, color);
  MainFrame->RefreshMap();
}

void MyLayerTree::OnRightClick(wxTreeEvent & event)
{
//
// right click on some node [mouse action]
//
  wxMenuItem *menuItem;
  wxMenu *menu = new wxMenu;
  wxTreeItemId item = event.GetItem();
  SelectItem(item);
  wxPoint pt = event.GetPoint();
  if (item == Root)
    {
      CurrentItem = wxTreeItemId();
      MarkCurrentItem();
      menuItem = new wxMenuItem(menu, Tree_HideAll, wxT("Hide All"));
      menuItem->SetBitmap(wxBitmap(off_xpm));
      menu->Append(menuItem);
      menuItem = new wxMenuItem(menu, Tree_ShowAll, wxT("Show All"));
      menuItem->SetBitmap(wxBitmap(on_xpm));
      menu->Append(menuItem);
      menuItem = new wxMenuItem(menu, Tree_RemoveAll, wxT("Remove All"));
      menuItem->SetBitmap(wxBitmap(remove_xpm));
      menu->Append(menuItem);
      menuItem =
        new wxMenuItem(menu, Tree_CRS, wxT("Map Reference System [CRS]"));
      menuItem->SetBitmap(wxBitmap(crs_xpm));
      menu->Append(menuItem);
      PopupMenu(menu, pt);
      return;
    }
  LayerObject *obj = (LayerObject *) GetItemData(item);
  if (obj == NULL)
    {
      CurrentItem = wxTreeItemId();
      MarkCurrentItem();
      menuItem = new wxMenuItem(menu, Tree_HideAll, wxT("Hide All"));
      menuItem->SetBitmap(wxBitmap(off_xpm));
      menu->Append(menuItem);
      menuItem = new wxMenuItem(menu, Tree_ShowAll, wxT("Show All"));
      menuItem->SetBitmap(wxBitmap(on_xpm));
      menu->Append(menuItem);
      menuItem = new wxMenuItem(menu, Tree_RemoveAll, wxT("Remove All"));
      menuItem->SetBitmap(wxBitmap(remove_xpm));
      menu->Append(menuItem);
      menuItem =
        new wxMenuItem(menu, Tree_CRS, wxT("Map Reference System [CRS]"));
      menuItem->SetBitmap(wxBitmap(crs_xpm));
      menu->Append(menuItem);
      PopupMenu(menu, pt);
      return;
  } else
    {
      CurrentItem = item;
      MarkCurrentItem();
      WmsLayer *layer = obj->GetLayer();
      wxString name;
      if (layer->GetTileServiceLayer() != NULL)
        name = wxString::FromUTF8(layer->GetTileServiceLayer()->GetTitle());
      else
        name = wxString::FromUTF8(layer->GetTitle());
      menu->SetTitle(name);
      if (layer->IsVisible() == true)
        {
          menuItem = new wxMenuItem(menu, Tree_Visible, wxT("Hide"));
          menuItem->SetBitmap(wxBitmap(off_xpm));
          menu->Append(menuItem);
      } else
        {
          menuItem = new wxMenuItem(menu, Tree_Visible, wxT("Show"));
          menuItem->SetBitmap(wxBitmap(on_xpm));
          menu->Append(menuItem);
        }
      if (layer->GetTileServiceLayer() == NULL)
        {
          menuItem = new wxMenuItem(menu, Tree_Configure, wxT("Configure"));
          menuItem->SetBitmap(wxBitmap(config_xpm));
          menu->Append(menuItem);
        }
      menuItem = new wxMenuItem(menu, Tree_LayerInfo, wxT("WMS Metadata"));
      menuItem->SetBitmap(wxBitmap(about_xpm));
      menu->Append(menuItem);
      menuItem = new wxMenuItem(menu, Tree_RemoveLayer, wxT("Remove"));
      menuItem->SetBitmap(wxBitmap(remove_xpm));
      menu->Append(menuItem);
      menu->AppendSeparator();
      menuItem =
        new wxMenuItem(menu, Tree_CRS, wxT("Map Reference System [CRS]"));
      menuItem->SetBitmap(wxBitmap(crs_xpm));
      menu->Append(menuItem);
      PopupMenu(menu, pt);
      return;
    }
}

void MyLayerTree::OnCmdLayerInfo(wxCommandEvent & WXUNUSED(event))
{
//
// menu event - metadata from some WMS layer
//
  LayerObject *obj = (LayerObject *) GetItemData(CurrentItem);
  if (obj == NULL)
    return;
  WmsLayer *layer = obj->GetLayer();
  WmsInfoDialog dlg;
  dlg.Create(MainFrame, layer);
  dlg.ShowModal();
}

void MyLayerTree::OnCmdRemoveLayer(wxCommandEvent & WXUNUSED(event))
{
//
// menu event - removing some WMS layer
//
  LayerObject *obj = (LayerObject *) GetItemData(CurrentItem);
  if (obj == NULL)
    return;
  WmsLayer *layer = obj->GetLayer();
  Delete(CurrentItem);
  CurrentItem = wxTreeItemId();
  MainFrame->GetMapView()->RemoveWmsLayer(layer);
  MainFrame->RefreshMap();
  MainFrame->UpdateTools();
}

void MyLayerTree::OnCmdRemoveAll(wxCommandEvent & WXUNUSED(event))
{
//
// menu event - removing all WMS layers [Remove ALL]
//
  LayerObject *obj;
  wxTreeItemId layer;
  wxTreeItemIdValue cookie;
  ::wxBeginBusyCursor();
  Hide();
  layer = GetFirstChild(Root, cookie);
  while (layer.IsOk() == true)
    {
      // making a Layer to be visible
      obj = (LayerObject *) GetItemData(layer);
      WmsLayer *lyr = obj->GetLayer();
      if (obj == NULL)
        return;
      MainFrame->GetMapView()->RemoveWmsLayer(lyr);
      layer = GetNextChild(Root, cookie);
    }
  DeleteChildren(Root);
  Show();
  ::wxEndBusyCursor();
  CurrentItem = wxTreeItemId();
  MainFrame->RefreshMap();
}

void MyLayerTree::OnCmdShowAll(wxCommandEvent & WXUNUSED(event))
{
//
// menu event - changing the visibility state [SHOW ALL]
//
  LayerObject *obj;
  wxTreeItemId layer;
  wxTreeItemIdValue cookie;
  ::wxBeginBusyCursor();
  Hide();
  layer = GetFirstChild(Root, cookie);
  while (layer.IsOk() == true)
    {
      // making a Layer to be visible
      obj = (LayerObject *) GetItemData(layer);
      WmsLayer *lyr = obj->GetLayer();
      lyr->Show();
      SetItemImage(layer, 1);
      SetItemTextColour(layer, wxColour(0, 0, 0));
      layer = GetNextChild(Root, cookie);
    }
  Show();
  ::wxEndBusyCursor();
  MainFrame->RefreshMap();
}

void MyLayerTree::OnCmdHideAll(wxCommandEvent & WXUNUSED(event))
{
//
// menu event - changing the visibility state [HIDE ALL]
//
  LayerObject *obj;
  wxTreeItemId layer;
  wxTreeItemIdValue cookie;
  wxTreeItemId subClass;
  ::wxBeginBusyCursor();
  Hide();
  layer = GetFirstChild(Root, cookie);
  while (layer.IsOk() == true)
    {
      // making a Layer to be invisible
      obj = (LayerObject *) GetItemData(layer);
      WmsLayer *lyr = obj->GetLayer();
      lyr->Hide();
      SetItemImage(layer, 2);
      SetItemTextColour(layer, wxColour(192, 192, 192));
      layer = GetNextChild(Root, cookie);
    }
  Show();
  ::wxEndBusyCursor();
  MainFrame->RefreshMap();
}

void MyLayerTree::OnCmdMapCRS(wxCommandEvent & WXUNUSED(event))
{
//
// menu event - setting the Map CRS
//
  WmsMapCrsDialog dlg;
  dlg.Create(MainFrame);
  int ret = dlg.ShowModal();
  if (ret == wxID_OK)
    {
      if (dlg.GetCRS().Len() > 0)
        {
          char *crs = new char[dlg.GetCRS().Len() + 1];
          strcpy(crs, dlg.GetCRS().ToUTF8());
          MainFrame->SetMapCRS(crs);
          delete[]crs;
        }
    }
}

void MyLayerTree::OnCmdVisible(wxCommandEvent & WXUNUSED(event))
{
//
// menu event - changing the visibility state
//
  int idx = 1;
  wxColour color(0, 0, 0);
  LayerObject *obj = (LayerObject *) GetItemData(CurrentItem);
  if (obj == NULL)
    return;
  WmsLayer *layer = obj->GetLayer();
  if (layer->IsVisible() == 1)
    {
      layer->Hide();
      color = wxColour(192, 192, 192);
      idx = 2;
  } else
    layer->Show();

  SetItemImage(CurrentItem, idx);
  SetItemTextColour(CurrentItem, color);

  MainFrame->RefreshMap();
}

void MyLayerTree::OnCmdConfigure(wxCommandEvent & WXUNUSED(event))
{
//
// menu event - changing the Layer Configuration
//
  LayerObject *obj = (LayerObject *) GetItemData(CurrentItem);
  if (obj == NULL)
    return;
  WmsLayer *layer = obj->GetLayer();
  WmsLayerDialog dlg;
  dlg.Create(MainFrame, layer);
  int ret = dlg.ShowModal();
  if (ret == wxID_OK)
    {
      bool bboxChanged = false;
      if (layer->GetMinX() != dlg.GetMinX())
        bboxChanged = true;
      if (layer->GetMaxX() != dlg.GetMaxX())
        bboxChanged = true;
      if (layer->GetMinY() != dlg.GetMinY())
        bboxChanged = true;
      if (layer->GetMaxY() != dlg.GetMaxY())
        bboxChanged = true;
      layer->Reconfigure(dlg.IsTiled(), dlg.GetTileWidth(), dlg.GetTileHeight(),
                         dlg.IsOpaque(), dlg.GetVersion(), dlg.GetStyleName(),
                         dlg.GetStyleTitle(), dlg.GetStyleAbstract(),
                         dlg.GetFormat(), dlg.GetCRS(), dlg.IsSwapXY(),
                         dlg.GetMinX(), dlg.GetMaxX(), dlg.GetMinY(),
                         dlg.GetMaxY());
      if (bboxChanged == true)
        MainFrame->SetFullExtent();
      MainFrame->RefreshMap();
    }
}

void MyLayerTree::OnDragStart(wxTreeEvent & event)
{
//
// user event: starting DRAG [drag&drop]
//
  DraggedItem = event.GetItem();
  LayerObject *obj = (LayerObject *) GetItemData(DraggedItem);
  if (obj == NULL)
    {
      DraggedItem = wxTreeItemId();
      return;
    }
  event.Allow();
}

void MyLayerTree::OnDragEnd(wxTreeEvent & event)
{
//
// user event: DRAG ended [drag&drop]
//
  LayerObject *obj = (LayerObject *) GetItemData(DraggedItem);
  if (obj == NULL)
    return;
  wxTreeItemId currentItem = event.GetItem();
  WmsLayer *layer = obj->GetLayer();
  int idx = 1;
  if (layer->IsVisible() == 0)
    idx = 2;
// 
// creating the new node
//
  wxTreeItemId position;
  wxString name = wxString::FromUTF8(layer->GetName());
  if (currentItem == Root)
    position = PrependItem(Root, name, idx);
  else if (currentItem == GetLastChild(Root))
    position = AppendItem(Root, name, idx);
  else
    position = InsertItem(Root, currentItem, name, idx);
  obj = new LayerObject(layer);
  SetItemData(position, (wxTreeItemData *) obj);
  Delete(DraggedItem);
  DraggedItem = wxTreeItemId();
// updating the WMS Layer list
  MainFrame->GetMapView()->ResetWmsLayers();
  wxTreeItemIdValue cookie;
  wxTreeItemId lyr = GetFirstChild(Root, cookie);
  while (lyr.IsOk() == true)
    {
      obj = (LayerObject *) GetItemData(lyr);
      layer = obj->GetLayer();
      MainFrame->GetMapView()->ReinsertWmsLayer(layer);
      lyr = GetNextChild(Root, cookie);
    }
  MainFrame->RefreshMap();
}

MyCatalogTree::MyCatalogTree(WmsDialog * parent, wxSize sz, wxWindowID id):wxTreeCtrl(parent, id, wxDefaultPosition,
           sz)
{
//
// constructor: TREE control to manage Map Layers
//
  MainDialog = parent;
  CurrentItem = wxTreeItemId();
// setting up icons 
  Images = new wxImageList(16, 16, true);
  wxIcon icons[3];
  icons[0] = wxIcon(wms_xpm);
  icons[1] = wxIcon(group_xpm);
  icons[2] = wxIcon(layer_xpm);
  Images->Add(icons[0]);
  Images->Add(icons[1]);
  Images->Add(icons[2]);
  SetImageList(Images);

// setting up event handlers 
  Connect(wxID_ANY, wxEVT_COMMAND_TREE_SEL_CHANGED,
          (wxObjectEventFunction) & MyCatalogTree::OnSelChanged);
  Connect(Tree_ShowAll, wxEVT_COMMAND_MENU_SELECTED,
          (wxObjectEventFunction) & MyLayerTree::OnCmdShowAll);
  Connect(Tree_HideAll, wxEVT_COMMAND_MENU_SELECTED,
          (wxObjectEventFunction) & MyLayerTree::OnCmdHideAll);
}

MyCatalogTree::~MyCatalogTree()
{
// destructor
  if (Images)
    delete Images;
}

void MyCatalogTree::AddLayer(rl2WmsLayerPtr handle, const char *layer)
{
//
// adds a layer to the Catalog TREE list
//
  if (Root.IsOk() == true)
    {
      AddLayer(Root, handle, layer);
      return;
    }
  int icon = 2;
  wxString name = wxString::FromUTF8(layer);
  CatalogObject *obj = new CatalogObject(handle, layer);
  if (wms_layer_has_children(handle))
    icon = 1;
  Root = AddRoot(name, icon);
  SetItemData(Root, (wxTreeItemData *) obj);
  if (wms_layer_has_children(handle))
    {
      // expanding Root's Children
      int nLayers = get_wms_layer_children_count(handle);
      for (int i = 0; i < nLayers; i++)
        {
          rl2WmsLayerPtr layer = get_wms_child_layer(handle, i);
          const char *x_name = get_wms_layer_title(layer);
          AddLayer(Root, layer, x_name);
        }
    }
}

void MyCatalogTree::AddLayer(wxTreeItemId parent, rl2WmsLayerPtr handle,
                             const char *layer)
{
//
// adds a child layer to its parent
//
  wxTreeItemId item;
  int icon = 2;
  wxString name = wxString::FromUTF8(layer);
  CatalogObject *obj = new CatalogObject(handle, layer);
  if (wms_layer_has_children(handle))
    icon = 1;
  item = AppendItem(parent, name, icon);
  SetItemData(item, (wxTreeItemData *) obj);
  ExpandChildren(item, handle);
}

void MyCatalogTree::AddTiledRoot(const char *layer)
{
//
// adds a tiled layer Root to the Catalog TREE list
//
  wxString name = wxString::FromUTF8(layer);
  CatalogObject *obj = new CatalogObject(layer);
  Root = AddRoot(name, 1);
  SetItemData(Root, (wxTreeItemData *) obj);
}

void MyCatalogTree::AddTiledLayer(rl2WmsTiledLayerPtr handle, const char *layer)
{
//
// adds a tiled layer to the Catalog TREE list
//
  AddTiledLayer(Root, handle, layer);
}

void MyCatalogTree::AddTiledLayer(wxTreeItemId parent,
                                  rl2WmsTiledLayerPtr handle, const char *layer)
{
//
// adds a child tiled layer to its parent
//
  wxTreeItemId item;
  int icon = 2;
  wxString name = wxString::FromUTF8(layer);
  CatalogObject *obj = new CatalogObject(handle, layer);
  if (wms_tiled_layer_has_children(handle))
    icon = 1;
  item = AppendItem(parent, name, icon);
  SetItemData(item, (wxTreeItemData *) obj);
  ExpandChildren(item, handle);
}

void MyCatalogTree::OnSelChanged(wxTreeEvent & event)
{
//
// selecting some node [mouse action]
//
  wxTreeItemId item = event.GetItem();
  CurrentItem = item;
  MarkCurrentItem();
  CatalogObject *obj = (CatalogObject *) GetItemData(item);
  if (obj->GetHandle() != NULL)
    MainDialog->SelectLayer(obj->GetHandle());
  else if (obj->GetTiledHandle() != NULL)
    MainDialog->SelectLayer(obj->GetTiledHandle());
  else
    MainDialog->SelectLayer();
}

void MyCatalogTree::RootAutoSelect()
{
//
// auto-selecting the Root node
//
  if (Root.IsOk() == false)
    return;
  CurrentItem = Root;
  SetItemBold(Root, true);
  SelectItem(Root);
  MarkCurrentItem();
  CatalogObject *obj = (CatalogObject *) GetItemData(Root);
  MainDialog->SelectLayer(obj->GetHandle());
}

void MyCatalogTree::MarkCurrentItem()
{
//
// setting the Current Item as BOLD
//
  wxTreeItemId item;
  wxTreeItemIdValue cookie;
  item = GetFirstChild(Root, cookie);
  while (item.IsOk() == true)
    {
      if (item == CurrentItem)
        SetItemBold(item, true);
      else
        SetItemBold(item, false);
      item = GetNextChild(Root, cookie);
    }
}

void MyCatalogTree::ExpandChildren(wxTreeItemId parent, rl2WmsLayerPtr handle)
{
//
// expanding Children Layers
//
  if (parent.IsOk() == false)
    return;
  int nLayers = get_wms_layer_children_count(handle);
  for (int i = 0; i < nLayers; i++)
    {
      rl2WmsLayerPtr layer = get_wms_child_layer(handle, i);
      const char *x_name = get_wms_layer_title(layer);
      AddLayer(parent, layer, x_name);
    }
}

void MyCatalogTree::ExpandChildren(wxTreeItemId parent,
                                   rl2WmsTiledLayerPtr handle)
{
//
// expanding Children Tiled Layers
//
  if (parent.IsOk() == false)
    return;
  int nLayers = get_wms_tiled_layer_children_count(handle);
  for (int i = 0; i < nLayers; i++)
    {
      rl2WmsTiledLayerPtr layer = get_wms_child_tiled_layer(handle, i);
      const char *x_name = get_wms_tiled_layer_name(layer);
      AddTiledLayer(parent, layer, x_name);
    }
}
