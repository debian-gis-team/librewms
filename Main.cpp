/*
/ Main.cpp
/ the main core of LibreWMS
/
/ version 1.0, 2013 July 28
/
/ Author: Sandro Furieri a-furieri@lqt.it
/
/ Copyright (C) 2013  Alessandro Furieri
/
/    This program is free software: you can redistribute it and/or modify
/    it under the terms of the GNU General Public License as published by
/    the Free Software Foundation, either version 3 of the License, or
/    (at your option) any later version.
/
/    This program is distributed in the hope that it will be useful,
/    but WITHOUT ANY WARRANTY; without even the implied warranty of
/    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/    GNU General Public License for more details.
/
/    You should have received a copy of the GNU General Public License
/    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/
*/

#include "Classdef.h"

#include "wx/menu.h"
#include "wx/aboutdlg.h"
#include "wx/config.h"

//
// ICONs in XPM format [universally portable]
//
#include "icons/icon.xpm"
#include "icons/icon_info.xpm"
#include "icons/wms.xpm"
#include "icons/center.xpm"
#include "icons/identify.xpm"
#include "icons/zoom_in.xpm"
#include "icons/zoom_out.xpm"
#include "icons/user_scale.xpm"
#include "icons/pan.xpm"
#include "icons/cache.xpm"
#include "icons/wms_abort.xpm"
#include "icons/printer.xpm"
#include "icons/about.xpm"
#include "icons/exit.xpm"

IMPLEMENT_APP(MyApp)
     bool MyApp::OnInit()
{
//
// main APP implementation
//
  MyFrame *frame = new MyFrame(wxT("LibreWMS      [free WMS viewer]"),
                               wxPoint(0, 0), wxSize(800, 600));
  frame->Show(true);
  SetTopWindow(frame);
  frame->CreateMemoryDB();
  frame->LoadConfig();
  return true;
}

MyFrame::MyFrame(const wxString & title, const wxPoint & pos, const wxSize & size):
wxFrame((wxFrame *) NULL, -1, title, pos,
        size)
{
//
// main GUI frame constructor
//

// forcing DECIMAL POINT IS COMMA
  setlocale(LC_NUMERIC, "C");

// creating the internal WMS Cache
  WmsCache = create_wms_cache();

  ::wxInitAllImageHandlers();

  IsIdentify = false;
  IsZoomIn = false;
  IsZoomOut = false;
  IsPan = false;

  BtnWms = new wxBitmap(wms_xpm);
  BtnCenter = new wxBitmap(center_xpm);
  BtnIdentify = new wxBitmap(identify_xpm);
  BtnZoomIn = new wxBitmap(zoom_in_xpm);
  BtnZoomOut = new wxBitmap(zoom_out_xpm);
  BtnUserScale = new wxBitmap(user_scale_xpm);
  BtnPan = new wxBitmap(pan_xpm);
  BtnCache = new wxBitmap(cache_xpm);
  BtnWmsAbort = new wxBitmap(wms_abort_xpm);
  BtnPrinter = new wxBitmap(printer_xpm);
  BtnAbout = new wxBitmap(about_xpm);
  BtnExit = new wxBitmap(exit_xpm);

//
// setting up the application icon
//      
  wxIcon MyIcon(icon_xpm);
  SetIcon(MyIcon);

//
// setting up panes
//
  LayerTree = new MyLayerTree(this);
  MapView = new MyMapView(this);
  Manager.SetManagedWindow(this);
  wxAuiPaneInfo paneView = wxAuiPaneInfo().Centre();
  paneView.Name(wxT("map_view"));
  paneView.CaptionVisible(false);
  paneView.Floatable(true);
  paneView.Dockable(true);
  paneView.Movable(true);
  paneView.Gripper(false);
  paneView.CloseButton(false);
  Manager.AddPane(MapView, paneView);
  wxAuiPaneInfo paneTree = wxAuiPaneInfo().Left();
  paneTree.Name(wxT("tree_view"));
  paneTree.DefaultPane();
  paneTree.CaptionVisible(false);
  paneTree.Floatable(true);
  paneTree.Dockable(true);
  paneTree.Movable(true);
  paneTree.Gripper(true);
  paneTree.CloseButton(false);
  paneTree.BestSize(wxSize(200, 480));
  Manager.AddPane(LayerTree, paneTree, wxPoint(0, 10));
  Manager.Update();
  Centre();
  ClearLayerTree();

//
// setting up the status bar
//
  wxStatusBar *statusBar = new wxStatusBar(this);
  statusBar->SetFieldsCount(4);
  int fldWidth[4];
  fldWidth[0] = -1;
  fldWidth[1] = -1;
  fldWidth[2] = -1;
  fldWidth[3] = -1;
  statusBar->SetStatusWidths(4, fldWidth);
  SetStatusBar(statusBar);

//
// setting up the menu bar
//
  wxMenu *menuFile = new wxMenu;
  wxMenuItem *menuItem;
  menuItem =
    new wxMenuItem(menuFile, ID_Predefined, wxT("&Predefined WMS services"));
  menuFile->Append(menuItem);
  menuItem = new wxMenuItem(menuFile, ID_Wms, wxT("&Add a WMS datasource"));
  menuItem->SetBitmap(*BtnWms);
  menuFile->Append(menuItem);
  menuFile->AppendSeparator();
  menuItem = new wxMenuItem(menuFile, ID_Center, wxT("Zoom to &Full Extent"));
  menuItem->SetBitmap(*BtnCenter);
  menuFile->Append(menuItem);
  menuItem =
    new wxMenuItem(menuFile, ID_ZoomIn, wxT("Zoom &In"), wxT(""), wxITEM_RADIO);
  menuFile->Append(menuItem);
  menuItem =
    new wxMenuItem(menuFile, ID_ZoomOut,
                   wxT("Zoom &Out"), wxT(""), wxITEM_RADIO);
  menuFile->Append(menuItem);
  menuItem = new wxMenuItem(menuFile, ID_UserScale, wxT("User defined &Scale"));
  menuItem->SetBitmap(*BtnUserScale);
  menuFile->Append(menuItem);
  menuItem =
    new wxMenuItem(menuFile, ID_Pan, wxT("&Pan"), wxT(""), wxITEM_RADIO);
  menuFile->Append(menuItem);
  menuItem =
    new wxMenuItem(menuFile, ID_Identify,
                   wxT("Identify"), wxT(""), wxITEM_RADIO);
  menuFile->Append(menuItem);
  menuFile->AppendSeparator();
  menuItem = new wxMenuItem(menuFile, ID_Cache, wxT("&WMS Cache"));
  menuItem->SetBitmap(*BtnCache);
  menuFile->Append(menuItem);
  menuFile->AppendSeparator();
  menuItem = new wxMenuItem(menuFile, ID_WmsAbort, wxT("WMS &Abort Refresh"));
  menuItem->SetBitmap(*BtnWmsAbort);
  menuFile->Append(menuItem);
  menuFile->AppendSeparator();
  menuItem = new wxMenuItem(menuFile, ID_Printer, wxT("&Printer"));
  menuItem->SetBitmap(*BtnPrinter);
  menuFile->Append(menuItem);
  menuFile->AppendSeparator();
  menuItem = new wxMenuItem(menuFile, wxID_ABOUT, wxT("&About ..."));
  menuItem->SetBitmap(*BtnAbout);
  menuFile->Append(menuItem);
  menuFile->AppendSeparator();
  menuItem = new wxMenuItem(menuFile, wxID_EXIT, wxT("&Quit"));
  menuItem->SetBitmap(*BtnExit);
  menuFile->Append(menuItem);
  wxMenuBar *menuBar = new wxMenuBar;
  menuBar->Append(menuFile, wxT("&Files"));
  SetMenuBar(menuBar);

//
// setting up menu initial state 
//
  menuBar->Enable(ID_Center, false);
  menuBar->Enable(ID_Identify, false);
  menuBar->Enable(ID_ZoomIn, false);
  menuBar->Enable(ID_ZoomOut, false);
  menuBar->Enable(ID_UserScale, false);
  menuBar->Enable(ID_Pan, false);
  menuBar->Enable(ID_WmsAbort, false);
  menuBar->Enable(ID_Printer, false);

//
// setting up the toolbar
//      
  wxToolBar *toolBar = CreateToolBar();
  toolBar->AddTool(ID_Wms, wxT("Add a WMS Layer"),
                   *BtnWms, wxNullBitmap, wxITEM_NORMAL,
                   wxT("Add a WMS Layer"));
  toolBar->AddSeparator();
  toolBar->AddTool(ID_Center, wxT("Zoom to Full Extent"),
                   *BtnCenter, wxNullBitmap, wxITEM_NORMAL,
                   wxT("Zoom to Full Extent"));
  toolBar->AddTool(ID_ZoomIn, wxT("Zoom In"), *BtnZoomIn, wxNullBitmap,
                   wxITEM_RADIO, wxT("Zoom In"));
  toolBar->AddTool(ID_ZoomOut, wxT("Zoom Out"), *BtnZoomOut, wxNullBitmap,
                   wxITEM_RADIO, wxT("Zoom Out"));
  toolBar->AddTool(ID_UserScale, wxT("User defined Scale"), *BtnUserScale,
                   wxNullBitmap, wxITEM_RADIO, wxT("User defined Scale"));
  toolBar->AddTool(ID_Pan, wxT("Pan"), *BtnPan, wxNullBitmap, wxITEM_RADIO,
                   wxT("Pan"));
  toolBar->AddTool(ID_Identify, wxT("Identify"), *BtnIdentify, wxNullBitmap,
                   wxITEM_RADIO, wxT("Identify"));
  toolBar->AddSeparator();
  toolBar->AddTool(ID_Cache, wxT("WMS Cache"),
                   *BtnCache, wxNullBitmap, wxITEM_NORMAL, wxT("WMS Cache"));
  toolBar->AddSeparator();
  toolBar->AddTool(ID_WmsAbort, wxT("WMS Abort Refresh"),
                   *BtnWmsAbort, wxNullBitmap, wxITEM_NORMAL,
                   wxT("WMS Abort Refresh"));
  toolBar->AddSeparator();
  toolBar->AddTool(ID_Printer, wxT("Printer"),
                   *BtnPrinter, wxNullBitmap, wxITEM_NORMAL, wxT("Printer"));
  toolBar->AddSeparator();
  toolBar->AddTool(wxID_ABOUT, wxT("About ..."), *BtnAbout, wxNullBitmap,
                   wxITEM_NORMAL, wxT("About ..."));
  toolBar->AddSeparator();
  toolBar->AddTool(wxID_EXIT, wxT("Quit"), *BtnExit, wxNullBitmap,
                   wxITEM_NORMAL, wxT("Quit"));
  toolBar->Realize();
  SetToolBar(toolBar);

//
// setting up the toolbar initial state
//
  toolBar->EnableTool(ID_Center, false);
  toolBar->EnableTool(ID_Identify, false);
  toolBar->EnableTool(ID_ZoomIn, false);
  toolBar->EnableTool(ID_ZoomOut, false);
  toolBar->EnableTool(ID_UserScale, false);
  toolBar->EnableTool(ID_Pan, false);
  toolBar->EnableTool(ID_WmsAbort, false);
  toolBar->EnableTool(ID_Printer, false);

//
// setting up event handlers for menu and toolbar
//
  Connect(ID_Wms, wxEVT_COMMAND_MENU_SELECTED,
          (wxObjectEventFunction) & MyFrame::OnWms);
  Connect(ID_Predefined, wxEVT_COMMAND_MENU_SELECTED,
          (wxObjectEventFunction) & MyFrame::OnPredefinedWms);
  Connect(ID_Center, wxEVT_COMMAND_MENU_SELECTED,
          (wxObjectEventFunction) & MyFrame::OnCenter);
  Connect(ID_Identify, wxEVT_COMMAND_MENU_SELECTED,
          (wxObjectEventFunction) & MyFrame::OnIdentify);
  Connect(ID_ZoomIn, wxEVT_COMMAND_MENU_SELECTED,
          (wxObjectEventFunction) & MyFrame::OnZoomIn);
  Connect(ID_ZoomOut, wxEVT_COMMAND_MENU_SELECTED,
          (wxObjectEventFunction) & MyFrame::OnZoomOut);
  Connect(ID_UserScale, wxEVT_COMMAND_MENU_SELECTED,
          (wxObjectEventFunction) & MyFrame::OnUserScale);
  Connect(ID_Pan, wxEVT_COMMAND_MENU_SELECTED,
          (wxObjectEventFunction) & MyFrame::OnPan);
  Connect(ID_Cache, wxEVT_COMMAND_MENU_SELECTED,
          (wxObjectEventFunction) & MyFrame::OnWmsCache);
  Connect(ID_WmsAbort, wxEVT_COMMAND_MENU_SELECTED,
          (wxObjectEventFunction) & MyFrame::OnWmsAbort);
  Connect(ID_Printer, wxEVT_COMMAND_MENU_SELECTED,
          (wxObjectEventFunction) & MyFrame::OnPrinter);
  Connect(wxID_ABOUT, wxEVT_COMMAND_MENU_SELECTED,
          (wxObjectEventFunction) & MyFrame::OnAbout);
  Connect(wxID_EXIT, wxEVT_COMMAND_MENU_SELECTED,
          (wxObjectEventFunction) & MyFrame::OnQuit);
  Connect(wxID_ANY, wxEVT_CLOSE_WINDOW,
          (wxObjectEventFunction) & MyFrame::OnClose);
}

MyFrame::~MyFrame()
{
//
// main GUI frame destructor
//
  ConfigLayout = Manager.SavePerspective();
  GetPosition(&ConfigPaneX, &ConfigPaneY);
  GetSize(&ConfigPaneWidth, &ConfigPaneHeight);
  SaveConfig();
  Manager.UnInit();
  CloseMemoryDB();
  if (WmsCache != NULL)
    destroy_wms_cache(WmsCache);
  if (BtnCenter != NULL)
    delete BtnCenter;
  if (BtnIdentify != NULL)
    delete BtnIdentify;
  if (BtnZoomIn != NULL)
    delete BtnZoomIn;
  if (BtnZoomOut != NULL)
    delete BtnZoomOut;
  if (BtnUserScale != NULL)
    delete BtnUserScale;
  if (BtnPan != NULL)
    delete BtnPan;
  if (BtnCache != NULL)
    delete BtnCache;
  if (BtnWmsAbort != NULL)
    delete BtnWmsAbort;
  if (BtnPrinter != NULL)
    delete BtnPrinter;
  if (BtnAbout != NULL)
    delete BtnAbout;
  if (BtnExit != NULL)
    delete BtnExit;
}

void MyFrame::CreateMemoryDB()
{
//
// creating a Memory DB (SQlite/Spatialite)
//
  char *errMsg = NULL;
  int ret = sqlite3_open_v2(":memory:", &SQLiteHandle,
                            SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE, NULL);
  if (ret)
    {
      // an error occurred
      wxString errCause;
      errCause = wxString::FromUTF8(sqlite3_errmsg(SQLiteHandle));
      sqlite3_close(SQLiteHandle);
      wxMessageBox(wxT("An error occurred\n\n") + errCause + wxT("\n"),
                   wxT("LibreWMS"), wxOK | wxICON_ERROR, this);
      SQLiteHandle = NULL;
      InternalCache = NULL;
      return;
    }
// setting up the internal cache
  InternalCache = spatialite_alloc_connection();
  spatialite_init_ex(SQLiteHandle, InternalCache, 0);
// activating Foreign Key constraints
  ret = sqlite3_exec(SQLiteHandle, "PRAGMA foreign_keys = 1", NULL, 0, &errMsg);
  if (ret != SQLITE_OK)
    {
      wxMessageBox(wxT("Unable to activate FOREIGN_KEY constraints"),
                   wxT("LibreWMS"), wxOK | wxICON_INFORMATION, this);
      sqlite3_free(errMsg);
      SQLiteHandle = NULL;
      return;
    }
  InitializeSpatialMetadata();
  InitializeWmsList();
}

void MyFrame::InitializeSpatialMetadata()
{
// attempting to perform self-initialization for Memory DB
  int ret;
  char *errMsg = NULL;
  if (!SQLiteHandle)
    return;
  ret =
    sqlite3_exec(SQLiteHandle, "SELECT InitSpatialMetadata(1)", NULL, NULL,
                 &errMsg);
  if (ret != SQLITE_OK)
    {
      wxMessageBox(wxT("Unable to initialite SpatialMetadata: ") +
                   wxString::FromUTF8(errMsg), wxT("LibreWMS"),
                   wxOK | wxICON_ERROR, this);
      sqlite3_free(errMsg);
      return;
    }
}

void MyFrame::CloseMemoryDB()
{
//
// disconnecting the SQLite Memory DB
//
  if (!SQLiteHandle)
    return;
  sqlite3_close(SQLiteHandle);
  SQLiteHandle = NULL;
  spatialite_cleanup_ex(InternalCache);
  InternalCache = NULL;
}

void MyFrame::UpdateTools()
{
//
// updating the Tools State
//
  wxMenuBar *menuBar = GetMenuBar();
  if (MapView->IsValidMap() == false)
    {
      menuBar->Enable(ID_Center, false);
      menuBar->Enable(ID_ZoomIn, false);
      menuBar->Enable(ID_ZoomOut, false);
      menuBar->Enable(ID_UserScale, false);
      menuBar->Enable(ID_Pan, false);
      menuBar->Enable(ID_Identify, false);
      menuBar->Enable(ID_Printer, false);
  } else
    {
      menuBar->Enable(ID_Center, true);
      menuBar->Enable(ID_ZoomIn, true);
      menuBar->Enable(ID_ZoomOut, true);
      menuBar->Enable(ID_UserScale, true);
      menuBar->Enable(ID_Pan, true);
      if (MapView->CanIdentify() == true)
        menuBar->Enable(ID_Identify, true);
      else
        menuBar->Enable(ID_Identify, false);
      menuBar->Enable(ID_Printer, true);
    }

  wxToolBar *toolBar = GetToolBar();
  if (MapView->IsValidMap() == false)
    {
      toolBar->EnableTool(ID_Center, false);
      toolBar->EnableTool(ID_ZoomIn, false);
      toolBar->EnableTool(ID_ZoomOut, false);
      toolBar->EnableTool(ID_UserScale, false);
      toolBar->EnableTool(ID_Pan, false);
      toolBar->EnableTool(ID_Identify, false);
      toolBar->EnableTool(ID_Printer, false);
  } else
    {
      toolBar->EnableTool(ID_Center, true);
      toolBar->EnableTool(ID_ZoomIn, true);
      toolBar->EnableTool(ID_ZoomOut, true);
      toolBar->EnableTool(ID_UserScale, true);
      toolBar->EnableTool(ID_Pan, true);
      if (MapView->CanIdentify() == true)
        toolBar->EnableTool(ID_Identify, true);
      else
        toolBar->EnableTool(ID_Identify, false);
      toolBar->EnableTool(ID_Printer, true);
    }

  if (MapView->IsValidMap() == false)
    {
      IsIdentify = false;
      IsZoomIn = false;
      IsZoomOut = false;
      IsPan = false;
  } else
    {
      if (IsIdentify == false && IsPan == false && IsZoomOut == false)
        IsZoomIn = true;
      if (IsIdentify == true)
        {
          menuBar->Check(ID_Identify, true);
          toolBar->ToggleTool(ID_Identify, true);
        }
      if (IsPan == true)
        {
          menuBar->Check(ID_Pan, true);
          toolBar->ToggleTool(ID_Pan, true);
        }
      if (IsZoomIn == true)
        {
          menuBar->Check(ID_ZoomIn, true);
          toolBar->ToggleTool(ID_ZoomIn, true);
        }
      if (IsZoomOut == true)
        {
          menuBar->Check(ID_ZoomOut, true);
          toolBar->ToggleTool(ID_ZoomOut, true);
        }
    }
}

void MyFrame::UpdateWmsAbortTool(bool mode)
{
//
// updating the WMS Abort Tool State
//
  wxMenuBar *menuBar = GetMenuBar();
  menuBar->Enable(ID_WmsAbort, mode);

  wxToolBar *toolBar = GetToolBar();
  toolBar->EnableTool(ID_WmsAbort, mode);
}

void MyFrame::UpdatePrinterTool(bool mode)
{
//
// updating the Printer Tool State
//
  wxMenuBar *menuBar = GetMenuBar();
  menuBar->Enable(ID_Printer, mode);

  wxToolBar *toolBar = GetToolBar();
  toolBar->EnableTool(ID_Printer, mode);
}

void MyFrame::OnWmsCache(wxCommandEvent & WXUNUSED(event))
{
//
// WMS Cache dialog
//
  WmsCacheDialog dlg;
  dlg.Create(this, WmsCache);
  dlg.ShowModal();
}

void MyFrame::OnWmsAbort(wxCommandEvent & WXUNUSED(event))
{
//
// WMS Abort Refresh request
//
  MapView->SetWmsAbort();
}

void MyFrame::OnCenter(wxCommandEvent & WXUNUSED(event))
{
//
// resetting to Full Extent
//
  MapView->SetFullExtent();
  MapView->PrepareMap();
}

void MyFrame::OnIdentify(wxCommandEvent & WXUNUSED(event))
{
//
// current map click is: Identify
//
  wxMenuBar *menuBar = GetMenuBar();
  wxToolBar *toolBar = GetToolBar();
  if (IsIdentify == false)
    {
      if (menuBar)
        menuBar->Check(ID_Identify, true);
      if (toolBar)
        toolBar->ToggleTool(ID_Identify, true);
      IsIdentify = true;
      IsZoomIn = false;
      IsZoomOut = false;
      IsPan = false;
    }
}

void MyFrame::OnZoomIn(wxCommandEvent & WXUNUSED(event))
{
//
// current map click is: ZoomIn
//
  wxMenuBar *menuBar = GetMenuBar();
  wxToolBar *toolBar = GetToolBar();
  if (IsZoomIn == false)
    {
      menuBar->Check(ID_ZoomIn, true);
      if (toolBar)
        toolBar->ToggleTool(ID_ZoomIn, true);
      IsIdentify = false;
      IsZoomIn = true;
      IsZoomOut = false;
      IsPan = false;
    }
}

void MyFrame::OnZoomOut(wxCommandEvent & WXUNUSED(event))
{
//
// current map click is: ZoomOut
//
  wxMenuBar *menuBar = GetMenuBar();
  wxToolBar *toolBar = GetToolBar();
  if (IsZoomOut == false)
    {
      if (menuBar)
        menuBar->Check(ID_ZoomOut, true);
      if (toolBar)
        toolBar->ToggleTool(ID_ZoomOut, true);
      IsIdentify = false;
      IsZoomIn = false;
      IsZoomOut = true;
      IsPan = false;
    }
}

void MyFrame::OnUserScale(wxCommandEvent & WXUNUSED(event))
{
//
// User selected Scale
//
  UserScaleDialog dlg;
  int scale = MapView->GetCurrentScale();
  dlg.Create(this, scale, MapView->GetPixelRatio());
  int ret = dlg.ShowModal();
  if (ret == wxID_OK)
    {
      if (scale != dlg.GetCurrentScale())
        MapView->SetCurrentScale(dlg.GetCurrentScale());
    }
}

void MyFrame::OnPrinter(wxCommandEvent & WXUNUSED(event))
{
//
// Printer
//
  PrinterDialog dlg;
  dlg.Create(this);
  int ret = dlg.ShowModal();
  if (ret == wxID_OK)
    {
      if (dlg.IsPDF())
        {
          wxFileDialog fileDialog(this, wxT("PDF output file"),
                                  wxT(""), wxT("map.pdf"),
                                  wxT("PDF document (*.pdf)|*.pdf"),
                                  wxFD_SAVE | wxFD_OVERWRITE_PROMPT,
                                  wxDefaultPosition, wxDefaultSize,
                                  wxT("filedlg"));
          wxString lastDir = GetLastDirectory();
          if (lastDir.Len() >= 1)
            fileDialog.SetDirectory(lastDir);
          ret = fileDialog.ShowModal();
          if (ret == wxID_OK)
            {
              // exporting the output PDF file
              ::wxBeginBusyCursor();
              int len = fileDialog.GetPath().Len();
              char *path = new char[len + 1];
              strcpy(path, fileDialog.GetPath().ToUTF8());
              PdfPrinter *printer = new
                PdfPrinter(this, path, dlg.GetDPI(), dlg.GetPageWidth(),
                           dlg.GetPageHeight(),
                           dlg.GetHorzMarginSize(), dlg.GetVertMarginSize(),
                           dlg.IsPreserveScale(), dlg.IsPreserveWidth(),
                           dlg.IsPreserveHeight());
              if (printer->IsValid() != true)
                {
                  wxMessageBox(wxT("Invalid PDF Printer !!!"),
                               wxT("LibreWMS"), wxOK | wxICON_ERROR, this);
              } else
                printer->Go();
              delete[]path;
            }
      } else
        {
          wxFileDialog fileDialog(this, wxT("TIFF output file"), wxT(""),
                                  wxT("map.tif"),
                                  wxT("TIFF image (*.tif)|*.tif"),
                                  wxFD_SAVE | wxFD_OVERWRITE_PROMPT,
                                  wxDefaultPosition, wxDefaultSize,
                                  wxT("filedlg"));
          wxString lastDir = GetLastDirectory();
          if (lastDir.Len() >= 1)
            fileDialog.SetDirectory(lastDir);
          ret = fileDialog.ShowModal();
          if (ret == wxID_OK)
            {
              // exporting the output TIFF file
              ::wxBeginBusyCursor();
              int len = fileDialog.GetPath().Len();
              char *path = new char[len + 1];
              strcpy(path, fileDialog.GetPath().ToUTF8());
              bool is_tiled = dlg.IsTiled();
              int tile_strip_size;
              if (is_tiled)
                tile_strip_size = dlg.GetTileSize();
              else
                tile_strip_size = dlg.GetStripSize();
              TiffWriter *writer = new
                TiffWriter(this, path, dlg.GetWidth(), dlg.GetHeight(),
                           dlg.IsGeoTiff(), dlg.HasWorldfile(),
                           dlg.GetCompression(),
                           is_tiled, tile_strip_size, dlg.IsPreserveScale(),
                           dlg.IsPreserveWidth(),
                           dlg.IsPreserveHeight());
              if (writer->IsValid() != true)
                {
                  wxMessageBox(wxT
                               ("Invalid TIFF Writer - Insufficient Memory !!!"),
                               wxT("LibreWMS"), wxOK | wxICON_ERROR, this);
              } else
                writer->Go();
              delete[]path;
            }
        }
    }
}

void MyFrame::OnPan(wxCommandEvent & WXUNUSED(event))
{
//
// current map click is: Pan
//
  wxMenuBar *menuBar = GetMenuBar();
  wxToolBar *toolBar = GetToolBar();
  if (IsPan == false)
    {
      if (menuBar)
        menuBar->Check(ID_Pan, true);
      if (toolBar)
        toolBar->ToggleTool(ID_Pan, true);
      IsIdentify = false;
      IsZoomIn = false;
      IsZoomOut = false;
      IsPan = true;
    }
}

void MyFrame::UpdateMapCRS()
{
//
// updating the status bar - CRS
//
  if (GetStatusBar() == NULL)
    return;
  const char *crs = MapView->GetMapCRS();
  if (crs == NULL)
    GetStatusBar()->SetStatusText(wxT("no current WMS Layer"), 0);
  else
    {
      wxString str = wxT("CRS ") + wxString::FromUTF8(crs);
      GetStatusBar()->SetStatusText(str, 0);
    }
}

void MyFrame::UpdateMapScale()
{
//
// updating the status bar - Current Scale
//
  if (GetStatusBar() == NULL)
    return;
  wxString scale;
  if (MapView->IsValidMap())
    {
      char dummy[256];
      sprintf(dummy, "Scale 1:%d", MapView->GetCurrentScale());
      scale = wxString::FromUTF8(dummy);
    }
  GetStatusBar()->SetStatusText(scale, 1);
}

void MyFrame::UpdateMapCoords(wxString & coords)
{
//
// updating the status bar [Coordinates]
//
  if (GetStatusBar() == NULL)
    return;
  GetStatusBar()->SetStatusText(coords, 2);
}

bool MyFrame::GetProgressRect(wxRect & rect)
{
//
// retrieving the status bar Progress rect
//
  if (GetStatusBar() == NULL)
    return false;
  return GetStatusBar()->GetFieldRect(3, rect);
}

void MyFrame::SaveConfig()
{
//
// saves layout configuration
//

  wxConfig *config = new wxConfig(wxT("LibreWMS"));
  config->Write(wxT("Layout"), ConfigLayout);
  config->Write(wxT("PaneX"), ConfigPaneX);
  config->Write(wxT("PaneY"), ConfigPaneY);
  config->Write(wxT("PaneWidth"), ConfigPaneWidth);
  config->Write(wxT("PaneHeight"), ConfigPaneHeight);
  config->Write(wxT("HttpProxy"), HttpProxy);
  config->Write(wxT("LastDirectory"), LastDirectory);
  delete config;
}

void MyFrame::LoadConfig()
{
//
// loads layout configuration
//
  ConfigLayout = wxT("");
  wxConfig *config = new wxConfig(wxT("LibreWMS"));
  config->Read(wxT("Layout"), &ConfigLayout);
  config->Read(wxT("PaneX"), &ConfigPaneX, -1);
  config->Read(wxT("PaneY"), &ConfigPaneY, -1);
  config->Read(wxT("PaneWidth"), &ConfigPaneWidth, -1);
  config->Read(wxT("PaneHeight"), &ConfigPaneHeight, -1);
  config->Read(wxT("HttpProxy"), &HttpProxy);
  config->Read(wxT("LastDirectory"), &LastDirectory);
  delete config;
  if (ConfigLayout.Len() > 0)
    Manager.LoadPerspective(ConfigLayout, true);
  if (ConfigPaneX >= 0 && ConfigPaneY >= 0 && ConfigPaneWidth > 0
      && ConfigPaneHeight > 0)
    SetSize(ConfigPaneX, ConfigPaneY, ConfigPaneWidth, ConfigPaneHeight);
}

void MyFrame::OnPredefinedWms(wxCommandEvent & WXUNUSED(event))
{
//
// selecting a pre-defined WMS datasource
//
  PredefinedWmsDialog dlg;
  dlg.Create(this, SQLiteHandle);
  int ret = dlg.ShowModal();
  if (ret == wxID_OK)
    {
      CurrentWmsDatasource = dlg.GetURL();
      WmsSelection(true);
    }
}

void MyFrame::WmsSelection(bool immediate)
{
//
// opening a WMS layer
//
  WmsDialog dlg;
  dlg.Create(this, HttpProxy, immediate);
  int ret = dlg.ShowModal();
  if (ret == wxID_OK)
    {
      HttpProxy = dlg.GetHttpProxy();
      CurrentWmsDatasource = dlg.GetURL();
      WmsService *service =
        MapView->InsertWmsService(dlg.GetServiceVersion(), dlg.GetServiceName(),
                                  dlg.GetServiceTitle(),
                                  dlg.GetServiceAbstract(),
                                  dlg.GetTileServiceName(),
                                  dlg.GetTileServiceTitle(),
                                  dlg.GetTileServiceAbstract(),
                                  dlg.GetURL_GetMap_Get(),
                                  dlg.GetURL_GetMap_Post(),
                                  dlg.GetURL_GetTileService_Get(),
                                  dlg.GetURL_GetTileService_Post(),
                                  dlg.GetURL_GetFeatureInfo_Get(),
                                  dlg.GetURL_GetFeatureInfo_Post(),
                                  dlg.GetGmlMimeType(), dlg.GetXmlMimeType(),
                                  dlg.GetContactPerson(),
                                  dlg.GetContactOrganization(),
                                  dlg.GetContactPosition(),
                                  dlg.GetPostalAddress(), dlg.GetCity(),
                                  dlg.GetStateProvince(), dlg.GetPostCode(),
                                  dlg.GetCountry(), dlg.GetVoiceTelephone(),
                                  dlg.GetFaxTelephone(), dlg.GetEMailAddress(),
                                  dlg.GetFees(), dlg.GetAccessConstraints(),
                                  dlg.GetLayerLimit(),
                                  dlg.GetMaxWidth(), dlg.GetMaxHeight());
      for (int s = 0; s < dlg.CountFormats(); s++)
        service->AddFormat(dlg.GetFormatByIndex(s));
      WmsLayer *lyr;
      if (dlg.IsWmsTileService())
        {
          // special WMS TileService Layer
          lyr = new WmsLayer(service, dlg.GetTiledLayerName(),
                             dlg.GetTiledLayerTitle(),
                             dlg.GetTiledLayerAbstract(),
                             dlg.GetTiledLayerMinLong(),
                             dlg.GetTiledLayerMinLat(),
                             dlg.GetTiledLayerMaxLong(),
                             dlg.GetTiledLayerMaxLat(),
                             dlg.GetTiledLayerPad(),
                             dlg.GetTiledLayerBands(),
                             dlg.GetTiledLayerDataType());
          if (lyr != NULL)
            {
              int nPatterns = dlg.GetTilePatternCount();
              for (int i = 0; i < nPatterns; i++)
                {
                  rl2WmsTilePatternPtr handle = dlg.GetTilePatternHandle(i);
                  const char *srs = dlg.GetTilePatternSRS(i);
                  int width = dlg.GetTilePatternWidth(i);
                  int height = dlg.GetTilePatternHeight(i);
                  double base_x = dlg.GetTilePatternBaseX(i);
                  double base_y = dlg.GetTilePatternBaseY(i);
                  double ext_x = dlg.GetTilePatternExtentX(i);
                  double ext_y = dlg.GetTilePatternExtentY(i);
                  lyr->GetTileServiceLayer()->AddTilePattern(handle, srs, width,
                                                             height, base_x,
                                                             base_y, ext_x,
                                                             ext_y);
                }
              lyr->GetTileServiceLayer()->BuildFixedScales();
              MapView->InsertWmsLayer(lyr);
              MapView->BuildCRSlist();
            }
      } else
        {
          // ordinary WMS Layer
          lyr = new WmsLayer(service, dlg.GetName(), dlg.GetTitle(),
                             dlg.GetAbstract(), dlg.IsTiled(),
                             dlg.GetTileWidth(), dlg.GetTileHeight(),
                             dlg.IsQueryable(), dlg.IsOpaque(),
                             dlg.GetVersion(), dlg.GetStyleName(),
                             dlg.GetStyleTitle(), dlg.GetStyleAbstract(),
                             dlg.GetFormat(), dlg.GetCRS(),
                             dlg.IsSwapXY(), dlg.GetMinX(),
                             dlg.GetMaxX(), dlg.GetMinY(), dlg.GetMaxY(),
                             dlg.GetGeoMinX(), dlg.GetGeoMaxX(),
                             dlg.GetGeoMinY(), dlg.GetGeoMaxY(),
                             dlg.GetMinScaleDenominator(),
                             dlg.GetMaxScaleDenominator());
          if (lyr != NULL)
            {
              for (int s = 0; s < dlg.CountStyles(); s++)
                {
                  const char *name;
                  const char *title;
                  const char *abstract;
                  dlg.GetStyleByIndex(s, &name, &title, &abstract);
                  lyr->AddStyle(name, title, abstract);
                }
              for (int s = 0; s < dlg.CountCRS(); s++)
                {
                  WmsCRS *crs = lyr->AddCRS(dlg.GetCrsByIndex(s));
                  if (crs != NULL)
                    crs->SetBBox(dlg.GetBBoxByCRS(crs->GetCRS()));
                }
              if (lyr->IsQueryable() == false)
                {
                  // attempting to identify queryable children 
                  rl2WmsLayerPtr lyr_handle = dlg.GetLayerHandle();
                  AddQueryableChildren(lyr, lyr_handle);
                }
              MapView->InsertWmsLayer(lyr);
              MapView->BuildCRSlist();
            }
        }
    }
}

void MyFrame::AddQueryableChildren(WmsLayer * lyr, rl2WmsLayerPtr handle)
{
//
// recursively adding Queryable children Layers
//
  int count = get_wms_layer_children_count(handle);
  for (int i = 0; i < count; i++)
    {
      // testing for Queryable children
      const char *name = NULL;
      const char *title = NULL;
      rl2WmsLayerPtr child = get_wms_child_layer(handle, i);
      if (child == NULL)
        continue;
      if (is_wms_layer_queryable(child))
        {
          name = get_wms_layer_name(child);
          title = get_wms_layer_title(child);
        }
      if (name != NULL)
        lyr->AddQueryableChild(name, title);
      else
        AddQueryableChildren(lyr, child);
    }
}

void MyFrame::OnWms(wxCommandEvent & WXUNUSED(event))
{
//
// opening a WMS datasource
  WmsSelection(false);
}

void MyFrame::ClearLayerTree()
{
// resets the layer TREE list to the empty state
  LayerTree->Show(false);
  LayerTree->FlushAll();
  LayerTree->Show(true);
}

void MyFrame::OnQuit(wxCommandEvent & WXUNUSED(event))
{
//
// EXIT - event handler
//
  Close(true);
}

void MyFrame::OnClose(wxCloseEvent & event)
{
//
// immedtiately before closing the main window
//
  if (MapView->IsDownloadInProgress() == true)
    {
      MapView->SetWmsAbort();
      event.Veto();
      return;
    }
  if (MapView->IsPrintInProgress() == true)
    {
      MapView->SetWmsAbort();
      event.Veto();
      return;
    }
  Destroy();
}

void MyFrame::OnAbout(wxCommandEvent & WXUNUSED(event))
{
//
// ABOUT dialog - event handler
//
  char ver[128];
  wxAboutDialogInfo dlg;
  dlg.SetIcon(wxIcon(icon_info_xpm));
  dlg.SetName(wxT("LibreWMS"));
  const char *version = VERSION;
  dlg.SetVersion(wxString::FromUTF8(version));
  wxString str = wxT("a free WMS viewer built on the top of RasterLite2\n\n");
  sprintf(ver, "%d.%d.%d", wxMAJOR_VERSION, wxMINOR_VERSION, wxRELEASE_NUMBER);
  str += wxT("wxWidgets version ") + wxString::FromUTF8(ver) + wxT("\n");
  strcpy(ver, rl2_version());
  str += wxT("RasterLite2 version ") + wxString::FromUTF8(ver) + wxT("\n");
  dlg.SetDescription(str);
  dlg.SetCopyright(wxT("by Alessandro Furieri - 2014"));
  dlg.SetWebSite(wxT("http://www.gaia-gis.it"));
  wxString license =
    wxT("This program is free software; you can redistribute it\n");
  license +=
    wxT("and/or modify it under the terms of the GNU General Public License\n");
  license += wxT("(GPL) as published by the Free Software Foundation\n\n");
  license +=
    wxT
    ("A copy of the GPL can be found at\nhttp://www.gnu.org/licenses/gpl.txt");
  dlg.SetLicense(license);
  ::wxAboutBox(dlg);
}

void MyFrame::InitializeWmsList()
{
//
// initializing the pre-defined WMS List
//
  int ret;
  char *errMsg = NULL;
  if (!SQLiteHandle)
    return;
// creating the WMS_LIST table
  ret = sqlite3_exec(SQLiteHandle, "CREATE TABLE wms_list ("
                     "country TEXT NOT NULL,\n"
                     "state_province TEXT NOT NULL,\n"
                     "category TEXT NOT NULL,\n"
                     "url TEXT NOT NULL)", NULL, NULL, &errMsg);
  if (ret != SQLITE_OK)
    {
      wxMessageBox(wxT("Unable to CREATE TABLE wms_list: ") +
                   wxString::FromUTF8(errMsg), wxT("LibreWMS"),
                   wxOK | wxICON_ERROR, this);
      sqlite3_free(errMsg);
      return;
    }
// starting a TRANSACTION
  ret = sqlite3_exec(SQLiteHandle, "BEGIN", NULL, NULL, &errMsg);
  if (ret != SQLITE_OK)
    {
      wxMessageBox(wxT("BEGIN error: ") +
                   wxString::FromUTF8(errMsg), wxT("LibreWMS"),
                   wxOK | wxICON_ERROR, this);
      sqlite3_free(errMsg);
      return;
    }
// populating the pre-defined WMS list
  AddPredefinedWMS("", "WmsLite", "localhost WMS server",
                   "http://127.0.0.1:8080/wmslite?service=WMS&request=GetCapabilities");
  AddPredefinedWMS("Italy", "Toscana", "ortophoto",
                   "http://www502.regione.toscana.it/wmsraster/com.rt.wms.RTmap/wms?map=wmsofc&service=WMS&request=GetCapabilities");
  AddPredefinedWMS("Italy", "Toscana", "land registry",
                   "http://www502.regione.toscana.it/wmsraster/com.rt.wms.RTmap/wms?map=wmscatasto&service=WMS&request=GetCapabilities");
  AddPredefinedWMS("Italy", "Toscana", "admin boundaries",
                   "http://www502.regione.toscana.it/wmsraster/com.rt.wms.RTmap/wms?map=wmsambamm&service=WMS&request=GetCapabilities");
  AddPredefinedWMS("Italy", "Toscana", "zoning",
                   "http://www502.regione.toscana.it/wmsraster/com.rt.wms.RTmap/wms?map=wmsambprogr&service=WMS&request=GetCapabilities");
  AddPredefinedWMS("Italy", "Toscana", "land use",
                   "http://www502.regione.toscana.it/wmsraster/com.rt.wms.RTmap/wms?map=wmsucs&service=WMS&request=GetCapabilities");
  AddPredefinedWMS("Italy", "Toscana", "census",
                   "http://www502.regione.toscana.it/wmsraster/com.rt.wms.RTmap/wms?map=wmsambcens&service=WMS&request=GetCapabilities");
  AddPredefinedWMS("Italy", "Toscana", "speleology",
                   "http://www502.regione.toscana.it/wmsraster/com.rt.wms.RTmap/wms?map=wmsspeleologia&service=WMS&request=GetCapabilities");
  AddPredefinedWMS("Italy", "Toscana", "trekking",
                   "http://www502.regione.toscana.it/wmsraster/com.rt.wms.RTmap/wms?map=wmssentieristica&service=WMS&request=GetCapabilities");
  AddPredefinedWMS("Italy", "Toscana", "hydrography",
                   "http://www502.regione.toscana.it/wmsraster/com.rt.wms.RTmap/wms?map=wmsidrogr&service=WMS&request=GetCapabilities");
  AddPredefinedWMS("Italy", "Toscana", "roads",
                   "http://www502.regione.toscana.it/wmsraster/com.rt.wms.RTmap/wms?map=wmsitnt&service=WMS&request=GetCapabilities");
  AddPredefinedWMS("Italy", "Toscana", "map",
                   "http://www502.regione.toscana.it/wmsraster/com.rt.wms.RTmap/wms?map=wmsctr&service=WMS&request=GetCapabilities");
  AddPredefinedWMS("Italy", "Toscana", "historical",
                   "http://www502.regione.toscana.it/wmsraster/com.rt.wms.RTmap/wms?map=wmscastore&service=WMS&request=GetCapabilities");
  AddPredefinedWMS("Italy", "Emilia Romagna", "ortophoto",
                   "http://servizigis.regione.emilia-romagna.it/wms/agea2011_rgb?service=wms&request=getcapabilities&version=130");
  AddPredefinedWMS("Italy", "Emilia Romagna", "map",
                   "http://servizigis.regione.emilia-romagna.it/wms/dbtr2008_ctr5?service=wms&request=getcapabilities&version=130");
  AddPredefinedWMS("Italy", "Emilia Romagna", "miscellaneous",
                   "http://servizigis.regione.emilia-romagna.it/wms/dbtr2008?service=wms&request=getcapabilities&version=130");
  AddPredefinedWMS("Italy", "Sardegna", "ortophoto",
                   "http://webgis.regione.sardegna.it/wmsconnector/com.esri.wms.Esrimap/ras_wms?service=WMS&request=GetCapabilities");
  AddPredefinedWMS("Italy", "Sardegna", "miscellaneous",
                   "http://webgis.regione.sardegna.it/geoserver/ows?service=WMS&request=GetCapabilities ");
  AddPredefinedWMS("Italy", "Piemonte", "ortophoto",
                   "http://geomap.reteunitaria.piemonte.it/ws/taims/rp-01/taimsortoregp/wms_ortoregp2010?service=WMS&request=getCapabilities");
  AddPredefinedWMS("Italy", "Piemonte", "map",
                   "http://geomap.reteunitaria.piemonte.it/ws/taims/rp-01/taimssfwms/CatalogoSfondoRegione?service=WMS&request=getCapabilities");
  AddPredefinedWMS("Italy", "Piemonte", "admin boundaries",
                   "http://geomap.reteunitaria.piemonte.it/ws/taims/rp-01/taimslimammwms/wms_limitiAmm?service=WMS&request=getCapabilities");
  AddPredefinedWMS("Italy", "Umbria", "ortophoto",
                   "http://geo.umbriaterritorio.it/arcgis/services/public/ORTOFOTO_2011_GB/MapServer/WMSServer?service=WMS&request=getCapabilities");
  AddPredefinedWMS("Italy", "Abruzzo", "ortophoto",
                   "http://geoportale2.regione.abruzzo.it/ecwp/ecw_wms.dll?service=WMS&request=getCapabilities");
  AddPredefinedWMS("Italy", "Campania", "miscellaneous",
                   "http://sit.regione.campania.it:80/arcgis/services/cartografia_base/MapServer/WMSServer?service=WMS&request=getCapabilities");
  AddPredefinedWMS("Italy", "Sicilia", "ortophoto",
                   "http://map.sitr.regione.sicilia.it/ArcGIS/services/Ortofoto_ATA20072008/MapServer/WMSServer?service=WMS&request=getCapabilities");
  AddPredefinedWMS("Italy", "Bolzano / Bozen", "niscellaneous",
                   "http://sdi.provincia.bz.it/geoserver/inspire/wms?service=wms&version=1.3.0&request=GetCapabilities");
  AddPredefinedWMS("Italy", "Torino", "map",
                   "http://geomap.reteunitaria.piemonte.it/ws/siccms/coto-01/wmsg01/wms_sicc31_carta_tecnica_bn?service=WMS&request=getCapabilities");
  AddPredefinedWMS("Italy", "ISPRA 25k", "geological map",
                   "http://sgi1.isprambiente.it/ArcGIS/services/servizi/geologia25k/mapserver/WMSServer?Request=GetCapabilities");
  AddPredefinedWMS("Italy", "ISPRA 100k", "geological map",
                   "http://sgi1.isprambiente.it/ArcGIS/services/servizi/geologia100k/mapserver/WMSServer?Request=GetCapabilities");
  AddPredefinedWMS("USA", "USGS Alabama", "geological map",
                   "http://mrdata.usgs.gov/services/al?request=getcapabilities&service=WMS&version=1.1.1&");
  AddPredefinedWMS("USA", "USGS Alaska", "geological map",
                   "http://mrdata.usgs.gov/services/akgeol?request=getcapabilities&service=WMS&version=1.1.1&");
  AddPredefinedWMS("USA", "USGS Arkansas", "geological map",
                   "http://mrdata.usgs.gov/services/ar?request=getcapabilities&service=WMS&version=1.1.1&");
  AddPredefinedWMS("USA", "USGS Arizona", "geological map",
                   "http://mrdata.usgs.gov/services/az?request=getcapabilities&service=WMS&version=1.1.1&");
  AddPredefinedWMS("USA", "USGS California", "geological map",
                   "http://mrdata.usgs.gov/services/ca?request=getcapabilities&service=WMS&version=1.1.1&");
  AddPredefinedWMS("USA", "USGS Colorado", "geological map",
                   "http://mrdata.usgs.gov/services/co?request=getcapabilities&service=WMS&version=1.1.1&");
  AddPredefinedWMS("USA", "USGS Connecticut", "geological map",
                   "http://mrdata.usgs.gov/services/ct?request=getcapabilities&service=WMS&version=1.1.1&");
  AddPredefinedWMS("USA", "USGS Delaware", "geological map",
                   "http://mrdata.usgs.gov/services/de?request=getcapabilities&service=WMS&version=1.1.1&");
  AddPredefinedWMS("USA", "USGS Florida", "geological map",
                   "http://mrdata.usgs.gov/services/fl?request=getcapabilities&service=WMS&version=1.1.1&");
  AddPredefinedWMS("USA", "USGS Georgia", "geological map",
                   "http://mrdata.usgs.gov/services/ga?request=getcapabilities&service=WMS&version=1.1.1&");
  AddPredefinedWMS("USA", "USGS Hawaii", "geological map",
                   "http://mrdata.usgs.gov/services/hi?request=getcapabilities&service=WMS&version=1.1.1&");
  AddPredefinedWMS("USA", "USGS Iowa", "geological map",
                   "http://mrdata.usgs.gov/services/ia?request=getcapabilities&service=WMS&version=1.1.1&");
  AddPredefinedWMS("USA", "USGS Idaho", "geological map",
                   "http://mrdata.usgs.gov/services/id?request=getcapabilities&service=WMS&version=1.1.1&");
  AddPredefinedWMS("USA", "USGS Illinois", "geological map",
                   "http://mrdata.usgs.gov/services/il?request=getcapabilities&service=WMS&version=1.1.1&");
  AddPredefinedWMS("USA", "USGS Indiana", "geological map",
                   "http://mrdata.usgs.gov/services/in?request=getcapabilities&service=WMS&version=1.1.1&");
  AddPredefinedWMS("USA", "USGS Kansas", "geological map",
                   "http://mrdata.usgs.gov/services/ks?request=getcapabilities&service=WMS&version=1.1.1&");
  AddPredefinedWMS("USA", "USGS Kentucky", "geological map",
                   "http://mrdata.usgs.gov/services/ky?request=getcapabilities&service=WMS&version=1.1.1&");
  AddPredefinedWMS("USA", "USGS Louisiana", "geological map",
                   "http://mrdata.usgs.gov/services/la?request=getcapabilities&service=WMS&version=1.1.1&");
  AddPredefinedWMS("USA", "USGS Massachussets", "geological map",
                   "http://mrdata.usgs.gov/services/ma?request=getcapabilities&service=WMS&version=1.1.1&");
  AddPredefinedWMS("USA", "USGS Maryland", "geological map",
                   "http://mrdata.usgs.gov/services/md?request=getcapabilities&service=WMS&version=1.1.1&");
  AddPredefinedWMS("USA", "USGS Maine", "geological map",
                   "http://mrdata.usgs.gov/services/me?request=getcapabilities&service=WMS&version=1.1.1&");
  AddPredefinedWMS("USA", "USGS Michigan", "geological map",
                   "http://mrdata.usgs.gov/services/mi?request=getcapabilities&service=WMS&version=1.1.1&");
  AddPredefinedWMS("USA", "USGS Minnesota", "geological map",
                   "http://mrdata.usgs.gov/services/mn?request=getcapabilities&service=WMS&version=1.1.1&");
  AddPredefinedWMS("USA", "USGS Missouri", "geological map",
                   "http://mrdata.usgs.gov/services/mo?request=getcapabilities&service=WMS&version=1.1.1&");
  AddPredefinedWMS("USA", "USGS Mississippi", "geological map",
                   "http://mrdata.usgs.gov/services/ms?request=getcapabilities&service=WMS&version=1.1.1&");
  AddPredefinedWMS("USA", "USGS Montana", "geological map",
                   "http://mrdata.usgs.gov/services/mt?request=getcapabilities&service=WMS&version=1.1.1&");
  AddPredefinedWMS("USA", "USGS North Carolina", "geological map",
                   "http://mrdata.usgs.gov/services/nc?request=getcapabilities&service=WMS&version=1.1.1&");
  AddPredefinedWMS("USA", "USGS North Dakota", "geological map",
                   "http://mrdata.usgs.gov/services/nd?request=getcapabilities&service=WMS&version=1.1.1&");
  AddPredefinedWMS("USA", "USGS Nebraska", "geological map",
                   "http://mrdata.usgs.gov/services/ne?request=getcapabilities&service=WMS&version=1.1.1&");
  AddPredefinedWMS("USA", "USGS New Hampshire", "geological map",
                   "http://mrdata.usgs.gov/services/nh?request=getcapabilities&service=WMS&version=1.1.1&");
  AddPredefinedWMS("USA", "USGS New Jersey", "geological map",
                   "http://mrdata.usgs.gov/services/nj?request=getcapabilities&service=WMS&version=1.1.1&");
  AddPredefinedWMS("USA", "USGS New Mexico", "geological map",
                   "http://mrdata.usgs.gov/services/nm?request=getcapabilities&service=WMS&version=1.1.1&");
  AddPredefinedWMS("USA", "USGS Nevada", "geological map",
                   "http://mrdata.usgs.gov/services/nv?request=getcapabilities&service=WMS&version=1.1.1&");
  AddPredefinedWMS("USA", "USGS New York", "geological map",
                   "http://mrdata.usgs.gov/services/ny?request=getcapabilities&service=WMS&version=1.1.1&");
  AddPredefinedWMS("USA", "USGS Ohio", "geological map",
                   "http://mrdata.usgs.gov/services/oh?request=getcapabilities&service=WMS&version=1.1.1&");
  AddPredefinedWMS("USA", "USGS Oklahoma", "geological map",
                   "http://mrdata.usgs.gov/services/ok?request=getcapabilities&service=WMS&version=1.1.1&");
  AddPredefinedWMS("USA", "USGS Oregon", "geological map",
                   "http://mrdata.usgs.gov/services/or?request=getcapabilities&service=WMS&version=1.1.1&");
  AddPredefinedWMS("USA", "USGS Pennsylvania", "geological map",
                   "http://mrdata.usgs.gov/services/pa?request=getcapabilities&service=WMS&version=1.1.1&");
  AddPredefinedWMS("USA", "USGS Rhode Island", "geological map",
                   "http://mrdata.usgs.gov/services/ri?request=getcapabilities&service=WMS&version=1.1.1&");
  AddPredefinedWMS("USA", "USGS South Carolina", "geological map",
                   "http://mrdata.usgs.gov/services/sc?request=getcapabilities&service=WMS&version=1.1.1&");
  AddPredefinedWMS("USA", "USGS South Dakota", "geological map",
                   "http://mrdata.usgs.gov/services/sd?request=getcapabilities&service=WMS&version=1.1.1&");
  AddPredefinedWMS("USA", "USGS Tennessee", "geological map",
                   "http://mrdata.usgs.gov/services/tn?request=getcapabilities&service=WMS&version=1.1.1&");
  AddPredefinedWMS("USA", "USGS Texas", "geological map",
                   "http://mrdata.usgs.gov/services/tx?request=getcapabilities&service=WMS&version=1.1.1&");
  AddPredefinedWMS("USA", "USGS Utah", "geological map",
                   "http://mrdata.usgs.gov/services/ut?request=getcapabilities&service=WMS&version=1.1.1&");
  AddPredefinedWMS("USA", "USGS Virginia", "geological map",
                   "http://mrdata.usgs.gov/services/va?request=getcapabilities&service=WMS&version=1.1.1&");
  AddPredefinedWMS("USA", "USGS Vermont", "geological map",
                   "http://mrdata.usgs.gov/services/vt?request=getcapabilities&service=WMS&version=1.1.1&");
  AddPredefinedWMS("USA", "USGS Washington", "geological map",
                   "http://mrdata.usgs.gov/services/wa?request=getcapabilities&service=WMS&version=1.1.1&");
  AddPredefinedWMS("USA", "USGS Wisconsin", "geological map",
                   "http://mrdata.usgs.gov/services/wi?request=getcapabilities&service=WMS&version=1.1.1&");
  AddPredefinedWMS("USA", "USGS West Virginia", "geological map",
                   "http://mrdata.usgs.gov/services/wv?request=getcapabilities&service=WMS&version=1.1.1&");
  AddPredefinedWMS("USA", "USGS Wyoming", "geological map",
                   "http://mrdata.usgs.gov/services/wy?request=getcapabilities&service=WMS&version=1.1.1&");
  AddPredefinedWMS("USA", "USGS active-mines", "geological map",
                   "http://mrdata.usgs.gov/services/active-mines?request=getcapabilities&service=WMS&version=1.1.1&");
  AddPredefinedWMS("Earth", "NASA/JPL OnEarth", "satellite",
                   "http://wms.jpl.nasa.gov/wms.cgi?SERVICE=WMS&REQUEST=GetCapabilities&VERSION=1.1.1");
  AddPredefinedWMS("Moon", "NASA/JPL OnMoon", "satellite",
                   "http://onmoon.jpl.nasa.gov/wms.cgi?SERVICE=WMS&REQUEST=GetCapabilities&VERSION=1.1.1");
  AddPredefinedWMS("Mars", "NASA/JPL OnMars", "satellite",
                   "http://onmars.jpl.nasa.gov/wms.cgi?SERVICE=WMS&REQUEST=GetCapabilities&VERSION=1.1.1");

// committing the still pending TRANSACTION
  ret = sqlite3_exec(SQLiteHandle, "COMMIT", NULL, NULL, &errMsg);
  if (ret != SQLITE_OK)
    {
      wxMessageBox(wxT("COMMIT error: ") +
                   wxString::FromUTF8(errMsg), wxT("LibreWMS"),
                   wxOK | wxICON_ERROR, this);
      sqlite3_free(errMsg);
      return;
    }
}

void MyFrame::AddPredefinedWMS(const char *country, const char *state_province,
                               const char *category, const char *url)
{
//
// inserting a WMS service into the pre-defined catalog
//
  int ret;
  char *sql;
  char *errMsg = NULL;
  if (!SQLiteHandle)
    return;
// preparing the INSERT statement
  sql =
    sqlite3_mprintf
    ("INSERT INTO wms_list (country, state_province, category, url) "
     "VALUES (%Q, %Q, %Q, %Q)", country, state_province, category, url);
// inserting into the table
  ret = sqlite3_exec(SQLiteHandle, sql, NULL, NULL, &errMsg);
  if (ret != SQLITE_OK)
    {
      wxMessageBox(wxT("Unable to INSERT INTO wms_list: ") +
                   wxString::FromUTF8(errMsg), wxT("LibreWMS"),
                   wxOK | wxICON_ERROR, this);
      sqlite3_free(errMsg);
    }
  sqlite3_free(sql);
}

bool MyFrame::IsGeographicCRS(const char *crs)
{
// testing if some CRS is of the Geographic [Long/Lat] type
  if (IsSwapXYcrs(crs) == true)
    return true;
  if (crs == NULL)
    return false;
  if (strcmp(crs, "CRS:84") == 0)
    return true;
  if (strcmp(crs, "IAU2000:30100") == 0)
    return true;
  if (strcmp(crs, "IAU2000:49900") == 0)
    return true;
  return false;
}

bool MyFrame::IsSwapXYcrs(const char *crs)
{
// testing if some CRS requires swapped axes 
  int ret;
  char *sql;
  char **results;
  int rows;
  int columns;
  int i;
  int len;
  int srid = -1;
  int geo = -1;
  if (crs == NULL)
    return false;
  if (strcmp(crs, "CRS:84") == 0)
    return false;
  len = strlen(crs);
  for (i = 0; i < len; i++)
    {
      if (*(crs + i) == ':')
        {
          // delimiter found
          if (i == 4 && memcmp(crs, "EPSG", 4) == 0)
            {
              srid = atoi(crs + i + 1);
              break;
            }
        }
    }
  if (srid <= 0)
    return false;

  sql = sqlite3_mprintf("SELECT Count(*) FROM spatial_ref_sys "
                        "WHERE srid = %d AND proj4text LIKE '%%+proj=longlat%%'",
                        srid);
  ret = sqlite3_get_table(SQLiteHandle, sql, &results, &rows, &columns, NULL);
  sqlite3_free(sql);
  if (ret != SQLITE_OK)
    goto skip;

  for (i = 1; i <= rows; i++)
    geo = atoi(results[(i * columns) + 0]);
  sqlite3_free_table(results);
skip:
  if (geo > 0)
    return true;
  return false;
}

bool MyFrame::BBoxFromLongLat(const char *crs, double *minx, double *maxx,
                              double *miny, double *maxy)
{
// converting a BBox into some CRS from Long/Lat coords
  int srid = -1;
  char *proj_from;
  char *proj_to;
  gaiaGeomCollPtr g_min_in;
  gaiaGeomCollPtr g_min_out;
  gaiaGeomCollPtr g_max_in;
  gaiaGeomCollPtr g_max_out;
  gaiaPointPtr pt;
  if (crs == NULL)
    return false;
  if (strcmp(crs, "CRS:84") == 0)
    srid = 4326;
  else
    {
      int i;
      int len = strlen(crs);
      for (i = 0; i < len; i++)
        {
          if (*(crs + i) == ':')
            {
              // delimiter found
              if (i == 4 && memcmp(crs, "EPSG", 4) == 0)
                {
                  srid = atoi(crs + i + 1);
                  break;
                }
            }
        }
    }
  if (srid <= 0)
    return false;

// attempting to reproject from Long/Lat into the given CRS
  proj_from = GetProjParams(4326);
  proj_to = GetProjParams(srid);
  if (proj_to == NULL || proj_from == NULL)
    {
      if (proj_from)
        free(proj_from);
      if (proj_to)
        free(proj_to);
      return false;
    }

  g_min_in = gaiaAllocGeomColl();
  gaiaAddPointToGeomColl(g_min_in, *minx, *miny);
  g_max_in = gaiaAllocGeomColl();
  gaiaAddPointToGeomColl(g_max_in, *maxx, *maxy);
  g_min_out = gaiaTransform(g_min_in, proj_from, proj_to);
  g_max_out = gaiaTransform(g_max_in, proj_from, proj_to);
  free(proj_from);
  free(proj_to);
  gaiaFreeGeomColl(g_min_in);
  gaiaFreeGeomColl(g_max_in);
  pt = g_min_out->FirstPoint;
  *minx = pt->X;
  *miny = pt->Y;
  gaiaFreeGeomColl(g_min_out);
  pt = g_max_out->FirstPoint;
  *maxx = pt->X;
  *maxy = pt->Y;
  gaiaFreeGeomColl(g_max_out);
  return true;
}

char *MyFrame::GetProjParams(int srid)
{
// attempting to retrieving PROJ4 geodetic definitions

  sqlite3 *sqlite = SQLiteHandle;
  char *sql;
  char **results;
  int rows;
  int columns;
  int i;
  int ret;
  int len;
  const char *proj4text;
  char *proj_params = NULL;
  char *errMsg = NULL;
  sql = sqlite3_mprintf
    ("SELECT proj4text FROM spatial_ref_sys WHERE srid = %d", srid);
  ret = sqlite3_get_table(sqlite, sql, &results, &rows, &columns, &errMsg);
  sqlite3_free(sql);
  if (ret != SQLITE_OK)
    {
      fprintf(stderr, "unknown SRID: %d\t<%s>\n", srid, errMsg);
      sqlite3_free(errMsg);
      return NULL;
    }
  for (i = 1; i <= rows; i++)
    {
      proj4text = results[(i * columns)];
      if (proj4text != NULL)
        {
          len = strlen(proj4text);
          proj_params = (char *) malloc(len + 1);
          strcpy(proj_params, proj4text);
        }
    }
  if (proj_params == NULL)
    fprintf(stderr, "unknown SRID: %d\n", srid);
  sqlite3_free_table(results);
  return proj_params;
}
