/*
/ MapView.cpp
/ a panel to show the Map of LibreWMS
/
/ version 1.0, 2013 July 28
/
/ Author: Sandro Furieri a-furieri@lqt.it
/
/ Copyright (C) 2013  Alessandro Furieri
/
/    This program is free software: you can redistribute it and/or modify
/    it under the terms of the GNU General Public License as published by
/    the Free Software Foundation, either version 3 of the License, or
/    (at your option) any later version.
/
/    This program is distributed in the hope that it will be useful,
/    but WITHOUT ANY WARRANTY; without even the implied warranty of
/    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/    GNU General Public License for more details.
/
/    You should have received a copy of the GNU General Public License
/    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/
*/

#include "Classdef.h"
#include "wx/image.h"

#include <float.h>

#include "icons/crosshair.xpm"

#ifdef _WIN32
#include <windows.h>
#include <process.h>
#include <io.h>
#else
#include <pthread.h>
#include <unistd.h>
#endif

MyMapView::MyMapView(MyFrame * parent, wxWindowID id):
wxPanel(parent, id, wxDefaultPosition, wxSize(440, 480),
        wxBORDER_SUNKEN | wxWANTS_CHARS)
{
//
// constructor: a frame to show the Map
//
  MainFrame = parent;
  Pdf = NULL;
  Tiff = NULL;
  FrameMinX = DBL_MAX;
  FrameMaxY = DBL_MAX;
  FirstRequest = NULL;
  LastRequest = NULL;
  Proxy = NULL;
  PixelRatio = DBL_MAX;
  TimerMouseWheel = NULL;
  TimerDynamic = NULL;
  UpdateTimer = NULL;
  PrintTimer = NULL;
  FirstService = NULL;
  LastService = NULL;
  FirstLayer = NULL;
  LastLayer = NULL;
  FirstCRS = NULL;
  LastCRS = NULL;
  WmsFixedScalesCount = 0;
  WmsFixedScales = NULL;
  ProgressControl = NULL;
  DownloadInProgress = false;
  PrintInProgress = false;
  PendingRefresh = false;
  PendingOnSize = false;
  PendingWmsAbort = false;
  IdentifyPanel = NULL;
  Invalidate();

  BitmapWidth = wxSystemSettings::GetMetric(wxSYS_SCREEN_X);
  BitmapHeight = wxSystemSettings::GetMetric(wxSYS_SCREEN_Y);
  MapBitmap = wxBitmap(BitmapWidth, BitmapHeight, -1);
  ScreenBitmap = wxBitmap(BitmapWidth, BitmapHeight, -1);

  wxImage crossImage = wxBitmap(crosshair_xpm).ConvertToImage();
#ifdef __WXMSW__
// setting up a WINDOWS cursor
  crossImage.SetOption(wxIMAGE_OPTION_CUR_HOTSPOT_X, 8);
  crossImage.SetOption(wxIMAGE_OPTION_CUR_HOTSPOT_Y, 8);
  CursorCross = wxCursor(crossImage);
#else
#ifdef __WXMAC__
// setting up a MacOsX cursor
  crossImage.SetOption(wxIMAGE_OPTION_CUR_HOTSPOT_X, 8);
  crossImage.SetOption(wxIMAGE_OPTION_CUR_HOTSPOT_Y, 8);
  CursorCross = wxCursor(crossImage);
#else
// setting up an UNIX -likecursor
  int row;
  int col;
  unsigned short bits[16];
  unsigned short mask[16];
  for (row = 0; row < 16; row++)
    {
      bits[row] = 0x0000;
      mask[row] = 0x0000;
      for (col = 0; col < 16; col++)
        {
          unsigned char pixel = crossImage.GetRed(col, row);
          if (pixel == 0)
            {
              switch (col)
                {
                  case 0:
                    bits[row] |= 0x8000;
                    break;
                  case 1:
                    bits[row] |= 0x4000;
                    break;
                  case 2:
                    bits[row] |= 0x2000;
                    break;
                  case 3:
                    bits[row] |= 0x1000;
                    break;
                  case 4:
                    bits[row] |= 0x0800;
                    break;
                  case 5:
                    bits[row] |= 0x0400;
                    break;
                  case 6:
                    bits[row] |= 0x0200;
                    break;
                  case 7:
                    bits[row] |= 0x0100;
                    break;
                  case 8:
                    bits[row] |= 0x0080;
                    break;
                  case 9:
                    bits[row] |= 0x0040;
                    break;
                  case 10:
                    bits[row] |= 0x0020;
                    break;
                  case 11:
                    bits[row] |= 0x0010;
                    break;
                  case 12:
                    bits[row] |= 0x0008;
                    break;
                  case 13:
                    bits[row] |= 0x0004;
                    break;
                  case 14:
                    bits[row] |= 0x0002;
                    break;
                  case 15:
                    bits[row] |= 0x0001;
                    break;
                };
              switch (col)
                {
                  case 0:
                    mask[row] |= 0x8000;
                    break;
                  case 1:
                    mask[row] |= 0x4000;
                    break;
                  case 2:
                    mask[row] |= 0x2000;
                    break;
                  case 3:
                    mask[row] |= 0x1000;
                    break;
                  case 4:
                    mask[row] |= 0x0800;
                    break;
                  case 5:
                    mask[row] |= 0x0400;
                    break;
                  case 6:
                    mask[row] |= 0x0200;
                    break;
                  case 7:
                    mask[row] |= 0x0100;
                    break;
                  case 8:
                    mask[row] |= 0x0080;
                    break;
                  case 9:
                    mask[row] |= 0x0040;
                    break;
                  case 10:
                    mask[row] |= 0x0020;
                    break;
                  case 11:
                    mask[row] |= 0x0010;
                    break;
                  case 12:
                    mask[row] |= 0x0008;
                    break;
                  case 13:
                    mask[row] |= 0x0004;
                    break;
                  case 14:
                    mask[row] |= 0x0002;
                    break;
                  case 15:
                    mask[row] |= 0x0001;
                    break;
                };
            }
          if (pixel == 255)
            {
              switch (col)
                {
                  case 0:
                    mask[row] |= 0x8000;
                    break;
                  case 1:
                    mask[row] |= 0x4000;
                    break;
                  case 2:
                    mask[row] |= 0x2000;
                    break;
                  case 3:
                    mask[row] |= 0x1000;
                    break;
                  case 4:
                    mask[row] |= 0x0800;
                    break;
                  case 5:
                    mask[row] |= 0x0400;
                    break;
                  case 6:
                    mask[row] |= 0x0200;
                    break;
                  case 7:
                    mask[row] |= 0x0100;
                    break;
                  case 8:
                    mask[row] |= 0x0080;
                    break;
                  case 9:
                    mask[row] |= 0x0040;
                    break;
                  case 10:
                    mask[row] |= 0x0020;
                    break;
                  case 11:
                    mask[row] |= 0x0010;
                    break;
                  case 12:
                    mask[row] |= 0x0008;
                    break;
                  case 13:
                    mask[row] |= 0x0004;
                    break;
                  case 14:
                    mask[row] |= 0x0002;
                    break;
                  case 15:
                    mask[row] |= 0x0001;
                    break;
                };
            }
        }
    }
  CursorCross =
    wxCursor((char *) bits, 16, 16, 8, 8, (char *) mask, wxWHITE, wxBLACK);
#endif
#endif
  CursorHand = wxCursor(wxCURSOR_HAND);
  SetCursor(CursorCross);

// setting up event handlers
  Connect(wxID_ANY, wxEVT_ERASE_BACKGROUND,
          (wxObjectEventFunction) & MyMapView::OnEraseBackground);
  Connect(wxID_ANY, wxEVT_SIZE, (wxObjectEventFunction) & MyMapView::OnSize);
  Connect(wxID_ANY, wxEVT_PAINT, (wxObjectEventFunction) & MyMapView::OnPaint);
  Connect(wxID_ANY, wxEVT_MOTION, wxMouseEventHandler(MyMapView::OnMouseMove),
          NULL, this);
  Connect(wxID_ANY, wxEVT_LEFT_DOWN,
          wxMouseEventHandler(MyMapView::OnMouseClick), NULL, this);
  Connect(wxID_ANY, wxEVT_LEFT_DCLICK,
          wxMouseEventHandler(MyMapView::OnMouseDoubleClick), NULL, this);
  Connect(wxID_ANY, wxEVT_MOUSEWHEEL,
          wxMouseEventHandler(MyMapView::OnMouseWheel), NULL, this);
  Connect(ID_WHEEL_TIMER, wxEVT_TIMER,
          wxTimerEventHandler(MyMapView::OnTimerMouseWheel), NULL, this);
  Connect(ID_UPDATE_TIMER, wxEVT_TIMER,
          wxTimerEventHandler(MyMapView::OnTimerUpdate), NULL, this);
  Connect(ID_PRINT_TIMER, wxEVT_TIMER,
          wxTimerEventHandler(MyMapView::OnTimerPrint), NULL, this);
  Connect(wxID_ANY, wxEVT_LEFT_UP,
          wxMouseEventHandler(MyMapView::OnMouseDragStop), NULL, this);
  Connect(wxID_ANY, wxEVT_LEAVE_WINDOW,
          wxMouseEventHandler(MyMapView::OnMouseDragStop), NULL, this);
  Connect(ID_DYNAMIC_TIMER, wxEVT_TIMER,
          wxTimerEventHandler(MyMapView::OnTimerDynamic), NULL, this);
  Connect(ID_MAP_WMS_THREAD_FINISHED, wxEVT_COMMAND_BUTTON_CLICKED,
          (wxObjectEventFunction) & MyMapView::OnMapWmsThreadFinished);
  Connect(ID_PDF_WMS_THREAD_FINISHED, wxEVT_COMMAND_BUTTON_CLICKED,
          (wxObjectEventFunction) & MyMapView::OnPdfWmsThreadFinished);
  Connect(ID_TIFF_WMS_THREAD_FINISHED, wxEVT_COMMAND_BUTTON_CLICKED,
          (wxObjectEventFunction) & MyMapView::OnTiffWmsThreadFinished);
}

MyMapView::~MyMapView()
{
  WmsLayer *pw;
  WmsLayer *pwn;
  WmsService *ps;
  WmsService *psn;
  if (WmsFixedScales != NULL)
    delete[]WmsFixedScales;
  ResetCRSlist();
  ResetGetMapRequests();
  if (UpdateTimer != NULL)
    {
      UpdateTimer->Stop();
      delete UpdateTimer;
    }
  if (PrintTimer != NULL)
    {
      PrintTimer->Stop();
      delete PrintTimer;
    }
  if (TimerMouseWheel)
    {
      TimerMouseWheel->Stop();
      delete TimerMouseWheel;
    }
  if (TimerDynamic)
    {
      TimerDynamic->Stop();
      delete TimerDynamic;
    }
  if (Proxy != NULL)
    delete[]Proxy;
  pw = FirstLayer;
  while (pw != NULL)
    {
      pwn = pw->GetNext();
      delete pw;
      pw = pwn;
    }
  ps = FirstService;
  while (ps != NULL)
    {
      psn = ps->GetNext();
      delete ps;
      ps = psn;
    }
}

void MyMapView::Invalidate()
{
// resetting to initial state
  ValidMap = false;
  ActiveLayer = NULL;
  DragStartX = -1;
  DragStartY = -1;
  LastDragX = -1;
  LastDragY = -1;
  if (UpdateTimer)
    {
      UpdateTimer->Stop();
      delete UpdateTimer;
    }
  UpdateTimer = NULL;
  if (TimerMouseWheel)
    {
      TimerMouseWheel->Stop();
      delete TimerMouseWheel;
    }
  TimerMouseWheel = NULL;
  if (TimerDynamic)
    {
      TimerDynamic->Stop();
      delete TimerDynamic;
    }
  TimerDynamic = NULL;
  WheelTics = 0;
}

void MyMapView::OnSize(wxSizeEvent & WXUNUSED(event))
{
//
// this window has changed its size
//

// refreshing the map
  if (PrintInProgress == true)
    return;
  if (DownloadInProgress == true)
    PendingWmsAbort = true;
  PendingOnSize = true;
  if (ProgressControl != NULL)
    {
      // resizing the Progress control
      wxRect rect;
      if (MainFrame->GetProgressRect(rect) == true)
        ProgressControl->SetSize(rect);
    }
  PrepareMap();
  ResetMarker();
  if (IdentifyPanel != NULL)
    IdentifyPanel->Reset();
}

void MyMapView::OnPaint(wxPaintEvent & WXUNUSED(event))
{
//
// this window needs to be repainted
//
  wxPaintDC dc(this);
  if (ValidMap == false)
    {
      // no map: black background
      wxSize sz = GetClientSize();
      dc.SetBrush(wxBrush(wxColour(128, 110, 96), wxSOLID));
      dc.SetPen(wxPen(wxColour(0, 0, 0), 1));
      dc.DrawRectangle(0, 0, sz.GetWidth(), sz.GetHeight());
      dc.SetBrush(wxNullBrush);
      dc.SetPen(wxNullPen);
  } else if (dc.IsOk() && ScreenBitmap.IsOk())
    {
      wxMemoryDC *memDC = new wxMemoryDC(ScreenBitmap);
      dc.Blit(0, 0, BitmapWidth, BitmapHeight, memDC, 0, 0);
      delete memDC;
    }
  MainFrame->UpdateMapCRS();
}

void MyMapView::OnMouseMove(wxMouseEvent & event)
{
//
// MOUSE motion - event handler
// 
  double x;
  double y;
  char *dummy;
  wxString coords;
  bool busy = wxIsBusy();
  if (busy == true && PrintInProgress == true)
    return;
  if (event.LeftIsDown() == true && MainFrame->IsModePan() == true && !busy)
    {
      SetCursor(CursorHand);
      if (abs(LastDragX - event.GetX()) > 25
          || abs(LastDragY - event.GetY()) > 25)
        DragMap(event.GetX(), event.GetY());
      return;
    }
  if (event.LeftIsDown() == true && MainFrame->IsModeZoomIn() == true && !busy)
    {
      // marking the Zoom-in rectangle
      ResetScreenBitmap();
      wxMemoryDC *memDC = new wxMemoryDC(ScreenBitmap);
      memDC->SetPen(wxPen(wxColour(255, 0, 0), 2));
      memDC->SetBrush(wxBrush(wxColour(0, 0, 0), wxTRANSPARENT));
      int xx;
      int yy;
      int width;
      int height;
      if (event.GetX() > DragStartX)
        {
          xx = DragStartX;
          width = event.GetX() - DragStartX;
      } else
        {
          xx = event.GetX();
          width = DragStartX - event.GetX();
        }
      if (event.GetY() > DragStartY)
        {
          yy = DragStartY;
          height = event.GetY() - DragStartY;
      } else
        {
          yy = event.GetY();
          height = DragStartY - event.GetY();
        }
      memDC->DrawRectangle(xx, yy, width + 1, height + 1);
      delete memDC;
      Refresh();
    }
  SetCursor(CursorCross);

  if (FrameMinX == DBL_MAX || FrameMaxY == DBL_MAX || PixelRatio == DBL_MAX)
    dummy = sqlite3_mprintf("#err#     #err#");
  else
    {
      x = FrameMinX + ((double) (event.GetX()) * PixelRatio);
      y = FrameMaxY - ((double) (event.GetY()) * PixelRatio);
      if (MainFrame->IsGeographicCRS(GetMapCRS()) == true)
        dummy = sqlite3_mprintf("%1.6f     %1.6f", x, y);
      else
        dummy = sqlite3_mprintf("%1.3f     %1.3f", x, y);
    }
  coords = wxString::FromUTF8(dummy);
  sqlite3_free(dummy);

  MainFrame->UpdateMapScale();
  MainFrame->UpdateMapCoords(coords);
  MainFrame->UpdateMapCRS();
}

void MyMapView::OnTimerDynamic(wxTimerEvent & WXUNUSED(event))
{
//
// Dynamic Timer event handler
// 
// marking the Current Geometry
//
  DynamicPhase++;
  if (DynamicPhase % 2 == 0)
    DynamicOddEven = true;
  else
    DynamicOddEven = false;
  ResetScreenBitmap();
  if (DynamicPhase < 60)
    {
      if (!TimerDynamic)
        TimerDynamic = new wxTimer(this, ID_DYNAMIC_TIMER);
      TimerDynamic->Start(250, wxTIMER_ONE_SHOT);
  } else
    {
      ImageMarkerOdd.Destroy();
      ImageMarkerEven.Destroy();
      ResetScreenBitmap();
    }
}

void MyMapView::OnTimerUpdate(wxTimerEvent & WXUNUSED(event))
{
//
// Update Timer event handler
// 
  UpdatePendingTiles();
  //
  // restarting the timer
  //
  UpdateTimer->Start(125, wxTIMER_ONE_SHOT);
}

void MyMapView::OnTimerPrint(wxTimerEvent & WXUNUSED(event))
{
//
// Print Timer event handler
// 
  if (Pdf != NULL)
    Pdf->UpdatePendingTiles();
  if (Tiff != NULL)
    Tiff->UpdatePendingTiles();
  //
  // restarting the timer
  //
  PrintTimer->Start(125, wxTIMER_ONE_SHOT);
}

void MyMapView::OnMouseWheel(wxMouseEvent & event)
{
//
// MOUSE wheel rotation - event handler
//
  if (wxIsBusy())
    return;
  int tics = event.GetWheelRotation() / event.GetWheelDelta();
  if (!TimerMouseWheel)
    TimerMouseWheel = new wxTimer(this, ID_WHEEL_TIMER);
  WheelTics += tics;
  TimerMouseWheel->Start(100, wxTIMER_ONE_SHOT);
}

void MyMapView::OnTimerMouseWheel(wxTimerEvent & WXUNUSED(event))
{
//
// MOUSE wheel rotation - Timer event handler
// 
  if (wxIsBusy())
    return;
  if (WheelTics > 0)
    {
      // zoom in
      double factor = 1.0 - ((double) WheelTics * 0.025);
      PixelRatio /= factor;
  } else
    {
      // zoom out
      double factor = 1.0 - ((double) (WheelTics * -1) * 0.025);
      PixelRatio *= factor;
    }
  WheelTics = 0;
  PrepareMap();
}

void MyMapView::OnMouseClick(wxMouseEvent & event)
{
//
// MOUSE Click - event handler
// 
  SetFocus();
  if (wxIsBusy())
    {
      if (PrintInProgress == true)
        return;
      PendingWmsAbort = true;
      PendingRefresh = true;
    }

  if (MainFrame->IsModeIdentify() == true)
    {
      // identify
      if (ActiveLayer->HasQueryableChildren())
        DoIdentifyMultiLayer(event.GetX(), event.GetY());
      else
        DoIdentify(event.GetX(), event.GetY());
      return;
    }
  if (MainFrame->IsModeZoomIn() == true)
    {
      // zoom in - Rect
      DragStartX = event.GetX();
      DragStartY = event.GetY();
      LastDragX = event.GetX();
      LastDragY = event.GetY();
      return;
    }
  if (MainFrame->IsModeZoomOut() == true)
    {
      // zoom out
      FrameCX = (FrameMinX + (event.GetX() * PixelRatio));
      FrameCY = (FrameMaxY - (event.GetY() * PixelRatio));
      PixelRatio *= 2.0;
      PrepareMap();
      return;
    }

  if (MainFrame->IsModePan() == true)
    {
      DragStartX = event.GetX();
      DragStartY = event.GetY();
      LastDragX = event.GetX();
      LastDragY = event.GetY();
    }
}

void MyMapView::OnMouseDoubleClick(wxMouseEvent & event)
{
//
// MOUSE Double Click - event handler
// 
  SetFocus();
  if (wxIsBusy())
    {
      if (PrintInProgress == true)
        return;
      PendingWmsAbort = true;
      PendingRefresh = true;
    }

  if (MainFrame->IsModeIdentify() == true)
    {
      // identify
      if (ActiveLayer->HasQueryableChildren())
        DoIdentifyMultiLayer(event.GetX(), event.GetY());
      else
        DoIdentify(event.GetX(), event.GetY());
      return;
    }
  if (MainFrame->IsModeZoomIn() == true)
    {
      FrameCX = (FrameMinX + (event.GetX() * PixelRatio));
      FrameCY = (FrameMaxY - (event.GetY() * PixelRatio));
      PixelRatio /= 4.0;
      PrepareMap();
      DragStartX = -1;
      DragStartY = -1;
      LastDragX = -1;
      LastDragY = -1;
      return;
    }
  if (MainFrame->IsModeZoomOut() == true)
    {
      // zoom out
      FrameCX = (FrameMinX + (event.GetX() * PixelRatio));
      FrameCY = (FrameMaxY - (event.GetY() * PixelRatio));
      PixelRatio *= 4.0;
      PrepareMap();
      DragStartX = -1;
      DragStartY = -1;
      LastDragX = -1;
      LastDragY = -1;
      return;
    }
}

void MyMapView::SetCurrentScale(int scale)
{
//
// User Selected Scale
//
  FrameCX = FrameMinX + ((FrameMaxX - FrameMinX) / 2.0);
  FrameCY = FrameMinY + ((FrameMaxY - FrameMinY) / 2.0);
  double old = PixelRatio;
  PixelRatio = (double) scale *old / (double) CurrentScale;
  PrepareMap();
}

void MyMapView::OnMouseDragStop(wxMouseEvent & event)
{
//
// MOUSE left button up - event handler
// 
  if (wxIsBusy())
    return;
  if (DragStartX < 0 || DragStartY < 0)
    return;
  LastDragX = event.GetX();
  LastDragY = event.GetY();
  if (MainFrame->IsModePan() == true)
    {
      // computing the new Map position
      if (DragStartX == LastDragX && DragStartY == LastDragY)
        goto none;
      double shiftX = (double) (LastDragX - DragStartX) * PixelRatio;
      double shiftY = (double) (LastDragY - DragStartY) * PixelRatio;
      FrameCX -= shiftX;
      FrameCY += shiftY;
      PrepareMap();
    }
  if (MainFrame->IsModeZoomIn() == true)
    {
      // computing the new Map position 
      if (DragStartX < 0 || DragStartY < 0)
        goto none;
      if (DragStartX == LastDragX && DragStartY == LastDragY)
        {
          FrameCX = (FrameMinX + (event.GetX() * PixelRatio));
          FrameCY = (FrameMaxY - (event.GetY() * PixelRatio));
          PixelRatio /= 2.0;
          PrepareMap();
          DragStartX = -1;
          DragStartY = -1;
          LastDragX = -1;
          LastDragY = -1;
          return;
        }
      // computing the new Zoom
      wxSize sz = GetClientSize();
      FrameWidth = sz.GetWidth();
      FrameHeight = sz.GetHeight();
      // computing the current PixelRatio
      double x0 = FrameMinX + ((double) DragStartX * PixelRatio);
      double y0 = FrameMaxY - ((double) DragStartY * PixelRatio);
      double x1 = FrameMinX + ((double) LastDragX * PixelRatio);
      double y1 = FrameMaxY - ((double) LastDragY * PixelRatio);
      double extentX;
      double extentY;
      if (x0 > x1)
        {
          extentX = x0 - x1;
          FrameCX = x1 + (extentX / 2.0);
      } else
        {
          extentX = x1 - x0;
          FrameCX = x0 + (extentX / 2.0);
        }
      if (y0 > y1)
        {
          extentY = y0 - y1;
          FrameCY = y1 + (extentY / 2.0);
      } else
        {
          extentY = y1 - y0;
          FrameCY = y0 + (extentY / 2.0);
        }
      double x_ratio = extentX / (double) (FrameWidth);
      double y_ratio = extentY / (double) (FrameHeight);
      if (extentX / x_ratio <= FrameWidth && extentY / x_ratio <= FrameHeight)
        PixelRatio = x_ratio;
      else if (extentX / y_ratio <= FrameWidth
               && extentY / y_ratio <= FrameHeight)
        PixelRatio = y_ratio;
      else
        {
          if (x_ratio > y_ratio)
            PixelRatio = x_ratio;
          else
            PixelRatio = y_ratio;
        }
      PrepareMap();
    }
none:
  SetCursor(CursorCross);
  DragStartX = -1;
  DragStartY = -1;
  LastDragX = -1;
  LastDragY = -1;
}

void MyMapView::DragMap(int end_x, int end_y)
{
//
// Map dragging
//
  if (wxIsBusy())
    return;
  int width = FrameWidth;
  int height = FrameHeight;
  int baseX = 0;
  int baseY = 0;
  baseX = end_x - DragStartX;
  baseY = end_y - DragStartY;

  wxClientDC dc(this);
  dc.SetBrush(wxBrush(wxColour(192, 192, 192)));
  dc.DrawRectangle(-1, -1, width + 1, height + 1);
// copying the ScreenImage - dragged
  wxMemoryDC bmpDc(ScreenBitmap);
  int eff_width = width;
  int eff_height = height;
  int sx = 0;
  int sy = 0;
  int ix = 0;
  int iy = 0;
  if (baseX < 0)
    {
      ix = 0;
      sx = 0 - baseX;
      eff_width = width + baseX;
  } else
    {
      ix = baseX;
      eff_width = width - baseX;
    }
  if (baseY < 0)
    {
      iy = 0;
      sy = 0 - baseY;
      eff_height = height + baseY;
  } else
    {
      iy = baseY;
      eff_height = height - baseY;
    }
  dc.Blit(ix, iy, eff_width, eff_height, &bmpDc, sx, sy);
  LastDragX = end_x;
  LastDragY = end_y;
}

void MyMapView::SetFullExtent()
{
//
// setting the Frame to full extent
//
  WmsLayer *lyr = FirstLayer;
  if (lyr == NULL)
    {
      ValidMap = false;
      return;
  } else
    {
      ValidMap = true;
      if (lyr->GetTileServiceLayer() != NULL)
        {
          // TileService Layer
          MapMinX = lyr->GetTileServiceLayer()->GetMinLong();
          MapMinY = lyr->GetTileServiceLayer()->GetMinLat();
          MapMaxX = lyr->GetTileServiceLayer()->GetMaxLong();
          MapMaxY = lyr->GetTileServiceLayer()->GetMaxLat();
      } else
        {
          // Ordinary Layer
          if (lyr->IsSwapXY() == true)
            {
              MapMinY = lyr->GetMinX();
              MapMaxY = lyr->GetMaxX();
              MapMinX = lyr->GetMinY();
              MapMaxX = lyr->GetMaxY();
          } else
            {
              MapMinX = lyr->GetMinX();
              MapMaxX = lyr->GetMaxX();
              MapMinY = lyr->GetMinY();
              MapMaxY = lyr->GetMaxY();
            }
        }
    }
  while (lyr != NULL)
    {
      if (lyr->GetTileServiceLayer() != NULL)
        {
          // TileService Layer
          if (lyr->GetTileServiceLayer()->GetMinLong() < MapMinX)
            MapMinX = lyr->GetTileServiceLayer()->GetMinLong();
          if (lyr->GetTileServiceLayer()->GetMinLat() < MapMinY)
            MapMinY = lyr->GetTileServiceLayer()->GetMinLat();
          if (lyr->GetTileServiceLayer()->GetMaxLong() > MapMaxX)
            MapMaxX = lyr->GetTileServiceLayer()->GetMaxLong();
          if (lyr->GetTileServiceLayer()->GetMaxLat() > MapMaxY)
            MapMaxY = lyr->GetTileServiceLayer()->GetMaxLat();
      } else
        {
          // Ordinary Layer
          if (lyr->IsSwapXY() == true)
            {
              if (lyr->GetMinX() < MapMinY)
                MapMinY = lyr->GetMinX();
              if (lyr->GetMaxX() > MapMaxY)
                MapMaxY = lyr->GetMaxX();
              if (lyr->GetMinY() < MapMinX)
                MapMinX = lyr->GetMinY();
              if (lyr->GetMaxY() > MapMaxX)
                MapMaxX = lyr->GetMaxY();
          } else
            {
              if (lyr->GetMinX() < MapMinX)
                MapMinX = lyr->GetMinX();
              if (lyr->GetMaxX() > MapMaxX)
                MapMaxX = lyr->GetMaxX();
              if (lyr->GetMinY() < MapMinY)
                MapMinY = lyr->GetMinY();
              if (lyr->GetMaxY() > MapMaxY)
                MapMaxY = lyr->GetMaxY();
            }
        }
      lyr = lyr->GetNext();
    }
  MapExtentX = MapMaxX - MapMinX;
  MapExtentY = MapMaxY - MapMinY;
  MapCX = MapMinX + (MapExtentX / 2.0);
  MapCY = MapMinY + (MapExtentY / 2.0);
// computig the Frame Extent
  FrameCX = MapCX;
  FrameCY = MapCY;
  wxSize sz = GetClientSize();
  FrameWidth = sz.GetWidth();
  FrameHeight = sz.GetHeight();
// computing the current PixelRatio
  double x_ratio = MapExtentX / (double) (FrameWidth);
  double y_ratio = MapExtentY / (double) (FrameHeight);
  if (MapExtentX / x_ratio <= FrameWidth && MapExtentY / x_ratio <= FrameHeight)
    PixelRatio = x_ratio;
  else if (MapExtentX / y_ratio <= FrameWidth
           && MapExtentY / y_ratio <= FrameHeight)
    PixelRatio = y_ratio;
  else
    {
      if (x_ratio > y_ratio)
        PixelRatio = x_ratio;
      else
        PixelRatio = y_ratio;
    }
}

bool MyMapView::CanIdentify()
{
//
// test if Identify could be currently be enabled
//
  bool queryable = false;
  bool xml_gml = false;
  if (ActiveLayer == NULL)
    return false;
  if (ActiveLayer->IsVisible() == 0)
    return false;
  if (ActiveLayer->IsQueryable() == true)
    queryable = true;
  if (ActiveLayer->HasQueryableChildren() == true)
    queryable = true;
  if (queryable == false)
    return false;
  if (ActiveLayer->GetWmsService()->GetGmlMimeType() != NULL)
    xml_gml = true;
  if (ActiveLayer->GetWmsService()->GetXmlMimeType() != NULL)
    xml_gml = true;
  if (xml_gml == false)
    return false;
  return true;
}

void MyMapView::GetTileOrigin(double min_x, double min_y, double start_x,
                              double start_y, double tile_width,
                              double tile_height, double *base_x,
                              double *base_y)
{
//
// computing the Tile Origin
//
  double x = min_x;
  double y = min_y;
  if (start_x > min_x)
    {
      while (x + tile_width < start_x)
        x += tile_width;
  } else
    {
      while (x - tile_width > start_x)
        x -= tile_width;
    }
  if (start_y > min_y)
    {
      while (y + tile_height < start_y)
        y += tile_height;
  } else
    {
      while (y - tile_height > start_y)
        y -= tile_height;
    }
  *base_x = x;
  *base_y = y;
}

#ifdef _WIN32
DWORD WINAPI DoMapDownloadTilesThread(void *arg)
#else
void *DoMapDownloadTilesThread(void *arg)
#endif
{
//
// threaded function: downloading WMS tiles
//
  MyMapView *MapView = (MyMapView *) arg;
  WmsLayer *layer = MapView->GetLastLayer();
  while (layer != NULL)
    {
      if (MapView->IsPendingWmsAbort() == true)
        break;
      if (layer->GetTileServiceLayer() != NULL)
        MapView->Consider(layer->GetTileServiceLayer()->GetName());
      else
        MapView->Consider(layer->GetName());
      WmsGetMapRequest *req = MapView->GetFirstRequest();
      while (req != NULL)
        {
          // attempting first to display already cached tiles
          if (MapView->IsPendingWmsAbort() == true)
            break;
          if (req->IsTileService() == true
              && layer->GetTileServiceLayer() != NULL)
            {
              if (MapView->IsPendingWmsAbort() == true)
                break;
              if (strcmp
                  (layer->GetTileServiceLayer()->GetName(),
                   req->GetLayerName()) == 0)
                req->ExecuteMap(1);
            }
          if (req->IsTileService() == false
              && layer->GetTileServiceLayer() == NULL)
            {
              if (MapView->IsPendingWmsAbort() == true)
                break;
              if (strcmp(layer->GetName(), req->GetLayerName()) == 0)
                req->ExecuteMap(1);
            }
          req = req->GetNext();
        }
      for (int retry_count = 0; retry_count < 5; retry_count++)
        {
          bool retry = false;
          req = MapView->GetFirstRequest();
          while (req != NULL)
            {
              // downloading any not already cached tile
              bool ret = true;
              if (MapView->IsPendingWmsAbort() == true)
                break;
              if (req->IsTileService() == true
                  && layer->GetTileServiceLayer() != NULL)
                {
                  if (MapView->IsPendingWmsAbort() == true)
                    break;
                  if (strcmp
                      (layer->GetTileServiceLayer()->GetName(),
                       req->GetLayerName()) == 0
                      && req->IsImageReady() == false)
                    ret = req->ExecuteMap(0);
                }
              if (req->IsTileService() == false
                  && layer->GetTileServiceLayer() == NULL)
                {
                  if (MapView->IsPendingWmsAbort() == true)
                    break;
                  if (strcmp(layer->GetName(), req->GetLayerName()) == 0
                      && req->IsImageReady() == false)
                    ret = req->ExecuteMap(0);
                }
              if (ret == false)
                retry = true;
              req = req->GetNext();
            }
          if (retry == false)
            break;
        }
      layer = layer->GetPrev();
    }
  wxCommandEvent event(wxEVT_COMMAND_BUTTON_CLICKED,
                       ID_MAP_WMS_THREAD_FINISHED);
  MapView->GetEventHandler()->AddPendingEvent(event);
#ifdef _WIN32
  return 0;
#else
  pthread_exit(NULL);
#endif
}

double MyMapView::GetWmsBestResolution(double pixel_ratio)
{
// adjusting the current scale so to match a Fixed Scale
  double best_resolution = *(WmsFixedScales + 0);
  double max_resolution = *(WmsFixedScales + (WmsFixedScalesCount - 1));
  if (pixel_ratio <= max_resolution)
    return max_resolution;
  for (int i = 1; i < WmsFixedScalesCount; i++)
    {
      if (pixel_ratio > *(WmsFixedScales + i))
        {
          best_resolution = *(WmsFixedScales + i - 1);
          break;
        }
    }
  return best_resolution;
}

void MyMapView::PrepareMap()
{
//
// draws the MapBitmap
//
  if (PrintInProgress == true)
    {
      PendingRefresh = true;
      return;
    }
  if (DownloadInProgress == true)
    {
      PendingRefresh = true;
      return;
    }
  PendingRefresh = false;
  if (FirstLayer == NULL)
    ValidMap = false;
  if (!ValidMap)
    {
      // displaying an empty map
      Invalidate();
      Refresh();
      return;
    }
  MainFrame->UpdateTools();
  wxMemoryDC *dc = NULL;
  dc = new wxMemoryDC(MapBitmap);
  if (dc->IsOk() == false)
    {
      delete dc;
      return;
    }

  wxSize sz = GetClientSize();
  FrameWidth = sz.GetWidth();
  FrameHeight = sz.GetHeight();
  BuildFixedScales();
  if (IsWmsFixedScale() == true)
    {
      // adjusting the current scale so to match a Fixed Scale
      PixelRatio = GetWmsBestResolution(PixelRatio);
    }

  dc->SetBrush(wxBrush(wxColour(255, 255, 255), wxSOLID));
  dc->SetPen(wxPen(wxColour(255, 255, 255), wxSOLID));
// initializing the bitmap background
  dc->DrawRectangle(0, 0, FrameWidth, FrameHeight);

// computing the current Frame
  FrameMinX = FrameCX - ((FrameWidth / 2.0) * PixelRatio);
  FrameMaxX = FrameCX + ((FrameWidth / 2.0) * PixelRatio);
  FrameMinY = FrameCY - ((FrameHeight / 2.0) * PixelRatio);
  FrameMaxY = FrameCY + ((FrameHeight / 2.0) * PixelRatio);
  FrameExtentX = FrameMaxX - FrameMinX;
  FrameExtentY = FrameMaxY - FrameMinY;

  if (MainFrame->IsGeographicCRS(GetMapCRS()) == true)
    {
      // LongLat: computing the Scale [WMS standard pixel always corresponds to 0.28 mm]
      double extent = FrameExtentX * 111319.4907932455;
      CurrentScale = extent / (FrameWidth * 0.00028);
  } else
    {
      // planar: computing the Scale [WMS standard pixel always corresponds to 0.28 mm]
      CurrentScale = FrameExtentX / (FrameWidth * 0.00028);
    }
  MainFrame->UpdateMapScale();

  dc->SetBrush(wxNullBrush);
  dc->SetPen(wxNullPen);
  delete dc;
  ResetScreenBitmap();

  ::wxBeginBusyCursor();
  if (Proxy != NULL)
    delete[]Proxy;
  Proxy = NULL;
  if (MainFrame->GetHttpProxy().Len() > 0)
    {
      Proxy = new char[MainFrame->GetHttpProxy().Len() + 1];
      strcpy(Proxy, MainFrame->GetHttpProxy().ToUTF8());
    }
  ResetGetMapRequests();

  WmsLayer *layer = LastLayer;
  while (layer != NULL)
    {
      if (layer->IsVisible()
          && layer->IsCovered(FrameMinX, FrameMinY, FrameMaxX, FrameMaxY))
        {
          double baseX;
          double baseY;
          double tileW = FrameMaxX - FrameMinX;
          double tileH = FrameMaxY - FrameMinY;
          int tile_width = FrameWidth;
          int tile_height = FrameHeight;
          WmsTilePattern *tilepattern = NULL;
          bool tileservice_layer = false;
          if (layer->GetTileServiceLayer() != NULL)
            {
              // TileService layer
              WmsTileServiceLayer *pTL = layer->GetTileServiceLayer();
              tileservice_layer = true;
              if (pTL->GetTileParams(PixelRatio, FrameMinX,
                                     FrameMinY,
                                     &tile_width,
                                     &tile_height, &tileW,
                                     &tileH, &baseX,
                                     &baseY, &tilepattern) == false)
                {
                  layer = layer->GetPrev();
                  continue;
                }
          } else
            {
              // ordinary WMS layer
              if (layer->IsTiled())
                {
                  // tiled layer
                  tileW = (double) (layer->GetTileWidth()) * PixelRatio;
                  tileH = (double) (layer->GetTileHeight()) * PixelRatio;
                  tile_width = layer->GetTileWidth();
                  tile_height = layer->GetTileHeight();
                  if (layer->IsSwapXY())
                    GetTileOrigin(layer->GetMinY(), layer->GetMinX(), FrameMinX,
                                  FrameMinY, tileW, tileH, &baseX, &baseY);
                  else
                    GetTileOrigin(layer->GetMinX(), layer->GetMinY(), FrameMinX,
                                  FrameMinY, tileW, tileH, &baseX, &baseY);
              } else
                {
                  // untiled layer
                  tileW = (double) FrameWidth *PixelRatio;
                  tileH = (double) FrameHeight *PixelRatio;
                  tile_width = FrameWidth;
                  tile_height = FrameHeight;
                  baseX = FrameMinX;
                  baseY = FrameMinY;
                }
            }
          int image_x = (baseX - FrameMinX) / PixelRatio;
          int image_y = FrameHeight + ((FrameMinY - baseY) / PixelRatio);
          WmsTileSet *tileSet;
          tileSet =
            new WmsTileSet(FrameMinX, FrameMinY, FrameMaxX, FrameMaxY,
                           baseX, baseY, tileW, tileH, image_x, image_y,
                           FrameWidth, FrameHeight, tile_width, tile_height);

          // preparing the Tile Requests
          double pos_y = baseY;
          int row_no = 0;
          double lim_x = FrameMaxX;
          double lim_y = FrameMaxY;
          layer->MaxLimits(&lim_x, &lim_y);
          while (pos_y < lim_y)
            {
              // looping on tile-rows (bottom-up)
              double pos_x = baseX;
              int col_no = 0;
              while (pos_x < lim_x)
                {
                  // looping on tile-cols (left-right)
                  int pxW = (int) ((lim_x - pos_x) / PixelRatio);
                  int pxH = (int) ((lim_y - pos_y) / PixelRatio);
                  if (pxW < 1 || pxH < 1)
                    {
                      // avoiding to request useless/invisible tiles
                      ;
                  } else
                    tileSet->AddTile(pos_x, pos_y, row_no, col_no);
                  pos_x += tileW;
                  col_no++;
                }
              pos_y += tileH;
              row_no++;
            }

          WmsTileRequest *tile = tileSet->GetFirst();
          while (tile != NULL)
            {
              // looping on tile requests   
              if (tileservice_layer == true)
                {
                  WmsTileServiceLayer *pTL = layer->GetTileServiceLayer();
                  AddGetMapRequest(tile->GetImageX(), tile->GetImageY(),
                                   layer->GetWmsService()->GetURL_GetMap_Get(),
                                   pTL->GetName(), layer->IsSwapXY(),
                                   tile->GetMinX(), tile->GetMinY(),
                                   tile->GetMaxX(), tile->GetMaxY(),
                                   tileSet->GetTileWidth(),
                                   tileSet->GetTileHeight(), tilepattern);
              } else
                {
                  AddGetMapRequest(tile->GetImageX(), tile->GetImageY(),
                                   layer->GetWmsService()->GetURL_GetMap_Get(),
                                   layer->GetVersion(), layer->GetName(),
                                   layer->GetCRS(), layer->IsSwapXY(),
                                   tile->GetMinX(), tile->GetMinY(),
                                   tile->GetMaxX(), tile->GetMaxY(),
                                   tileSet->GetTileWidth(),
                                   tileSet->GetTileHeight(),
                                   layer->GetStyleName(), layer->GetFormat(),
                                   layer->IsOpaque());
                }
              tile = tile->GetNext();
            }
          delete tileSet;
        }
      layer = layer->GetPrev();
    }

// creating the Progress control
  if (ProgressControl != NULL)
    {
      delete ProgressControl;
      ProgressControl = NULL;
    }
  wxRect rect;
  if (MainFrame->GetProgressRect(rect) == true)
    {
      wxPoint pt = wxPoint(rect.GetX(), rect.GetY());
      wxSize sz = wxSize(rect.GetWidth(), rect.GetHeight());
      ProgressControl =
        new wxGauge(MainFrame->GetStatusBar(), wxID_ANY, CountGetMapRequests(),
                    pt, sz);
    }
  UpdateTimer = new wxTimer(this, ID_UPDATE_TIMER);
  UpdateTimer->Start(125, wxTIMER_ONE_SHOT);
  DownloadInProgress = true;
  PendingRefresh = false;
  PendingOnSize = false;
  PendingWmsAbort = false;
  MainFrame->UpdateWmsAbortTool(true);

#ifdef _WIN32
  HANDLE thread_handle;
  DWORD dwThreadId;
  thread_handle =
    CreateThread(NULL, 0, DoMapDownloadTilesThread, this, 0, &dwThreadId);
  SetThreadPriority(thread_handle, THREAD_PRIORITY_IDLE);
#else
  pthread_t thread_id;
  int ok_prior = 0;
  int policy;
  int min_prio;
  pthread_attr_t attr;
  struct sched_param sp;
  pthread_attr_init(&attr);
  if (pthread_attr_setschedpolicy(&attr, SCHED_RR) == 0)
    {
      // attempting to set the lowest priority  
      if (pthread_attr_getschedpolicy(&attr, &policy) == 0)
        {
          min_prio = sched_get_priority_min(policy);
          sp.sched_priority = min_prio;
          if (pthread_attr_setschedparam(&attr, &sp) == 0)
            {
              // ok, setting the lowest priority  
              ok_prior = 1;
              pthread_create(&thread_id, &attr, DoMapDownloadTilesThread, this);
            }
        }
    }
  if (!ok_prior)
    {
      // failure: using standard priority
      pthread_create(&thread_id, NULL, DoMapDownloadTilesThread, this);
    }
#endif
}

void MyMapView::OnMapWmsThreadFinished(wxCommandEvent & WXUNUSED(event))
{
//
// the WMS download thread (Map refresh) signals termination
//
  if (UpdateTimer != NULL)
    {
      UpdateTimer->Stop();
      delete UpdateTimer;
    }
  UpdateTimer = NULL;
  Consider();
  UpdatePendingTiles();
  ResetGetMapRequests();
  ::wxEndBusyCursor();
  if (ProgressControl != NULL)
    delete ProgressControl;
  ProgressControl = NULL;
  ResetScreenBitmap();
  DownloadInProgress = false;
  if (PendingOnSize == true || PendingRefresh == true)
    PrepareMap();
  PendingOnSize = false;
  PendingWmsAbort = false;
  MainFrame->UpdateWmsAbortTool(false);
}

void MyMapView::OnPdfWmsThreadFinished(wxCommandEvent & WXUNUSED(event))
{
//
// the WMS download thread (PDF Printer) signals termination
//
  if (PrintTimer != NULL)
    {
      PrintTimer->Stop();
      delete PrintTimer;
    }
  PrintTimer = NULL;
  if (Pdf != NULL)
    {
      Pdf->Consider();
      Pdf->UpdatePendingTiles();
      delete Pdf;
    }
  Pdf = NULL;
  PrintInProgress = false;
  PendingWmsAbort = false;
  ::wxEndBusyCursor();
  MainFrame->UpdateWmsAbortTool(false);
  MainFrame->UpdatePrinterTool(true);
}

void MyMapView::OnTiffWmsThreadFinished(wxCommandEvent & WXUNUSED(event))
{
//
// the WMS download thread (TIFF Writer) signals termination
//
  if (PrintTimer != NULL)
    {
      PrintTimer->Stop();
      delete PrintTimer;
    }
  PrintTimer = NULL;
  if (Tiff != NULL)
    {
      Tiff->Consider();
      Tiff->UpdatePendingTiles();
      if (Tiff->IsMonochrome() == true)
        {
          TiffMonochromeDialog dlg;
          dlg.Create(this);
          if (dlg.ShowModal() == wxID_OK)
            {
              if (dlg.IsMonochrome() == true)
                {
                  Tiff->SetColorSpace(RL2_PIXEL_MONOCHROME);
                  Tiff->SetCompression(dlg.GetCompression());
                }
              goto export_img;
          } else
            goto end;
        }
      if (Tiff->IsGrayscale() == true)
        {
          TiffGrayscaleDialog dlg;
          dlg.Create(this, Tiff->GetCompression());
          if (dlg.ShowModal() == wxID_OK)
            {
              if (dlg.IsGrayscale() == true)
                {
                  Tiff->SetColorSpace(RL2_PIXEL_GRAYSCALE);
                  Tiff->SetCompression(dlg.GetCompression());
                }
              goto export_img;
          } else
            goto end;
        }
      if (Tiff->IsPalette() == true)
        {
          TiffPaletteDialog dlg;
          dlg.Create(this, Tiff->GetCompression());
          if (dlg.ShowModal() == wxID_OK)
            {
              if (dlg.IsPalette() == true)
                {
                  Tiff->SetColorSpace(RL2_PIXEL_PALETTE);
                  Tiff->SetCompression(dlg.GetCompression());
                }
              goto export_img;
          } else
            goto end;
        }

    export_img:
      Tiff->ExportImage();
    end:
      delete Tiff;
    }
  Tiff = NULL;
  PrintInProgress = false;
  PendingWmsAbort = false;
  ::wxEndBusyCursor();
  MainFrame->UpdateWmsAbortTool(false);
  MainFrame->UpdatePrinterTool(true);
}

void MyMapView::SetPrintInProgress(PdfPrinter * pdf)
{
//
// stating some Printing process
//
  Pdf = pdf;
  PrintTimer = new wxTimer(this, ID_PRINT_TIMER);
  PrintTimer->Start(125, wxTIMER_ONE_SHOT);
  PrintInProgress = true;
  PendingWmsAbort = false;
  MainFrame->UpdateWmsAbortTool(false);
}

void MyMapView::SetPrintInProgress(TiffWriter * tiff)
{
//
// stating some Printing process
//
  Tiff = tiff;
  PrintTimer = new wxTimer(this, ID_PRINT_TIMER);
  PrintTimer->Start(125, wxTIMER_ONE_SHOT);
  PrintInProgress = true;
  PendingWmsAbort = false;
  MainFrame->UpdateWmsAbortTool(false);
}

void MyMapView::UpdatePendingTiles()
{
// updating any available tile on the screen
  int count = 0;
  WmsLayer *layer = LastLayer;
  while (layer != NULL)
    {
      WmsGetMapRequest *req = FirstRequest;
      while (req != NULL)
        {
          if (req->ToBeIgnored() == true)
            {
              req = req->GetNext();
              continue;
            }
          bool skip = true;
          if (req->IsTileService() == true
              && layer->GetTileServiceLayer() != NULL)
            {
              if (strcmp
                  (layer->GetTileServiceLayer()->GetName(),
                   req->GetLayerName()) == 0)
                skip = false;
            }
          if (req->IsTileService() == false
              && layer->GetTileServiceLayer() == NULL)
            {
              if (strcmp(layer->GetName(), req->GetLayerName()) == 0)
                skip = false;
            }
          if (skip == true)
            {
              req = req->GetNext();
              continue;
            }
          if (req->IsImageReady() == true && req->IsImageDone() == false
              && req->GetImage().IsOk() == true)
            {
              UpdateTile(req->GetImage(), req->GetPosX(), req->GetPosY(),
                         req->GetWidth(), req->GetHeight());
              req->Done();
            }
          if (req->IsImageDone() == true)
            count++;
          req = req->GetNext();
        }
      layer = layer->GetPrev();
    }
// updating the Progress control
  if (ProgressControl != NULL)
    {
      ProgressControl->Show();
      if (count == 0)
        ProgressControl->Pulse();
      else
        ProgressControl->SetValue(count);
    }
}

void MyMapView::UpdateTile(wxImage & Image, int pos_x, int pos_y, int width,
                           int height)
{
// drawing some WMS tile
  if (PendingOnSize == true)
    return;
  wxMemoryDC *dc = new wxMemoryDC(MapBitmap);
  wxBitmap bmp = wxBitmap(Image);
  wxMemoryDC *memDC = new wxMemoryDC(bmp);
  dc->Blit(pos_x, pos_y, width, height, memDC, 0, 0, wxCOPY, true);
  delete dc;
  ResetScreenBitmap();
}

void MyMapView::ResetScreenBitmap()
{
// copying the MapBitmap into the ScreenBitmap
  wxMemoryDC *screenDc = new wxMemoryDC(ScreenBitmap);
  wxGraphicsContext *gr = wxGraphicsContext::Create(*screenDc);
  gr->DrawBitmap(MapBitmap, 0, 0, BitmapWidth, BitmapHeight);
  if (ImageMarkerOdd.IsOk() == true && DynamicOddEven == true)
    gr->DrawBitmap(ImageMarkerOdd, 0, 0, ImageMarkerOdd.GetWidth(),
                   ImageMarkerOdd.GetHeight());
  if (ImageMarkerEven.IsOk() == true && DynamicOddEven == false)
    gr->DrawBitmap(ImageMarkerEven, 0, 0, ImageMarkerEven.GetWidth(),
                   ImageMarkerEven.GetHeight());
  delete gr;
  delete screenDc;
  Refresh();
  Update();
}

void MyMapView::DoIdentify(int mouse_x, int mouse_y)
{
//
// Identify (Single Layer)
//
  char *err_msg;
  WmsLayer *layer = ActiveLayer;
  if (layer == NULL)
    return;
  char *proxy = NULL;
  if (MainFrame->GetHttpProxy().Len() > 0)
    {
      proxy = new char[MainFrame->GetHttpProxy().Len() + 1];
      strcpy(proxy, MainFrame->GetHttpProxy().ToUTF8());
    }
  const char *xmlMimeType = layer->GetWmsService()->GetGmlMimeType();
  if (xmlMimeType == NULL)
    xmlMimeType = layer->GetWmsService()->GetXmlMimeType();
  ::wxBeginBusyCursor();
  rl2WmsFeatureCollectionPtr coll =
    do_wms_GetFeatureInfo_get(layer->
                              GetWmsService()->GetURL_GetFeatureInfo_Get(),
                              proxy,
                              layer->GetVersion(),
                              xmlMimeType,
                              layer->GetName(),
                              layer->GetCRS(), layer->IsSwapXY(), FrameMinX,
                              FrameMinY, FrameMaxX,
                              FrameMaxY, FrameWidth, FrameHeight, mouse_x,
                              mouse_y, &err_msg);
  ::wxEndBusyCursor();
  if (proxy != NULL)
    delete[]proxy;

  if (coll != NULL)
    {
      // showing the GetFeatureInfo results
      double mx = FrameMinX + ((double) mouse_x * PixelRatio);
      double my = FrameMaxY - ((double) mouse_y * PixelRatio);
      wms_feature_collection_parse_geometries(coll,
                                              GetSridFromCRS(layer->GetCRS()),
                                              mx, my, MainFrame->GetSQLite());
      wxString lyrname = wxString::FromUTF8(layer->GetTitle());
      CleanHtml(layer->GetTitle(), lyrname);
      wxString html_table;
      HtmlFromGetFeatureInfo(coll, 0, lyrname, html_table);
      wxString html = wxT("<html><body>") + html_table + wxT("</body></html>");
      GeometryList *list = new GeometryList();
      PopulateGeometryList(list, 0, coll);
      if (IdentifyPanel == NULL)
        IdentifyPanel =
          new WmsIdentifyPanel(this, html, coll, list, mouse_x, mouse_y);
      else
        IdentifyPanel->Initialize(html, coll, list, mouse_x, mouse_y);
      IdentifyPanel->Show();
    }
}

void MyMapView::DoIdentifyMultiLayer(int mouse_x, int mouse_y)
{
//
// Identify (Multiple Layers)
//
  bool ok = false;
  char *err_msg;
  WmsLayer *layer = ActiveLayer;
  GeometryList *list = NULL;
  if (layer == NULL)
    return;
  char *proxy = NULL;
  rl2WmsFeatureCollectionPtr coll = NULL;
  if (MainFrame->GetHttpProxy().Len() > 0)
    {
      proxy = new char[MainFrame->GetHttpProxy().Len() + 1];
      strcpy(proxy, MainFrame->GetHttpProxy().ToUTF8());
    }
  list = new GeometryList();

  wxString html = wxT("<html><body>");
  ::wxBeginBusyCursor();
  WmsQueryableChildLayer *child = ActiveLayer->GetFirstChild();
  int idx = 0;
  while (child != NULL)
    {
      // querying children layers one at each time
      const char *xmlMimeType = layer->GetWmsService()->GetGmlMimeType();
      if (xmlMimeType == NULL)
        xmlMimeType = layer->GetWmsService()->GetXmlMimeType();
      coll =
        do_wms_GetFeatureInfo_get(layer->
                                  GetWmsService()->GetURL_GetFeatureInfo_Get(),
                                  proxy, layer->GetVersion(), xmlMimeType,
                                  child->GetName(), layer->GetCRS(),
                                  layer->IsSwapXY(), FrameMinX, FrameMinY,
                                  FrameMaxX, FrameMaxY, FrameWidth, FrameHeight,
                                  mouse_x, mouse_y, &err_msg);
      if (coll != NULL)
        {
          // showing the GetFeatureInfo results
          double mx = FrameMinX + ((double) mouse_x * PixelRatio);
          double my = FrameMaxY - ((double) mouse_y * PixelRatio);
          wms_feature_collection_parse_geometries(coll,
                                                  GetSridFromCRS(layer->GetCRS
                                                                 ()), mx, my,
                                                  MainFrame->GetSQLite());
          wxString lyrname;
          CleanHtml(child->GetTitle(), lyrname);
          wxString html_table;
          HtmlFromGetFeatureInfo(coll, idx, lyrname, html_table);
          html += html_table + wxT("<br>");
          PopulateGeometryList(list, idx, coll);
          ok = true;
        }
      idx++;
      child = child->GetNext();
    }
  ::wxEndBusyCursor();
  html += wxT("</body></html>");
  if (proxy != NULL)
    delete[]proxy;
  if (ok == false)
    {
      if (list != NULL)
        delete list;
      if (coll != NULL)
        destroy_wms_feature_collection(coll);
      return;
    }

  if (IdentifyPanel == NULL)
    IdentifyPanel =
      new WmsIdentifyPanel(this, html, coll, list, mouse_x, mouse_y);
  else
    IdentifyPanel->Initialize(html, coll, list, mouse_x, mouse_y);
}

void MyMapView::CleanHtml(const char *dirty, wxString & clean)
{
//
// ensuring that every HTML string should be well formatted
//
  clean = wxT("");
  const char *p = dirty;
  while (*p != '\0')
    {
      if (*p == ' ')
        clean += wxT("&nbsp;");
      else if (*p == '<')
        clean += wxT("&lt;");
      else if (*p == '>')
        clean += wxT("&gt;");
      else if (*p == '&')
        clean += wxT("&amp;");
      else if (*p == '"')
        clean += wxT("&quot;");
      else
        {
          char buf[16];
          sprintf(buf, "%c", *p);
          clean += wxString::FromUTF8(buf);
        }
      p++;
    }
}

void MyMapView::PopulateGeometryList(GeometryList * list, int layer_id,
                                     rl2WmsFeatureCollectionPtr coll)
{
//
// creating a Geometry List
//
  for (int row = 0; row < get_wms_feature_members_count(coll); row++)
    {
      rl2WmsFeatureMemberPtr feature = get_wms_feature_member(coll, row);
      for (int col = 0; col < get_wms_feature_attributes_count(feature); col++)
        {
          gaiaGeomCollPtr geom =
            get_wms_feature_attribute_geometry(feature, col);
          if (geom != NULL)
            list->Add(layer_id, row, col, geom);
        }
    }
}

void MyMapView::HtmlFromGetFeatureInfo(rl2WmsFeatureCollectionPtr coll, int idx,
                                       wxString & layer_name, wxString & html)
{
//
// building an HTML page corresponding to GetFeatureInfo results
//
  char href[128];
  html = wxT("Layer: <b>") + layer_name + wxT("</b><br><table border=\"\1\">");
  for (int row = 0; row < get_wms_feature_members_count(coll); row++)
    {
      rl2WmsFeatureMemberPtr feature = get_wms_feature_member(coll, row);
      if (row == 0)
        {
          // titles
          html += wxT("<tr>");
          for (int col = 0; col < get_wms_feature_attributes_count(feature);
               col++)
            {
              html += wxT("<th bgcolor=\"#b0b0b0\">");
              wxString name;
              CleanHtml(get_wms_feature_attribute_name(feature, col), name);
              html += name;
              html += wxT("</th>");
            }
          html += wxT("</tr>");
        }
      html += wxT("<tr>");
      if ((row % 2) == 0)
        {
          // even rows
          for (int col = 0; col < get_wms_feature_attributes_count(feature);
               col++)
            {
              html += wxT("<td>");
              if (get_wms_feature_attribute_geometry(feature, col) != NULL)
                {
                  sprintf(href, "<a href=\"%d_%d_%d", idx, row, col);
                  html += wxString::FromUTF8(href) + wxT("\">GEOMETRY</a>");
              } else
                {
                  wxString value;
                  CleanHtml(get_wms_feature_attribute_value(feature, col),
                            value);
                  html += value;
                }
              html += wxT("</td>");
            }
      } else
        {
          // odd rows
          for (int col = 0; col < get_wms_feature_attributes_count(feature);
               col++)
            {
              html += wxT("<td bgcolor=\"#d0d0d0\">");
              if (get_wms_feature_attribute_geometry(feature, col) != NULL)
                {
                  sprintf(href, "<a href=\"%d_%d_%d", idx, row, col);
                  html += wxString::FromUTF8(href) + wxT("\">GEOMETRY</a>");
              } else
                {
                  wxString value;
                  CleanHtml(get_wms_feature_attribute_value(feature, col),
                            value);
                  html += value;
                }
              html += wxT("</td>");
            }
        }
      html += wxT("</tr>");
    }
  html += wxT("</table><br>");
}

WmsService *MyMapView::InsertWmsService(const char *version, const char *name,
                                        const char *title, const char *abstract,
                                        const char *tile_service_name,
                                        const char *tile_service_title,
                                        const char *tile_service_abstract,
                                        const char *url_GetMap_get,
                                        const char *url_GetMap_post,
                                        const char *url_GetTileService_get,
                                        const char *url_GetTileService_post,
                                        const char *url_GetFeatureInfo_get,
                                        const char *url_GetFeatureInfo_post,
                                        const char *gml_mime_type,
                                        const char *xml_mime_type,
                                        const char *contact_person,
                                        const char *contact_organization,
                                        const char *contact_position,
                                        const char *postal_address,
                                        const char *city,
                                        const char *state_province,
                                        const char *post_code,
                                        const char *country,
                                        const char *voice_telephone,
                                        const char *fax_telephone,
                                        const char *email_address,
                                        const char *fees,
                                        const char *access_constraints,
                                        int layer_limit, int max_width,
                                        int max_height)
{
//
//  adding a new WMS Service (if not already defined)
//
  WmsService *service =
    new WmsService(version, name, title, abstract, tile_service_name,
                   tile_service_title, tile_service_abstract, url_GetMap_get,
                   url_GetMap_post, url_GetTileService_get,
                   url_GetTileService_post,
                   url_GetFeatureInfo_get,
                   url_GetFeatureInfo_post, gml_mime_type, xml_mime_type,
                   contact_person,
                   contact_organization, contact_position, postal_address,
                   city, state_province, post_code, country, voice_telephone,
                   fax_telephone, email_address,
                   fees, access_constraints, layer_limit, max_width,
                   max_height);
  WmsService *service2 = FindAlreadyDefined(service);
  if (service2 != NULL)
    {
      delete service;
      return service2;
    }
  if (FirstService == NULL)
    FirstService = service;
  service->SetPrev(LastService);
  if (LastService != NULL)
    LastService->SetNext(service);
  LastService = service;
  return service;
}

void MyMapView::InsertWmsLayer(WmsLayer * lyr)
{
//
// adding a new WMS layer [ordinary WMS layer]
//
  bool already_defined;
  bool mixed_crs;
  bool multiple_fixed_scales;
  CheckWmsLayer(lyr, &already_defined, &mixed_crs, &multiple_fixed_scales);
  if (already_defined)
    {
      wxMessageBox(wxT
                   ("WARNING: the same WMS Layer is already defined; rejected"),
                   wxT("LibreWMS"), wxOK | wxICON_WARNING, this);
      delete lyr;
      return;
    }
  if (mixed_crs)
    wxMessageBox(wxT
                 ("WARNING: there are WMS Layers using different CRS definitions !!!"),
                 wxT("LibreWMS"), wxOK | wxICON_WARNING, this);
  if (multiple_fixed_scales)
    {
      wxMessageBox(wxT
                   ("WARNING: there are WMS Layers requiring different zoom factors: rejected"),
                   wxT("LibreWMS"), wxOK | wxICON_WARNING, this);
      delete lyr;
      return;
    }
  if (LastLayer == NULL)
    LastLayer = lyr;
  lyr->SetNext(FirstLayer);
  if (FirstLayer != NULL)
    FirstLayer->SetPrev(lyr);
  FirstLayer = lyr;
  MainFrame->GetLayerTree()->AddLayer(lyr);
  if (CountWmsLayers() == 1)
    {
      // initializing the Map
      SetFullExtent();
    }
  PrepareMap();
}

void MyMapView::ResetWmsLayers()
{
//
// resetting the WMS Layer list
//
  FirstLayer = NULL;
  LastLayer = NULL;
}

void MyMapView::ReinsertWmsLayer(WmsLayer * lyr)
{
//
// reinserting yet again a WMS Layer (Tree new order)
//
  lyr->SetPrev(NULL);
  lyr->SetNext(NULL);
  if (FirstLayer == NULL)
    FirstLayer = lyr;
  lyr->SetPrev(LastLayer);
  if (LastLayer != NULL)
    LastLayer->SetNext(lyr);
  LastLayer = lyr;
}

void MyMapView::RemoveWmsLayer(WmsLayer * layer)
{
//
// removing a WMS layer
//
  WmsService *serv;
  WmsService *serv_x = layer->GetWmsService();
  if (ActiveLayer == layer)
    ActiveLayer = NULL;
  if (layer->GetPrev() != NULL)
    layer->GetPrev()->SetNext(layer->GetNext());
  if (layer->GetNext() != NULL)
    layer->GetNext()->SetPrev(layer->GetPrev());
  if (FirstLayer == layer)
    FirstLayer = layer->GetNext();
  if (LastLayer == layer)
    LastLayer = layer->GetPrev();
  delete layer;
  serv = FirstService;
  while (serv != NULL)
    {
      if (serv == serv_x)
        {
          if (serv->GetRefCount() <= 0)
            RemoveWmsService(serv);
          return;
        }
      serv = serv->GetNext();
    }
  BuildCRSlist();
}

void MyMapView::RemoveWmsService(WmsService * service)
{
//
// removing a WMS Servuce
//
  if (service->GetPrev() != NULL)
    service->GetPrev()->SetNext(service->GetNext());
  if (service->GetNext() != NULL)
    service->GetNext()->SetPrev(service->GetPrev());
  if (FirstService == service)
    FirstService = service->GetNext();
  if (LastService == service)
    LastService = service->GetPrev();
  delete service;
}

WmsService *MyMapView::FindAlreadyDefined(WmsService * service)
{
//
// attempting to identify an already defined WMS Service
//
  WmsService *service2 = FirstService;
  while (service2 != NULL)
    {
      if (service2->Equals(service))
        return service2;
      service2 = service2->GetNext();
    }
  return NULL;
}

void MyMapView::CheckWmsLayer(WmsLayer * layer, bool * already_defined,
                              bool * mixed_crs, bool * multiple_fixed_scales)
{
// checks a new WMS layer for consistency before adding into the list
  *already_defined = false;
  *mixed_crs = false;
  *multiple_fixed_scales = false;
  WmsLayer *lyr = FirstLayer;
  while (lyr != NULL)
    {
      if (lyr->Equals(layer))
        *already_defined = true;
      if (strcmp(lyr->GetCRS(), layer->GetCRS()) != 0)
        *mixed_crs = true;
      if (lyr->GetTileServiceLayer() != NULL
          && layer->GetTileServiceLayer() != NULL)
        {
          if (lyr->
              GetTileServiceLayer()->CompareFixedScales(layer->
                                                        GetTileServiceLayer())
              == false)
            *multiple_fixed_scales = true;
        }
      lyr = lyr->GetNext();
    }
}

int MyMapView::CountWmsLayers()
{
// counting how many WMS Layers are currently set
  int count = 0;
  WmsLayer *lyr = FirstLayer;
  while (lyr != NULL)
    {
      count++;
      lyr = lyr->GetNext();
    }
  return count;
}

const char *MyMapView::GetMapCRS()
{
// retrieving the Map CRS
  if (LastLayer == NULL)
    return NULL;
  if (LastLayer->GetTileServiceLayer() != NULL)
    {
      // TileService layer
      WmsTilePattern *pP = LastLayer->GetTileServiceLayer()->GetFirst();
      if (pP == NULL)
        return NULL;
      return pP->GetSRS();
    }
  return LastLayer->GetCRS();
}

WmsFormat::WmsFormat(const char *name)
{
//
// WMS Format - constructor
//
  int len = strlen(name);
  Name = new char[len + 1];
  strcpy(Name, name);
  Next = NULL;
}

WmsFormat::~WmsFormat()
{
//
// WMS Format - destructor
//
  if (Name != NULL)
    delete[]Name;
}

WmsStyle::WmsStyle(const char *name, const char *title, const char *abstract)
{
//
// WMS Style - constructor
//
  int len = strlen(name);
  Name = new char[len + 1];
  strcpy(Name, name);
  Title = NULL;
  if (title != NULL)
    {
      len = strlen(title);
      Title = new char[len + 1];
      strcpy(Title, title);
    }
  Abstract = NULL;
  if (abstract != NULL)
    {
      len = strlen(abstract);
      Abstract = new char[len + 1];
      strcpy(Abstract, abstract);
    }
  Next = NULL;
}

WmsStyle::~WmsStyle()
{
//
// WMS Style - destructor
//
  if (Name != NULL)
    delete[]Name;
  if (Title != NULL)
    delete[]Title;
  if (Abstract != NULL)
    delete[]Abstract;
}

WmsCRS::WmsCRS(const char *crs)
{
//
// WMS CRS - constructor
//
  int len = strlen(crs);
  CRS = new char[len + 1];
  strcpy(CRS, crs);
  BBox = NULL;
  Next = NULL;
}

WmsCRS::~WmsCRS()
{
//
// WMS CRS - destructor
//
  if (CRS != NULL)
    delete[]CRS;
  if (BBox != NULL)
    delete BBox;
}

WmsService::WmsService(const char *version, const char *name, const char *title,
                       const char *abstract, const char *tile_service_name,
                       const char *tile_service_title,
                       const char *tile_service_abstract,
                       const char *url_GetMap_get, const char *url_GetMap_post,
                       const char *url_GetTileService_get,
                       const char *url_GetTileService_post,
                       const char *url_GetFeatureInfo_get,
                       const char *url_GetFeatureInfo_post,
                       const char *gml_mime_type, const char *xml_mime_type,
                       const char *contact_person,
                       const char *contact_organization,
                       const char *contact_position, const char *postal_address,
                       const char *city, const char *state_province,
                       const char *post_code, const char *country,
                       const char *voice_telephone, const char *fax_telephone,
                       const char *email_address, const char *fees,
                       const char *access_constraints, int layer_limit,
                       int max_width, int max_height)
{
//
// WMS Service - constructor
//
  int len = strlen(name);
  Name = new char[len + 1];
  strcpy(Name, name);
  Version = NULL;
  if (version != NULL)
    {
      len = strlen(version);
      Version = new char[len + 1];
      strcpy(Version, version);
    }
  Title = NULL;
  if (title != NULL)
    {
      len = strlen(title);
      Title = new char[len + 1];
      strcpy(Title, title);
    }
  Abstract = NULL;
  if (abstract != NULL)
    {
      len = strlen(abstract);
      Abstract = new char[len + 1];
      strcpy(Abstract, abstract);
    }
  TileServiceName = NULL;
  if (tile_service_name != NULL)
    {
      len = strlen(tile_service_name);
      TileServiceName = new char[len + 1];
      strcpy(TileServiceName, tile_service_name);
    }
  TileServiceTitle = NULL;
  if (tile_service_title != NULL)
    {
      len = strlen(tile_service_title);
      TileServiceTitle = new char[len + 1];
      strcpy(TileServiceTitle, tile_service_title);
    }
  TileServiceAbstract = NULL;
  if (tile_service_abstract != NULL)
    {
      len = strlen(tile_service_abstract);
      TileServiceAbstract = new char[len + 1];
      strcpy(TileServiceAbstract, tile_service_abstract);
    }
  URL_GetMap_Get = NULL;
  if (url_GetMap_get != NULL)
    {
      len = strlen(url_GetMap_get);
      URL_GetMap_Get = new char[len + 1];
      strcpy(URL_GetMap_Get, url_GetMap_get);
    }
  URL_GetMap_Post = NULL;
  if (url_GetMap_post != NULL)
    {
      len = strlen(url_GetMap_post);
      URL_GetMap_Post = new char[len + 1];
      strcpy(URL_GetMap_Post, url_GetMap_post);
    }
  URL_GetTileService_Get = NULL;
  if (url_GetTileService_get != NULL)
    {
      len = strlen(url_GetTileService_get);
      URL_GetTileService_Get = new char[len + 1];
      strcpy(URL_GetTileService_Get, url_GetTileService_get);
    }
  URL_GetTileService_Post = NULL;
  if (url_GetTileService_post != NULL)
    {
      len = strlen(url_GetTileService_post);
      URL_GetTileService_Post = new char[len + 1];
      strcpy(URL_GetTileService_Post, url_GetTileService_post);
    }
  URL_GetFeatureInfo_Get = NULL;
  if (url_GetFeatureInfo_get != NULL)
    {
      len = strlen(url_GetFeatureInfo_get);
      URL_GetFeatureInfo_Get = new char[len + 1];
      strcpy(URL_GetFeatureInfo_Get, url_GetFeatureInfo_get);
    }
  GmlMimeType = NULL;
  if (gml_mime_type != NULL)
    {
      len = strlen(gml_mime_type);
      GmlMimeType = new char[len + 1];
      strcpy(GmlMimeType, gml_mime_type);
    }
  XmlMimeType = NULL;
  if (xml_mime_type != NULL)
    {
      len = strlen(xml_mime_type);
      XmlMimeType = new char[len + 1];
      strcpy(XmlMimeType, xml_mime_type);
    }
  URL_GetFeatureInfo_Post = NULL;
  if (url_GetFeatureInfo_post != NULL)
    {
      len = strlen(url_GetFeatureInfo_post);
      URL_GetFeatureInfo_Post = new char[len + 1];
      strcpy(URL_GetFeatureInfo_Post, url_GetFeatureInfo_post);
    }
  ContactPerson = NULL;
  if (contact_person != NULL)
    {
      len = strlen(contact_person);
      ContactPerson = new char[len + 1];
      strcpy(ContactPerson, contact_person);
    }
  ContactOrganization = NULL;
  if (contact_organization != NULL)
    {
      len = strlen(contact_organization);
      ContactOrganization = new char[len + 1];
      strcpy(ContactOrganization, contact_organization);
    }
  ContactPosition = NULL;
  if (contact_position != NULL)
    {
      len = strlen(contact_position);
      ContactPosition = new char[len + 1];
      strcpy(ContactPosition, contact_position);
    }
  PostalAddress = NULL;
  if (postal_address != NULL)
    {
      len = strlen(postal_address);
      PostalAddress = new char[len + 1];
      strcpy(PostalAddress, postal_address);
    }
  City = NULL;
  if (city != NULL)
    {
      len = strlen(city);
      City = new char[len + 1];
      strcpy(City, city);
    }
  StateProvince = NULL;
  if (state_province != NULL)
    {
      len = strlen(state_province);
      StateProvince = new char[len + 1];
      strcpy(StateProvince, state_province);
    }
  PostCode = NULL;
  if (post_code != NULL)
    {
      len = strlen(post_code);
      PostCode = new char[len + 1];
      strcpy(PostCode, post_code);
    }
  Country = NULL;
  if (country != NULL)
    {
      len = strlen(country);
      Country = new char[len + 1];
      strcpy(Country, country);
    }
  VoiceTelephone = NULL;
  if (voice_telephone != NULL)
    {
      len = strlen(voice_telephone);
      VoiceTelephone = new char[len + 1];
      strcpy(VoiceTelephone, voice_telephone);
    }
  FaxTelephone = NULL;
  if (fax_telephone != NULL)
    {
      len = strlen(fax_telephone);
      FaxTelephone = new char[len + 1];
      strcpy(FaxTelephone, fax_telephone);
    }
  EMailAddress = NULL;
  if (email_address != NULL)
    {
      len = strlen(email_address);
      EMailAddress = new char[len + 1];
      strcpy(EMailAddress, email_address);
    }
  Fees = NULL;
  if (fees != NULL)
    {
      len = strlen(fees);
      Fees = new char[len + 1];
      strcpy(Fees, fees);
    }
  AccessConstraints = NULL;
  if (access_constraints != NULL)
    {
      len = strlen(access_constraints);
      AccessConstraints = new char[len + 1];
      strcpy(AccessConstraints, access_constraints);
    }
  LayerLimit = layer_limit;
  MaxWidth = max_width;
  MaxHeight = max_height;
  RefCount = 0;
  Prev = NULL;
  Next = NULL;
  FirstFormat = NULL;
  LastFormat = NULL;
}

WmsService::~WmsService()
{
//
// WMS Service - destructor
//
  WmsFormat *pF;
  WmsFormat *pFn;
  if (Version != NULL)
    delete[]Version;
  if (Name != NULL)
    delete[]Name;
  if (Title != NULL)
    delete[]Title;
  if (Abstract != NULL)
    delete[]Abstract;
  if (TileServiceName != NULL)
    delete[]TileServiceName;
  if (TileServiceTitle != NULL)
    delete[]TileServiceTitle;
  if (TileServiceAbstract != NULL)
    delete[]TileServiceAbstract;
  if (URL_GetMap_Get != NULL)
    delete[]URL_GetMap_Get;
  if (URL_GetMap_Post != NULL)
    delete[]URL_GetMap_Post;
  if (URL_GetTileService_Get != NULL)
    delete[]URL_GetTileService_Get;
  if (URL_GetTileService_Post != NULL)
    delete[]URL_GetTileService_Post;
  if (URL_GetFeatureInfo_Get != NULL)
    delete[]URL_GetFeatureInfo_Get;
  if (URL_GetFeatureInfo_Post != NULL)
    delete[]URL_GetFeatureInfo_Post;
  if (GmlMimeType != NULL)
    delete[]GmlMimeType;
  if (XmlMimeType != NULL)
    delete[]XmlMimeType;
  if (ContactPerson != NULL)
    delete[]ContactPerson;
  if (ContactOrganization != NULL)
    delete[]ContactOrganization;
  if (ContactPosition != NULL)
    delete[]ContactPosition;
  if (PostalAddress != NULL)
    delete[]PostalAddress;
  if (City != NULL)
    delete[]City;
  if (StateProvince != NULL)
    delete[]StateProvince;
  if (PostCode != NULL)
    delete[]PostCode;
  if (Country != NULL)
    delete[]Country;
  if (VoiceTelephone != NULL)
    delete[]VoiceTelephone;
  if (FaxTelephone != NULL)
    delete[]FaxTelephone;
  if (EMailAddress != NULL)
    delete[]EMailAddress;
  if (Fees != NULL)
    delete[]Fees;
  if (AccessConstraints != NULL)
    delete[]AccessConstraints;
  pF = FirstFormat;
  while (pF != NULL)
    {
      pFn = pF->GetNext();
      delete pF;
      pF = pFn;
    }
}

bool WmsService::FormatAlreadyDefined(const char *name)
{
//
// checks if a Format is already defined 
//
  WmsFormat *fmt = FirstFormat;
  while (fmt != NULL)
    {
      if (strcmp(fmt->GetName(), name) == 0)
        return true;
      fmt = fmt->GetNext();
    }
  return false;
}

void WmsService::AddFormat(const char *name)
{
//
// adding a Format definition
//
  if (FormatAlreadyDefined(name))
    return;
  WmsFormat *fmt = new WmsFormat(name);
  if (FirstFormat == NULL)
    FirstFormat = fmt;
  if (LastFormat != NULL)
    LastFormat->SetNext(fmt);
  LastFormat = fmt;
}

int WmsService::CountFormats()
{
//
// counting how many Format definitions are there
//
  int count = 0;
  WmsFormat *pF = FirstFormat;
  while (pF != NULL)
    {
      count++;
      pF = pF->GetNext();
    }
  return count;
}

const char *WmsService::GetFormatByIndex(int idx)
{
//
// return a Format as identified by its index
//
  int count = 0;
  WmsFormat *pF = FirstFormat;
  while (pF != NULL)
    {
      if (count == idx)
        return pF->GetName();
      count++;
      pF = pF->GetNext();
    }
  return NULL;
}

bool WmsService::Equals(WmsService * other)
{
//
// check if two WMS Services are the same
//
  int valid = 0;
  if (this->Name == NULL && other->Name == NULL)
    valid++;
  else if (this->Name != NULL && other->Name != NULL)
    {
      if (strcmp(this->Name, other->Name) == 0)
        valid++;
    }
  if (this->Title == NULL && other->Title == NULL)
    valid++;
  else if (this->Title != NULL && other->Title != NULL)
    {
      if (strcmp(this->Title, other->Title) == 0)
        valid++;
    }
  if (this->Abstract == NULL && other->Abstract == NULL)
    valid++;
  else if (this->Abstract != NULL && other->Abstract != NULL)
    {
      if (strcmp(this->Abstract, other->Abstract) == 0)
        valid++;
    }
  if (this->URL_GetMap_Get == NULL && other->URL_GetMap_Get == NULL)
    valid++;
  else if (this->URL_GetMap_Get != NULL && other->URL_GetMap_Get != NULL)
    {
      if (strcmp(this->URL_GetMap_Get, other->URL_GetMap_Get) == 0)
        valid++;
    }
  if (this->URL_GetMap_Post == NULL && other->URL_GetMap_Post == NULL)
    valid++;
  else if (this->URL_GetMap_Post != NULL && other->URL_GetMap_Post != NULL)
    {
      if (strcmp(this->URL_GetMap_Post, other->URL_GetMap_Post) == 0)
        valid++;
    }
  if (this->URL_GetFeatureInfo_Get == NULL
      && other->URL_GetFeatureInfo_Get == NULL)
    valid++;
  else if (this->URL_GetFeatureInfo_Get != NULL
           && other->URL_GetFeatureInfo_Get != NULL)
    {
      if (strcmp(this->URL_GetFeatureInfo_Get, other->URL_GetFeatureInfo_Get) ==
          0)
        valid++;
    }
  if (this->URL_GetFeatureInfo_Post == NULL
      && other->URL_GetFeatureInfo_Post == NULL)
    valid++;
  else if (this->URL_GetFeatureInfo_Post != NULL
           && other->URL_GetFeatureInfo_Post != NULL)
    {
      if (strcmp(this->URL_GetFeatureInfo_Post, other->URL_GetFeatureInfo_Post)
          == 0)
        valid++;
    }
  if (this->LayerLimit == other->LayerLimit)
    valid++;
  if (this->MaxWidth == other->MaxWidth)
    valid++;
  if (this->MaxHeight == other->MaxHeight)
    valid++;
  if (valid == 10)
    return true;
  return false;
}

WmsLayer::WmsLayer(WmsService * service, const char *name, const char *title,
                   const char *abstract, int tiled, int tile_width,
                   int tile_height, int queryable, int opaque,
                   const char *version, const char *style_name,
                   const char *style_title, const char *style_abstract,
                   const char *format, const char *crs, int swap_xy,
                   double min_x, double max_x, double min_y, double max_y,
                   double geo_min_x, double geo_max_x, double geo_min_y,
                   double geo_max_y, double min_scale_denominator,
                   double max_scale_denominator)
{
//
// WMS Layer - constructor [ordinary WMS Layer]
//
  int len;
  TileLayer = NULL;
  Service = service;
  service->AddReference();
  if (name == NULL)
    {
      Name = new char[1];
      *Name = '\0';
  } else
    {
      len = strlen(name);
      Name = new char[len + 1];
      strcpy(Name, name);
    }
  Title = NULL;
  if (title != NULL)
    {
      len = strlen(title);
      Title = new char[len + 1];
      strcpy(Title, title);
    }
  Abstract = NULL;
  if (abstract != NULL)
    {
      len = strlen(abstract);
      Abstract = new char[len + 1];
      strcpy(Abstract, abstract);
    }
  Tiled = tiled;
  TileWidth = tile_width;
  TileHeight = tile_height;
  Queryable = queryable;
  Opaque = opaque;
  Version = NULL;
  if (version != NULL)
    {
      len = strlen(version);
      Version = new char[len + 1];
      strcpy(Version, version);
    }
  StyleName = NULL;
  if (style_name != NULL)
    {
      len = strlen(style_name);
      StyleName = new char[len + 1];
      strcpy(StyleName, style_name);
    }
  StyleTitle = NULL;
  if (style_title != NULL)
    {
      len = strlen(style_title);
      StyleTitle = new char[len + 1];
      strcpy(StyleTitle, style_title);
    }
  StyleAbstract = NULL;
  if (style_abstract != NULL)
    {
      len = strlen(style_abstract);
      StyleAbstract = new char[len + 1];
      strcpy(StyleAbstract, style_abstract);
    }
  Format = NULL;
  if (format != NULL)
    {
      len = strlen(format);
      Format = new char[len + 1];
      strcpy(Format, format);
    }
  CRS = NULL;
  if (crs != NULL)
    {
      len = strlen(crs);
      CRS = new char[len + 1];
      strcpy(CRS, crs);
    }
  SwapXY = swap_xy;
  MinX = min_x;
  MaxX = max_x;
  MinY = min_y;
  MaxY = max_y;
  GeoMinX = geo_min_x;
  GeoMaxX = geo_max_x;
  GeoMinY = geo_min_y;
  GeoMaxY = geo_max_y;
  MinScaleDenominator = min_scale_denominator;
  MaxScaleDenominator = max_scale_denominator;
  Visible = 1;
  Prev = NULL;
  Next = NULL;
  FirstStyle = NULL;
  LastStyle = NULL;
  FirstCRS = NULL;
  LastCRS = NULL;
  FirstChild = NULL;
  LastChild = NULL;
}

WmsLayer::WmsLayer(WmsService * service, const char *name, const char *title,
                   const char *abstract, double min_long, double min_lat,
                   double max_long, double max_lat, const char *pad,
                   const char *bands, const char *data_type)
{
//
// WMS Layer - constructor [special TileService Layer]
//
  TileLayer = new WmsTileServiceLayer(name, title, abstract, min_lat, min_long,
                                      max_lat, max_long, pad, bands, data_type);
  Service = service;
  service->AddReference();
  Name = NULL;
  Title = NULL;
  Abstract = NULL;
  Version = NULL;
  StyleName = NULL;
  StyleTitle = NULL;
  StyleAbstract = NULL;
  Format = NULL;
  CRS = NULL;
  Visible = 1;
  Prev = NULL;
  Next = NULL;
  FirstStyle = NULL;
  LastStyle = NULL;
  FirstCRS = NULL;
  LastCRS = NULL;
  Queryable = 0;

  FirstChild = NULL;
  LastChild = NULL;
}

WmsLayer::~WmsLayer()
{
//
// WMS Layer - destructor
//
  WmsStyle *pS;
  WmsStyle *pSn;
  WmsCRS *pC;
  WmsCRS *pCn;
  WmsQueryableChildLayer *pQ;
  WmsQueryableChildLayer *pQn;
  if (TileLayer != NULL)
    delete TileLayer;
  if (Name != NULL)
    delete[]Name;
  if (Title != NULL)
    delete[]Title;
  if (Abstract != NULL)
    delete[]Abstract;
  if (Version != NULL)
    delete[]Version;
  if (StyleName != NULL)
    delete[]StyleName;
  if (StyleTitle != NULL)
    delete[]StyleTitle;
  if (StyleAbstract != NULL)
    delete[]StyleAbstract;
  if (Format != NULL)
    delete[]Format;
  if (CRS != NULL)
    delete[]CRS;
  pS = FirstStyle;
  while (pS != NULL)
    {
      pSn = pS->GetNext();
      delete pS;
      pS = pSn;
    }
  pC = FirstCRS;
  while (pC != NULL)
    {
      pCn = pC->GetNext();
      delete pC;
      pC = pCn;
    }
  pQ = FirstChild;
  while (pQ != NULL)
    {
      pQn = pQ->GetNext();
      delete pQ;
      pQ = pQn;
    }
  Service->DeleteReference();
}

bool WmsLayer::ChangeCRS(MyFrame * MainFrame, const char *crs)
{
// attempting to change the CRS for a WmsLayer
  double minx;
  double miny;
  double maxx;
  double maxy;

  if (CRS != NULL)
    {
      if (strcmp(CRS, crs) == 0)
        {
          // unchanged
          return false;
        }
    }
// attempting to reproject the Geographic BBox into the CRS
  minx = GeoMinX;
  miny = GeoMinY;
  maxx = GeoMaxX;
  maxy = GeoMaxY;
  if (MainFrame->BBoxFromLongLat(crs, &minx, &maxx, &miny, &maxy) == false)
    {
      // invalid reprojection: leaving unchanged
      return false;
    }

  if (CRS != NULL)
    delete[]CRS;
  CRS = NULL;
  int len = strlen(crs);
  CRS = new char[len + 1];
  strcpy(CRS, crs);

// XY axes relative order
  bool check_axes = false;
  if (Service->GetVersion() != NULL)
    {
      if (strcmp(Service->GetVersion(), "1.3.0") == 0)
        check_axes = true;
    }
  if (check_axes == true)
    {
      if (MainFrame->IsSwapXYcrs(crs) == true)
        SwapXY = 1;
      else
        SwapXY = 0;
  } else
    SwapXY = 0;
  if (SwapXY == true)
    {
      MinY = minx;
      MaxY = maxx;
      MinX = miny;
      MaxX = maxy;
  } else
    {
      MinX = minx;
      MaxX = maxx;
      MinY = miny;
      MaxY = maxy;
    }
  return true;
}

void WmsLayer::Reconfigure(int tiled, int tile_width,
                           int tile_height, int opaque,
                           const char *version, const char *style_name,
                           const char *style_title, const char *style_abstract,
                           const char *format, const char *crs, int swap_xy,
                           double min_x, double max_x, double min_y,
                           double max_y)
{
//
// WMS Layer - reconfiguring
//
  int len;
  if (Version != NULL)
    delete[]Version;
  if (StyleName != NULL)
    delete[]StyleName;
  if (StyleTitle != NULL)
    delete[]StyleTitle;
  if (StyleAbstract != NULL)
    delete[]StyleAbstract;
  if (Format != NULL)
    delete[]Format;
  if (CRS != NULL)
    delete[]CRS;
  Tiled = tiled;
  TileWidth = tile_width;
  TileHeight = tile_height;
  Opaque = opaque;
  Version = NULL;
  if (version != NULL)
    {
      len = strlen(version);
      Version = new char[len + 1];
      strcpy(Version, version);
    }
  StyleName = NULL;
  if (style_name != NULL)
    {
      len = strlen(style_name);
      StyleName = new char[len + 1];
      strcpy(StyleName, style_name);
    }
  StyleTitle = NULL;
  if (style_title != NULL)
    {
      len = strlen(style_title);
      StyleTitle = new char[len + 1];
      strcpy(StyleTitle, style_title);
    }
  StyleAbstract = NULL;
  if (style_abstract != NULL)
    {
      len = strlen(style_abstract);
      StyleAbstract = new char[len + 1];
      strcpy(StyleAbstract, style_abstract);
    }
  Format = NULL;
  if (format != NULL)
    {
      len = strlen(format);
      Format = new char[len + 1];
      strcpy(Format, format);
    }
  CRS = NULL;
  if (crs != NULL)
    {
      len = strlen(crs);
      CRS = new char[len + 1];
      strcpy(CRS, crs);
    }
  SwapXY = swap_xy;
  MinX = min_x;
  MaxX = max_x;
  MinY = min_y;
  MaxY = max_y;
}

bool WmsLayer::StyleAlreadyDefined(const char *name, const char *title,
                                   const char *abstract)
{
//
// checks if a Style is already defined 
//
  WmsStyle *stl = FirstStyle;
  while (stl != NULL)
    {
      bool ok_name = false;
      bool ok_title = false;
      bool ok_abstract = false;
      if (stl->GetName() == NULL && name == NULL)
        ok_name = true;
      if (stl->GetName() != NULL && name != NULL)
        {
          if (strcmp(stl->GetName(), name) == 0)
            ok_name = true;
        }
      if (stl->GetTitle() == NULL && title == NULL)
        ok_title = true;
      if (stl->GetTitle() != NULL && title != NULL)
        {
          if (strcmp(stl->GetTitle(), title) == 0)
            ok_title = true;
        }
      if (stl->GetAbstract() == NULL && abstract == NULL)
        ok_abstract = true;
      if (stl->GetAbstract() != NULL && abstract != NULL)
        {
          if (strcmp(stl->GetAbstract(), abstract) == 0)
            ok_abstract = true;
        }
      if (ok_name && ok_title && ok_abstract)
        return true;
      stl = stl->GetNext();
    }
  return false;
}

void WmsLayer::AddStyle(const char *name, const char *title,
                        const char *abstract)
{
//
// adding a Style definition
//
  if (StyleAlreadyDefined(name, title, abstract))
    return;
  WmsStyle *stl = new WmsStyle(name, title, abstract);
  if (FirstStyle == NULL)
    FirstStyle = stl;
  if (LastStyle != NULL)
    LastStyle->SetNext(stl);
  LastStyle = stl;
}

int WmsLayer::CountStyles()
{
//
// counting how many Style definitions are there
//
  int count = 0;
  WmsStyle *pS = FirstStyle;
  while (pS != NULL)
    {
      count++;
      pS = pS->GetNext();
    }
  return count;
}

bool WmsLayer::GetStyleByIndex(int idx, const char **name, const char **title,
                               const char **abstract)
{
//
// return a Style as identified by its index
//
  int count = 0;
  WmsStyle *pS = FirstStyle;
  while (pS != NULL)
    {
      if (count == idx)
        {
          *name = pS->GetName();
          *title = pS->GetTitle();
          *abstract = pS->GetAbstract();
          return true;
        }
      count++;
      pS = pS->GetNext();
    }
  *name = NULL;
  *title = NULL;
  *abstract = NULL;
  return false;
}

bool WmsLayer::CrsAlreadyDefined(const char *str)
{
//
// checks if a CRS is already defined 
//
  WmsCRS *crs = FirstCRS;
  while (crs != NULL)
    {
      if (strcmp(crs->GetCRS(), str) == 0)
        return true;
      crs = crs->GetNext();
    }
  return false;
}

WmsCRS *WmsLayer::AddCRS(const char *name)
{
//
// adding a CRS definition
//
  if (CrsAlreadyDefined(name))
    return NULL;
  WmsCRS *crs = new WmsCRS(name);
  if (FirstCRS == NULL)
    FirstCRS = crs;
  if (LastCRS != NULL)
    LastCRS->SetNext(crs);
  LastCRS = crs;
  return crs;
}

int WmsLayer::CountCRS()
{
//
// counting how many CRS definitions are there
//
  int count = 0;
  WmsCRS *pC = FirstCRS;
  while (pC != NULL)
    {
      count++;
      pC = pC->GetNext();
    }
  return count;
}

const char *WmsLayer::GetCrsByIndex(int idx)
{
//
// return a CRS name as identified by its index
//
  int count = 0;
  WmsCRS *pC = FirstCRS;
  while (pC != NULL)
    {
      if (count == idx)
        return pC->GetCRS();
      count++;
      pC = pC->GetNext();
    }
  return NULL;
}

const char *WmsLayer::GetCRS()
{
//
// return the current Layer CRS
//
  if (TileLayer != NULL)
    {
      // TileService layer
      WmsTilePattern *pP = TileLayer->GetFirst();
      if (pP == NULL)
        return NULL;
      return pP->GetSRS();
    }
  return CRS;
}

bool WmsLayer::HasBBox(const char *crs)
{
//
// checks if a CRS really has a BBox
//
  WmsCRS *pC = FirstCRS;
  while (pC != NULL)
    {
      if (strcmp(pC->GetCRS(), crs) == 0)
        {
          if (pC->GetBBox() != NULL)
            return true;
          else
            return false;
        }
      pC = pC->GetNext();
    }
  return false;
}

void WmsLayer::GetBBox(const char *crs, double *minx, double *miny,
                       double *maxx, double *maxy)
{
//
// return a CRS BBox (if any)
//
  *minx = DBL_MAX;
  *miny = DBL_MAX;
  *maxx = DBL_MAX;
  *maxy = DBL_MAX;
  WmsCRS *pC = FirstCRS;
  while (pC != NULL)
    {
      if (strcmp(pC->GetCRS(), crs) == 0)
        {
          if (pC->GetBBox() != NULL)
            {
              *minx = pC->GetBBox()->GetMinX();
              *maxx = pC->GetBBox()->GetMaxX();
              *miny = pC->GetBBox()->GetMinY();
              *maxy = pC->GetBBox()->GetMaxY();
              return;
            }
        }
      pC = pC->GetNext();
    }
}

WmsLayer *WmsLayer::Clone()
{
//
// WMS Layer - Clone constructor
//
  WmsStyle *pS;
  WmsCRS *pC;
  WmsQueryableChildLayer *pQ;
  WmsLayer *cloned =
    new WmsLayer(this->Service, this->Name, this->Title, this->Abstract,
                 this->Tiled, this->TileWidth, this->TileHeight,
                 this->Queryable, this->Opaque, this->Version,
                 this->StyleName, this->StyleTitle, this->StyleAbstract,
                 this->Format, this->CRS, this->SwapXY,
                 this->MinX, this->MaxX, this->MinY, this->MaxY, this->GeoMinX,
                 this->GeoMaxX,
                 this->GeoMinY, this->GeoMaxY, this->MinScaleDenominator,
                 this->MaxScaleDenominator);
  if (this->TileLayer != NULL)
    cloned->TileLayer = this->TileLayer->Clone();
  pS = FirstStyle;
  while (pS != NULL)
    {
      cloned->AddStyle(pS->GetName(), pS->GetTitle(), pS->GetAbstract());
      pS = pS->GetNext();
    }
  pC = FirstCRS;
  while (pC != NULL)
    {
      cloned->AddCRS(pC->GetCRS());
      pC = pC->GetNext();
    }
  pQ = FirstChild;
  while (pQ != NULL)
    {
      cloned->AddQueryableChild(pQ->GetName(), pQ->GetTitle());
      pQ = pQ->GetNext();
    }
  return cloned;
}

bool WmsLayer::Equals(WmsLayer * other)
{
//
// checks if two WMS Layers are the same
//
  int valid = 0;
  if (this->Service == other->Service)
    valid++;
  if (this->Name == NULL && other->Name == NULL)
    valid++;
  else if (this->Name != NULL && other->Name != NULL)
    {
      if (strcmp(this->Name, other->Name) == 0)
        valid++;
    }
  if (this->Title == NULL && other->Title == NULL)
    valid++;
  else if (this->Title != NULL && other->Title != NULL)
    {
      if (strcmp(this->Title, other->Title) == 0)
        valid++;
    }
  if (this->Abstract == NULL && other->Abstract == NULL)
    valid++;
  else if (this->Abstract != NULL && other->Abstract != NULL)
    {
      if (strcmp(this->Abstract, other->Abstract) == 0)
        valid++;
    }
  if (valid == 4)
    {
      if (this->TileLayer != NULL && other->TileLayer != NULL)
        return this->TileLayer->Equals(other->TileLayer);
      return true;
    }
  return false;
}

bool WmsLayer::IsCovered(double minx, double miny, double maxx, double maxy)
{
//
// checks if a Layer is (partially) covered by a Bounding Box
//
  if (TileLayer != NULL)
    {
      // TileService Layer
      if (TileLayer->GetMinLong() > maxx)
        return false;
      if (TileLayer->GetMinLat() > maxy)
        return false;
      if (TileLayer->GetMaxLong() < minx)
        return false;
      if (TileLayer->GetMaxLat() < miny)
        return false;
  } else
    {
      // Ordinary Layer
      if (SwapXY == true)
        {
          if (MinY > maxx)
            return false;
          if (MinX > maxy)
            return false;
          if (MaxY < minx)
            return false;
          if (MaxX < miny)
            return false;
      } else
        {
          if (MinX > maxx)
            return false;
          if (MinY > maxy)
            return false;
          if (MaxX < minx)
            return false;
          if (MaxY < miny)
            return false;
        }
    }
  return true;
}

void WmsLayer::MaxLimits(double *maxx, double *maxy)
{
//
// checks the Max Limits
//
  if (TileLayer != NULL)
    {
      // TileService Layer
      if (*maxx > TileLayer->GetMaxLong())
        *maxx = TileLayer->GetMaxLong();
      if (*maxy > TileLayer->GetMaxLat())
        *maxy = TileLayer->GetMaxLat();
  } else
    {
      // Ordinary Layer
      if (SwapXY == true)
        {
          if (*maxy > MaxX)
            *maxy = MaxX;
          if (*maxx > MaxY)
            *maxx = MaxY;
      } else
        {
          if (*maxx > MaxX)
            *maxx = MaxX;
          if (*maxy > MaxY)
            *maxy = MaxY;
        }
    }
}

void WmsLayer::AddQueryableChild(const char *name, const char *title)
{
//
// adding a child queryable layer
//
  WmsQueryableChildLayer *child = new WmsQueryableChildLayer(name, title);
  if (FirstChild == NULL)
    FirstChild = child;
  if (LastChild != NULL)
    LastChild->SetNext(child);
  LastChild = child;
}

WmsQueryableChildLayer::WmsQueryableChildLayer(const char *name,
                                               const char *title)
{
// constructor
  int len;
  Name = NULL;
  if (name != NULL)
    {
      len = strlen(name);
      Name = new char[len + 1];
      strcpy(Name, name);
    }
  Title = NULL;
  if (title != NULL)
    {
      len = strlen(title);
      Title = new char[len + 1];
      strcpy(Title, title);
    }
  Next = NULL;
}

WmsQueryableChildLayer::~WmsQueryableChildLayer()
{
// destructor
  if (Name != NULL)
    delete[]Name;
  if (Title != NULL)
    delete[]Title;
}

WmsTileSet::WmsTileSet(double minx, double miny, double maxx, double maxy,
                       double base_x, double base_y, double tile_horz,
                       double tile_vert, int image_x, int image_y, int image_w,
                       int image_h, int tile_w, int tile_h)
{
//
// creating a WMS TileSet object
//
  FrameMinX = minx;
  FrameMinY = miny;
  FrameMaxX = maxx;
  FrameMaxY = maxy;
  BaseX = base_x;
  BaseY = base_y;
  TileHorzSize = tile_horz;
  TileVertSize = tile_vert;
  ImageOriginX = image_x;
  ImageOriginY = image_y;
  ImageWidth = image_w;
  ImageHeight = image_h;
  TileWidth = tile_w;
  TileHeight = tile_h;
  First = NULL;
  Last = NULL;
}

WmsTileSet::~WmsTileSet()
{
//
// memory cleanup - destroying a WMS TileSet object
//
  WmsTileRequest *pR;
  WmsTileRequest *pRn;
  pR = First;
  while (pR != NULL)
    {
      pRn = pR->GetNext();
      delete pR;
      pR = pRn;
    }
}

void WmsTileSet::AddTile(double pos_x, double pos_y, int row, int col)
{
//
// adding a tile into the tileSet
//
  double minx = pos_x;
  double miny = pos_y;
  double maxx = pos_x + TileHorzSize;
  double maxy = pos_y + TileVertSize;
  int image_x = ImageOriginX + (TileWidth * col);
  int image_y = ImageOriginY - (TileHeight * row) - TileHeight;
  WmsTileRequest *tile =
    new WmsTileRequest(row, col, minx, miny, maxx, maxy, image_x, image_y);
  if (First == NULL)
    First = tile;
  if (Last != NULL)
    Last->SetNext(tile);
  Last = tile;
}

WmsTileServiceLayer::WmsTileServiceLayer(const char *name, const char *title,
                                         const char *abstract, double min_lat,
                                         double min_long, double max_lat,
                                         double max_long, const char *pad,
                                         const char *bands,
                                         const char *data_type)
{
//
// WMS TileService Layer - constructor
//
  int len;
  if (name == NULL)
    {
      Name = new char[1];
      *Name = '\0';
  } else
    {
      len = strlen(name);
      Name = new char[len + 1];
      strcpy(Name, name);
    }
  Title = NULL;
  if (title != NULL)
    {
      len = strlen(title);
      Title = new char[len + 1];
      strcpy(Title, title);
    }
  Abstract = NULL;
  if (abstract != NULL)
    {
      len = strlen(abstract);
      Abstract = new char[len + 1];
      strcpy(Abstract, abstract);
    }
  MinLong = min_long;
  MinLat = min_lat;
  MaxLong = max_long;
  MaxLat = max_lat;
  Pad = NULL;
  if (pad != NULL)
    {
      len = strlen(pad);
      Pad = new char[len + 1];
      strcpy(Pad, pad);
    }
  Bands = NULL;
  if (bands != NULL)
    {
      len = strlen(bands);
      Bands = new char[len + 1];
      strcpy(Bands, bands);
    }
  DataType = NULL;
  if (data_type != NULL)
    {
      len = strlen(data_type);
      DataType = new char[len + 1];
      strcpy(DataType, data_type);
    }
  WmsFixedScalesCount = 0;
  WmsFixedScales = NULL;
  First = NULL;
  Last = NULL;
}

WmsTileServiceLayer::~WmsTileServiceLayer()
{
//
// WMS TileService Layer - destructor
//
  WmsTilePattern *pP;
  WmsTilePattern *pPn;
  if (Name != NULL)
    delete[]Name;
  if (Title != NULL)
    delete[]Title;
  if (Abstract != NULL)
    delete[]Abstract;
  if (Pad != NULL)
    delete[]Pad;
  if (Bands != NULL)
    delete[]Bands;
  if (DataType != NULL)
    delete[]DataType;
  pP = First;
  while (pP != NULL)
    {
      pPn = pP->GetNext();
      delete pP;
      pP = pPn;
    }
  if (WmsFixedScales != NULL)
    delete[]WmsFixedScales;
}

void WmsTileServiceLayer::BuildFixedScales()
{
// building the Fixed Scales array
  WmsTilePattern *pP;
  if (WmsFixedScales != NULL)
    delete[]WmsFixedScales;
  WmsFixedScales = NULL;
  WmsFixedScalesCount = 0;
  pP = First;
  while (pP != NULL)
    {
      // counting how many TilePatterns are there
      WmsFixedScalesCount++;
      pP = pP->GetNext();
    }
  if (WmsFixedScalesCount <= 0)
    return;
  WmsFixedScales = new double[WmsFixedScalesCount];
  int i = 0;
  pP = First;
  while (pP != NULL)
    {
      // computing Fixed Scales
      *(WmsFixedScales + i++) =
        pP->GetTileExtentX() / (double) (pP->GetTileWidth());
      pP = pP->GetNext();
    }
}

bool WmsTileServiceLayer::CompareFixedScales(WmsTileServiceLayer * other)
{
// checks if two TileService layers share the same fixed scales
  int i;
  if (this->WmsFixedScalesCount != other->WmsFixedScalesCount)
    return false;
  for (i = 0; i < this->WmsFixedScalesCount; i++)
    {
      if (*(this->WmsFixedScales + i) != *(other->WmsFixedScales + i))
        return false;
    }
  return true;
}

WmsTileServiceLayer *WmsTileServiceLayer::Clone()
{
//
// WMS TileService Layer - Clone constructor
//
  WmsTilePattern *pP;
  WmsTileServiceLayer *cloned =
    new WmsTileServiceLayer(this->Name, this->Title, this->Abstract,
                            this->MinLong, this->MinLat, this->MaxLong,
                            this->MaxLat, this->Pad, this->Bands,
                            this->DataType);
  pP = First;
  while (pP != NULL)
    {
      cloned->AddTilePattern(pP->GetHandle(), pP->GetSRS(), pP->GetTileWidth(),
                             pP->GetTileHeight(), pP->GetTileBaseX(),
                             pP->GetTileBaseY(), pP->GetTileExtentX(),
                             pP->GetTileExtentY());
      pP = pP->GetNext();
    }
  return cloned;
}

void WmsTileServiceLayer::AddTilePattern(rl2WmsTilePatternPtr handle,
                                         const char *srs, int width, int height,
                                         double base_x, double base_y,
                                         double ext_x, double ext_y)
{
// inserts a TilePattern into a TileService Layer
  WmsTilePattern *pattern =
    new WmsTilePattern(handle, srs, width, height, base_x, base_y, ext_x,
                       ext_y);
  if (First == NULL)
    First = pattern;
  if (Last != NULL)
    Last->SetNext(pattern);
  Last = pattern;
}

bool WmsTileServiceLayer::Equals(WmsTileServiceLayer * other)
{
//
// checks if two WMS TileService Layers are the same
//
  int valid = 0;
  if (this->Name == NULL && other->Name == NULL)
    valid++;
  else if (this->Name != NULL && other->Name != NULL)
    {
      if (strcmp(this->Name, other->Name) == 0)
        valid++;
    }
  if (this->Title == NULL && other->Title == NULL)
    valid++;
  else if (this->Title != NULL && other->Title != NULL)
    {
      if (strcmp(this->Title, other->Title) == 0)
        valid++;
    }
  if (this->Abstract == NULL && other->Abstract == NULL)
    valid++;
  else if (this->Abstract != NULL && other->Abstract != NULL)
    {
      if (strcmp(this->Abstract, other->Abstract) == 0)
        valid++;
    }
  if (valid == 3)
    return true;
  return false;
}

bool WmsTileServiceLayer::GetTileParams(double scale, double frame_minx,
                                        double frame_miny, int *tile_width,
                                        int *tile_height, double *tileW,
                                        double *tileH, double *baseX,
                                        double *baseY,
                                        WmsTilePattern ** tilepattern)
{
// computing Tile Params for a TileService layer request
  *tile_width = -1;
  *tile_height = -1;
  *tileW = DBL_MAX;
  *tileH = DBL_MAX;
  *baseX = DBL_MAX;
  *baseY = DBL_MAX;
  *tilepattern = NULL;
  WmsTilePattern *pP = First;
  while (pP != NULL)
    {
      double pix_scale = pP->GetTileExtentX() / (double) (pP->GetTileWidth());
      if (pix_scale == scale)
        {
          *tilepattern = pP;
          *tile_width = pP->GetTileWidth();
          *tile_height = pP->GetTileHeight();
          *tileW = pP->GetTileExtentX();
          *tileH = pP->GetTileExtentY();
          double x = pP->GetTileBaseX();
          double y = pP->GetTileBaseY();
          while (y > MinLat)
            y -= pP->GetTileExtentY();
          if (frame_minx > pP->GetTileBaseX())
            {
              while (x + pP->GetTileExtentX() < frame_minx)
                x += pP->GetTileExtentX();
          } else
            {
              while (x - pP->GetTileExtentX() > frame_minx)
                x -= pP->GetTileExtentX();
            }
          if (frame_miny > y)
            {
              while (y + pP->GetTileExtentY() < frame_miny)
                y += pP->GetTileExtentY();
          } else
            {
              while (y - pP->GetTileExtentY() > frame_miny)
                y -= pP->GetTileExtentY();
            }
          *baseX = x;
          *baseY = y;
          break;
        }
      pP = pP->GetNext();
    }
  if (*tile_width <= 0 || *tile_height <= 0)
    return false;
  if (*tileW == DBL_MAX || *tileH == DBL_MAX)
    return false;
  if (*tilepattern == NULL)
    return false;
  return true;
}

WmsTilePattern::WmsTilePattern(rl2WmsTilePatternPtr handle, const char *srs,
                               int width, int height, double base_x,
                               double base_y, double ext_x, double ext_y)
{
//
// WMS Tile Pattern - constructor
//
  int len = strlen(srs);
  SRS = new char[len + 1];
  strcpy(SRS, srs);
  Handle = clone_wms_tile_pattern(handle);
  TileWidth = width;
  TileHeight = height;
  TileBaseX = base_x;
  TileBaseY = base_y;
  TileExtentX = ext_x;
  TileExtentY = ext_y;
  Next = NULL;
}

WmsTilePattern::~WmsTilePattern()
{
//
// WMS Tile Pattern - destructor
//
  if (SRS != NULL)
    delete[]SRS;
  if (Handle != NULL)
    destroy_wms_tile_pattern(Handle);
}

void WmsTilePattern::GetSampleURL(wxString & url)
{
//
// return the sample URL corresponding to this TilePattern
//
  char *xurl = get_wms_tile_pattern_sample_url(Handle);
  url = wxString::FromUTF8(xurl);
  if (xurl != NULL)
    sqlite3_free(xurl);
}

void MyMapView::ResetGetMapRequests()
{
// cleaning the GetMap requests list
  WmsGetMapRequest *pR;
  WmsGetMapRequest *pRn;
  pR = FirstRequest;
  while (pR != NULL)
    {
      pRn = pR->GetNext();
      delete pR;
      pR = pRn;
    }
  FirstRequest = NULL;
  LastRequest = NULL;
}

void MyMapView::Consider()
{
// enabling the whole GetMap requests list
  WmsGetMapRequest *pR = FirstRequest;
  while (pR != NULL)
    {
      pR->Consider();
      pR = pR->GetNext();
    }
}

void MyMapView::Consider(const char *layer_name)
{
// enabling the GetMap requests list for a selected layer
  WmsGetMapRequest *pR = FirstRequest;
  while (pR != NULL)
    {
      if (strcmp(pR->GetLayerName(), layer_name) == 0)
        pR->Consider();
      pR = pR->GetNext();
    }
}

int MyMapView::CountGetMapRequests()
{
// counting how many pending GetMap requests are queued
  int count = 0;
  WmsGetMapRequest *pR = FirstRequest;
  while (pR != NULL)
    {
      count++;
      pR = pR->GetNext();
    }
  return count;
}

WmsCRScandidate::WmsCRScandidate(const char *crs)
{
// constructor
  int len = strlen(crs);
  CRS = new char[len + 1];
  strcpy(CRS, crs);
  Count = 1;
  Next = NULL;
}

WmsCRScandidate::~WmsCRScandidate()
{
// destructor
  if (CRS != NULL)
    delete[]CRS;
}

WmsCRScandidates::~WmsCRScandidates()
{
// destructor
  WmsCRScandidate *pC;
  WmsCRScandidate *pCn;
  pC = First;
  while (pC != NULL)
    {
      pCn = pC->GetNext();
      delete pC;
      pC = pCn;
    }
}

void WmsCRScandidates::AddCandidate(const char *crs)
{
// evaluating a new CRS candidate
  WmsCRScandidate *pC = First;
  while (pC != NULL)
    {
      if (strcmp(pC->GetCRS(), crs) == 0)
        {
          // already defined
          pC->Increment();
          return;
        }
      pC = pC->GetNext();
    }
// not yet defined
  pC = new WmsCRScandidate(crs);
  if (First == NULL)
    First = pC;
  if (Last != NULL)
    Last->SetNext(pC);
  Last = pC;
}

int MyMapView::GetSridFromCRS(const char *crs)
{
// attempting to return the current Map SRID
  int i;
  if (crs == NULL)
    return -1;
  i = strlen(crs) - 1;
  for (; i >= 0; i--)
    {
      if (*(crs + i) == ':')
        return atoi(crs + i + 1);
    }
  return -1;
}

void MyMapView::SetMapCRS(const char *crs)
{
// setting a new Map CRS
  bool changed = false;
  WmsLayer *lyr = FirstLayer;
  while (lyr != NULL)
    {
      if (lyr->ChangeCRS(MainFrame, crs) == true)
        changed = true;
      lyr = lyr->GetNext();
    }
  if (changed == true)
    SetFullExtent();
  PrepareMap();
}

void MyMapView::ResetCRSlist()
{
// resetting the CRS list to the initial empty state
  WmsCRS *pC;
  WmsCRS *pCn;
  pC = FirstCRS;
  while (pC != NULL)
    {
      pCn = pC->GetNext();
      delete pC;
      pC = pCn;
    }
  FirstCRS = NULL;
  LastCRS = NULL;
}

void MyMapView::BuildCRSlist()
{
// building a list of CRSes supported by any currently defined WMS layer
  WmsCRScandidate *pC;
  WmsCRScandidates cand;
  ResetCRSlist();
  WmsLayer *lyr = FirstLayer;
  while (lyr != NULL)
    {
      // testing all candidates
      if (lyr->GetTileServiceLayer() != NULL)
        {
          // TileService WMS layer
          cand.Increment();
          WmsTilePattern *pT = lyr->GetTileServiceLayer()->GetFirst();
          if (pT != NULL)
            cand.AddCandidate(pT->GetSRS());
      } else
        {
          // ordinary WMS layer
          cand.Increment();
          for (int s = 0; s < lyr->CountCRS(); s++)
            {
              const char *crs = lyr->GetCrsByIndex(s);
              cand.AddCandidate(crs);
        }}
      lyr = lyr->GetNext();
    }

// creating the list of common CRSes
  pC = cand.GetFirst();
  while (pC != NULL)
    {
      if (pC->GetCount() == cand.GetCount())
        {
          WmsCRS *crs = new WmsCRS(pC->GetCRS());
          if (FirstCRS == NULL)
            FirstCRS = crs;
          if (LastCRS != NULL)
            LastCRS->SetNext(crs);
          LastCRS = crs;
        }
      pC = pC->GetNext();
    }
}

void MyMapView::BuildFixedScales()
{
// building the Fixed Scales array
  if (WmsFixedScales != NULL)
    delete[]WmsFixedScales;
  WmsFixedScales = NULL;
  WmsFixedScalesCount = 0;
  WmsLayer *lyr = FirstLayer;
  while (lyr != NULL)
    {
      if (lyr->GetTileServiceLayer() != NULL)
        {
          // found a TileService layer
          int cnt = lyr->GetTileServiceLayer()->GetFixedScalesCount();
          if (cnt > 0)
            {
              // allocating and initializing the Map Fixed Scales array
              double *src = lyr->GetTileServiceLayer()->GetFixedScales();
              WmsFixedScalesCount = cnt;
              WmsFixedScales = new double[cnt];
              for (int i = 0; i < cnt; i++)
                *(WmsFixedScales + i) = *(src + i);
            }
          return;
        }
      lyr = lyr->GetNext();
    }
}

void MyMapView::AddGetMapRequest(int pos_x, int pos_y, const char *url,
                                 const char *version,
                                 const char *layer,
                                 const char *crs, int swap_xy,
                                 double minx, double miny,
                                 double maxx, double maxy,
                                 int width, int height,
                                 const char *style,
                                 const char *format, int opaque)
{
// appending a GetMap request into the list [Ordinary]
  rl2WmsCachePtr cache_handle = MainFrame->GetWmsCache();
  WmsGetMapRequest *req = new WmsGetMapRequest(pos_x, pos_y, cache_handle, url,
                                               Proxy, version, layer, crs,
                                               swap_xy, minx, miny,
                                               maxx, maxy, width, height, style,
                                               format, opaque, 1);
  if (FirstRequest == NULL)
    FirstRequest = req;
  if (LastRequest != NULL)
    LastRequest->SetNext(req);
  LastRequest = req;
}

void MyMapView::AddGetMapRequest(int pos_x, int pos_y, const char *url,
                                 const char *layer, int swap_xy,
                                 double minx, double miny,
                                 double maxx, double maxy, int width,
                                 int height, WmsTilePattern * tilepattern)
{
// appending a GetMap request into the list [TileService]
  rl2WmsCachePtr cache_handle = MainFrame->GetWmsCache();
  WmsGetMapRequest *req = new WmsGetMapRequest(pos_x, pos_y, cache_handle, url,
                                               Proxy, layer, swap_xy, minx,
                                               miny,
                                               maxx, maxy, width, height,
                                               tilepattern, 1);
  if (FirstRequest == NULL)
    FirstRequest = req;
  if (LastRequest != NULL)
    LastRequest->SetNext(req);
  LastRequest = req;
}

WmsGetMapRequest::WmsGetMapRequest(int pos_x, int pos_y,
                                   rl2WmsCachePtr cache_handle, const char *url,
                                   const char *proxy, const char *version,
                                   const char *layer, const char *crs,
                                   int swap_xy, double minx, double miny,
                                   double maxx, double maxy, int width,
                                   int height, const char *style,
                                   const char *format, int opaque,
                                   int with_image)
{
// constructor
  PosX = pos_x;
  PosY = pos_y;
  CacheHandle = cache_handle;
  Url = url;
  Proxy = proxy;
  Version = version;
  Layer = layer;
  Crs = crs;
  SwapXY = swap_xy;
  MinX = minx;
  MinY = miny;
  MaxX = maxx;
  MaxY = maxy;
  Width = width;
  Height = height;
  Style = style;
  Format = format;
  Opaque = opaque;
  TilePattern = NULL;
  CacheHit = false;
  if (with_image)
    {
      Image = wxImage(Width, Height);
      Image.SetAlpha();
    }
  RGBA = NULL;
  ImageReady = false;
  ImageDone = false;
  Ignore = true;
  Next = NULL;
}

WmsGetMapRequest::WmsGetMapRequest(int pos_x, int pos_y,
                                   rl2WmsCachePtr cache_handle, const char *url,
                                   const char *proxy, const char *layer,
                                   int swap_xy, double minx, double miny,
                                   double maxx, double maxy, int width,
                                   int height, WmsTilePattern * tilepattern,
                                   int with_image)
{
// constructor
  PosX = pos_x;
  PosY = pos_y;
  CacheHandle = cache_handle;
  Url = url;
  Proxy = proxy;
  Version = NULL;
  Layer = layer;
  Crs = NULL;
  SwapXY = swap_xy;
  MinX = minx;
  MinY = miny;
  MaxX = maxx;
  MaxY = maxy;
  Width = width;
  Height = height;
  Style = NULL;
  Format = NULL;
  TilePattern = tilepattern;
  CacheHit = false;
  if (with_image)
    {
      Image = wxImage(Width, Height);
      Image.SetAlpha();
    }
  RGBA = NULL;
  ImageReady = false;
  ImageDone = false;
  Ignore = true;
  Next = NULL;
}

void WmsGetMapRequest::Done()
{
// finalizing a tile request
  RGBA = NULL;
  ImageReady = false;
  ImageDone = true;
}

bool WmsGetMapRequest::ExecuteMap(int from_cache)
{
// executing a WMS GetMap request
  char *err_msg = NULL;
  unsigned char *rgba;
  bool ret = false;
  if (TilePattern != NULL)
    {
      char *url =
        get_wms_tile_pattern_request_url(TilePattern->GetHandle(), Url, MinX,
                                         MinY);
      if (url != NULL)
        {
          rgba =
            do_wms_GetMap_TileService_get(CacheHandle, url, Proxy, Width,
                                          Height, from_cache, &err_msg);
          sqlite3_free(url);
      } else
        rgba = NULL;
  } else
    rgba =
      do_wms_GetMap_get(CacheHandle, Url, Proxy, Version, Layer, Crs, SwapXY,
                        MinX, MinY, MaxX, MaxY,
                        Width, Height, Style, Format, Opaque, from_cache,
                        &err_msg);
  if (rgba == NULL)
    return false;
  if (from_cache)
    CacheHit = true;

  if (rgba != NULL)
    {
      // WMS tile
      unsigned char *p = rgba;
      for (int y = 0; y < Height; y++)
        {
          for (int x = 0; x < Width; x++)
            {
              unsigned char r = *p++;
              unsigned char g = *p++;
              unsigned char b = *p++;
              unsigned char alpha = *p++;
              Image.SetRGB(x, y, r, g, b);
              Image.SetAlpha(x, y, alpha);
            }
        }
      free(rgba);
      ImageReady = true;
      ret = true;
  } else
    {
      // default white tile
      for (int y = 0; y < Height; y++)
        {
          for (int x = 0; x < Width; x++)
            {
              Image.SetRGB(x, y, 255, 255, 255);
              if (Opaque == 0)
                Image.SetAlpha(x, y, 128);
              else
                Image.SetAlpha(x, y, 255);
            }
        }
      ret = false;
    }
  return ret;
}

bool WmsGetMapRequest::ExecutePrint(int from_cache)
{
// executing a WMS GetMap request
  char *err_msg = NULL;
  unsigned char *rgba;
  bool ret = false;
  if (TilePattern != NULL)
    {
      char *url =
        get_wms_tile_pattern_request_url(TilePattern->GetHandle(), Url, MinX,
                                         MinY);
      if (url != NULL)
        {
          rgba =
            do_wms_GetMap_TileService_get(CacheHandle, url, Proxy, Width,
                                          Height, from_cache, &err_msg);
          sqlite3_free(url);
      } else
        rgba = NULL;
  } else
    rgba =
      do_wms_GetMap_get(CacheHandle, Url, Proxy, Version, Layer, Crs, SwapXY,
                        MinX, MinY, MaxX, MaxY,
                        Width, Height, Style, Format, Opaque, from_cache,
                        &err_msg);
  if (rgba == NULL)
    return false;
  if (from_cache)
    CacheHit = true;

  if (rgba != NULL)
    {
      RGBA = rgba;
      ImageReady = true;
      ret = true;
  } else
    ret = false;
  return ret;
}

GeometryList::~GeometryList()
{
// destructor
  GeometryRef *pG;
  GeometryRef *pGn;
  pG = First;
  while (pG != NULL)
    {
      pGn = pG->GetNext();
      delete pG;
      pG = pGn;
    }
}

void GeometryList::Add(int layer_id, int line_no, int col_no,
                       gaiaGeomCollPtr geom)
{
// adding a Geometry ref into the list
  GeometryRef *ptr = new GeometryRef(layer_id, line_no, col_no, geom);
  if (First == NULL)
    First = ptr;
  if (Last != NULL)
    Last->SetNext(ptr);
  Last = ptr;
}

gaiaGeomCollPtr GeometryList::Find(int layer_id, int line_no, int col_no)
{
// retrieving some Geometry from the list

  GeometryRef *pG = First;
  while (pG != NULL)
    {
      if (pG->GetLayerId() == layer_id && pG->GetLineNo() == line_no
          && pG->GetColNo() == col_no)
        return pG->GetGeometry();
      pG = pG->GetNext();
    }
  return NULL;
}

void MyMapView::MarkGeometry(gaiaGeomCollPtr geom)
{
// starts the Dynamic Marker
  ResetMarker();
  BuildMarker(ImageMarkerOdd, true, geom);
  BuildMarker(ImageMarkerEven, false, geom);
  DynamicOddEven = true;
  DynamicPhase = 0;
  TimerDynamic = new wxTimer(this, ID_DYNAMIC_TIMER);
  TimerDynamic->Start(250, wxTIMER_ONE_SHOT);
}

void MyMapView::BuildMarker(wxImage & image, bool odd_even,
                            gaiaGeomCollPtr geom)
{
// building a Geometry Marker
  rl2GraphicsContextPtr ctx = rl2_graph_create_context(FrameWidth, FrameHeight);
// background initialization - full transparent
  rl2_graph_set_brush(ctx, 255, 255, 255, 0);
  rl2_graph_draw_rectangle(ctx, -1, -1, FrameWidth + 2, FrameHeight + 2);

// drawing the geometric entity
  int x;
  int y;
// setting standard graphic setting for Polygons
  if (odd_even == false)
    {
      rl2_graph_set_pen(ctx, 0, 0, 255, 255, 1, RL2_PENSTYLE_SOLID);
      rl2_graph_set_brush(ctx, 255, 255, 0, 128);
  } else
    {
      rl2_graph_set_pen(ctx, 255, 0, 0, 255, 1, RL2_PENSTYLE_SOLID);
      rl2_graph_set_brush(ctx, 0, 255, 255, 128);
    }
  gaiaPolygonPtr pg = geom->FirstPolygon;
  while (pg)
    {
      // drawing a POLYGON
      int iv;
      double dx;
      double dy;
      double z;
      double m;
      int x;
      int y;
      int lastX = 0;
      int lastY = 0;
      int ib;
      gaiaRingPtr ring = pg->Exterior;
      // exterior border
      for (iv = 0; iv < ring->Points; iv++)
        {
          if (ring->DimensionModel == GAIA_XY_Z)
            {
              gaiaGetPointXYZ(ring->Coords, iv, &dx, &dy, &z);
          } else if (ring->DimensionModel == GAIA_XY_M)
            {
              gaiaGetPointXYM(ring->Coords, iv, &dx, &dy, &m);
          } else if (ring->DimensionModel == GAIA_XY_Z_M)
            {
              gaiaGetPointXYZM(ring->Coords, iv, &dx, &dy, &z, &m);
          } else
            {
              gaiaGetPoint(ring->Coords, iv, &dx, &dy);
            }
          x = (int) ((dx - FrameMinX) / PixelRatio);
          y = FrameHeight - (int) ((dy - FrameMinY) / PixelRatio);
          if (iv == 0)
            {
              rl2_graph_move_to_point(ctx, x, y);
              lastX = x;
              lastY = y;
          } else
            {
              if (x == lastX && y == lastY)
                ;
              else
                {
                  rl2_graph_add_line_to_path(ctx, x, y);
                  lastX = x;
                  lastY = y;
                }
            }
        }
      rl2_graph_close_subpath(ctx);
      for (ib = 0; ib < pg->NumInteriors; ib++)
        {
          // interior borders
          ring = pg->Interiors + ib;
          for (iv = 0; iv < ring->Points; iv++)
            {
              if (ring->DimensionModel == GAIA_XY_Z)
                {
                  gaiaGetPointXYZ(ring->Coords, iv, &dx, &dy, &z);
              } else if (ring->DimensionModel == GAIA_XY_M)
                {
                  gaiaGetPointXYM(ring->Coords, iv, &dx, &dy, &m);
              } else if (ring->DimensionModel == GAIA_XY_Z_M)
                {
                  gaiaGetPointXYZM(ring->Coords, iv, &dx, &dy, &z, &m);
              } else
                {
                  gaiaGetPoint(ring->Coords, iv, &dx, &dy);
                }
              x = (int) ((dx - FrameMinX) / PixelRatio);
              y = FrameHeight - (int) ((dy - FrameMinY) / PixelRatio);
              if (iv == 0)
                {
                  rl2_graph_move_to_point(ctx, x, y);
                  lastX = x;
                  lastY = y;
              } else
                {
                  if (x == lastX && y == lastY)
                    ;
                  else
                    {
                      rl2_graph_add_line_to_path(ctx, x, y);
                      lastX = x;
                      lastY = y;
                    }
                }
            }
          rl2_graph_close_subpath(ctx);
        }
      rl2_graph_fill_path(ctx, 1);
      rl2_graph_stroke_path(ctx, 0);
      pg = pg->Next;
    }

// setting standard graphic setting for Linestrings
  if (odd_even == false)
    rl2_graph_set_pen(ctx, 0, 0, 255, 255, 4, RL2_PENSTYLE_SOLID);
  else
    rl2_graph_set_pen(ctx, 255, 0, 0, 255, 4, RL2_PENSTYLE_SOLID);
  gaiaLinestringPtr ln = geom->FirstLinestring;
  while (ln)
    {
      // drawing a LINESTRING
      int iv;
      double dx;
      double dy;
      double z;
      double m;
      int x;
      int y;
      int lastX = 0;
      int lastY = 0;
      for (iv = 0; iv < ln->Points; iv++)
        {
          if (ln->DimensionModel == GAIA_XY_Z)
            {
              gaiaGetPointXYZ(ln->Coords, iv, &dx, &dy, &z);
          } else if (ln->DimensionModel == GAIA_XY_M)
            {
              gaiaGetPointXYM(ln->Coords, iv, &dx, &dy, &m);
          } else if (ln->DimensionModel == GAIA_XY_Z_M)
            {
              gaiaGetPointXYZM(ln->Coords, iv, &dx, &dy, &z, &m);
          } else
            {
              gaiaGetPoint(ln->Coords, iv, &dx, &dy);
            }
          x = (int) ((dx - FrameMinX) / PixelRatio);
          y = FrameHeight - (int) ((dy - FrameMinY) / PixelRatio);
          if (iv == 0)
            {
              rl2_graph_move_to_point(ctx, x, y);
              lastX = x;
              lastY = y;
          } else
            {
              if (x == lastX && y == lastY)
                ;
              else
                {
                  rl2_graph_add_line_to_path(ctx, x, y);
                  lastX = x;
                  lastY = y;
                }
            }
        }
      rl2_graph_stroke_path(ctx, 0);
      ln = ln->Next;
    }

// setting standard graphic setting for Points
  if (odd_even == false)
    {
      rl2_graph_set_pen(ctx, 0, 0, 255, 255, 1, RL2_PENSTYLE_SOLID);
      rl2_graph_set_brush(ctx, 255, 255, 0, 128);
  } else
    {
      rl2_graph_set_pen(ctx, 255, 0, 0, 255, 1, RL2_PENSTYLE_SOLID);
      rl2_graph_set_brush(ctx, 0, 255, 255, 128);
    }
  gaiaPointPtr pt = geom->FirstPoint;
  while (pt)
    {
      // drawing a POINT
      x = (int) ((pt->X - FrameMinX) / PixelRatio);
      y = FrameHeight - (int) ((pt->Y - FrameMinY) / PixelRatio);
      rl2_graph_draw_ellipse(ctx, x - 10, y - 10, 20, 20);
      pt = pt->Next;
    }

// preparing the Marker image
  unsigned char *rgb = rl2_graph_get_context_rgb_array(ctx);
  unsigned char *alpha = rl2_graph_get_context_alpha_array(ctx);
  image = wxImage(FrameWidth, FrameHeight);
  image.SetData(rgb);
  image.SetAlpha(alpha);
  rl2_graph_destroy_context(ctx);
}

void MyMapView::ResetMarker()
{
// stops the Dynamic Marker
  if (TimerDynamic)
    {
      TimerDynamic->Stop();
      delete TimerDynamic;
    }
  TimerDynamic = NULL;
  ImageMarkerOdd.Destroy();
  ImageMarkerEven.Destroy();
  ResetScreenBitmap();
}
