/*
/ Printer.cpp
/ PDF / TIFF export modules
/
/ version 1.0, 2013 October 1
/
/ Author: Sandro Furieri a-furieri@lqt.it
/
/ Copyright (C) 2013  Alessandro Furieri
/
/    This program is free software: you can redistribute it and/or modify
/    it under the terms of the GNU General Public License as published by
/    the Free Software Foundation, either version 3 of the License, or
/    (at your option) any later version.
/
/    This program is distributed in the hope that it will be useful,
/    but WITHOUT ANY WARRANTY; without even the implied warranty of
/    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/    GNU General Public License for more details.
/
/    You should have received a copy of the GNU General Public License
/    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/
*/

#include "Classdef.h"

PdfPrinter::PdfPrinter(MyFrame * parent, const char *path, int dpi,
                       double page_w, double page_h, double horz_margin_sz,
                       double vert_margin_sz, bool preserve_scale,
                       bool preserve_width, bool preserve_height)
{
// constructor
  MainFrame = parent;
  MapView = parent->GetMapView();
  int len = strlen(path);
  Path = new char[len + 1];
  strcpy(Path, path);
  FrameWidth = (page_w - (horz_margin_sz * 2.0)) * (double) dpi;
  FrameHeight = (page_h - (vert_margin_sz * 2.0)) * (double) dpi;
  Ctx =
    rl2_graph_create_pdf_context(Path, dpi, page_w, page_h, horz_margin_sz,
                                 vert_margin_sz);
  if (preserve_scale == true)
    InitPreserveScale();
  if (preserve_width == true)
    InitPreserveWidth();
  if (preserve_height == true)
    InitPreserveHeight();
  ProgressControl = NULL;
  FirstRequest = NULL;
  LastRequest = NULL;
}

PdfPrinter::~PdfPrinter()
{
// destructor
  WmsGetMapRequest *pR;
  WmsGetMapRequest *pRn;
  pR = FirstRequest;
  while (pR != NULL)
    {
      pRn = pR->GetNext();
      delete pR;
      pR = pRn;
    }
  if (ProgressControl != NULL)
    delete ProgressControl;
  if (Path != NULL)
    delete[]Path;
  if (Ctx != NULL)
    rl2_graph_destroy_context(Ctx);
}

void PdfPrinter::InitPreserveScale()
{
// initializing the Map preserving the current Scale
  FrameCX = MapView->GetFrameCX();
  FrameCY = MapView->GetFrameCY();
  PixelRatio = MapView->GetPixelRatio();
  FrameExtentX = FrameWidth * PixelRatio;
  FrameExtentY = FrameHeight * PixelRatio;
  FrameMinX = FrameCX - (FrameExtentX / 2.0);
  FrameMinY = FrameCY - (FrameExtentY / 2.0);
  FrameMaxX = FrameCX + (FrameExtentX / 2.0);
  FrameMaxY = FrameCY + (FrameExtentY / 2.0);
}

void PdfPrinter::InitPreserveWidth()
{
// initializing the Map preserving the current Width
  FrameCX = MapView->GetFrameCX();
  FrameCY = MapView->GetFrameCY();
  double minx = MapView->GetFrameMinX();
  double maxx = MapView->GetFrameMaxX();
  double extx = maxx - minx;
// computing the current PixelRatio
  PixelRatio = extx / FrameWidth;
  if (MapView->IsWmsFixedScale())
    {
      // adjusting the current scale so to match a Fixed Scale
      PixelRatio = MapView->GetWmsBestResolution(PixelRatio);
    }
  FrameExtentX = FrameWidth * PixelRatio;
  FrameExtentY = FrameHeight * PixelRatio;
  FrameMinX = FrameCX - (FrameExtentX / 2.0);
  FrameMinY = FrameCY - (FrameExtentY / 2.0);
  FrameMaxX = FrameCX + (FrameExtentX / 2.0);
  FrameMaxY = FrameCY + (FrameExtentY / 2.0);
}

void PdfPrinter::InitPreserveHeight()
{
// initializing the Map preserving the current Height
  FrameCX = MapView->GetFrameCX();
  FrameCY = MapView->GetFrameCY();
  double miny = MapView->GetFrameMinY();
  double maxy = MapView->GetFrameMaxY();
  double exty = maxy - miny;
// computing the current PixelRatio
  PixelRatio = exty / FrameHeight;
  if (MapView->IsWmsFixedScale())
    {
      // adjusting the current scale so to match a Fixed Scale
      PixelRatio = MapView->GetWmsBestResolution(PixelRatio);
    }
  FrameExtentX = FrameWidth * PixelRatio;
  FrameExtentY = FrameHeight * PixelRatio;
  FrameMinX = FrameCX - (FrameExtentX / 2.0);
  FrameMinY = FrameCY - (FrameExtentY / 2.0);
  FrameMaxX = FrameCX + (FrameExtentX / 2.0);
  FrameMaxY = FrameCY + (FrameExtentY / 2.0);
}

void PdfPrinter::Go()
{
// preparing to print the PDF map
  WmsLayer *layer = MapView->GetLastLayer();
  while (layer != NULL)
    {
      if (layer->IsVisible()
          && layer->IsCovered(FrameMinX, FrameMinY, FrameMaxX, FrameMaxY))
        {
          double baseX;
          double baseY;
          double tileW = FrameMaxX - FrameMinX;
          double tileH = FrameMaxY - FrameMinY;
          int tile_width = FrameWidth;
          int tile_height = FrameHeight;
          WmsTilePattern *tilepattern = NULL;
          bool tileservice_layer = false;
          if (layer->GetTileServiceLayer() != NULL)
            {
              // TileService layer
              WmsTileServiceLayer *pTL = layer->GetTileServiceLayer();
              tileservice_layer = true;
              if (pTL->GetTileParams(PixelRatio, FrameMinX,
                                     FrameMinY,
                                     &tile_width,
                                     &tile_height, &tileW,
                                     &tileH, &baseX,
                                     &baseY, &tilepattern) == false)
                {
                  layer = layer->GetPrev();
                  continue;
                }
          } else
            {
              // ordinary WMS layer
              if (layer->IsTiled())
                {
                  tileW = (double) (layer->GetTileWidth()) * PixelRatio;
                  tileH = (double) (layer->GetTileHeight()) * PixelRatio;
                  tile_width = layer->GetTileWidth();
                  tile_height = layer->GetTileHeight();
                }
              if (layer->IsSwapXY())
                MapView->GetTileOrigin(layer->GetMinY(), layer->GetMinX(),
                                       FrameMinX, FrameMinY, tileW, tileH,
                                       &baseX, &baseY);
              else
                MapView->GetTileOrigin(layer->GetMinX(), layer->GetMinY(),
                                       FrameMinX, FrameMinY, tileW, tileH,
                                       &baseX, &baseY);
            }
          int image_x = (baseX - FrameMinX) / PixelRatio;
          int image_y = FrameHeight + ((FrameMinY - baseY) / PixelRatio);
          WmsTileSet *tileSet;
          tileSet =
            new WmsTileSet(FrameMinX, FrameMinY, FrameMaxX, FrameMaxY,
                           baseX, baseY, tileW, tileH, image_x, image_y,
                           FrameWidth, FrameHeight, tile_width, tile_height);

          // preparing the Tile Requests
          double pos_y = baseY;
          int row_no = 0;
          double lim_x = FrameMaxX;
          double lim_y = FrameMaxY;
          layer->MaxLimits(&lim_x, &lim_y);
          while (pos_y < lim_y)
            {
              // looping on tile-rows (bottom-up)
              double pos_x = baseX;
              int col_no = 0;
              while (pos_x < lim_x)
                {
                  // looping on tile-cols (left-right)
                  tileSet->AddTile(pos_x, pos_y, row_no, col_no);
                  pos_x += tileW;
                  col_no++;
                }
              pos_y += tileH;
              row_no++;
            }

          WmsTileRequest *tile = tileSet->GetFirst();
          while (tile != NULL)
            {
              // looping on tile requests   
              if (tileservice_layer == true)
                {
                  WmsTileServiceLayer *pTL = layer->GetTileServiceLayer();
                  AddGetMapRequest(tile->GetImageX(), tile->GetImageY(),
                                   layer->GetWmsService()->GetURL_GetMap_Get(),
                                   pTL->GetName(), layer->IsSwapXY(),
                                   tile->GetMinX(), tile->GetMinY(),
                                   tile->GetMaxX(), tile->GetMaxY(),
                                   tileSet->GetTileWidth(),
                                   tileSet->GetTileHeight(), tilepattern);
              } else
                {
                  AddGetMapRequest(tile->GetImageX(), tile->GetImageY(),
                                   layer->GetWmsService()->GetURL_GetMap_Get(),
                                   layer->GetVersion(), layer->GetName(),
                                   layer->GetCRS(), layer->IsSwapXY(),
                                   tile->GetMinX(), tile->GetMinY(),
                                   tile->GetMaxX(), tile->GetMaxY(),
                                   tileSet->GetTileWidth(),
                                   tileSet->GetTileHeight(),
                                   layer->GetStyleName(), layer->GetFormat(),
                                   layer->IsOpaque());
                }
              tile = tile->GetNext();
            }
          delete tileSet;
        }
      layer = layer->GetPrev();
    }

// creating the Progress control
  if (ProgressControl != NULL)
    {
      delete ProgressControl;
      ProgressControl = NULL;
    }
  wxRect rect;
  if (MainFrame->GetProgressRect(rect) == true)
    {
      wxPoint pt = wxPoint(rect.GetX(), rect.GetY());
      wxSize sz = wxSize(rect.GetWidth(), rect.GetHeight());
      ProgressControl =
        new wxGauge(MainFrame->GetStatusBar(), wxID_ANY, CountGetMapRequests(),
                    pt, sz);
    }
  MapView->SetPrintInProgress(this);
  MainFrame->UpdateWmsAbortTool(true);

#ifdef _WIN32
  HANDLE thread_handle;
  DWORD dwThreadId;
  thread_handle =
    CreateThread(NULL, 0, DoPdfDownloadTilesThread, this, 0, &dwThreadId);
  SetThreadPriority(thread_handle, THREAD_PRIORITY_IDLE);
#else
  pthread_t thread_id;
  int ok_prior = 0;
  int policy;
  int min_prio;
  pthread_attr_t attr;
  struct sched_param sp;
  pthread_attr_init(&attr);
  if (pthread_attr_setschedpolicy(&attr, SCHED_RR) == 0)
    {
      // attempting to set the lowest priority  
      if (pthread_attr_getschedpolicy(&attr, &policy) == 0)
        {
          min_prio = sched_get_priority_min(policy);
          sp.sched_priority = min_prio;
          if (pthread_attr_setschedparam(&attr, &sp) == 0)
            {
              // ok, setting the lowest priority  
              ok_prior = 1;
              pthread_create(&thread_id, &attr, DoPdfDownloadTilesThread, this);
            }
        }
    }
  if (!ok_prior)
    {
      // failure: using standard priority
      pthread_create(&thread_id, NULL, DoPdfDownloadTilesThread, this);
    }
#endif
}

#ifdef _WIN32
DWORD WINAPI DoPdfDownloadTilesThread(void *arg)
#else
void *DoPdfDownloadTilesThread(void *arg)
#endif
{
//
// threaded function: downloading WMS tiles
//
  PdfPrinter *Pdf = (PdfPrinter *) arg;
  MyMapView *MapView = Pdf->GetMapView();
  WmsLayer *layer = MapView->GetLastLayer();
  while (layer != NULL)
    {
      if (MapView->IsPendingWmsAbort() == true)
        break;
      if (layer->GetTileServiceLayer() != NULL)
        Pdf->Consider(layer->GetTileServiceLayer()->GetName());
      else
        Pdf->Consider(layer->GetName());
      WmsGetMapRequest *req = Pdf->GetFirstRequest();
      while (req != NULL)
        {
          // attempting first to display already cached tiles
          if (MapView->IsPendingWmsAbort() == true)
            break;
          if (req->IsTileService() == true
              && layer->GetTileServiceLayer() != NULL)
            {
              if (MapView->IsPendingWmsAbort() == true)
                break;
              if (strcmp
                  (layer->GetTileServiceLayer()->GetName(),
                   req->GetLayerName()) == 0)
                req->ExecutePrint(1);
            }
          if (req->IsTileService() == false
              && layer->GetTileServiceLayer() == NULL)
            {
              if (MapView->IsPendingWmsAbort() == true)
                break;
              if (strcmp(layer->GetName(), req->GetLayerName()) == 0)
                req->ExecutePrint(1);
            }
          req = req->GetNext();
        }
      for (int retry_count = 0; retry_count < 5; retry_count++)
        {
          bool retry = false;
          req = Pdf->GetFirstRequest();
          while (req != NULL)
            {
              // downloading any not already cached tile
              bool ret = true;
              if (MapView->IsPendingWmsAbort() == true)
                break;
              if (req->IsTileService() == true
                  && layer->GetTileServiceLayer() != NULL)
                {
                  if (MapView->IsPendingWmsAbort() == true)
                    break;
                  if (strcmp
                      (layer->GetTileServiceLayer()->GetName(),
                       req->GetLayerName()) == 0
                      && req->IsImageReady() == false)
                    ret = req->ExecutePrint(0);
                }
              if (req->IsTileService() == false
                  && layer->GetTileServiceLayer() == NULL)
                {
                  if (MapView->IsPendingWmsAbort() == true)
                    break;
                  if (strcmp(layer->GetName(), req->GetLayerName()) == 0
                      && req->IsImageReady() == false)
                    ret = req->ExecutePrint(0);
                }
              if (ret == false)
                retry = true;
              req = req->GetNext();
            }
          if (retry == false)
            break;
        }
      layer = layer->GetPrev();
    }
  wxCommandEvent event(wxEVT_COMMAND_BUTTON_CLICKED,
                       ID_PDF_WMS_THREAD_FINISHED);
  MapView->GetEventHandler()->AddPendingEvent(event);
#ifdef _WIN32
  return 0;
#else
  pthread_exit(NULL);
#endif
}

void PdfPrinter::AddGetMapRequest(int pos_x, int pos_y, const char *url,
                                  const char *version,
                                  const char *layer,
                                  const char *crs, int swap_xy,
                                  double minx, double miny,
                                  double maxx, double maxy,
                                  int width, int height,
                                  const char *style,
                                  const char *format, int opaque)
{
// appending a GetMap request into the list [Ordinary]
  rl2WmsCachePtr cache_handle = MainFrame->GetWmsCache();
  WmsGetMapRequest *req = new WmsGetMapRequest(pos_x, pos_y, cache_handle, url,
                                               MapView->GetProxy(), version,
                                               layer, crs, swap_xy, minx, miny,
                                               maxx, maxy, width, height, style,
                                               format, opaque, 0);
  if (FirstRequest == NULL)
    FirstRequest = req;
  if (LastRequest != NULL)
    LastRequest->SetNext(req);
  LastRequest = req;
}

void PdfPrinter::AddGetMapRequest(int pos_x, int pos_y, const char *url,
                                  const char *layer, int swap_xy,
                                  double minx, double miny,
                                  double maxx, double maxy, int width,
                                  int height, WmsTilePattern * tilepattern)
{
// appending a GetMap request into the list [TileService]
  rl2WmsCachePtr cache_handle = MainFrame->GetWmsCache();
  WmsGetMapRequest *req = new WmsGetMapRequest(pos_x, pos_y, cache_handle, url,
                                               MapView->GetProxy(), layer,
                                               swap_xy, minx, miny,
                                               maxx, maxy, width, height,
                                               tilepattern, 0);
  if (FirstRequest == NULL)
    FirstRequest = req;
  if (LastRequest != NULL)
    LastRequest->SetNext(req);
  LastRequest = req;
}

int PdfPrinter::CountGetMapRequests()
{
// counting how many pending GetMap requests are queued
  int count = 0;
  WmsGetMapRequest *pR = FirstRequest;
  while (pR != NULL)
    {
      count++;
      pR = pR->GetNext();
    }
  return count;
}

void PdfPrinter::Consider()
{
// enabling the whole GetMap requests list
  WmsGetMapRequest *pR = FirstRequest;
  while (pR != NULL)
    {
      pR->Consider();
      pR = pR->GetNext();
    }
}

void PdfPrinter::Consider(const char *layer_name)
{
// enabling the GetMap requests list for a selected layer
  WmsGetMapRequest *pR = FirstRequest;
  while (pR != NULL)
    {
      if (strcmp(pR->GetLayerName(), layer_name) == 0)
        pR->Consider();
      pR = pR->GetNext();
    }
}

void PdfPrinter::UpdatePendingTiles()
{
// updating any available tile on the PDF canvass
  int count = 0;
  WmsLayer *layer = MapView->GetLastLayer();
  while (layer != NULL)
    {
      WmsGetMapRequest *req = FirstRequest;
      while (req != NULL)
        {
          if (req->ToBeIgnored() == true)
            {
              req = req->GetNext();
              continue;
            }
          bool skip = true;
          if (req->IsTileService() == true
              && layer->GetTileServiceLayer() != NULL)
            {
              if (strcmp
                  (layer->GetTileServiceLayer()->GetName(),
                   req->GetLayerName()) == 0)
                skip = false;
            }
          if (req->IsTileService() == false
              && layer->GetTileServiceLayer() == NULL)
            {
              if (strcmp(layer->GetName(), req->GetLayerName()) == 0)
                skip = false;
            }
          if (skip == true)
            {
              req = req->GetNext();
              continue;
            }
          if (req->IsImageReady() == true && req->IsImageDone() == false
              && req->GetRGBA() != NULL)
            {
              PrintTile(req->GetRGBA(), req->GetPosX(), req->GetPosY(),
                        req->GetWidth(), req->GetHeight());
              req->Done();
            }
          if (req->IsImageDone() == true)
            count++;
          req = req->GetNext();
        }
      layer = layer->GetPrev();
    }
// updating the Progress control
  if (ProgressControl != NULL)
    {
      ProgressControl->Show();
      if (count == 0)
        ProgressControl->Pulse();
      else
        ProgressControl->SetValue(count);
    }
}

void PdfPrinter::PrintTile(unsigned char *rgba, int pos_x, int pos_y, int width,
                           int height)
{
// printing some WMS tile
  if (rgba == NULL)
    return;

// creating a Graphics Bitmap 
  rl2GraphicsBitmapPtr bmp = rl2_graph_create_bitmap(rgba, width, height);
  if (bmp == NULL)
    return;
// rendering the Bitmap 
  if (Ctx != NULL)
    rl2_graph_draw_bitmap(Ctx, bmp, pos_x, pos_y);
  rl2_graph_destroy_bitmap(bmp);
}

TiffWriter::TiffWriter(MyFrame * parent, const char *path, int width,
                       int height, bool geo_tiff, bool has_worldfile,
                       unsigned char compression, bool is_tiled,
                       int tile_size, bool preserve_scale,
                       bool preserve_width, bool preserve_height)
{
// constructor
  MainFrame = parent;
  MapView = parent->GetMapView();
  int len = strlen(path);
  Path = new char[len + 1];
  strcpy(Path, path);
  FrameWidth = width;
  FrameHeight = height;
  Pixels = (unsigned char *) malloc(width * height * 3);
  if (Pixels != NULL)
    memset(Pixels, 255, width * height * 3);
  TotPixels = 0;
  MonoPixels = 0;
  GrayPixels = 0;
  MaxPalette = 0;
  IsTiled = is_tiled;
  TileSize = tile_size;
  GeoTiff = geo_tiff;
  Worldfile = has_worldfile;
  Compression = compression;
  ColorSpace = RL2_PIXEL_RGB;
  if (preserve_scale == true)
    InitPreserveScale();
  if (preserve_width == true)
    InitPreserveWidth();
  if (preserve_height == true)
    InitPreserveHeight();
  ProgressControl = NULL;
  FirstRequest = NULL;
  LastRequest = NULL;
}

TiffWriter::~TiffWriter()
{
// destructor
  WmsGetMapRequest *pR;
  WmsGetMapRequest *pRn;
  if (Pixels != NULL)
    free(Pixels);
  pR = FirstRequest;
  while (pR != NULL)
    {
      pRn = pR->GetNext();
      delete pR;
      pR = pRn;
    }
  if (ProgressControl != NULL)
    delete ProgressControl;
  if (Path != NULL)
    delete[]Path;
}

void TiffWriter::InitPreserveScale()
{
// initializing the Map preserving the current Scale
  FrameCX = MapView->GetFrameCX();
  FrameCY = MapView->GetFrameCY();
  PixelRatio = MapView->GetPixelRatio();
  FrameExtentX = (double) FrameWidth *PixelRatio;
  FrameExtentY = (double) FrameHeight *PixelRatio;
  FrameMinX = FrameCX - (FrameExtentX / 2.0);
  FrameMinY = FrameCY - (FrameExtentY / 2.0);
  FrameMaxX = FrameCX + (FrameExtentX / 2.0);
  FrameMaxY = FrameCY + (FrameExtentY / 2.0);
}

void TiffWriter::InitPreserveWidth()
{
// initializing the Map preserving the current Width
  FrameCX = MapView->GetFrameCX();
  FrameCY = MapView->GetFrameCY();
  double minx = MapView->GetFrameMinX();
  double maxx = MapView->GetFrameMaxX();
  double extx = maxx - minx;
// computing the current PixelRatio
  PixelRatio = extx / (double) (FrameWidth);
  FrameExtentX = (double) FrameWidth *PixelRatio;
  FrameExtentY = (double) FrameHeight *PixelRatio;
  FrameMinX = FrameCX - (FrameExtentX / 2.0);
  FrameMinY = FrameCY - (FrameExtentY / 2.0);
  FrameMaxX = FrameCX + (FrameExtentX / 2.0);
  FrameMaxY = FrameCY + (FrameExtentY / 2.0);
  if (MapView->IsWmsFixedScale())
    {
      // adjusting the current scale so to match a Fixed Scale
      PixelRatio = MapView->GetWmsBestResolution(PixelRatio);
    }
}

void TiffWriter::InitPreserveHeight()
{
// initializing the Map preserving the current Height
  FrameCX = MapView->GetFrameCX();
  FrameCY = MapView->GetFrameCY();
  double miny = MapView->GetFrameMinY();
  double maxy = MapView->GetFrameMaxY();
  double exty = maxy - miny;
// computing the current PixelRatio
  PixelRatio = exty / (double) (FrameHeight);
  FrameExtentX = (double) FrameWidth *PixelRatio;
  FrameExtentY = (double) FrameHeight *PixelRatio;
  FrameMinX = FrameCX - (FrameExtentX / 2.0);
  FrameMinY = FrameCY - (FrameExtentY / 2.0);
  FrameMaxX = FrameCX + (FrameExtentX / 2.0);
  FrameMaxY = FrameCY + (FrameExtentY / 2.0);
  if (MapView->IsWmsFixedScale())
    {
      // adjusting the current scale so to match a Fixed Scale
      PixelRatio = MapView->GetWmsBestResolution(PixelRatio);
    }
}

int TiffWriter::GetSrid()
{
// returning the current SRID
  int srid = MapView->GetSridFromCRS(MapView->GetLastLayer()->GetCRS());
  if (srid == 84)
    srid = 4326;
  return srid;
}

void TiffWriter::Go()
{
// preparing to print the TIFF image
  WmsLayer *layer = MapView->GetLastLayer();
  while (layer != NULL)
    {
      if (layer->IsVisible()
          && layer->IsCovered(FrameMinX, FrameMinY, FrameMaxX, FrameMaxY))
        {
          double baseX;
          double baseY;
          double tileW = FrameMaxX - FrameMinX;
          double tileH = FrameMaxY - FrameMinY;
          int tile_width = FrameWidth;
          int tile_height = FrameHeight;
          WmsTilePattern *tilepattern = NULL;
          bool tileservice_layer = false;
          if (layer->GetTileServiceLayer() != NULL)
            {
              // TileService layer
              WmsTileServiceLayer *pTL = layer->GetTileServiceLayer();
              tileservice_layer = true;
              if (pTL->GetTileParams(PixelRatio, FrameMinX,
                                     FrameMinY,
                                     &tile_width,
                                     &tile_height, &tileW,
                                     &tileH, &baseX,
                                     &baseY, &tilepattern) == false)
                {
                  layer = layer->GetPrev();
                  continue;
                }
          } else
            {
              // ordinary WMS layer
              if (layer->IsTiled())
                {
                  tileW = (double) (layer->GetTileWidth()) * PixelRatio;
                  tileH = (double) (layer->GetTileHeight()) * PixelRatio;
                  tile_width = layer->GetTileWidth();
                  tile_height = layer->GetTileHeight();
                }
              if (layer->IsSwapXY())
                MapView->GetTileOrigin(layer->GetMinY(), layer->GetMinX(),
                                       FrameMinX, FrameMinY, tileW, tileH,
                                       &baseX, &baseY);
              else
                MapView->GetTileOrigin(layer->GetMinX(), layer->GetMinY(),
                                       FrameMinX, FrameMinY, tileW, tileH,
                                       &baseX, &baseY);
            }
          int image_x = (baseX - FrameMinX) / PixelRatio;
          int image_y = FrameHeight + ((FrameMinY - baseY) / PixelRatio);
          WmsTileSet *tileSet;
          tileSet =
            new WmsTileSet(FrameMinX, FrameMinY, FrameMaxX, FrameMaxY,
                           baseX, baseY, tileW, tileH, image_x, image_y,
                           FrameWidth, FrameHeight, tile_width, tile_height);

          // preparing the Tile Requests
          double pos_y = baseY;
          int row_no = 0;
          double lim_x = FrameMaxX;
          double lim_y = FrameMaxY;
          layer->MaxLimits(&lim_x, &lim_y);
          while (pos_y < lim_y)
            {
              // looping on tile-rows (bottom-up)
              double pos_x = baseX;
              int col_no = 0;
              while (pos_x < lim_x)
                {
                  // looping on tile-cols (left-right)
                  tileSet->AddTile(pos_x, pos_y, row_no, col_no);
                  pos_x += tileW;
                  col_no++;
                }
              pos_y += tileH;
              row_no++;
            }

          WmsTileRequest *tile = tileSet->GetFirst();
          while (tile != NULL)
            {
              // looping on tile requests   
              if (tileservice_layer == true)
                {
                  WmsTileServiceLayer *pTL = layer->GetTileServiceLayer();
                  AddGetMapRequest(tile->GetImageX(), tile->GetImageY(),
                                   layer->GetWmsService()->GetURL_GetMap_Get(),
                                   pTL->GetName(), layer->IsSwapXY(),
                                   tile->GetMinX(), tile->GetMinY(),
                                   tile->GetMaxX(), tile->GetMaxY(),
                                   tileSet->GetTileWidth(),
                                   tileSet->GetTileHeight(), tilepattern);
              } else
                {
                  AddGetMapRequest(tile->GetImageX(), tile->GetImageY(),
                                   layer->GetWmsService()->GetURL_GetMap_Get(),
                                   layer->GetVersion(), layer->GetName(),
                                   layer->GetCRS(), layer->IsSwapXY(),
                                   tile->GetMinX(), tile->GetMinY(),
                                   tile->GetMaxX(), tile->GetMaxY(),
                                   tileSet->GetTileWidth(),
                                   tileSet->GetTileHeight(),
                                   layer->GetStyleName(), layer->GetFormat(),
                                   layer->IsOpaque());
                }
              tile = tile->GetNext();
            }
          delete tileSet;
        }
      layer = layer->GetPrev();
    }

// creating the Progress control
  if (ProgressControl != NULL)
    {
      delete ProgressControl;
      ProgressControl = NULL;
    }
  wxRect rect;
  if (MainFrame->GetProgressRect(rect) == true)
    {
      wxPoint pt = wxPoint(rect.GetX(), rect.GetY());
      wxSize sz = wxSize(rect.GetWidth(), rect.GetHeight());
      ProgressControl =
        new wxGauge(MainFrame->GetStatusBar(), wxID_ANY, CountGetMapRequests(),
                    pt, sz);
    }
  MapView->SetPrintInProgress(this);
  MainFrame->UpdateWmsAbortTool(true);

#ifdef _WIN32
  HANDLE thread_handle;
  DWORD dwThreadId;
  thread_handle =
    CreateThread(NULL, 0, DoTiffDownloadTilesThread, this, 0, &dwThreadId);
  SetThreadPriority(thread_handle, THREAD_PRIORITY_IDLE);
#else
  pthread_t thread_id;
  int ok_prior = 0;
  int policy;
  int min_prio;
  pthread_attr_t attr;
  struct sched_param sp;
  pthread_attr_init(&attr);
  if (pthread_attr_setschedpolicy(&attr, SCHED_RR) == 0)
    {
      // attempting to set the lowest priority  
      if (pthread_attr_getschedpolicy(&attr, &policy) == 0)
        {
          min_prio = sched_get_priority_min(policy);
          sp.sched_priority = min_prio;
          if (pthread_attr_setschedparam(&attr, &sp) == 0)
            {
              // ok, setting the lowest priority  
              ok_prior = 1;
              pthread_create(&thread_id, &attr, DoTiffDownloadTilesThread,
                             this);
            }
        }
    }
  if (!ok_prior)
    {
      // failure: using standard priority
      pthread_create(&thread_id, NULL, DoTiffDownloadTilesThread, this);
    }
#endif
}

#ifdef _WIN32
DWORD WINAPI DoTiffDownloadTilesThread(void *arg)
#else
void *DoTiffDownloadTilesThread(void *arg)
#endif
{
//
// threaded function: downloading WMS tiles
//
  TiffWriter *Tiff = (TiffWriter *) arg;
  MyMapView *MapView = Tiff->GetMapView();
  WmsLayer *layer = MapView->GetLastLayer();
  while (layer != NULL)
    {
      if (MapView->IsPendingWmsAbort() == true)
        break;
      if (layer->GetTileServiceLayer() != NULL)
        Tiff->Consider(layer->GetTileServiceLayer()->GetName());
      else
        Tiff->Consider(layer->GetName());
      WmsGetMapRequest *req = Tiff->GetFirstRequest();
      while (req != NULL)
        {
          // attempting first to display already cached tiles
          if (MapView->IsPendingWmsAbort() == true)
            break;
          if (req->IsTileService() == true
              && layer->GetTileServiceLayer() != NULL)
            {
              if (MapView->IsPendingWmsAbort() == true)
                break;
              if (strcmp
                  (layer->GetTileServiceLayer()->GetName(),
                   req->GetLayerName()) == 0)
                req->ExecutePrint(1);
            }
          if (req->IsTileService() == false
              && layer->GetTileServiceLayer() == NULL)
            {
              if (MapView->IsPendingWmsAbort() == true)
                break;
              if (strcmp(layer->GetName(), req->GetLayerName()) == 0)
                req->ExecutePrint(1);
            }
          req = req->GetNext();
        }
      for (int retry_count = 0; retry_count < 5; retry_count++)
        {
          bool retry = false;
          req = Tiff->GetFirstRequest();
          while (req != NULL)
            {
              // downloading any not already cached tile
              bool ret = true;
              if (MapView->IsPendingWmsAbort() == true)
                break;
              if (req->IsTileService() == true
                  && layer->GetTileServiceLayer() != NULL)
                {
                  if (MapView->IsPendingWmsAbort() == true)
                    break;
                  if (strcmp
                      (layer->GetTileServiceLayer()->GetName(),
                       req->GetLayerName()) == 0
                      && req->IsImageReady() == false)
                    ret = req->ExecutePrint(0);
                }
              if (req->IsTileService() == false
                  && layer->GetTileServiceLayer() == NULL)
                {
                  if (MapView->IsPendingWmsAbort() == true)
                    break;
                  if (strcmp(layer->GetName(), req->GetLayerName()) == 0
                      && req->IsImageReady() == false)
                    ret = req->ExecutePrint(0);
                }
              if (ret == false)
                retry = true;
              req = req->GetNext();
            }
          if (retry == false)
            break;
        }
      layer = layer->GetPrev();
    }
  wxCommandEvent event(wxEVT_COMMAND_BUTTON_CLICKED,
                       ID_TIFF_WMS_THREAD_FINISHED);
  MapView->GetEventHandler()->AddPendingEvent(event);
#ifdef _WIN32
  return 0;
#else
  pthread_exit(NULL);
#endif
}

void TiffWriter::AddGetMapRequest(int pos_x, int pos_y, const char *url,
                                  const char *version,
                                  const char *layer,
                                  const char *crs, int swap_xy,
                                  double minx, double miny,
                                  double maxx, double maxy,
                                  int width, int height,
                                  const char *style,
                                  const char *format, int opaque)
{
// appending a GetMap request into the list [Ordinary]
  rl2WmsCachePtr cache_handle = MainFrame->GetWmsCache();
  WmsGetMapRequest *req = new WmsGetMapRequest(pos_x, pos_y, cache_handle, url,
                                               MapView->GetProxy(), version,
                                               layer, crs, swap_xy, minx, miny,
                                               maxx, maxy, width, height, style,
                                               format, opaque, 0);
  if (FirstRequest == NULL)
    FirstRequest = req;
  if (LastRequest != NULL)
    LastRequest->SetNext(req);
  LastRequest = req;
}

void TiffWriter::AddGetMapRequest(int pos_x, int pos_y, const char *url,
                                  const char *layer, int swap_xy,
                                  double minx, double miny,
                                  double maxx, double maxy, int width,
                                  int height, WmsTilePattern * tilepattern)
{
// appending a GetMap request into the list [TileService]
  rl2WmsCachePtr cache_handle = MainFrame->GetWmsCache();
  WmsGetMapRequest *req = new WmsGetMapRequest(pos_x, pos_y, cache_handle, url,
                                               MapView->GetProxy(), layer,
                                               swap_xy, minx, miny,
                                               maxx, maxy, width, height,
                                               tilepattern, 0);
  if (FirstRequest == NULL)
    FirstRequest = req;
  if (LastRequest != NULL)
    LastRequest->SetNext(req);
  LastRequest = req;
}

int TiffWriter::CountGetMapRequests()
{
// counting how many pending GetMap requests are queued
  int count = 0;
  WmsGetMapRequest *pR = FirstRequest;
  while (pR != NULL)
    {
      count++;
      pR = pR->GetNext();
    }
  return count;
}

void TiffWriter::Consider()
{
// enabling the whole GetMap requests list
  WmsGetMapRequest *pR = FirstRequest;
  while (pR != NULL)
    {
      pR->Consider();
      pR = pR->GetNext();
    }
}

void TiffWriter::Consider(const char *layer_name)
{
// enabling the GetMap requests list for a selected layer
  WmsGetMapRequest *pR = FirstRequest;
  while (pR != NULL)
    {
      if (strcmp(pR->GetLayerName(), layer_name) == 0)
        pR->Consider();
      pR = pR->GetNext();
    }
}

void TiffWriter::UpdatePendingTiles()
{
// updating any available tile on the TIFF canvass
  int count = 0;
  WmsLayer *layer = MapView->GetLastLayer();
  while (layer != NULL)
    {
      WmsGetMapRequest *req = FirstRequest;
      while (req != NULL)
        {
          if (req->ToBeIgnored() == true)
            {
              req = req->GetNext();
              continue;
            }
          bool skip = true;
          if (req->IsTileService() == true
              && layer->GetTileServiceLayer() != NULL)
            {
              if (strcmp
                  (layer->GetTileServiceLayer()->GetName(),
                   req->GetLayerName()) == 0)
                skip = false;
            }
          if (req->IsTileService() == false
              && layer->GetTileServiceLayer() == NULL)
            {
              if (strcmp(layer->GetName(), req->GetLayerName()) == 0)
                skip = false;
            }
          if (skip == true)
            {
              req = req->GetNext();
              continue;
            }
          if (req->IsImageReady() == true && req->IsImageDone() == false
              && req->GetRGBA() != NULL)
            {
              PrintTile(req->GetRGBA(), req->GetPosX(), req->GetPosY(),
                        req->GetWidth(), req->GetHeight());
              req->Done();
            }
          if (req->IsImageDone() == true)
            count++;
          req = req->GetNext();
        }
      layer = layer->GetPrev();
    }
// updating the Progress control
  if (ProgressControl != NULL)
    {
      ProgressControl->Show();
      if (count == 0)
        ProgressControl->Pulse();
      else
        ProgressControl->SetValue(count);
    }
}

void TiffWriter::PrintTile(unsigned char *rgba, int pos_x, int pos_y, int width,
                           int height)
{
// printing some WMS tile
  if (rgba == NULL)
    return;

  for (int y = 0; y < height; y++)
    {
      int img_y = pos_y + y;
      unsigned char *p_in = rgba + (y * width * 4);
      unsigned char *p_line = Pixels + (img_y * (FrameWidth * 3));
      if (img_y < 0 || img_y >= FrameHeight)
        continue;
      for (int x = 0; x < width; x++)
        {
          int img_x = pos_x + x;
          unsigned char *p_out = p_line + (img_x * 3);
          unsigned char r = *p_in++;
          unsigned char g = *p_in++;
          unsigned char b = *p_in++;
          unsigned char alpha = *p_in++;
          if (alpha < 192)
            {
              /* skipping mostly transparent pixels */
              continue;
            }
          if (img_x < 0 || img_x > FrameWidth)
            continue;
          *p_out++ = r;
          *p_out++ = g;
          *p_out++ = b;
          TotPixels++;
          if (r == g && r == b)
            {
              /* Gray pixel */
              GrayPixels++;
            }
          if ((r == 0 && g == 0 && b == 0)
              || (r == 255 && g == 255 && b == 255))
            {
              /* Monochrome pixel */
              MonoPixels++;
            }
          if (MaxPalette <= 256)
            {
              /* testing for Palette colorspace */
              int ok = 0;
              for (int i = 0; i < MaxPalette; i++)
                {
                  if (Red[i] == r && Green[i] == g && Blue[i] == b)
                    {
                      /* matches an already defined entry */
                      ok = 1;
                      break;
                    }
                }
              if (!ok)
                {
                  if (MaxPalette == 256)
                    {
                      /* surely not a Palette - invalidating */
                      MaxPalette = 257;
                  } else
                    {
                      /* inserting a new Palette entry */
                      Red[MaxPalette] = r;
                      Green[MaxPalette] = g;
                      Blue[MaxPalette] = b;
                      MaxPalette++;
                    }
                }
            }
        }
    }
}

rl2PalettePtr TiffWriter::BuildPalette()
{
// building a Palette
  rl2PalettePtr palette = rl2_create_palette(MaxPalette);
  for (int i = 0; i < MaxPalette; i++)
    rl2_set_palette_color(palette, i, Red[i], Green[i], Blue[i]);
  return palette;
}

unsigned char TiffWriter::FindPaletteIndex(unsigned char red,
                                           unsigned char green,
                                           unsigned char blue)
{
// retrieving a Palette index matching an RGB color
  int index;
  for (index = 0; index < MaxPalette; index++)
    {
      if (Red[index] == red && Green[index] == green && Blue[index] == blue)
        return index;
    }
  return 0;
}

void TiffWriter::ExportImage()
{
// exporting the TIFF Image
  rl2TiffDestinationPtr tiff;
  int n_samples = 1;
  unsigned char sample = RL2_SAMPLE_UINT8;
  unsigned char *bufpix;
  int bufpix_size;
  rl2PalettePtr palette;
  rl2PalettePtr palette2;
  int x;
  int y;
  const unsigned char *p_in;
  unsigned char *p_out;

  if (ColorSpace == RL2_PIXEL_PALETTE)
    palette = BuildPalette();
  else
    palette = NULL;
  if (ColorSpace == RL2_PIXEL_RGB)
    n_samples = 3;
  if (ColorSpace == RL2_PIXEL_MONOCHROME)
    sample = RL2_SAMPLE_1_BIT;

// creating the TIFF destination
  if (GeoTiff == true)
    tiff =
      rl2_create_geotiff_destination(Path, MainFrame->GetSQLite(), FrameWidth,
                                     FrameHeight, sample, ColorSpace, n_samples,
                                     palette, Compression,
                                     (IsTiled == true) ? 1 : 0, TileSize,
                                     GetSrid(), FrameMinX, FrameMinY, FrameMaxX,
                                     FrameMaxY, PixelRatio, PixelRatio,
                                     (Worldfile == true) ? 1 : 0);
  else if (Worldfile == true)
    tiff =
      rl2_create_tiff_worldfile_destination(Path, FrameWidth, FrameHeight,
                                            sample, ColorSpace, n_samples,
                                            palette, Compression,
                                            (IsTiled == true) ? 1 : 0, TileSize,
                                            GetSrid(), FrameMinX, FrameMinY,
                                            FrameMaxX, FrameMaxY, PixelRatio,
                                            PixelRatio);
  else
    tiff =
      rl2_create_tiff_destination(Path, FrameWidth, FrameHeight, sample,
                                  ColorSpace, n_samples, palette, Compression,
                                  (IsTiled == true) ? 1 : 0, TileSize);
  if (tiff == NULL)
    goto error;

  if (IsTiled)
    {
      // Tiled TIFF
      for (int row = 0; row < FrameHeight; row += TileSize)
        {
          for (int col = 0; col < FrameWidth; col += TileSize)
            {
              // inserting a TIFF tile
              if (ColorSpace == RL2_PIXEL_PALETTE)
                palette2 = BuildPalette();
              else
                palette2 = NULL;
              bufpix_size = TileSize * TileSize * n_samples;
              bufpix = (unsigned char *) malloc(bufpix_size);
              p_out = bufpix;
              for (y = 0; y < TileSize; y++)
                {
                  if (row + y >= FrameHeight)
                    {
                      for (x = 0; x < TileSize; x++)
                        {
                          *p_out++ = 0;
                          *p_out++ = 0;
                          *p_out++ = 0;
                        }
                    }
                  p_in = Pixels + ((row + y) * FrameWidth * 3) + (col * 3);
                  for (x = 0; x < TileSize; x++)
                    {
                      if (ColorSpace == RL2_PIXEL_RGB)
                        {
                          if (col + x >= FrameWidth)
                            {
                              *p_out++ = 0;
                              *p_out++ = 0;
                              *p_out++ = 0;
                              continue;
                            }
                          *p_out++ = *p_in++;
                          *p_out++ = *p_in++;
                          *p_out++ = *p_in++;
                      } else if (ColorSpace == RL2_PIXEL_GRAYSCALE)
                        {
                          if (col + x >= FrameWidth)
                            {
                              *p_out++ = 0;
                              continue;
                            }
                          *p_out++ = *p_in++;
                          p_in += 2;
                      } else if (ColorSpace == RL2_PIXEL_PALETTE)
                        {
                          if (col + x >= FrameWidth)
                            {
                              *p_out++ = 0;
                              continue;
                            }
                          unsigned char r = *p_in++;
                          unsigned char g = *p_in++;
                          unsigned char b = *p_in++;
                          unsigned char idx = FindPaletteIndex(r, g, b);
                          *p_out++ = idx;
                      } else if (ColorSpace == RL2_PIXEL_MONOCHROME)
                        {
                          if (col + x >= FrameWidth)
                            {
                              *p_out++ = 0;
                              continue;
                            }
                          if (*p_in++ < 128)
                            *p_out++ = 1;
                          else
                            *p_out++ = 0;
                          p_in += 2;
                        }
                    }
                }
              rl2RasterPtr raster = rl2_create_raster(TileSize, TileSize,
                                                      sample, ColorSpace,
                                                      n_samples,
                                                      bufpix, bufpix_size,
                                                      palette2, NULL, 0, NULL);
              if (raster == NULL)
                goto error;
              if (rl2_write_tiff_tile(tiff, raster, row, col) != RL2_OK)
                {
                  rl2_destroy_raster(raster);
                  goto error;
                }
              rl2_destroy_raster(raster);
            }
        }
  } else
    {
      // Striped TIFF
      for (int row = 0; row < FrameHeight; row++)
        {
          // inserting a TIFF strip
          if (ColorSpace == RL2_PIXEL_PALETTE)
            palette2 = BuildPalette();
          else
            palette2 = NULL;
          bufpix_size = FrameWidth * n_samples;
          bufpix = (unsigned char *) malloc(bufpix_size);
          p_in = Pixels + (row * FrameWidth * 3);
          p_out = bufpix;
          for (x = 0; x < FrameWidth; x++)
            {
              /* feeding the scanline buffer */
              if (ColorSpace == RL2_PIXEL_RGB)
                {
                  *p_out++ = *p_in++;
                  *p_out++ = *p_in++;
                  *p_out++ = *p_in++;
              } else if (ColorSpace == RL2_PIXEL_GRAYSCALE)
                {
                  *p_out++ = *p_in++;
                  p_in += 2;
              } else if (ColorSpace == RL2_PIXEL_PALETTE)
                {
                  unsigned char r = *p_in++;
                  unsigned char g = *p_in++;
                  unsigned char b = *p_in++;
                  unsigned char idx = FindPaletteIndex(r, g, b);
                  *p_out++ = idx;
              } else if (ColorSpace == RL2_PIXEL_MONOCHROME)
                {
                  if (*p_in++ < 128)
                    *p_out++ = 1;
                  else
                    *p_out++ = 0;
                  p_in += 2;
                }
            }
          rl2RasterPtr raster = rl2_create_raster(FrameWidth, 1,
                                                  sample, ColorSpace, n_samples,
                                                  bufpix, bufpix_size, palette2,
                                                  NULL, 0, NULL);
          if (raster == NULL)
            goto error;
          if (rl2_write_tiff_scanline(tiff, raster, row) != RL2_OK)
            {
              rl2_destroy_raster(raster);
              goto error;
            }
          rl2_destroy_raster(raster);
        }
    }

  if (Worldfile == true)
    rl2_write_tiff_worldfile(tiff);

// destroying the TIFF destination
  rl2_destroy_tiff_destination(tiff);
  if (palette != NULL)
    rl2_destroy_palette(palette);
  return;
error:
  fprintf(stderr, "Unable to export an image: \"%s\"\n", Path);
  if (tiff != NULL)
    rl2_destroy_tiff_destination(tiff);
  if (palette != NULL)
    rl2_destroy_palette(palette);
}
