/*
/ Dialogs.cpp
/ misc dialogs
/
/ version 1.0, 2013 July 28
/
/ Author: Sandro Furieri a-furieri@lqt.it
/
/ Copyright (C) 2013  Alessandro Furieri
/
/    This program is free software: you can redistribute it and/or modify
/    it under the terms of the GNU General Public License as published by
/    the Free Software Foundation, either version 3 of the License, or
/    (at your option) any later version.
/
/    This program is distributed in the hope that it will be useful,
/    but WITHOUT ANY WARRANTY; without even the implied warranty of
/    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/    GNU General Public License for more details.
/
/    You should have received a copy of the GNU General Public License
/    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/
*/

#include <float.h>

#include "Classdef.h"

#include "wx/clipbrd.h"

bool PredefinedWmsDialog::Create(MyFrame * parent, sqlite3 * sqlite)
{
//
// creating the dialog
//
  MainFrame = parent;
  SQLiteHandle = sqlite;
  CurrentEvtRow = -1;
  CurrentEvtColumn = -1;
  if (wxDialog::Create(parent, wxID_ANY,
                       wxT("Select a pre-defined WMS datasource")) == false)
    return false;
// populates individual controls
  CreateControls();
// sets dialog sizer
  GetSizer()->Fit(this);
  GetSizer()->SetSizeHints(this);
// centers the dialog window
  Centre();
  return true;
}

void PredefinedWmsDialog::CreateControls()
{
//
// creating individual control and setting initial values
//
  wxBoxSizer *topSizer = new wxBoxSizer(wxVERTICAL);
  this->SetSizer(topSizer);
  wxBoxSizer *boxSizer = new wxBoxSizer(wxVERTICAL);
  topSizer->Add(boxSizer, 0, wxALIGN_CENTER_HORIZONTAL | wxALL, 5);
// filters group box
  wxBoxSizer *filterSizer = new wxBoxSizer(wxHORIZONTAL);
  boxSizer->Add(filterSizer, 0, wxALIGN_TOP | wxALL, 5);
  wxStaticBox *filterBox = new wxStaticBox(this, wxID_STATIC,
                                           wxT("Filters"),
                                           wxDefaultPosition,
                                           wxDefaultSize);
  wxBoxSizer *filterBoxSizer = new wxStaticBoxSizer(filterBox, wxHORIZONTAL);
  filterSizer->Add(filterBoxSizer, 0, wxALIGN_CENTER_HORIZONTAL | wxALL, 0);
// Filter by Country
  wxStaticBox *countryFilterBox = new wxStaticBox(this, wxID_STATIC,
                                                  wxT("by Country"),
                                                  wxDefaultPosition,
                                                  wxDefaultSize);
  wxBoxSizer *countryFilterBoxSizer =
    new wxStaticBoxSizer(countryFilterBox, wxHORIZONTAL);
  filterBoxSizer->Add(countryFilterBoxSizer, 0,
                      wxALIGN_CENTER_HORIZONTAL | wxALL, 0);
  CountryList =
    new wxComboBox(this, ID_FILTER_COUNTRY, wxT(""), wxDefaultPosition,
                   wxSize(150, 21), 0, NULL, wxCB_DROPDOWN | wxCB_READONLY);
  PopulateCountries();
  countryFilterBoxSizer->Add(CountryList, 0, wxALIGN_CENTER_HORIZONTAL | wxALL,
                             0);
// Filter by State or Province
  filterBoxSizer->AddSpacer(10);
  wxStaticBox *stateFilterBox = new wxStaticBox(this, wxID_STATIC,
                                                wxT("by State or Province"),
                                                wxDefaultPosition,
                                                wxDefaultSize);
  wxBoxSizer *stateFilterBoxSizer =
    new wxStaticBoxSizer(stateFilterBox, wxHORIZONTAL);
  filterBoxSizer->Add(stateFilterBoxSizer, 0, wxALIGN_CENTER_HORIZONTAL | wxALL,
                      0);
  StateList =
    new wxComboBox(this, ID_FILTER_STATE, wxT(""), wxDefaultPosition,
                   wxSize(150, 21), 0, NULL, wxCB_DROPDOWN | wxCB_READONLY);
  PopulateStates();
  stateFilterBoxSizer->Add(StateList, 0, wxALIGN_CENTER_HORIZONTAL | wxALL, 0);
// Filter by Category
  filterBoxSizer->AddSpacer(10);
  wxStaticBox *categFilterBox = new wxStaticBox(this, wxID_STATIC,
                                                wxT("by Category"),
                                                wxDefaultPosition,
                                                wxDefaultSize);
  wxBoxSizer *categFilterBoxSizer =
    new wxStaticBoxSizer(categFilterBox, wxHORIZONTAL);
  filterBoxSizer->Add(categFilterBoxSizer, 0, wxALIGN_CENTER_HORIZONTAL | wxALL,
                      0);
  CategoryList =
    new wxComboBox(this, ID_FILTER_CATEGORY, wxT(""), wxDefaultPosition,
                   wxSize(150, 21), 0, NULL, wxCB_DROPDOWN | wxCB_READONLY);
  PopulateCategories();
  categFilterBoxSizer->Add(CategoryList, 0, wxALIGN_CENTER_HORIZONTAL | wxALL,
                           0);
// the pre-defined datasources Grid
  wxBoxSizer *gridSizer = new wxBoxSizer(wxHORIZONTAL);
  boxSizer->Add(gridSizer, 0, wxALIGN_CENTER_HORIZONTAL | wxALL, 0);
  WmsView = new wxGrid(this, wxID_ANY, wxPoint(5, 5), wxSize(700, 300));
  WmsView->CreateGrid(1, 4);
  WmsView->EnableEditing(false);
  WmsView->SetColLabelValue(0, wxT("Country"));
  WmsView->SetColLabelValue(1, wxT("State or Province"));
  WmsView->SetColLabelValue(2, wxT("Category"));
  WmsView->SetColLabelValue(3, wxT("WMS GetCapabilities URL"));
  gridSizer->Add(WmsView, 0, wxALIGN_RIGHT | wxALL, 5);
  PopulateGrid();
// selected WMS datasource group box
  wxBoxSizer *kkSizer = new wxBoxSizer(wxHORIZONTAL);
  boxSizer->Add(kkSizer, 0, wxALIGN_TOP | wxALL, 5);
  wxStaticBox *dsBox = new wxStaticBox(this, wxID_STATIC,
                                       wxT("Selected WMS Datasource"),
                                       wxDefaultPosition,
                                       wxDefaultSize);
  wxBoxSizer *dsBox0Sizer = new wxStaticBoxSizer(dsBox, wxHORIZONTAL);
  kkSizer->Add(dsBox0Sizer, 0, wxALIGN_CENTER_HORIZONTAL | wxALL, 0);
// Second row: datasource info
  wxBoxSizer *dsBoxSizer = new wxBoxSizer(wxVERTICAL);
  dsBox0Sizer->Add(dsBoxSizer, 0, wxALIGN_CENTER_HORIZONTAL | wxALL, 0);
  wxBoxSizer *countrySizer = new wxBoxSizer(wxHORIZONTAL);
  dsBoxSizer->Add(countrySizer, 0, wxALIGN_CENTER_HORIZONTAL | wxALL, 0);
  wxStaticText *countryLabel =
    new wxStaticText(this, wxID_STATIC, wxT("&Country:"));
  countrySizer->Add(countryLabel, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);
  wxTextCtrl *countryCtrl = new wxTextCtrl(this, ID_WMS_COUNTRY, wxT(""),
                                           wxDefaultPosition, wxSize(150, 22),
                                           wxTE_READONLY);
  countrySizer->Add(countryCtrl, 0, wxALIGN_CENTER_VERTICAL | wxALL, 0);
  countrySizer->AddSpacer(10);
  wxStaticText *stateLabel =
    new wxStaticText(this, wxID_STATIC, wxT("&State or Province:"));
  countrySizer->Add(stateLabel, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);
  wxTextCtrl *stateCtrl = new wxTextCtrl(this, ID_WMS_STATE, wxT(""),
                                         wxDefaultPosition, wxSize(150, 22),
                                         wxTE_READONLY);
  countrySizer->Add(stateCtrl, 0, wxALIGN_CENTER_VERTICAL | wxALL, 0);
  countrySizer->AddSpacer(10);
  wxStaticText *ctgLabel =
    new wxStaticText(this, wxID_STATIC, wxT("&Category:"));
  countrySizer->Add(ctgLabel, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);
  wxTextCtrl *ctgCtrl = new wxTextCtrl(this, ID_WMS_CATEGORY, wxT(""),
                                       wxDefaultPosition, wxSize(150, 22),
                                       wxTE_READONLY);
  countrySizer->Add(ctgCtrl, 0, wxALIGN_CENTER_VERTICAL | wxALL, 0);
  wxBoxSizer *urlSizer = new wxBoxSizer(wxHORIZONTAL);
  dsBoxSizer->Add(urlSizer, 0, wxALIGN_CENTER_HORIZONTAL | wxALL, 0);
  wxTextCtrl *urlCtrl = new wxTextCtrl(this, ID_WMS_URL, wxT(""),
                                       wxDefaultPosition, wxSize(700, 22),
                                       wxTE_READONLY);
  urlSizer->Add(urlCtrl, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);
// OK-Cancel buttons
  wxBoxSizer *btnSizer = new wxBoxSizer(wxHORIZONTAL);
  dsBoxSizer->Add(btnSizer, 0, wxALIGN_CENTER_HORIZONTAL | wxALL, 0);
  wxButton *okBtn = new wxButton(this, wxID_OK, wxT("&OK"));
  okBtn->Enable(false);
  btnSizer->Add(okBtn, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);
  wxButton *cancelBtn = new wxButton(this, wxID_CANCEL, wxT("&Cancel"));
  btnSizer->Add(cancelBtn, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);
// appends event handlers
  Connect(wxID_ANY, wxEVT_GRID_CELL_LEFT_CLICK,
          (wxObjectEventFunction) & PredefinedWmsDialog::OnLeftClick);
  Connect(wxID_ANY, wxEVT_GRID_CELL_RIGHT_CLICK,
          (wxObjectEventFunction) & PredefinedWmsDialog::OnRightClick);
  Connect(wxID_OK, wxEVT_COMMAND_BUTTON_CLICKED,
          (wxObjectEventFunction) & PredefinedWmsDialog::OnOk);
  Connect(wxID_CANCEL, wxEVT_COMMAND_BUTTON_CLICKED,
          (wxObjectEventFunction) & PredefinedWmsDialog::OnCancel);
  Connect(Wms_Copy, wxEVT_COMMAND_MENU_SELECTED,
          (wxObjectEventFunction) & PredefinedWmsDialog::OnCmdCopy);
  Connect(Wms_Datasource, wxEVT_COMMAND_MENU_SELECTED,
          (wxObjectEventFunction) &
          PredefinedWmsDialog::OnCmdSelectWmsDatasource);
  Connect(ID_FILTER_COUNTRY, wxEVT_COMMAND_COMBOBOX_SELECTED,
          (wxObjectEventFunction) & PredefinedWmsDialog::OnCountrySelected);
  Connect(ID_FILTER_STATE, wxEVT_COMMAND_COMBOBOX_SELECTED,
          (wxObjectEventFunction) & PredefinedWmsDialog::OnStateSelected);
  Connect(ID_FILTER_CATEGORY, wxEVT_COMMAND_COMBOBOX_SELECTED,
          (wxObjectEventFunction) & PredefinedWmsDialog::OnCategorySelected);
}

void PredefinedWmsDialog::PopulateCountries()
{
//
// populating the Countries list
//
  int ret;
  const char *sql;
  char **results;
  int rows;
  int columns;
  CountryList->Clear();
  CountryList->Append(wxT(""));

  sql = "SELECT DISTINCT country FROM wms_list ORDER BY country";
  ret = sqlite3_get_table(SQLiteHandle, sql, &results, &rows, &columns, NULL);
  if (ret != SQLITE_OK)
    goto skip;

  for (int i = 1; i <= rows; i++)
    {
      // populating the list
      wxString country = wxString::FromUTF8(results[(i * columns) + 0]);
      CountryList->Append(country);
    }
  sqlite3_free_table(results);
skip:
  CountryList->SetSelection(0);
}

void PredefinedWmsDialog::PopulateStates()
{
//
// populating the States list
//
  int ret;
  char *sql;
  char *filter = NULL;
  char **results;
  int rows;
  int columns;
  StateList->Clear();
  StateList->Append(wxT(""));

// preparing the filter clause
  if (CountryFilter != NULL)
    filter = sqlite3_mprintf("country = %Q", CountryFilter);

// preparing the SQL query
  if (filter == NULL)
    sql =
      sqlite3_mprintf
      ("SELECT DISTINCT state_province FROM wms_list ORDER BY state_province");
  else
    {
      sql = sqlite3_mprintf("SELECT DISTINCT state_province  "
                            "FROM wms_list WHERE %s ORDER BY state_province",
                            filter);
      sqlite3_free(filter);
    }
  ret = sqlite3_get_table(SQLiteHandle, sql, &results, &rows, &columns, NULL);
  sqlite3_free(sql);
  if (ret != SQLITE_OK)
    goto skip;

  for (int i = 1; i <= rows; i++)
    {
      // populating the list
      wxString country = wxString::FromUTF8(results[(i * columns) + 0]);
      StateList->Append(country);
    }
  sqlite3_free_table(results);
skip:
  StateList->SetSelection(0);
  if (StateFilter != NULL)
    delete[]StateFilter;
  StateFilter = NULL;
}

void PredefinedWmsDialog::PopulateCategories()
{
//
// populating the Categories list
//
  int ret;
  char *sql;
  char *filter = NULL;
  char **results;
  int rows;
  int columns;
  CategoryList->Clear();
  CategoryList->Append(wxT(""));

// preparing the filter clause
  if (CountryFilter != NULL && StateFilter != NULL)
    filter =
      sqlite3_mprintf("country = %Q AND state_province = %Q", CountryFilter,
                      StateFilter);
  else if (CountryFilter != NULL)
    filter = sqlite3_mprintf("country = %Q", CountryFilter);
  else if (StateFilter != NULL)
    filter = sqlite3_mprintf("state_province = %Q", StateFilter);

// preparing the SQL query
  if (filter == NULL)
    sql =
      sqlite3_mprintf
      ("SELECT DISTINCT category FROM wms_list ORDER BY category");
  else
    {
      sql = sqlite3_mprintf("SELECT DISTINCT category  "
                            "FROM wms_list WHERE %s ORDER BY category", filter);
      sqlite3_free(filter);
    }
  ret = sqlite3_get_table(SQLiteHandle, sql, &results, &rows, &columns, NULL);
  sqlite3_free(sql);
  if (ret != SQLITE_OK)
    goto skip;

  for (int i = 1; i <= rows; i++)
    {
      // populating the list
      wxString country = wxString::FromUTF8(results[(i * columns) + 0]);
      CategoryList->Append(country);
    }
  sqlite3_free_table(results);
skip:
  CategoryList->SetSelection(0);
  if (CategoryFilter != NULL)
    delete[]CategoryFilter;
  CategoryFilter = NULL;
}

void PredefinedWmsDialog::PopulateGrid()
{
//
// populating the Grid
//
  int ret;
  char *sql;
  char *filter = NULL;
  char **results;
  int rows;
  int columns;

  WmsView->ClearGrid();
  WmsView->Show(false);
  WmsView->ClearSelection();
  CurrentEvtRow = -1;
  CurrentEvtColumn = -1;
  int nr = WmsView->GetNumberRows();
  WmsView->DeleteRows(0, nr);

// preparing the filter clause
  if (CountryFilter != NULL && StateFilter != NULL && CategoryFilter != NULL)
    filter =
      sqlite3_mprintf("country = %Q AND state_province = %Q AND category = %Q",
                      CountryFilter, StateFilter, CategoryFilter);
  else if (CountryFilter != NULL && StateFilter != NULL)
    filter =
      sqlite3_mprintf("country = %Q AND state_province = %Q", CountryFilter,
                      StateFilter);
  else if (StateFilter != NULL && CategoryFilter != NULL)
    filter =
      sqlite3_mprintf("state_province = %Q AND category = %Q", StateFilter,
                      CategoryFilter);
  else if (CountryFilter != NULL && CategoryFilter != NULL)
    filter =
      sqlite3_mprintf("country = %Q AND category = %Q", CountryFilter,
                      CategoryFilter);
  else if (CountryFilter != NULL)
    filter = sqlite3_mprintf("country = %Q", CountryFilter);
  else if (StateFilter != NULL)
    filter = sqlite3_mprintf("state_province = %Q", StateFilter);
  else if (CategoryFilter != NULL)
    filter = sqlite3_mprintf("category = %Q", CategoryFilter);

// preparing the SQL query
  if (filter == NULL)
    sql = sqlite3_mprintf("SELECT country, state_province, category, url "
                          "FROM wms_list ORDER BY country, state_province, category");
  else
    {
      sql = sqlite3_mprintf("SELECT country, state_province, category, url "
                            "FROM wms_list WHERE %s "
                            "ORDER BY country, state_province, category",
                            filter);
      sqlite3_free(filter);
    }
  ret = sqlite3_get_table(SQLiteHandle, sql, &results, &rows, &columns, NULL);
  sqlite3_free(sql);
  if (ret != SQLITE_OK)
    goto skip;

  if (rows >= 1)
    WmsView->AppendRows(rows);
  for (int i = 1; i <= rows; i++)
    {
      // populating the WMS List
      wxString country = wxString::FromUTF8(results[(i * columns) + 0]);
      wxString state = wxString::FromUTF8(results[(i * columns) + 1]);
      wxString category = wxString::FromUTF8(results[(i * columns) + 2]);
      wxString url = wxString::FromUTF8(results[(i * columns) + 3]);
      WmsView->SetCellValue(i - 1, 0, country);
      WmsView->SetCellValue(i - 1, 1, state);
      WmsView->SetCellValue(i - 1, 2, category);
      WmsView->SetCellValue(i - 1, 3, url);
    }
  sqlite3_free_table(results);

skip:
  WmsView->SetRowLabelSize(wxGRID_AUTOSIZE);
  WmsView->AutoSize();
  WmsView->SetSize(700, 300);
  WmsView->Show(true);
}

void PredefinedWmsDialog::OnCountrySelected(wxCommandEvent & WXUNUSED(event))
{
//
// Country Filter selection changed
//
  wxString filter = CountryList->GetValue();
  if (filter.Len() == 0)
    {
      if (CountryFilter != NULL)
        delete[]CountryFilter;
      CountryFilter = NULL;
  } else
    {
      if (CountryFilter != NULL)
        delete[]CountryFilter;
      CountryFilter = new char[filter.Len() + 1];
      strcpy(CountryFilter, filter.ToUTF8());
    }
  PopulateStates();
  PopulateCategories();
  PopulateGrid();
}

void PredefinedWmsDialog::OnStateSelected(wxCommandEvent & WXUNUSED(event))
{
//
// State Filter selection changed
//
  wxString filter = StateList->GetValue();
  if (filter.Len() == 0)
    {
      if (StateFilter != NULL)
        delete[]StateFilter;
      StateFilter = NULL;
  } else
    {
      if (StateFilter != NULL)
        delete[]StateFilter;
      StateFilter = new char[filter.Len() + 1];
      strcpy(StateFilter, filter.ToUTF8());
    }
  PopulateCategories();
  PopulateGrid();
}

void PredefinedWmsDialog::OnCategorySelected(wxCommandEvent & WXUNUSED(event))
{
//
// Category Filter selection changed
//
  wxString filter = CategoryList->GetValue();
  if (filter.Len() == 0)
    {
      if (CategoryFilter != NULL)
        delete[]CategoryFilter;
      CategoryFilter = NULL;
  } else
    {
      if (CategoryFilter != NULL)
        delete[]CategoryFilter;
      CategoryFilter = new char[filter.Len() + 1];
      strcpy(CategoryFilter, filter.ToUTF8());
    }
  PopulateGrid();
}

void PredefinedWmsDialog::OnLeftClick(wxGridEvent & event)
{
//
// left click on some cell [mouse action]
//
  int previous = CurrentEvtRow;
  CurrentEvtRow = event.GetRow();
  CurrentEvtColumn = event.GetCol();
  if (CurrentEvtRow != previous)
    SelectWmsDatasource();
}

void PredefinedWmsDialog::OnRightClick(wxGridEvent & event)
{
//
// right click on some cell [mouse action]
//
  wxMenu *menu = new wxMenu();
  wxMenuItem *menuItem;
  wxPoint pt = event.GetPosition();
  CurrentEvtRow = event.GetRow();
  CurrentEvtColumn = event.GetCol();
  menuItem =
    new wxMenuItem(menu, Wms_Datasource,
                   wxT("Select as the current WMS &Datasource"));
  menu->Append(menuItem);
  menu->AppendSeparator();
  menuItem =
    new wxMenuItem(menu, Wms_Copy, wxT("&Copy the whole pre-defined WMS List"));
  menu->Append(menuItem);
  WmsView->PopupMenu(menu, pt);
}

void PredefinedWmsDialog::OnCmdCopy(wxCommandEvent & WXUNUSED(event))
{
//
// copying all WMS Datasources definitions into the clipboard
//
  wxString copyData;
  int row;
  int col;
  for (row = 0; row < WmsView->GetNumberRows(); row++)
    {
      for (col = 0; col < WmsView->GetNumberCols(); col++)
        {
          if (col != 0)
            copyData += wxT("\t");
          copyData += WmsView->GetCellValue(row, col);
        }
      copyData += wxT("\n");
    }
  if (wxTheClipboard->Open())
    {
      wxTheClipboard->SetData(new wxTextDataObject(copyData));
      wxTheClipboard->Close();
    }
}

void PredefinedWmsDialog::
OnCmdSelectWmsDatasource(wxCommandEvent & WXUNUSED(event))
{
//
// setting the currently selected WMS Layer
//
  SelectWmsDatasource();
}

void PredefinedWmsDialog::SelectWmsDatasource()
{
//
// setting the currently selected WMS Datasource
//
  WmsView->Show(false);
  WmsView->ClearSelection();
  WmsView->SelectRow(CurrentEvtRow);
  WmsView->Show(true);
  wxString country = WmsView->GetCellValue(CurrentEvtRow, 0);
  wxTextCtrl *countryCtrl = (wxTextCtrl *) FindWindow(ID_WMS_COUNTRY);
  countryCtrl->SetValue(country);
  wxString state = WmsView->GetCellValue(CurrentEvtRow, 1);
  wxTextCtrl *stateCtrl = (wxTextCtrl *) FindWindow(ID_WMS_STATE);
  stateCtrl->SetValue(state);
  wxString category = WmsView->GetCellValue(CurrentEvtRow, 2);
  wxTextCtrl *categoryCtrl = (wxTextCtrl *) FindWindow(ID_WMS_CATEGORY);
  categoryCtrl->SetValue(category);
  wxString url = WmsView->GetCellValue(CurrentEvtRow, 3);
  wxTextCtrl *urlCtrl = (wxTextCtrl *) FindWindow(ID_WMS_URL);
  urlCtrl->SetValue(url);
  wxButton *btn = (wxButton *) FindWindow(wxID_OK);
  btn->Enable(true);
}

void PredefinedWmsDialog::OnCancel(wxCommandEvent & WXUNUSED(event))
{
//
// all done: 
//
  wxDialog::EndModal(wxID_CANCEL);
}

void PredefinedWmsDialog::OnOk(wxCommandEvent & WXUNUSED(event))
{
//
// all done: 
//
  wxTextCtrl *urlCtrl = (wxTextCtrl *) FindWindow(ID_WMS_URL);
  URL = urlCtrl->GetValue();
  wxDialog::EndModal(wxID_OK);
}

bool WmsDialog::Create(MyFrame * parent, wxString & proxy, bool immediate)
{
//
// creating the dialog
//
  MainFrame = parent;
  SwapXY = 0;
  HttpProxy = proxy;
  if (HttpProxy.Len() == 0)
    ProxyEnabled = false;
  else
    ProxyEnabled = true;
  if (wxDialog::Create(parent, wxID_ANY, wxT("Open a WMS datasource")) == false)
    return false;
// populates individual controls
  CreateControls();
// sets dialog sizer
  GetSizer()->Fit(this);
  GetSizer()->SetSizeHints(this);
// centers the dialog window
  Centre();
  if (immediate == true)
    {
      ::wxBeginBusyCursor();
      PrepareCatalog();
      ::wxEndBusyCursor();
      UpdateSwapXY();
    }
  return true;
}

void WmsDialog::CreateControls()
{
//
// creating individual control and setting initial values
//
  wxBoxSizer *topSizer = new wxBoxSizer(wxVERTICAL);
  this->SetSizer(topSizer);
  wxBoxSizer *boxSizer = new wxBoxSizer(wxVERTICAL);
  topSizer->Add(boxSizer, 0, wxALIGN_CENTER_HORIZONTAL | wxALL, 5);
// HTTP Proxy
  wxStaticBox *proxyBox = new wxStaticBox(this, wxID_STATIC,
                                          wxT("HTTP Proxy"),
                                          wxDefaultPosition,
                                          wxDefaultSize);
  wxBoxSizer *proxyBoxSizer = new wxStaticBoxSizer(proxyBox, wxHORIZONTAL);
  boxSizer->Add(proxyBoxSizer, 0, wxALIGN_CENTER_HORIZONTAL | wxALL, 2);
  wxCheckBox *enableProxyCtrl = new wxCheckBox(this, ID_WMS_ENABLE_PROXY,
                                               wxT("Enable"),
                                               wxDefaultPosition,
                                               wxDefaultSize);
  enableProxyCtrl->SetValue(ProxyEnabled);
  proxyBoxSizer->Add(enableProxyCtrl, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);
  wxTextCtrl *proxyCtrl = new wxTextCtrl(this, ID_WMS_PROXY, HttpProxy,
                                         wxDefaultPosition, wxSize(600, 22));
  proxyBoxSizer->Add(proxyCtrl, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);
  proxyCtrl->Enable(ProxyEnabled);
// URL group box
  wxStaticBox *urlBox = new wxStaticBox(this, wxID_STATIC,
                                        wxT("WMS URL - GetCapabilities"),
                                        wxDefaultPosition,
                                        wxDefaultSize);
  wxBoxSizer *urlBoxSizer = new wxStaticBoxSizer(urlBox, wxVERTICAL);
  boxSizer->Add(urlBoxSizer, 0, wxALIGN_CENTER_HORIZONTAL | wxALL, 0);
// First row: GetCapabilities URL
  wxBoxSizer *urlSizer = new wxBoxSizer(wxVERTICAL);
  urlBoxSizer->Add(urlSizer, 0, wxALIGN_CENTER_HORIZONTAL | wxALL, 0);
  wxTextCtrl *urlCtrl = new wxTextCtrl(this, ID_WMS_URL, wxT("http://"),
                                       wxDefaultPosition, wxSize(680, 22));
  urlSizer->Add(urlCtrl, 0, wxALIGN_RIGHT | wxALL, 5);
  urlCtrl->SetValue(MainFrame->GetCurrentWmsDatasource());
// command buttons
  wxBoxSizer *url2Sizer = new wxBoxSizer(wxHORIZONTAL);
  urlSizer->Add(url2Sizer, 0, wxALIGN_CENTER_HORIZONTAL | wxALL, 0);
  wxStaticBox *catBox = new wxStaticBox(this, wxID_STATIC,
                                        wxT("WMS Catalog"),
                                        wxDefaultPosition,
                                        wxDefaultSize);
  wxBoxSizer *catBoxSizer = new wxStaticBoxSizer(catBox, wxVERTICAL);
  url2Sizer->Add(catBoxSizer, 0, wxALIGN_CENTER_HORIZONTAL | wxALL, 5);
  wxBoxSizer *okCancelBox = new wxBoxSizer(wxHORIZONTAL);
  catBoxSizer->Add(okCancelBox, 0, wxALIGN_CENTER_HORIZONTAL | wxALL, 0);
  wxButton *query = new wxButton(this, ID_WMS_CATALOG, wxT("&Load"));
  okCancelBox->Add(query, 0, wxALIGN_CENTER_VERTICAL | wxALL, 2);
  wxButton *reset = new wxButton(this, ID_WMS_RESET, wxT("&Reset"));
  reset->Enable(false);
  okCancelBox->Add(reset, 0, wxALIGN_CENTER_VERTICAL | wxALL, 2);
  wxButton *quit = new wxButton(this, wxID_CANCEL, wxT("&Quit"));
  okCancelBox->Add(quit, 0, wxALIGN_CENTER_VERTICAL | wxALL, 2);
// the Catalog tree view
  wxBoxSizer *treeSizer = new wxBoxSizer(wxHORIZONTAL);
  boxSizer->Add(treeSizer, 0, wxALIGN_CENTER_HORIZONTAL | wxALL, 0);
  WmsTree = new MyCatalogTree(this, wxSize(330, 250));
  treeSizer->Add(WmsTree, 0, wxALIGN_LEFT | wxALL, 5);
// Title / Abstract panel
  wxBoxSizer *title0BoxSizer = new wxBoxSizer(wxVERTICAL);
  treeSizer->Add(title0BoxSizer, 0, wxALIGN_CENTER_HORIZONTAL | wxALL, 0);
  wxStaticBox *titleBox = new wxStaticBox(this, wxID_ANY,
                                          wxT("Layer Title"),
                                          wxDefaultPosition,
                                          wxDefaultSize);
  wxBoxSizer *titleBoxSizer = new wxStaticBoxSizer(titleBox, wxHORIZONTAL);
  title0BoxSizer->Add(titleBoxSizer, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);
  wxHtmlWindow *titleCtrl = new wxHtmlWindow(this, ID_WMS_TITLE,
                                             wxDefaultPosition, wxSize(350,
                                                                       75));
  titleBoxSizer->Add(titleCtrl, 0, wxALIGN_CENTER_VERTICAL | wxALL, 0);
  wxStaticBox *abstractBox = new wxStaticBox(this, wxID_ANY,
                                             wxT("Layer Abstract"),
                                             wxDefaultPosition,
                                             wxDefaultSize);
  wxBoxSizer *abstractBoxSizer =
    new wxStaticBoxSizer(abstractBox, wxHORIZONTAL);
  title0BoxSizer->Add(abstractBoxSizer, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);
  wxHtmlWindow *abstractCtrl = new wxHtmlWindow(this, ID_WMS_ABSTRACT,
                                                wxDefaultPosition, wxSize(350,
                                                                          120));
  abstractBoxSizer->Add(abstractCtrl, 0, wxALIGN_CENTER_VERTICAL | wxALL, 0);
// selected Layer group box
  wxBoxSizer *kkSizer = new wxBoxSizer(wxHORIZONTAL);
  boxSizer->Add(kkSizer, 0, wxALIGN_TOP | wxALL, 5);
  wxStaticBox *lyrBox = new wxStaticBox(this, wxID_STATIC,
                                        wxT("Selected WMS Layer"),
                                        wxDefaultPosition,
                                        wxDefaultSize);
  wxBoxSizer *lyrBox0Sizer = new wxStaticBoxSizer(lyrBox, wxHORIZONTAL);
  kkSizer->Add(lyrBox0Sizer, 0, wxALIGN_CENTER_HORIZONTAL | wxALL, 0);
// Second row: Layer name
  wxBoxSizer *lyrBoxSizer = new wxBoxSizer(wxVERTICAL);
  lyrBox0Sizer->Add(lyrBoxSizer, 0, wxALIGN_CENTER_VERTICAL | wxALL, 0);
  wxBoxSizer *nameSizer = new wxBoxSizer(wxHORIZONTAL);
  lyrBoxSizer->Add(nameSizer, 0, wxALIGN_CENTER_VERTICAL | wxALL, 0);
  wxStaticBox *nameBox = new wxStaticBox(this, wxID_ANY,
                                         wxT("Layer Name"),
                                         wxDefaultPosition,
                                         wxDefaultSize);
  wxBoxSizer *nameBoxSizer = new wxStaticBoxSizer(nameBox, wxHORIZONTAL);
  nameSizer->Add(nameBoxSizer, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);
  wxTextCtrl *nameCtrl = new wxTextCtrl(this, ID_WMS_NAME, wxT(""),
                                        wxDefaultPosition, wxSize(570, 22),
                                        wxTE_READONLY);
  nameBoxSizer->Add(nameCtrl, 0, wxALIGN_CENTER_VERTICAL | wxALL, 0);
  wxStaticBox *swapBox = new wxStaticBox(this, wxID_ANY,
                                         wxT("Swap Axes"),
                                         wxDefaultPosition,
                                         wxDefaultSize);
  wxBoxSizer *swapBoxSizer = new wxStaticBoxSizer(swapBox, wxHORIZONTAL);
  nameSizer->Add(swapBoxSizer, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);
  wxCheckBox *swapCtrl = new wxCheckBox(this, ID_WMS_SWAP,
                                        wxT("Swap Y,X"),
                                        wxDefaultPosition, wxDefaultSize);
  swapCtrl->SetValue(false);
  swapCtrl->Enable(false);
  swapBoxSizer->Add(swapCtrl, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);
// First row: WMS options
  wxBoxSizer *wmsSizer = new wxBoxSizer(wxHORIZONTAL);
  lyrBoxSizer->Add(wmsSizer, 0, wxALIGN_CENTER_HORIZONTAL | wxALL, 0);
  wxString ver[4];
  ver[0] = wxT("WMS &1.0.0");
  ver[1] = wxT("WMS &1.1.0");
  ver[2] = wxT("WMS &1.1.1");
  ver[3] = wxT("WMS &1.3.0");
  wxRadioBox *versionBox = new wxRadioBox(this, ID_WMS_VERSION,
                                          wxT("WMS &Version"),
                                          wxDefaultPosition,
                                          wxDefaultSize, 4,
                                          ver, 4,
                                          wxRA_SPECIFY_ROWS);
  versionBox->Enable(false);
  versionBox->SetSelection(3);
  wmsSizer->Add(versionBox, 0, wxALIGN_TOP | wxALL, 5);
  wxBoxSizer *xxSizer = new wxBoxSizer(wxVERTICAL);
  wmsSizer->Add(xxSizer, 0, wxALIGN_TOP | wxALL, 5);
  wxStaticBox *crsBox = new wxStaticBox(this, wxID_STATIC,
                                        wxT("Reference System"),
                                        wxDefaultPosition,
                                        wxDefaultSize);
  wxBoxSizer *crsBoxSizer = new wxStaticBoxSizer(crsBox, wxVERTICAL);
  xxSizer->Add(crsBoxSizer, 0, wxALIGN_CENTER_VERTICAL | wxALL, 2);
  wxComboBox *crsList =
    new wxComboBox(this, ID_WMS_CRS, wxT(""), wxDefaultPosition,
                   wxSize(150, 21), 0, NULL,
                   wxCB_DROPDOWN | wxCB_SORT | wxCB_READONLY);
  crsList->Enable(false);
  crsBoxSizer->Add(crsList, 0, wxALIGN_CENTER_VERTICAL | wxALL, 2);
  wxStaticBox *stlBox = new wxStaticBox(this, wxID_STATIC,
                                        wxT("Style"),
                                        wxDefaultPosition,
                                        wxDefaultSize);
  wxBoxSizer *stlBoxSizer = new wxStaticBoxSizer(stlBox, wxVERTICAL);
  xxSizer->Add(stlBoxSizer, 0, wxALIGN_CENTER_VERTICAL | wxALL, 2);
  wxComboBox *stlList =
    new wxComboBox(this, ID_WMS_STYLE, wxT(""), wxDefaultPosition,
                   wxSize(150, 21), 0, NULL,
                   wxCB_DROPDOWN | wxCB_SORT | wxCB_READONLY);
  stlList->Enable(false);
  stlBoxSizer->Add(stlList, 0, wxALIGN_CENTER_VERTICAL | wxALL, 2);
  wxBoxSizer *yySizer = new wxBoxSizer(wxVERTICAL);
  wmsSizer->Add(yySizer, 0, wxALIGN_TOP | wxALL, 5);
  wxStaticBox *fmtBox = new wxStaticBox(this, wxID_STATIC,
                                        wxT("Image Format"),
                                        wxDefaultPosition,
                                        wxDefaultSize);
  wxBoxSizer *fmtBoxSizer = new wxStaticBoxSizer(fmtBox, wxVERTICAL);
  yySizer->Add(fmtBoxSizer, 0, wxALIGN_CENTER_VERTICAL | wxALL, 2);
  wxComboBox *fmtList =
    new wxComboBox(this, ID_WMS_FORMAT, wxT(""), wxDefaultPosition,
                   wxSize(150, 21), 0, NULL,
                   wxCB_DROPDOWN | wxCB_SORT | wxCB_READONLY);
  fmtList->Enable(false);
  fmtBoxSizer->Add(fmtList, 0, wxALIGN_CENTER_VERTICAL | wxALL, 2);
  wxString trans[2];
  trans[0] = wxT("yes");
  trans[1] = wxT("opaque");
  wxRadioBox *transBox = new wxRadioBox(this, ID_WMS_TRANSPARENT,
                                        wxT("Transparent Layer"),
                                        wxDefaultPosition,
                                        wxDefaultSize, 2,
                                        trans, 2,
                                        wxRA_SPECIFY_COLS);
  transBox->Enable(false);
  transBox->SetSelection(1);
  yySizer->Add(transBox, 0, wxALIGN_CENTER_HORIZONTAL | wxALL, 2);
  wxStaticBox *tileBox = new wxStaticBox(this, wxID_STATIC,
                                         wxT("Tile size"),
                                         wxDefaultPosition,
                                         wxDefaultSize);
  wxBoxSizer *tileBoxSizer = new wxStaticBoxSizer(tileBox, wxVERTICAL);
  wmsSizer->Add(tileBoxSizer, 0, wxALIGN_TOP | wxALL, 5);
  wxCheckBox *tiledCtrl = new wxCheckBox(this, ID_WMS_TILED,
                                         wxT("enable Tiles"),
                                         wxDefaultPosition, wxDefaultSize);
  tiledCtrl->SetValue(false);
  tiledCtrl->Enable(false);
  tileBoxSizer->Add(tiledCtrl, 0, wxALIGN_CENTER_HORIZONTAL | wxALL, 5);
  wxBoxSizer *tile1BoxSizer = new wxBoxSizer(wxHORIZONTAL);
  tileBoxSizer->Add(tile1BoxSizer, 0, wxALIGN_RIGHT | wxALL, 0);
  wxStaticText *wLabel = new wxStaticText(this, wxID_ANY, wxT("&Width:"),
                                          wxDefaultPosition, wxDefaultSize,
                                          wxALIGN_RIGHT);
  tile1BoxSizer->Add(wLabel, 0, wxALIGN_CENTER_VERTICAL | wxALL, 2);
  wxSpinCtrl *wCtrl = new wxSpinCtrl(this, ID_WMS_WIDTH, wxEmptyString,
                                     wxDefaultPosition, wxSize(80, 20),
                                     wxSP_ARROW_KEYS,
                                     0, 0, 0);
  wCtrl->Enable(false);
  wCtrl->SetValue(wxT(""));
  tile1BoxSizer->Add(wCtrl, 0, wxALIGN_CENTER_VERTICAL | wxALL, 2);
  wxBoxSizer *tile2BoxSizer = new wxBoxSizer(wxHORIZONTAL);
  tileBoxSizer->Add(tile2BoxSizer, 0, wxALIGN_RIGHT | wxALL, 0);
  wxStaticText *hLabel = new wxStaticText(this, wxID_ANY, wxT("&Height:"),
                                          wxDefaultPosition, wxDefaultSize,
                                          wxALIGN_RIGHT);
  tile2BoxSizer->Add(hLabel, 0, wxALIGN_CENTER_VERTICAL | wxALL, 2);
  wxSpinCtrl *hCtrl = new wxSpinCtrl(this, ID_WMS_HEIGHT, wxEmptyString,
                                     wxDefaultPosition, wxSize(80, 20),
                                     wxSP_ARROW_KEYS,
                                     0, 0, 0);
  tile2BoxSizer->Add(hCtrl, 0, wxALIGN_CENTER_VERTICAL | wxALL, 2);
  hCtrl->Enable(false);
  hCtrl->SetValue(wxT(""));
  wxButton *wms_ok = new wxButton(this, ID_WMS_OK, wxT("&Open"));
  wms_ok->Enable(false);
  wmsSizer->Add(wms_ok, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);
// appends event handlers
  Connect(ID_WMS_ENABLE_PROXY, wxEVT_COMMAND_CHECKBOX_CLICKED,
          (wxObjectEventFunction) & WmsDialog::OnProxy);
  Connect(ID_WMS_CRS, wxEVT_COMMAND_COMBOBOX_SELECTED,
          (wxObjectEventFunction) & WmsDialog::OnCrsChanged);
  Connect(ID_WMS_VERSION, wxEVT_COMMAND_RADIOBOX_SELECTED,
          (wxObjectEventFunction) & WmsDialog::OnVersionChanged);
  Connect(ID_WMS_SWAP, wxEVT_COMMAND_CHECKBOX_CLICKED,
          (wxObjectEventFunction) & WmsDialog::OnSwapXYChanged);
  Connect(ID_WMS_TILED, wxEVT_COMMAND_CHECKBOX_CLICKED,
          (wxObjectEventFunction) & WmsDialog::OnTiledChanged);
  Connect(ID_WMS_CATALOG, wxEVT_COMMAND_BUTTON_CLICKED,
          (wxObjectEventFunction) & WmsDialog::OnCatalog);
  Connect(ID_WMS_RESET, wxEVT_COMMAND_BUTTON_CLICKED,
          (wxObjectEventFunction) & WmsDialog::OnReset);
  Connect(ID_WMS_OK, wxEVT_COMMAND_BUTTON_CLICKED,
          (wxObjectEventFunction) & WmsDialog::OnWmsOk);
  Connect(wxID_CANCEL, wxEVT_COMMAND_BUTTON_CLICKED,
          (wxObjectEventFunction) & WmsDialog::OnQuit);
}

void WmsDialog::OnProxy(wxCommandEvent & WXUNUSED(event))
{
//
// enabling/disabling HTTP Proxy
//
  wxTextCtrl *proxy = (wxTextCtrl *) FindWindow(ID_WMS_PROXY);
  if (ProxyEnabled == false)
    ProxyEnabled = true;
  else
    ProxyEnabled = false;
  proxy->Enable(ProxyEnabled);
  if (ProxyEnabled == false)
    {
      HttpProxy = wxT("");
      proxy->SetValue(wxT(""));
    }
}

void WmsDialog::OnCrsChanged(wxCommandEvent & WXUNUSED(event))
{
//
// CRS changed: updating SWAP XY
//
  UpdateSwapXY();
}

void WmsDialog::OnVersionChanged(wxCommandEvent & WXUNUSED(event))
{
//
// Version changed: updating SWAP XY
//
  UpdateSwapXY();
}

void WmsDialog::UpdateSwapXY()
{
//
// updating SWAP XY
//
  wxCheckBox *swapBox = (wxCheckBox *) FindWindow(ID_WMS_SWAP);
  wxRadioBox *versionBox = (wxRadioBox *) FindWindow(ID_WMS_VERSION);
  wxComboBox *comboCtrl = (wxComboBox *) FindWindow(ID_WMS_CRS);
  if (versionBox->GetSelection() != 3)
    {
      swapBox->SetValue(false);
      SwapXY = 0;
      return;
    };
  wxString value = comboCtrl->GetValue();
  char *crs = new char[value.Len() + 1];
  strcpy(crs, value.ToUTF8());
  if (MainFrame->IsSwapXYcrs(crs) == true)
    {
      swapBox->SetValue(true);
      SwapXY = 1;
  } else
    {
      swapBox->SetValue(false);
      SwapXY = 0;
    }
  delete[]crs;
}

void WmsDialog::SelectLayer()
{
//
// setting the currently selected WMS Layer [NULL type]
//
  CurrentLayer = NULL;
  CurrentTiledLayer = NULL;
  wxTextCtrl *nameCtrl = (wxTextCtrl *) FindWindow(ID_WMS_NAME);
  wxHtmlWindow *titleCtrl = (wxHtmlWindow *) FindWindow(ID_WMS_TITLE);
  wxHtmlWindow *abstractCtrl = (wxHtmlWindow *) FindWindow(ID_WMS_ABSTRACT);
  nameCtrl->SetValue(wxT(""));
  nameCtrl->Enable(false);
  titleCtrl->SetPage(wxT("<html><body></body></html>"));
  titleCtrl->Enable(false);
  abstractCtrl->SetPage(wxT("<html><body></body></html>"));
  abstractCtrl->Enable(false);
  wxComboBox *comboCtrl = (wxComboBox *) FindWindow(ID_WMS_CRS);
  comboCtrl->Clear();
  comboCtrl->Enable(false);
  comboCtrl = (wxComboBox *) FindWindow(ID_WMS_FORMAT);
  comboCtrl->Clear();
  comboCtrl->Enable(false);
  comboCtrl = (wxComboBox *) FindWindow(ID_WMS_STYLE);
  comboCtrl->Clear();
  comboCtrl->Enable(false);
  wxRadioBox *trans = (wxRadioBox *) FindWindow(ID_WMS_TRANSPARENT);
  trans->Enable(false);
  wxCheckBox *tiled = (wxCheckBox *) FindWindow(ID_WMS_TILED);
  tiled->Enable(false);
  wxRadioBox *versionBox = (wxRadioBox *) FindWindow(ID_WMS_VERSION);
  versionBox->Enable(false);
  wxCheckBox *swapBox = (wxCheckBox *) FindWindow(ID_WMS_SWAP);
  swapBox->Enable(false);
  wxButton *btn = (wxButton *) FindWindow(ID_WMS_OK);
  btn->Enable(false);
}

void WmsDialog::SelectLayer(rl2WmsLayerPtr layer)
{
//
// setting the currently selected WMS Layer [ordinary WMS layer]
//
  bool enabledLayer = true;
  if (get_wms_layer_name(layer) == NULL)
    enabledLayer = false;
  CurrentLayer = layer;
  CurrentTiledLayer = NULL;
  wxTextCtrl *nameCtrl = (wxTextCtrl *) FindWindow(ID_WMS_NAME);
  wxHtmlWindow *titleCtrl = (wxHtmlWindow *) FindWindow(ID_WMS_TITLE);
  wxHtmlWindow *abstractCtrl = (wxHtmlWindow *) FindWindow(ID_WMS_ABSTRACT);
  wxString lyr_name;
  const char *x_name = get_wms_layer_name(layer);
  lyr_name = wxString::FromUTF8(x_name);
  nameCtrl->SetValue(lyr_name);
  x_name = get_wms_layer_title(layer);
  lyr_name = wxString::FromUTF8(x_name);
  lyr_name.Replace(wxT("\n"), wxT("<br>"));
  titleCtrl->SetPage(wxT("<html><body>") + lyr_name + wxT("</body></html>"));
  x_name = get_wms_layer_abstract(layer);
  lyr_name = wxString::FromUTF8(x_name);
  lyr_name.Replace(wxT("\n"), wxT("<br>"));
  abstractCtrl->SetPage(wxT("<html><body>") + lyr_name + wxT("</body></html>"));
  wxComboBox *comboCtrl = (wxComboBox *) FindWindow(ID_WMS_CRS);
  comboCtrl->Clear();
  int max = get_wms_layer_crs_count(layer);
  if (max > 0)
    {
      for (int s = 0; s < max; s++)
        {
          wxString str = wxString::FromUTF8(get_wms_layer_crs(layer, s));
          if (comboCtrl->FindString(str) == wxNOT_FOUND)
            comboCtrl->Append(str);
        }
      wxString str = wxString::FromUTF8(MainFrame->GetMapCRS());
      if (comboCtrl->SetStringSelection(str) == false)
        {
          str = wxString::FromUTF8(get_wms_layer_crs(layer, 0));
          int sel = comboCtrl->FindString(str);
          if (sel == wxNOT_FOUND)
            comboCtrl->SetSelection(0);
          else
            comboCtrl->SetSelection(sel);
        }
      comboCtrl->Enable(enabledLayer);
    }
  comboCtrl = (wxComboBox *) FindWindow(ID_WMS_FORMAT);
  comboCtrl->Clear();
  max = get_wms_format_count(Catalog, 1);
  if (max > 0)
    {
      for (int s = 0; s < max; s++)
        {
          wxString str = wxString::FromUTF8(get_wms_format(Catalog, s, 1));
          if (comboCtrl->FindString(str) == wxNOT_FOUND)
            comboCtrl->Append(str);
        }
      wxString str = wxString::FromUTF8(get_wms_format(Catalog, 0, 1));
      int sel = comboCtrl->FindString(str);
      if (sel == wxNOT_FOUND)
        comboCtrl->SetSelection(0);
      else
        comboCtrl->SetSelection(sel);
      comboCtrl->Enable(enabledLayer);
    }
  comboCtrl = (wxComboBox *) FindWindow(ID_WMS_STYLE);
  comboCtrl->Clear();
  max = get_wms_layer_style_count(layer);
  if (max > 0)
    {
      for (int s = 0; s < max; s++)
        {
          wxString str = wxString::FromUTF8(get_wms_layer_style_name(layer, s));
          if (comboCtrl->FindString(str) == wxNOT_FOUND)
            comboCtrl->Append(str);
        }
      wxString str = wxString::FromUTF8(get_wms_layer_style_name(layer, 0));
      int sel = comboCtrl->FindString(str);
      if (sel == wxNOT_FOUND)
        comboCtrl->SetSelection(0);
      else
        comboCtrl->SetSelection(sel);
      comboCtrl->Enable(enabledLayer);
    }
  wxRadioBox *trans = (wxRadioBox *) FindWindow(ID_WMS_TRANSPARENT);
  if (is_wms_layer_opaque(layer) <= 0)
    {
      trans->SetSelection(0);
      trans->Enable(enabledLayer);
  } else
    {
      trans->SetSelection(1);
      trans->Enable(false);
    }
  wxCheckBox *tiled = (wxCheckBox *) FindWindow(ID_WMS_TILED);
  tiled->SetValue(false);
  tiled->Enable(enabledLayer);
  wxRadioBox *versionBox = (wxRadioBox *) FindWindow(ID_WMS_VERSION);
  versionBox->Enable(enabledLayer);
  wxCheckBox *swapBox = (wxCheckBox *) FindWindow(ID_WMS_SWAP);
  swapBox->Enable(enabledLayer);
  wxButton *btn = (wxButton *) FindWindow(ID_WMS_OK);
  btn->Enable(enabledLayer);
  const char *version = get_wms_version(Catalog);
  if (version == NULL)
    {
      versionBox->Enable(0, enabledLayer);
      versionBox->Enable(1, false);
      versionBox->Enable(2, false);
      versionBox->Enable(3, false);
      versionBox->SetSelection(0);
  } else
    {
      if (strcmp(version, "1.3.0") == 0)
        {
          versionBox->Enable(0, enabledLayer);
          versionBox->Enable(1, enabledLayer);
          versionBox->Enable(2, enabledLayer);
          versionBox->Enable(3, enabledLayer);
          versionBox->SetSelection(3);
      } else if (strcmp(version, "1.1.1") == 0)
        {
          versionBox->Enable(0, enabledLayer);
          versionBox->Enable(1, enabledLayer);
          versionBox->Enable(2, enabledLayer);
          versionBox->Enable(3, false);
          versionBox->SetSelection(2);
      } else if (strcmp(version, "1.1.0") == 0)
        {
          versionBox->Enable(0, enabledLayer);
          versionBox->Enable(1, enabledLayer);
          versionBox->Enable(2, false);
          versionBox->Enable(3, false);
          versionBox->SetSelection(1);
      } else
        {
          versionBox->Enable(0, enabledLayer);
          versionBox->Enable(1, false);
          versionBox->Enable(2, false);
          versionBox->Enable(3, false);
          versionBox->SetSelection(0);
        }
    }
  UpdateSwapXY();
}

void WmsDialog::SelectLayer(rl2WmsTiledLayerPtr layer)
{
//
// setting the currently selected WMS TiledLayer [TileService]
//
  bool enabledLayer = true;
  if (get_wms_tiled_layer_name(layer) == NULL)
    enabledLayer = false;
  if (get_wms_tiled_layer_data_type(layer) != NULL)
    {
      // some Grid-Data layer [not directly visible]
      enabledLayer = false;
    }
  CurrentLayer = NULL;
  CurrentTiledLayer = layer;
  wxTextCtrl *nameCtrl = (wxTextCtrl *) FindWindow(ID_WMS_NAME);
  wxHtmlWindow *titleCtrl = (wxHtmlWindow *) FindWindow(ID_WMS_TITLE);
  wxHtmlWindow *abstractCtrl = (wxHtmlWindow *) FindWindow(ID_WMS_ABSTRACT);
  wxString lyr_name;
  const char *x_name = get_wms_tiled_layer_name(layer);
  lyr_name = wxString::FromUTF8(x_name);
  nameCtrl->SetValue(lyr_name);
  x_name = get_wms_tiled_layer_title(layer);
  lyr_name = wxString::FromUTF8(x_name);
  lyr_name.Replace(wxT("\n"), wxT("<br>"));
  titleCtrl->SetPage(wxT("<html><body>") + lyr_name + wxT("</body></html>"));
  x_name = get_wms_tiled_layer_abstract(layer);
  lyr_name = wxString::FromUTF8(x_name);
  lyr_name.Replace(wxT("\n"), wxT("<br>"));
  abstractCtrl->SetPage(wxT("<html><body>") + lyr_name + wxT("</body></html>"));
  wxComboBox *comboCtrl = (wxComboBox *) FindWindow(ID_WMS_CRS);
  comboCtrl->Clear();
  wxString str = wxString::FromUTF8(get_wms_tiled_layer_crs(layer));
  comboCtrl->Append(str);
  comboCtrl->SetSelection(0);
  comboCtrl->Enable(false);
  comboCtrl = (wxComboBox *) FindWindow(ID_WMS_FORMAT);
  comboCtrl->Clear();
  str = wxString::FromUTF8(get_wms_tiled_layer_format(layer));
  comboCtrl->Append(str);
  comboCtrl->SetSelection(0);
  comboCtrl->Enable(false);
  comboCtrl = (wxComboBox *) FindWindow(ID_WMS_STYLE);
  comboCtrl->Clear();
  str = wxString::FromUTF8(get_wms_tiled_layer_style(layer));
  comboCtrl->Append(str);
  comboCtrl->SetSelection(0);
  comboCtrl->Enable(false);
  wxRadioBox *trans = (wxRadioBox *) FindWindow(ID_WMS_TRANSPARENT);
  trans->SetSelection(1);
  trans->Enable(false);
  wxCheckBox *tiled = (wxCheckBox *) FindWindow(ID_WMS_TILED);
  tiled->SetValue(true);
  tiled->Enable(false);
  wxSpinCtrl *wCtrl = (wxSpinCtrl *) FindWindow(ID_WMS_WIDTH);
  wxSpinCtrl *hCtrl = (wxSpinCtrl *) FindWindow(ID_WMS_HEIGHT);
  int tile_width;
  int tile_height;
  get_wms_tiled_layer_tile_size(layer, &tile_width, &tile_height);
  char dummy[64];
  sprintf(dummy, "%d", tile_width);
  wxString tilex = wxString::FromUTF8(dummy);
  wCtrl->SetRange(tile_width, tile_width);
  wCtrl->SetValue(tilex);
  wCtrl->Enable(true);
  hCtrl->SetRange(tile_height, tile_height);
  sprintf(dummy, "%d", tile_height);
  tilex = wxString::FromUTF8(dummy);
  hCtrl->SetValue(tilex);
  hCtrl->Enable(true);
  wxRadioBox *versionBox = (wxRadioBox *) FindWindow(ID_WMS_VERSION);
  versionBox->Enable(enabledLayer);
  wxCheckBox *swapBox = (wxCheckBox *) FindWindow(ID_WMS_SWAP);
  swapBox->Enable(false);
  wxButton *btn = (wxButton *) FindWindow(ID_WMS_OK);
  btn->Enable(enabledLayer);
  const char *version = get_wms_version(Catalog);
  if (version == NULL)
    {
      versionBox->Enable(0, enabledLayer);
      versionBox->Enable(1, false);
      versionBox->Enable(2, false);
      versionBox->Enable(3, false);
      versionBox->SetSelection(0);
  } else
    {
      if (strcmp(version, "1.3.0") == 0)
        {
          versionBox->Enable(0, enabledLayer);
          versionBox->Enable(1, enabledLayer);
          versionBox->Enable(2, enabledLayer);
          versionBox->Enable(3, enabledLayer);
          versionBox->SetSelection(3);
      } else if (strcmp(version, "1.1.1") == 0)
        {
          versionBox->Enable(0, enabledLayer);
          versionBox->Enable(1, enabledLayer);
          versionBox->Enable(2, enabledLayer);
          versionBox->Enable(3, false);
          versionBox->SetSelection(2);
      } else if (strcmp(version, "1.1.0") == 0)
        {
          versionBox->Enable(0, enabledLayer);
          versionBox->Enable(1, enabledLayer);
          versionBox->Enable(2, false);
          versionBox->Enable(3, false);
          versionBox->SetSelection(1);
      } else
        {
          versionBox->Enable(0, enabledLayer);
          versionBox->Enable(1, false);
          versionBox->Enable(2, false);
          versionBox->Enable(3, false);
          versionBox->SetSelection(0);
        }
    }
  UpdateSwapXY();
}

void WmsDialog::OnSwapXYChanged(wxCommandEvent & WXUNUSED(event))
{
//
// Swap XY selection changed
//
  if (SwapXY == 0)
    SwapXY = 1;
  else
    SwapXY = 0;
}

void WmsDialog::OnTiledChanged(wxCommandEvent & WXUNUSED(event))
{
//
// Monolithic / Tiled selection changed
//
  wxCheckBox *tiledCtrl = (wxCheckBox *) FindWindow(ID_WMS_TILED);
  wxSpinCtrl *wCtrl = (wxSpinCtrl *) FindWindow(ID_WMS_WIDTH);
  wxSpinCtrl *hCtrl = (wxSpinCtrl *) FindWindow(ID_WMS_HEIGHT);
  if (tiledCtrl->GetValue() == false)
    {
      wCtrl->SetRange(0, 0);
      wCtrl->SetValue(wxT(""));
      wCtrl->Enable(false);
      hCtrl->SetRange(0, 0);
      hCtrl->SetValue(wxT(""));
      hCtrl->Enable(false);
  } else
    {
      wCtrl->SetRange(256, MaxWidth);
      wCtrl->SetValue(wxT("512"));
      wCtrl->Enable(true);
      hCtrl->SetRange(256, MaxHeight);
      hCtrl->SetValue(wxT("512"));
      hCtrl->Enable(true);
    }
}

void WmsDialog::OnCatalog(wxCommandEvent & WXUNUSED(event))
{
//
// event handler
//
  ::wxBeginBusyCursor();
  PrepareCatalog();
  ::wxEndBusyCursor();
}

void WmsDialog::PrepareCatalog()
{
//
// attempting to create a WMS Catalog from GetCapabilities
//
  wxTextCtrl *proxyCtrl = (wxTextCtrl *) FindWindow(ID_WMS_PROXY);
  wxTextCtrl *urlCtrl = (wxTextCtrl *) FindWindow(ID_WMS_URL);
  wxButton *catalogBtn = (wxButton *) FindWindow(ID_WMS_CATALOG);
  wxButton *resetBtn = (wxButton *) FindWindow(ID_WMS_RESET);
  if (ProxyEnabled == true)
    {
      HttpProxy = proxyCtrl->GetValue();
      if (HttpProxy.Len() == 0)
        {
          wxMessageBox(wxT("You must specify some HTTP Proxy URL !!!"),
                       wxT("LibreWMS"), wxOK | wxICON_WARNING, this);
          return;
        }
    }
  wxString url = urlCtrl->GetValue();
  if (url.Len() < 1)
    {
      wxMessageBox(wxT("You must specify some WMS GetCapabilities URL !!!"),
                   wxT("LibreWMS"), wxOK | wxICON_WARNING, this);
      return;
    }
  char xurl[1024];
  char *err_msg = NULL;
  strcpy(xurl, url.ToUTF8());
  char *proxy = NULL;
  if (ProxyEnabled == true && HttpProxy.Len() > 0)
    {
      proxy = new char[HttpProxy.Len() + 1];
      strcpy(proxy, HttpProxy.ToUTF8());
    }
  Catalog = create_wms_catalog(MainFrame->GetWmsCache(), xurl, proxy, &err_msg);
  if (proxy != NULL)
    delete[]proxy;
  if (Catalog == NULL)
    {
      wxString msg = wxString::FromUTF8(err_msg);
      wxMessageBox(wxT("unable to get a WMS Catalog\n\n") + msg,
                   wxT("LibreWMS"), wxOK | wxICON_ERROR, this);
  } else
    {
      MaxWidth = get_wms_max_width(Catalog);
      MaxHeight = get_wms_max_height(Catalog);
      if (MaxWidth <= 0)
        MaxWidth = 2048;
      if (MaxHeight <= 0)
        MaxHeight = 2048;
      if (is_wms_tile_service(Catalog))
        {
          int nLayers = get_wms_tile_service_count(Catalog);
          WmsTree->Show(false);
          WmsTree->FlushAll();
          const char *ts_name = get_wms_tile_service_name(Catalog);
          const char *ts_title = get_wms_tile_service_title(Catalog);
          if (ts_name != NULL)
            WmsTree->AddTiledRoot(ts_name);
          else if (ts_title != NULL)
            WmsTree->AddTiledRoot(ts_title);
          else
            WmsTree->AddTiledRoot("TileService");
          for (int i = 0; i < nLayers; i++)
            {
              // populating the WMS Catalog [TileService]
              rl2WmsTiledLayerPtr layer =
                get_wms_catalog_tiled_layer(Catalog, i);
              const char *x_name = get_wms_tiled_layer_name(layer);
              WmsTree->AddTiledLayer(layer, x_name);
            }
      } else
        {
          int nLayers = get_wms_catalog_count(Catalog);
          WmsTree->Show(false);
          WmsTree->FlushAll();
          for (int i = 0; i < nLayers; i++)
            {
              // populating the WMS Catalog [ordinary layers]
              rl2WmsLayerPtr layer = get_wms_catalog_layer(Catalog, i);
              const char *x_name = get_wms_layer_title(layer);
              WmsTree->AddLayer(layer, x_name);
            }
        }
      WmsTree->ExpandAll();
      WmsTree->Show(true);
      WmsTree->RootAutoSelect();
      urlCtrl->Enable(false);
      catalogBtn->Enable(false);
      resetBtn->Enable(true);
    }
  if (err_msg != NULL)
    free(err_msg);
}

void WmsDialog::OnReset(wxCommandEvent & WXUNUSED(event))
{
//
// resetting to initial empty state
//
  CurrentLayer = NULL;
  wxTextCtrl *urlCtrl = (wxTextCtrl *) FindWindow(ID_WMS_URL);
  wxButton *catalogBtn = (wxButton *) FindWindow(ID_WMS_CATALOG);
  wxButton *resetBtn = (wxButton *) FindWindow(ID_WMS_RESET);
  if (Catalog != NULL)
    destroy_wms_catalog(Catalog);
  Catalog = NULL;
  urlCtrl->Enable(true);
  catalogBtn->Enable(true);
  resetBtn->Enable(false);
  WmsTree->FlushAll();
  wxHtmlWindow *titleCtrl = (wxHtmlWindow *) FindWindow(ID_WMS_TITLE);
  titleCtrl->SetPage(wxT(""));
  wxHtmlWindow *abstractCtrl = (wxHtmlWindow *) FindWindow(ID_WMS_ABSTRACT);
  abstractCtrl->SetPage(wxT(""));
  wxTextCtrl *nameCtrl = (wxTextCtrl *) FindWindow(ID_WMS_NAME);
  nameCtrl->SetValue(wxT(""));
  wxComboBox *comboCtrl = (wxComboBox *) FindWindow(ID_WMS_CRS);
  comboCtrl->Clear();
  comboCtrl->SetSelection(wxNOT_FOUND);
  comboCtrl->Enable(false);
  comboCtrl = (wxComboBox *) FindWindow(ID_WMS_FORMAT);
  comboCtrl->Clear();
  comboCtrl->SetSelection(wxNOT_FOUND);
  comboCtrl->Enable(false);
  comboCtrl = (wxComboBox *) FindWindow(ID_WMS_STYLE);
  comboCtrl->Clear();
  comboCtrl->SetSelection(wxNOT_FOUND);
  comboCtrl->Enable(false);
  wxRadioBox *trans = (wxRadioBox *) FindWindow(ID_WMS_TRANSPARENT);
  trans->SetSelection(1);
  trans->Enable(false);
  wxCheckBox *tiled = (wxCheckBox *) FindWindow(ID_WMS_TILED);
  tiled->SetValue(false);
  tiled->Enable(false);
  wxRadioBox *versionBox = (wxRadioBox *) FindWindow(ID_WMS_VERSION);
  versionBox->Enable(false);
  wxCheckBox *swapBox = (wxCheckBox *) FindWindow(ID_WMS_SWAP);
  swapBox->Enable(false);
  wxButton *load = (wxButton *) FindWindow(ID_WMS_OK);
  load->Enable(false);
}

int WmsDialog::IsTiled()
{
// return the Tiled mode
  wxCheckBox *tiled = (wxCheckBox *) FindWindow(ID_WMS_TILED);
  if (tiled->GetValue() == false)
    return 0;
  return 1;
}

int WmsDialog::GetTileWidth()
{
// return the TileWidth
  wxSpinCtrl *wCtrl = (wxSpinCtrl *) FindWindow(ID_WMS_WIDTH);
  return wCtrl->GetValue();
}

int WmsDialog::GetTileHeight()
{
// return the TileHeight
  wxSpinCtrl *hCtrl = (wxSpinCtrl *) FindWindow(ID_WMS_HEIGHT);
  return hCtrl->GetValue();
}

int WmsDialog::IsOpaque()
{
// return the Opaque mode
  wxRadioBox *trans = (wxRadioBox *) FindWindow(ID_WMS_TRANSPARENT);
  if (trans->GetSelection() == 0)
    return 0;
  return 1;
}

const char *WmsDialog::GetVersion()
{
// return the Version string
  wxRadioBox *versionBox = (wxRadioBox *) FindWindow(ID_WMS_VERSION);
  if (Version != NULL)
    delete[]Version;
  Version = new char[6];
  switch (versionBox->GetSelection())
    {
      case 0:
        strcpy(Version, "1.0.0");
        break;
      case 1:
        strcpy(Version, "1.1.0");
        break;
      case 2:
        strcpy(Version, "1.1.1");
        break;
      default:
        strcpy(Version, "1.3.0");
        break;
    };
  return Version;
}

const char *WmsDialog::GetStyleName()
{
// return the Style Name string
  wxComboBox *comboCtrl = (wxComboBox *) FindWindow(ID_WMS_STYLE);
  wxString value = comboCtrl->GetValue();
  if (value.Len() == 0)
    return NULL;
  int cnt = get_wms_layer_style_count(CurrentLayer);
  for (int i = 0; i < cnt; i++)
    {
      const char *str = get_wms_layer_style_name(CurrentLayer, i);
      wxString style = wxString::FromUTF8(str);
      if (style == value)
        return str;
    }
  return NULL;
}

const char *WmsDialog::GetStyleTitle()
{
// return the Style Title string
  wxComboBox *comboCtrl = (wxComboBox *) FindWindow(ID_WMS_STYLE);
  wxString value = comboCtrl->GetValue();
  if (value.Len() == 0)
    return NULL;
  int cnt = get_wms_layer_style_count(CurrentLayer);
  for (int i = 0; i < cnt; i++)
    {
      const char *stl = get_wms_layer_style_name(CurrentLayer, i);
      const char *str = get_wms_layer_style_title(CurrentLayer, i);
      wxString style = wxString::FromUTF8(stl);
      if (style == value)
        return str;
    }
  return NULL;
}

const char *WmsDialog::GetStyleAbstract()
{
// return the Style Abstract string
  wxComboBox *comboCtrl = (wxComboBox *) FindWindow(ID_WMS_STYLE);
  wxString value = comboCtrl->GetValue();
  if (value.Len() == 0)
    return NULL;
  int cnt = get_wms_layer_style_count(CurrentLayer);
  for (int i = 0; i < cnt; i++)
    {
      const char *stl = get_wms_layer_style_name(CurrentLayer, i);
      const char *str = get_wms_layer_style_abstract(CurrentLayer, i);
      wxString style = wxString::FromUTF8(stl);
      if (style == value)
        return str;
    }
  return NULL;
}

const char *WmsDialog::GetFormat()
{
// return the Format string
  wxComboBox *comboCtrl = (wxComboBox *) FindWindow(ID_WMS_FORMAT);
  wxString value = comboCtrl->GetValue();
  if (value.Len() == 0)
    return NULL;
  int cnt = get_wms_format_count(Catalog, 1);
  for (int i = 0; i < cnt; i++)
    {
      const char *str = get_wms_format(Catalog, i, 1);
      wxString fmt = wxString::FromUTF8(str);
      if (fmt == value)
        return str;
    }
  return NULL;
}

const char *WmsDialog::GetCRS()
{
// return the CRS string
  wxComboBox *comboCtrl = (wxComboBox *) FindWindow(ID_WMS_CRS);
  wxString value = comboCtrl->GetValue();
  if (value.Len() == 0)
    return NULL;
  int cnt = get_wms_layer_crs_count(CurrentLayer);
  for (int i = 0; i < cnt; i++)
    {
      const char *str = get_wms_layer_crs(CurrentLayer, i);
      wxString crs = wxString::FromUTF8(str);
      if (crs == value)
        return str;
    }
  return NULL;
}

bool WmsDialog::GetStyleByIndex(int idx, const char **name, const char **title,
                                const char **abstract)
{
// return some WMS Format (retrieved by its Index)
  *name = get_wms_layer_style_name(CurrentLayer, idx);
  *title = get_wms_layer_style_title(CurrentLayer, idx);
  *abstract = get_wms_layer_style_abstract(CurrentLayer, idx);
  if (*name == NULL)
    return false;
  return true;
}

WmsBBox *WmsDialog::GetBBoxByCRS(const char *crs)
{
// return some CRS BBox (retrieved by CRS name)
  double minx;
  double miny;
  double maxx;
  double maxy;
  if (get_wms_layer_bbox(CurrentLayer, crs, &minx, &maxx, &miny, &maxy))
    return new WmsBBox(minx, miny, maxx, maxy);
  return NULL;
}

double WmsDialog::GetGeoMinX()
{
// return the WMS Geographic MinX
  double minx;
  double miny;
  double maxx;
  double maxy;
  if (!get_wms_layer_geo_bbox(CurrentLayer, &minx, &maxx, &miny, &maxy))
    return DBL_MAX;
  return minx;
}

double WmsDialog::GetGeoMaxX()
{
// return the WMS Geographic MaxX
  double minx;
  double miny;
  double maxx;
  double maxy;
  if (!get_wms_layer_geo_bbox(CurrentLayer, &minx, &maxx, &miny, &maxy))
    return DBL_MAX;
  return maxx;
}

double WmsDialog::GetGeoMinY()
{
// return the WMS Geographic MinY
  double minx;
  double miny;
  double maxx;
  double maxy;
  if (!get_wms_layer_geo_bbox(CurrentLayer, &minx, &maxx, &miny, &maxy))
    return DBL_MAX;
  return miny;
}

double WmsDialog::GetGeoMaxY()
{
// return the WMS Geographic MaxY
  double minx;
  double miny;
  double maxx;
  double maxy;
  if (!get_wms_layer_geo_bbox(CurrentLayer, &minx, &maxx, &miny, &maxy))
    return DBL_MAX;
  return maxy;
}

double WmsDialog::GetMinX()
{
// return the WMS CRS MinX
  double minx;
  double miny;
  double maxx;
  double maxy;
  const char *crs_str = NULL;
  wxComboBox *comboCtrl = (wxComboBox *) FindWindow(ID_WMS_CRS);
  wxString value = comboCtrl->GetValue();
  if (value.Len() == 0)
    return DBL_MAX;
  int cnt = get_wms_layer_crs_count(CurrentLayer);
  for (int i = 0; i < cnt; i++)
    {
      const char *str = get_wms_layer_crs(CurrentLayer, i);
      wxString crs = wxString::FromUTF8(str);
      if (crs == value)
        {
          crs_str = str;
          break;
        }
    }
  if (crs_str == NULL)
    return DBL_MAX;
  if (!get_wms_layer_bbox(CurrentLayer, crs_str, &minx, &maxx, &miny, &maxy))
    {
      if (!get_wms_layer_geo_bbox(CurrentLayer, &minx, &maxx, &miny, &maxy))
        return DBL_MAX;
      if (MainFrame->BBoxFromLongLat(crs_str, &minx, &maxx, &miny, &maxy) ==
          true)
        {
          if (IsSwapXY() == true)
            return miny;
          else
            return minx;
      } else
        return DBL_MAX;
    }
  return minx;
}

double WmsDialog::GetMaxX()
{
// return the WMS CRS MaxX
  double minx;
  double miny;
  double maxx;
  double maxy;
  const char *crs_str = NULL;
  wxTextCtrl *nameCtrl = (wxTextCtrl *) FindWindow(ID_WMS_NAME);
  wxString name = nameCtrl->GetValue();
  wxComboBox *comboCtrl = (wxComboBox *) FindWindow(ID_WMS_CRS);
  wxString value = comboCtrl->GetValue();
  if (value.Len() == 0)
    return DBL_MAX;
  int cnt = get_wms_layer_crs_count(CurrentLayer);
  for (int i = 0; i < cnt; i++)
    {
      const char *str = get_wms_layer_crs(CurrentLayer, i);
      wxString crs = wxString::FromUTF8(str);
      if (crs == value)
        {
          crs_str = str;
          break;
        }
    }
  if (crs_str == NULL)
    return DBL_MAX;
  if (!get_wms_layer_bbox(CurrentLayer, crs_str, &minx, &maxx, &miny, &maxy))
    {
      if (!get_wms_layer_geo_bbox(CurrentLayer, &minx, &maxx, &miny, &maxy))
        return DBL_MAX;
      if (MainFrame->BBoxFromLongLat(crs_str, &minx, &maxx, &miny, &maxy) ==
          true)
        {
          if (IsSwapXY() == true)
            return maxy;
          else
            return maxx;
      } else
        return DBL_MAX;
    }
  return maxx;
}

double WmsDialog::GetMinY()
{
// return the WMS CRS MinY
  double minx;
  double miny;
  double maxx;
  double maxy;
  const char *crs_str = NULL;
  wxComboBox *comboCtrl = (wxComboBox *) FindWindow(ID_WMS_CRS);
  wxString value = comboCtrl->GetValue();
  if (value.Len() == 0)
    return DBL_MAX;
  int cnt = get_wms_layer_crs_count(CurrentLayer);
  for (int i = 0; i < cnt; i++)
    {
      const char *str = get_wms_layer_crs(CurrentLayer, i);
      wxString crs = wxString::FromUTF8(str);
      if (crs == value)
        {
          crs_str = str;
          break;
        }
    }
  if (crs_str == NULL)
    return DBL_MAX;
  if (!get_wms_layer_bbox(CurrentLayer, crs_str, &minx, &maxx, &miny, &maxy))
    {
      if (!get_wms_layer_geo_bbox(CurrentLayer, &minx, &maxx, &miny, &maxy))
        return DBL_MAX;
      if (MainFrame->BBoxFromLongLat(crs_str, &minx, &maxx, &miny, &maxy) ==
          true)
        {
          if (IsSwapXY() == true)
            return minx;
          else
            return miny;
      } else
        return DBL_MAX;
    }
  return miny;
}

double WmsDialog::GetMaxY()
{
// return the WMS CRS MaxY
  double minx;
  double miny;
  double maxx;
  double maxy;
  const char *crs_str = NULL;
  wxComboBox *comboCtrl = (wxComboBox *) FindWindow(ID_WMS_CRS);
  wxString value = comboCtrl->GetValue();
  if (value.Len() == 0)
    return DBL_MAX;
  int cnt = get_wms_layer_crs_count(CurrentLayer);
  for (int i = 0; i < cnt; i++)
    {
      const char *str = get_wms_layer_crs(CurrentLayer, i);
      wxString crs = wxString::FromUTF8(str);
      if (crs == value)
        {
          crs_str = str;
          break;
        }
    }
  if (crs_str == NULL)
    return DBL_MAX;
  if (!get_wms_layer_bbox(CurrentLayer, crs_str, &minx, &maxx, &miny, &maxy))
    {
      if (!get_wms_layer_geo_bbox(CurrentLayer, &minx, &maxx, &miny, &maxy))
        return DBL_MAX;
      if (MainFrame->BBoxFromLongLat(crs_str, &minx, &maxx, &miny, &maxy) ==
          true)
        {
          if (IsSwapXY() == true)
            return maxx;
          else
            return maxy;
      } else
        return DBL_MAX;
    }
  return maxy;
}

double WmsDialog::GetTiledLayerMinLong()
{
// return the MinLong for a Tiled Layer [TileService]
  double minx;
  double miny;
  double maxx;
  double maxy;
  if (get_wms_tiled_layer_bbox(CurrentTiledLayer, &minx, &miny, &maxx, &maxy))
    return minx;
  return DBL_MAX;
}

double WmsDialog::GetTiledLayerMinLat()
{
// return the MinLat for a Tiled Layer [TileService]
  double minx;
  double miny;
  double maxx;
  double maxy;
  if (get_wms_tiled_layer_bbox(CurrentTiledLayer, &minx, &miny, &maxx, &maxy))
    return miny;
  return DBL_MAX;
}

double WmsDialog::GetTiledLayerMaxLong()
{
// return the MaxLong for a Tiled Layer [TileService]
  double minx;
  double miny;
  double maxx;
  double maxy;
  if (get_wms_tiled_layer_bbox(CurrentTiledLayer, &minx, &miny, &maxx, &maxy))
    return maxx;
  return DBL_MAX;
}

double WmsDialog::GetTiledLayerMaxLat()
{
// return the MaxLat for a Tiled Layer [TileService]
  double minx;
  double miny;
  double maxx;
  double maxy;
  if (get_wms_tiled_layer_bbox(CurrentTiledLayer, &minx, &miny, &maxx, &maxy))
    return maxy;
  return DBL_MAX;
}

const char *WmsDialog::GetTilePatternSRS(int i)
{
// return the SRS from some TilePattern defined within a TiledLayer
  int nPatterns = get_wms_tile_pattern_count(CurrentTiledLayer);
  if (i >= 0 && i < nPatterns)
    return get_wms_tile_pattern_srs(CurrentTiledLayer, i);
  return NULL;
}

int WmsDialog::GetTilePatternWidth(int i)
{
// return the TileWidth from some TilePattern defined within a TiledLayer
  int nPatterns = get_wms_tile_pattern_count(CurrentTiledLayer);
  if (i >= 0 && i < nPatterns)
    return get_wms_tile_pattern_tile_width(CurrentTiledLayer, i);
  return -1;
}

int WmsDialog::GetTilePatternHeight(int i)
{
// return the TileEight from some TilePattern defined within a TiledLayer
  int nPatterns = get_wms_tile_pattern_count(CurrentTiledLayer);
  if (i >= 0 && i < nPatterns)
    return get_wms_tile_pattern_tile_height(CurrentTiledLayer, i);
  return -1;
}

double WmsDialog::GetTilePatternBaseX(int i)
{
// return the BaseX from some TilePattern defined within a TiledLayer
  int nPatterns = get_wms_tile_pattern_count(CurrentTiledLayer);
  if (i >= 0 && i < nPatterns)
    return get_wms_tile_pattern_base_x(CurrentTiledLayer, i);
  return DBL_MAX;
}

double WmsDialog::GetTilePatternBaseY(int i)
{
// return the BaseY from some TilePattern defined within a TiledLayer
  int nPatterns = get_wms_tile_pattern_count(CurrentTiledLayer);
  if (i >= 0 && i < nPatterns)
    return get_wms_tile_pattern_base_y(CurrentTiledLayer, i);
  return DBL_MAX;
}

double WmsDialog::GetTilePatternExtentX(int i)
{
// return the ExtentX from some TilePattern defined within a TiledLayer
  int nPatterns = get_wms_tile_pattern_count(CurrentTiledLayer);
  if (i >= 0 && i < nPatterns)
    return get_wms_tile_pattern_extent_x(CurrentTiledLayer, i);
  return DBL_MAX;
}

double WmsDialog::GetTilePatternExtentY(int i)
{
// return the ExtentY from some TilePattern defined within a TiledLayer
  int nPatterns = get_wms_tile_pattern_count(CurrentTiledLayer);
  if (i >= 0 && i < nPatterns)
    return get_wms_tile_pattern_extent_y(CurrentTiledLayer, i);
  return DBL_MAX;
}

rl2WmsTilePatternPtr WmsDialog::GetTilePatternHandle(int i)
{
// return the Handle from some TilePattern defined within a TiledLayer
  int nPatterns = get_wms_tile_pattern_count(CurrentTiledLayer);
  if (i >= 0 && i < nPatterns)
    return get_wms_tile_pattern_handle(CurrentTiledLayer, i);
  return NULL;
}

void WmsDialog::OnQuit(wxCommandEvent & WXUNUSED(event))
{
//
// all done: 
//
  wxDialog::EndModal(wxID_CANCEL);
}

void WmsDialog::OnWmsOk(wxCommandEvent & WXUNUSED(event))
{
//
// all done: 
//
  wxTextCtrl *proxyCtrl = (wxTextCtrl *) FindWindow(ID_WMS_PROXY);
  if (ProxyEnabled == false)
    HttpProxy = wxT("");
  else
    HttpProxy = proxyCtrl->GetValue();
  wxTextCtrl *urlCtrl = (wxTextCtrl *) FindWindow(ID_WMS_URL);
  URL = urlCtrl->GetValue();
  wxDialog::EndModal(wxID_OK);
}

bool WmsLayerDialog::Create(MyFrame * parent, WmsLayer * lyr)
{
//
// creating the dialog
//
  MainFrame = parent;
  Layer = lyr;
  SwapXY = 0;
  MaxWidth = Layer->GetWmsService()->GetMaxWidth();
  MaxHeight = Layer->GetWmsService()->GetMaxHeight();
  if (MaxWidth <= 0)
    MaxWidth = 2048;
  if (MaxHeight <= 0)
    MaxHeight = 2048;
  if (wxDialog::Create(parent, wxID_ANY, wxT("WMS Layer configuration")) ==
      false)
    return false;
// populates individual controls
  CreateControls();
// sets dialog sizer
  GetSizer()->Fit(this);
  GetSizer()->SetSizeHints(this);
// centers the dialog window
  Centre();
  UpdateSwapXY();
  return true;
}

void WmsLayerDialog::CreateControls()
{
//
// creating individual control and setting initial values
//
  wxBoxSizer *topSizer = new wxBoxSizer(wxVERTICAL);
  this->SetSizer(topSizer);
  wxBoxSizer *boxSizer = new wxBoxSizer(wxVERTICAL);
  topSizer->Add(boxSizer, 0, wxALIGN_CENTER_HORIZONTAL | wxALL, 5);
// selected Layer group box
  wxBoxSizer *kkSizer = new wxBoxSizer(wxHORIZONTAL);
  boxSizer->Add(kkSizer, 0, wxALIGN_TOP | wxALL, 5);
  wxStaticBox *lyrBox = new wxStaticBox(this, wxID_STATIC,
                                        wxT("WMS Layer Configuration"),
                                        wxDefaultPosition,
                                        wxDefaultSize);
  wxBoxSizer *lyrBox0Sizer = new wxStaticBoxSizer(lyrBox, wxHORIZONTAL);
  kkSizer->Add(lyrBox0Sizer, 0, wxALIGN_CENTER_HORIZONTAL | wxALL, 0);
// Second row: Layer name
  wxBoxSizer *lyrBoxSizer = new wxBoxSizer(wxVERTICAL);
  lyrBox0Sizer->Add(lyrBoxSizer, 0, wxALIGN_CENTER_VERTICAL | wxALL, 0);
  wxBoxSizer *nameSizer = new wxBoxSizer(wxHORIZONTAL);
  lyrBoxSizer->Add(nameSizer, 0, wxALIGN_CENTER_VERTICAL | wxALL, 0);
  wxStaticBox *nameBox = new wxStaticBox(this, wxID_ANY,
                                         wxT("WMS Layer"),
                                         wxDefaultPosition,
                                         wxDefaultSize);
  wxBoxSizer *nameBoxSizer = new wxStaticBoxSizer(nameBox, wxHORIZONTAL);
  nameSizer->Add(nameBoxSizer, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);
  wxString lyrName = wxString::FromUTF8(Layer->GetName());
  wxTextCtrl *nameCtrl = new wxTextCtrl(this, ID_WMS_NAME, lyrName,
                                        wxDefaultPosition, wxSize(570, 22),
                                        wxTE_READONLY);
  nameBoxSizer->Add(nameCtrl, 0, wxALIGN_CENTER_VERTICAL | wxALL, 0);
  wxStaticBox *swapBox = new wxStaticBox(this, wxID_ANY,
                                         wxT("Swap Axes"),
                                         wxDefaultPosition,
                                         wxDefaultSize);
  wxBoxSizer *swapBoxSizer = new wxStaticBoxSizer(swapBox, wxHORIZONTAL);
  nameSizer->Add(swapBoxSizer, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);
  wxCheckBox *swapCtrl = new wxCheckBox(this, ID_WMS_SWAP,
                                        wxT("Swap Y,X"),
                                        wxDefaultPosition, wxDefaultSize);
  if (Layer->IsSwapXY() == 0)
    swapCtrl->SetValue(false);
  else
    swapCtrl->SetValue(true);
  swapBoxSizer->Add(swapCtrl, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);
// First row: WMS options
  wxBoxSizer *wmsSizer = new wxBoxSizer(wxHORIZONTAL);
  lyrBoxSizer->Add(wmsSizer, 0, wxALIGN_CENTER_HORIZONTAL | wxALL, 0);
  wxString ver[4];
  ver[0] = wxT("WMS &1.0.0");
  ver[1] = wxT("WMS &1.1.0");
  ver[2] = wxT("WMS &1.1.1");
  ver[3] = wxT("WMS &1.3.0");
  wxRadioBox *versionBox = new wxRadioBox(this, ID_WMS_VERSION,
                                          wxT("WMS &Version"),
                                          wxDefaultPosition,
                                          wxDefaultSize, 4,
                                          ver, 4,
                                          wxRA_SPECIFY_ROWS);
  const char *version = Layer->GetWmsService()->GetVersion();
  if (version == NULL)
    {
      versionBox->Enable(1, false);
      versionBox->Enable(2, false);
      versionBox->Enable(3, false);
  } else
    {
      if (strcmp(version, "1.1.0") == 0)
        {
          versionBox->Enable(2, false);
          versionBox->Enable(3, false);
      } else if (strcmp(version, "1.1.1") == 0)
        {
          versionBox->Enable(3, false);
      } else if (strcmp(version, "1.3.0") == 0)
        ;
      else
        {
          versionBox->Enable(1, false);
          versionBox->Enable(2, false);
          versionBox->Enable(3, false);
        }
    }
  if (strcmp(Layer->GetVersion(), "1.0.0") == 0)
    versionBox->SetSelection(0);
  else if (strcmp(Layer->GetVersion(), "1.1.0") == 0)
    versionBox->SetSelection(1);
  else if (strcmp(Layer->GetVersion(), "1.1.1") == 0)
    versionBox->SetSelection(2);
  else
    versionBox->SetSelection(3);
  wmsSizer->Add(versionBox, 0, wxALIGN_TOP | wxALL, 5);
  wxBoxSizer *xxSizer = new wxBoxSizer(wxVERTICAL);
  wmsSizer->Add(xxSizer, 0, wxALIGN_TOP | wxALL, 5);
  wxStaticBox *crsBox = new wxStaticBox(this, wxID_STATIC,
                                        wxT("Reference System"),
                                        wxDefaultPosition,
                                        wxDefaultSize);
  wxBoxSizer *crsBoxSizer = new wxStaticBoxSizer(crsBox, wxVERTICAL);
  xxSizer->Add(crsBoxSizer, 0, wxALIGN_CENTER_VERTICAL | wxALL, 2);
  wxComboBox *crsList =
    new wxComboBox(this, ID_WMS_CRS, wxT(""), wxDefaultPosition,
                   wxSize(150, 21), 0, NULL, wxCB_DROPDOWN | wxCB_READONLY);
  for (int s = 0; s < Layer->CountCRS(); s++)
    {
      const char *crs = Layer->GetCrsByIndex(s);
      wxString str = wxString::FromUTF8(crs);
      crsList->Append(str);
      if (strcmp(Layer->GetCRS(), crs) == 0)
        crsList->SetSelection(s);
    }
  crsBoxSizer->Add(crsList, 0, wxALIGN_CENTER_VERTICAL | wxALL, 2);
  wxStaticBox *stlBox = new wxStaticBox(this, wxID_STATIC,
                                        wxT("Style"),
                                        wxDefaultPosition,
                                        wxDefaultSize);
  wxBoxSizer *stlBoxSizer = new wxStaticBoxSizer(stlBox, wxVERTICAL);
  xxSizer->Add(stlBoxSizer, 0, wxALIGN_CENTER_VERTICAL | wxALL, 2);
  wxComboBox *stlList =
    new wxComboBox(this, ID_WMS_STYLE, wxT(""), wxDefaultPosition,
                   wxSize(150, 21), 0, NULL, wxCB_DROPDOWN | wxCB_READONLY);
  for (int s = 0; s < Layer->CountStyles(); s++)
    {
      const char *name;
      const char *title;
      const char *abstract;
      Layer->GetStyleByIndex(s, &name, &title, &abstract);
      wxString str = wxString::FromUTF8(name);
      stlList->Append(str);
      if (strcmp(Layer->GetStyleName(), name) == 0)
        stlList->SetSelection(s);
    }
  stlBoxSizer->Add(stlList, 0, wxALIGN_CENTER_VERTICAL | wxALL, 2);
  wxBoxSizer *yySizer = new wxBoxSizer(wxVERTICAL);
  wmsSizer->Add(yySizer, 0, wxALIGN_TOP | wxALL, 5);
  wxStaticBox *fmtBox = new wxStaticBox(this, wxID_STATIC,
                                        wxT("Image Format"),
                                        wxDefaultPosition,
                                        wxDefaultSize);
  wxBoxSizer *fmtBoxSizer = new wxStaticBoxSizer(fmtBox, wxVERTICAL);
  yySizer->Add(fmtBoxSizer, 0, wxALIGN_CENTER_VERTICAL | wxALL, 2);
  wxComboBox *fmtList =
    new wxComboBox(this, ID_WMS_FORMAT, wxT(""), wxDefaultPosition,
                   wxSize(150, 21), 0, NULL, wxCB_DROPDOWN | wxCB_READONLY);
  for (int s = 0; s < Layer->GetWmsService()->CountFormats(); s++)
    {
      const char *fmt = Layer->GetWmsService()->GetFormatByIndex(s);
      wxString str = wxString::FromUTF8(fmt);
      fmtList->Append(str);
      if (strcmp(Layer->GetFormat(), fmt) == 0)
        fmtList->SetSelection(s);
    }
  fmtBoxSizer->Add(fmtList, 0, wxALIGN_CENTER_VERTICAL | wxALL, 2);
  wxString trans[2];
  trans[0] = wxT("yes");
  trans[1] = wxT("opaque");
  wxRadioBox *transBox = new wxRadioBox(this, ID_WMS_TRANSPARENT,
                                        wxT("Transparent Layer"),
                                        wxDefaultPosition,
                                        wxDefaultSize, 2,
                                        trans, 2,
                                        wxRA_SPECIFY_COLS);
  if (Layer->IsOpaque() == 0)
    transBox->SetSelection(0);
  else
    transBox->SetSelection(1);
  yySizer->Add(transBox, 0, wxALIGN_CENTER_HORIZONTAL | wxALL, 2);
  wxStaticBox *tileBox = new wxStaticBox(this, wxID_STATIC,
                                         wxT("Tile size"),
                                         wxDefaultPosition,
                                         wxDefaultSize);
  wxBoxSizer *tileBoxSizer = new wxStaticBoxSizer(tileBox, wxVERTICAL);
  wmsSizer->Add(tileBoxSizer, 0, wxALIGN_TOP | wxALL, 5);
  wxCheckBox *tiledCtrl = new wxCheckBox(this, ID_WMS_TILED,
                                         wxT("enable Tiles"),
                                         wxDefaultPosition, wxDefaultSize);
  if (Layer->IsTiled() == 0)
    tiledCtrl->SetValue(false);
  else
    tiledCtrl->SetValue(true);
  tileBoxSizer->Add(tiledCtrl, 0, wxALIGN_CENTER_HORIZONTAL | wxALL, 5);
  wxBoxSizer *tile1BoxSizer = new wxBoxSizer(wxHORIZONTAL);
  tileBoxSizer->Add(tile1BoxSizer, 0, wxALIGN_RIGHT | wxALL, 0);
  wxStaticText *wLabel = new wxStaticText(this, wxID_ANY, wxT("&Width:"),
                                          wxDefaultPosition, wxDefaultSize,
                                          wxALIGN_RIGHT);
  tile1BoxSizer->Add(wLabel, 0, wxALIGN_CENTER_VERTICAL | wxALL, 2);
  wxSpinCtrl *wCtrl = new wxSpinCtrl(this, ID_WMS_WIDTH, wxEmptyString,
                                     wxDefaultPosition, wxSize(80, 20),
                                     wxSP_ARROW_KEYS,
                                     0, 0, 0);
  if (Layer->IsTiled() == 0)
    {
      wCtrl->Enable(false);
      wCtrl->SetValue(wxT(""));
  } else
    {
      wCtrl->SetRange(256, MaxWidth);
      char t_w[64];
      sprintf(t_w, "%d", Layer->GetTileWidth());
      wxString tW = wxString::FromUTF8(t_w);
      wCtrl->SetValue(tW);
    }
  tile1BoxSizer->Add(wCtrl, 0, wxALIGN_CENTER_VERTICAL | wxALL, 2);
  wxBoxSizer *tile2BoxSizer = new wxBoxSizer(wxHORIZONTAL);
  tileBoxSizer->Add(tile2BoxSizer, 0, wxALIGN_RIGHT | wxALL, 0);
  wxStaticText *hLabel = new wxStaticText(this, wxID_ANY, wxT("&Height:"),
                                          wxDefaultPosition, wxDefaultSize,
                                          wxALIGN_RIGHT);
  tile2BoxSizer->Add(hLabel, 0, wxALIGN_CENTER_VERTICAL | wxALL, 2);
  wxSpinCtrl *hCtrl = new wxSpinCtrl(this, ID_WMS_HEIGHT, wxEmptyString,
                                     wxDefaultPosition, wxSize(80, 20),
                                     wxSP_ARROW_KEYS,
                                     0, 0, 0);
  tile2BoxSizer->Add(hCtrl, 0, wxALIGN_CENTER_VERTICAL | wxALL, 2);
  if (Layer->IsTiled() == 0)
    {
      hCtrl->Enable(false);
      hCtrl->SetValue(wxT(""));
  } else
    {
      hCtrl->SetRange(256, MaxHeight);
      char t_h[64];
      sprintf(t_h, "%d", Layer->GetTileHeight());
      wxString tH = wxString::FromUTF8(t_h);
      hCtrl->SetValue(tH);
    }
  wxBoxSizer *buttonSizer = new wxBoxSizer(wxVERTICAL);
  wmsSizer->Add(buttonSizer, 0, wxALIGN_CENTER_VERTICAL | wxALL, 0);
  wxButton *wms_ok = new wxButton(this, ID_WMS_OK, wxT("&Apply Changes"));
  buttonSizer->Add(wms_ok, 0, wxALIGN_CENTER_HORIZONTAL | wxALL, 5);
  wxButton *wms_quit = new wxButton(this, wxID_CANCEL, wxT("&Quit"));
  buttonSizer->Add(wms_quit, 0, wxALIGN_CENTER_HORIZONTAL | wxALL, 5);
// appends event handlers
  Connect(ID_WMS_CRS, wxEVT_COMMAND_COMBOBOX_SELECTED,
          (wxObjectEventFunction) & WmsLayerDialog::OnCrsChanged);
  Connect(ID_WMS_VERSION, wxEVT_COMMAND_RADIOBOX_SELECTED,
          (wxObjectEventFunction) & WmsLayerDialog::OnVersionChanged);
  Connect(ID_WMS_SWAP, wxEVT_COMMAND_CHECKBOX_CLICKED,
          (wxObjectEventFunction) & WmsLayerDialog::OnSwapXYChanged);
  Connect(ID_WMS_TILED, wxEVT_COMMAND_CHECKBOX_CLICKED,
          (wxObjectEventFunction) & WmsLayerDialog::OnTiledChanged);
  Connect(ID_WMS_OK, wxEVT_COMMAND_BUTTON_CLICKED,
          (wxObjectEventFunction) & WmsLayerDialog::OnWmsOk);
  Connect(wxID_CANCEL, wxEVT_COMMAND_BUTTON_CLICKED,
          (wxObjectEventFunction) & WmsLayerDialog::OnQuit);
}

void WmsLayerDialog::OnCrsChanged(wxCommandEvent & WXUNUSED(event))
{
//
// CRS changed: updating SWAP XY
//
  UpdateSwapXY();
}

void WmsLayerDialog::OnVersionChanged(wxCommandEvent & WXUNUSED(event))
{
//
// Version changed: updating SWAP XY
//
  UpdateSwapXY();
}

void WmsLayerDialog::UpdateSwapXY()
{
//
// updating SWAP XY
//
  wxCheckBox *swapBox = (wxCheckBox *) FindWindow(ID_WMS_SWAP);
  wxRadioBox *versionBox = (wxRadioBox *) FindWindow(ID_WMS_VERSION);
  wxComboBox *comboCtrl = (wxComboBox *) FindWindow(ID_WMS_CRS);
  if (versionBox->GetSelection() != 3)
    {
      swapBox->SetValue(false);
      SwapXY = 0;
      return;
    };
  wxString value = comboCtrl->GetValue();
  char *crs = new char[value.Len() + 1];
  strcpy(crs, value.ToUTF8());
  if (MainFrame->IsSwapXYcrs(crs) == true)
    {
      swapBox->SetValue(true);
      SwapXY = 1;
  } else
    {
      swapBox->SetValue(false);
      SwapXY = 0;
    }
  delete[]crs;
}

void WmsLayerDialog::OnSwapXYChanged(wxCommandEvent & WXUNUSED(event))
{
//
// Swap XY selection changed
//
  if (SwapXY == 0)
    SwapXY = 1;
  else
    SwapXY = 0;
}

void WmsLayerDialog::OnTiledChanged(wxCommandEvent & WXUNUSED(event))
{
//
// Monolithic / Tiled selection changed
//
  wxCheckBox *tiledCtrl = (wxCheckBox *) FindWindow(ID_WMS_TILED);
  wxSpinCtrl *wCtrl = (wxSpinCtrl *) FindWindow(ID_WMS_WIDTH);
  wxSpinCtrl *hCtrl = (wxSpinCtrl *) FindWindow(ID_WMS_HEIGHT);
  if (tiledCtrl->GetValue() == false)
    {
      wCtrl->SetRange(0, 0);
      wCtrl->SetValue(wxT(""));
      wCtrl->Enable(false);
      hCtrl->SetRange(0, 0);
      hCtrl->SetValue(wxT(""));
      hCtrl->Enable(false);
  } else
    {
      wCtrl->SetRange(256, MaxWidth);
      wCtrl->SetValue(wxT("512"));
      wCtrl->Enable(true);
      hCtrl->SetRange(256, MaxHeight);
      hCtrl->SetValue(wxT("512"));
      hCtrl->Enable(true);
    }
}

int WmsLayerDialog::IsTiled()
{
// return the Tiled mode
  wxCheckBox *tiled = (wxCheckBox *) FindWindow(ID_WMS_TILED);
  if (tiled->GetValue() == false)
    return 0;
  return 1;
}

int WmsLayerDialog::GetTileWidth()
{
// return the TileWidth
  wxSpinCtrl *wCtrl = (wxSpinCtrl *) FindWindow(ID_WMS_WIDTH);
  return wCtrl->GetValue();
}

int WmsLayerDialog::GetTileHeight()
{
// return the TileHeight
  wxSpinCtrl *hCtrl = (wxSpinCtrl *) FindWindow(ID_WMS_HEIGHT);
  return hCtrl->GetValue();
}

int WmsLayerDialog::IsOpaque()
{
// return the Opaque mode
  wxRadioBox *trans = (wxRadioBox *) FindWindow(ID_WMS_TRANSPARENT);
  if (trans->GetSelection() == 0)
    return 0;
  return 1;
}

const char *WmsLayerDialog::GetVersion()
{
// return the Version string
  wxRadioBox *versionBox = (wxRadioBox *) FindWindow(ID_WMS_VERSION);
  if (Version != NULL)
    delete[]Version;
  Version = new char[6];
  switch (versionBox->GetSelection())
    {
      case 0:
        strcpy(Version, "1.0.0");
        break;
      case 1:
        strcpy(Version, "1.1.0");
        break;
      case 2:
        strcpy(Version, "1.1.1");
        break;
      default:
        strcpy(Version, "1.3.0");
        break;
    };
  return Version;
}

const char *WmsLayerDialog::GetStyleName()
{
// return the Style Name string
  wxComboBox *comboCtrl = (wxComboBox *) FindWindow(ID_WMS_STYLE);
  wxString value = comboCtrl->GetValue();
  if (value.Len() == 0)
    return NULL;
  for (int i = 0; i < Layer->CountStyles(); i++)
    {
      const char *name;
      const char *title;
      const char *abstract;
      Layer->GetStyleByIndex(i, &name, &title, &abstract);
      wxString str = wxString::FromUTF8(name);
      if (str == value)
        return name;
    }
  return NULL;
}

const char *WmsLayerDialog::GetStyleTitle()
{
// return the Style Title string
  wxComboBox *comboCtrl = (wxComboBox *) FindWindow(ID_WMS_STYLE);
  wxString value = comboCtrl->GetValue();
  if (value.Len() == 0)
    return NULL;
  for (int i = 0; i < Layer->CountStyles(); i++)
    {
      const char *name;
      const char *title;
      const char *abstract;
      Layer->GetStyleByIndex(i, &name, &title, &abstract);
      wxString str = wxString::FromUTF8(name);
      if (str == value)
        return title;
    }
  return NULL;
}

const char *WmsLayerDialog::GetStyleAbstract()
{
// return the Style Abstract string
  wxComboBox *comboCtrl = (wxComboBox *) FindWindow(ID_WMS_STYLE);
  wxString value = comboCtrl->GetValue();
  if (value.Len() == 0)
    return NULL;
  for (int i = 0; i < Layer->CountStyles(); i++)
    {
      const char *name;
      const char *title;
      const char *abstract;
      Layer->GetStyleByIndex(i, &name, &title, &abstract);
      wxString str = wxString::FromUTF8(name);
      if (str == value)
        return abstract;
    }
  return NULL;
}

const char *WmsLayerDialog::GetFormat()
{
// return the Format string
  wxComboBox *comboCtrl = (wxComboBox *) FindWindow(ID_WMS_FORMAT);
  wxString value = comboCtrl->GetValue();
  if (value.Len() == 0)
    return NULL;
  int cnt = Layer->GetWmsService()->CountFormats();
  for (int i = 0; i < cnt; i++)
    {
      const char *str = Layer->GetWmsService()->GetFormatByIndex(i);
      wxString fmt = wxString::FromUTF8(str);
      if (fmt == value)
        return str;
    }
  return NULL;
}

const char *WmsLayerDialog::GetCRS()
{
// return the CRS string
  wxComboBox *comboCtrl = (wxComboBox *) FindWindow(ID_WMS_CRS);
  wxString value = comboCtrl->GetValue();
  if (value.Len() == 0)
    return NULL;
  for (int i = 0; i < Layer->CountCRS(); i++)
    {
      const char *str = Layer->GetCrsByIndex(i);
      wxString crs = wxString::FromUTF8(str);
      if (crs == value)
        return str;
    }
  return NULL;
}

double WmsLayerDialog::GetMinX()
{
// return the WMS CRS MinX
  wxComboBox *comboCtrl = (wxComboBox *) FindWindow(ID_WMS_CRS);
  wxString value = comboCtrl->GetValue();
  if (value.Len() == 0)
    return DBL_MAX;
  for (int i = 0; i < Layer->CountCRS(); i++)
    {
      const char *str = Layer->GetCrsByIndex(i);
      wxString crs = wxString::FromUTF8(str);
      if (crs == value)
        {
          double minx;
          double miny;
          double maxx;
          double maxy;
          if (Layer->HasBBox(str) == true)
            {
              Layer->GetBBox(str, &minx, &miny, &maxx, &maxy);
              return minx;
            }
          minx = Layer->GetGeoMinX();
          miny = Layer->GetGeoMinY();
          maxx = Layer->GetGeoMaxX();
          maxy = Layer->GetGeoMaxY();
          if (MainFrame->BBoxFromLongLat(str, &minx, &maxx, &miny, &maxy) ==
              true)
            {
              if (IsSwapXY() == true)
                return miny;
              else
                return minx;
            }
        }
    }
  return DBL_MAX;
}

double WmsLayerDialog::GetMinY()
{
// return the WMS CRS MinY
  wxComboBox *comboCtrl = (wxComboBox *) FindWindow(ID_WMS_CRS);
  wxString value = comboCtrl->GetValue();
  if (value.Len() == 0)
    return DBL_MAX;
  for (int i = 0; i < Layer->CountCRS(); i++)
    {
      const char *str = Layer->GetCrsByIndex(i);
      wxString crs = wxString::FromUTF8(str);
      if (crs == value)
        {
          double minx;
          double miny;
          double maxx;
          double maxy;
          if (Layer->HasBBox(str) == true)
            {
              Layer->GetBBox(str, &minx, &miny, &maxx, &maxy);
              return miny;
            }
          minx = Layer->GetGeoMinX();
          miny = Layer->GetGeoMinY();
          maxx = Layer->GetGeoMaxX();
          maxy = Layer->GetGeoMaxY();
          if (MainFrame->BBoxFromLongLat(str, &minx, &maxx, &miny, &maxy) ==
              true)
            {
              if (IsSwapXY() == true)
                return minx;
              else
                return miny;
            }
        }
    }
  return DBL_MAX;
}

double WmsLayerDialog::GetMaxX()
{
// return the WMS CRS MaxX
  wxComboBox *comboCtrl = (wxComboBox *) FindWindow(ID_WMS_CRS);
  wxString value = comboCtrl->GetValue();
  if (value.Len() == 0)
    return DBL_MAX;
  for (int i = 0; i < Layer->CountCRS(); i++)
    {
      const char *str = Layer->GetCrsByIndex(i);
      wxString crs = wxString::FromUTF8(str);
      if (crs == value)
        {
          double minx;
          double miny;
          double maxx;
          double maxy;
          if (Layer->HasBBox(str) == true)
            {
              Layer->GetBBox(str, &minx, &miny, &maxx, &maxy);
              return maxx;
            }
          minx = Layer->GetGeoMinX();
          miny = Layer->GetGeoMinY();
          maxx = Layer->GetGeoMaxX();
          maxy = Layer->GetGeoMaxY();
          if (MainFrame->BBoxFromLongLat(str, &minx, &maxx, &miny, &maxy) ==
              true)
            {
              if (IsSwapXY() == true)
                return maxy;
              else
                return maxx;
            }
        }
    }
  return DBL_MAX;
}

double WmsLayerDialog::GetMaxY()
{
// return the WMS CRS MaxY
  wxComboBox *comboCtrl = (wxComboBox *) FindWindow(ID_WMS_CRS);
  wxString value = comboCtrl->GetValue();
  if (value.Len() == 0)
    return DBL_MAX;
  for (int i = 0; i < Layer->CountCRS(); i++)
    {
      const char *str = Layer->GetCrsByIndex(i);
      wxString crs = wxString::FromUTF8(str);
      if (crs == value)
        {
          double minx;
          double miny;
          double maxx;
          double maxy;
          if (Layer->HasBBox(str) == true)
            {
              Layer->GetBBox(str, &minx, &miny, &maxx, &maxy);
              return maxy;
            }
          minx = Layer->GetGeoMinX();
          miny = Layer->GetGeoMinY();
          maxx = Layer->GetGeoMaxX();
          maxy = Layer->GetGeoMaxY();
          if (MainFrame->BBoxFromLongLat(str, &minx, &maxx, &miny, &maxy) ==
              true)
            {
              if (IsSwapXY() == true)
                return maxx;
              else
                return maxy;
            }
        }
    }
  return DBL_MAX;
}

void WmsLayerDialog::OnQuit(wxCommandEvent & WXUNUSED(event))
{
//
// all done: 
//
  wxDialog::EndModal(wxID_CANCEL);
}

void WmsLayerDialog::OnWmsOk(wxCommandEvent & WXUNUSED(event))
{
//
// all done: 
//
  wxDialog::EndModal(wxID_OK);
}

bool WmsInfoDialog::Create(MyFrame * parent, WmsLayer * layer)
{
//
// creating the dialog
//
  MainFrame = parent;
  Layer = layer;
  if (wxDialog::Create(parent, wxID_ANY, wxT("WMS Layer Infos"),
                       wxDefaultPosition, wxDefaultSize,
                       wxDEFAULT_DIALOG_STYLE | wxRESIZE_BORDER) == false)
    return false;
// populates individual controls
  CreateControls();
// sets dialog sizer
  GetSizer()->Fit(this);
  GetSizer()->SetSizeHints(this);
// centers the dialog window
  Centre();
  return true;
}

void WmsInfoDialog::CreateControls()
{
//
// creating individual control and setting initial values
//
  wxBoxSizer *topSizer = new wxBoxSizer(wxVERTICAL);
  this->SetSizer(topSizer);
  wxBoxSizer *boxSizer = new wxBoxSizer(wxVERTICAL);
  topSizer->Add(boxSizer, 0, wxALIGN_CENTER_HORIZONTAL | wxALL, 5);
  HtmlCtrl = new wxHtmlWindow(this, wxID_ANY,
                              wxDefaultPosition, wxSize(680, 400));
  boxSizer->Add(HtmlCtrl, 0, wxALIGN_RIGHT | wxALL, 5);
  WmsService *service = Layer->GetWmsService();
  wxString html =
    wxT("<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">");
  html += wxT("<html>");
  html += wxT("<head>");
  html +=
    wxT
    ("<meta content=\"text/html; charset=UTF-8\" http-equiv=\"content-type\">");
  html += wxT("<title>WMS Metadata</title>");
  html += wxT("</head>");
  html += wxT("<body bgcolor=\"#f8f8e8\">");
  html += wxT("<h2>Service info</h2>\n");
  html += wxT("<table>\n");
  wxString str = wxString::FromUTF8(service->GetName());
  html += wxT("<tr><td><b>Name</b></td><td>") + str + wxT("</td></tr>\n");
  str = wxString::FromUTF8(service->GetTitle());
  html += wxT("<tr><td><b>Title</b></td><td>") + str + wxT("</td></tr>\n");
  str = wxString::FromUTF8(service->GetAbstract());
  html += wxT("<tr><td><b>Abstract</b></td><td>") + str + wxT("</td></tr>\n");
  html += wxT("<tr><td></td><td><hr></td></tr>\n");
  html += wxT("<tr><td>Request</td><td><b>GetMap</b></td></tr>\n");
  str = wxString::FromUTF8(service->GetURL_GetMap_Get());
  html +=
    wxT("<tr><td><b>GET</b></td><td><a href=\"") + str + wxT("\">") + str +
    wxT("</a></td></tr>\n");
  str = wxString::FromUTF8(service->GetURL_GetMap_Post());
  html +=
    wxT("<tr><td><b>POST</b></td><td><a href=\"") + str + wxT("\">") + str +
    wxT("</a></td></tr>\n");
  if (service->GetURL_GetTileService_Get() != NULL
      || service->GetURL_GetTileService_Post())
    {
      // TileService
      html += wxT("<tr><td></td><td><hr></td></tr>\n");
      html += wxT("<tr><td>Request</td><td><b>GetTileService</b></td></tr>\n");
      str = wxString::FromUTF8(service->GetURL_GetTileService_Get());
      html +=
        wxT("<tr><td><b>GET</b></td><td><a href=\"") + str + wxT("\">") + str +
        wxT("</a></td></tr>\n");
      str = wxString::FromUTF8(service->GetURL_GetTileService_Post());
      html +=
        wxT("<tr><td><b>POST</b></td><td><a href=\"") + str + wxT("\">") + str +
        wxT("</a></td></tr>\n");
    }
  html += wxT("<tr><td></td><td><hr></td></tr>\n");
  html += wxT("<tr><td>Request</td><td><b>GetFeatureInfo</b></td></tr>\n");
  str = wxString::FromUTF8(service->GetURL_GetFeatureInfo_Get());
  html +=
    wxT("<tr><td><b>GET</b></td><td><a href=\"") + str + wxT("\">") + str +
    wxT("</a></td></tr>\n");
  str = wxString::FromUTF8(service->GetURL_GetFeatureInfo_Post());
  html +=
    wxT("<tr><td><b>POST</b></td><td><a href=\"") + str + wxT("\">") + str +
    wxT("</a></td></tr>\n");
  html += wxT("<tr><td></td><td><hr></td></tr>\n");
  html += wxT("<tr><td></td><td><b>Contact Information</b></td></tr>\n");
  str = wxString::FromUTF8(service->GetContactPerson());
  html +=
    wxT("<tr><td><b>Contact Person</b></td><td>") + str + wxT("</td></tr>\n");
  str = wxString::FromUTF8(service->GetContactOrganization());
  html +=
    wxT("<tr><td><b>Contact Organization</b></td><td>") + str +
    wxT("</td></tr>\n");
  str = wxString::FromUTF8(service->GetContactPosition());
  html +=
    wxT("<tr><td><b>Contact Position</b></td><td>") + str + wxT("</td></tr>\n");
  html += wxT("<tr><td></td><td><hr></td></tr>\n");
  html += wxT("<tr><td></td><td><b>Contact Address</b></td></tr>\n");
  str = wxString::FromUTF8(service->GetPostalAddress());
  html +=
    wxT("<tr><td><b>Postal Address</b></td><td>") + str + wxT("</td></tr>\n");
  str = wxString::FromUTF8(service->GetCity());
  html += wxT("<tr><td><b>City</b></td><td>") + str + wxT("</td></tr>\n");
  str = wxString::FromUTF8(service->GetStateProvince());
  html +=
    wxT("<tr><td><b>State / Province</b></td><td>") + str + wxT("</td></tr>\n");
  str = wxString::FromUTF8(service->GetPostCode());
  html += wxT("<tr><td><b>Post Code</b></td><td>") + str + wxT("</td></tr>\n");
  str = wxString::FromUTF8(service->GetCountry());
  html += wxT("<tr><td><b>Country</b></td><td>") + str + wxT("</td></tr>\n");
  html += wxT("<tr><td></td><td><hr></td></tr>\n");
  str = wxString::FromUTF8(service->GetVoiceTelephone());
  html +=
    wxT("<tr><td><b>Voice Telephone</b></td><td>") + str + wxT("</td></tr>\n");
  str = wxString::FromUTF8(service->GetFaxTelephone());
  html +=
    wxT("<tr><td><b>Fax Telephone</b></td><td>") + str + wxT("</td></tr>\n");
  str = wxString::FromUTF8(service->GetEMailAddress());
  html +=
    wxT("<tr><td><b>e-mail Address</b></td><td>") + str + wxT("</td></tr>\n");
  html += wxT("<tr><td></td><td><hr></td></tr>\n");
  str = wxString::FromUTF8(service->GetFees());
  html += wxT("<tr><td><b>Fees</b></td><td>") + str + wxT("</td></tr>\n");
  str = wxString::FromUTF8(service->GetAccessConstraints());
  html +=
    wxT("<tr><td><b>Access Constraints</b></td><td>") + str +
    wxT("</td></tr>\n");
  char dummy[1024];
  int max = service->GetLayerLimit();
  if (max > 0)
    sprintf(dummy, "%d", max);
  else
    *dummy = '\0';
  str = wxString::FromUTF8(dummy);
  html +=
    wxT("<tr><td><b>Layer Limit</b></td><td>") + str + wxT("</td></tr>\n");
  max = service->GetMaxWidth();
  if (max > 0)
    sprintf(dummy, "%d", max);
  else
    *dummy = '\0';
  str = wxString::FromUTF8(dummy);
  html += wxT("<tr><td><b>Max Width</b></td><td>") + str + wxT("</td></tr>\n");
  max = service->GetMaxHeight();
  if (max > 0)
    sprintf(dummy, "%d", max);
  else
    *dummy = '\0';
  html += wxT("<tr><td><b>Max Height</b></td><td>") + str + wxT("</td></tr>\n");
  html += wxT("</table>\n");
  if (Layer->GetTileServiceLayer() != NULL)
    {
      // TileService info
      html += wxT("<hr><h2>Tile Service info</h2>\n");
      html += wxT("<table>\n");
      html +=
        wxT
        ("<tr><td colspan=\"2\" align=\"center\"><h3>Tile Service</h3></td></tr>\n");
      str = wxString::FromUTF8(service->GetTileServiceName());
      html += wxT("<tr><td><b>Name</b></td><td>") + str + wxT("</td></tr>\n");
      str = wxString::FromUTF8(service->GetTileServiceTitle());
      html += wxT("<tr><td><b>Title</b></td><td>") + str + wxT("</td></tr>\n");
      str = wxString::FromUTF8(service->GetTileServiceAbstract());
      html +=
        wxT("<tr><td><b>Abstract</b></td><td>") + str + wxT("</td></tr>\n");
      html +=
        wxT
        ("<tr><td colspan=\"2\" align=\"center\"><h3>Tiled Group</h3></td></tr>\n");
      str = wxString::FromUTF8(Layer->GetTileServiceLayer()->GetName());
      html += wxT("<tr><td><b>Name</b></td><td>") + str + wxT("</td></tr>\n");
      str = wxString::FromUTF8(Layer->GetTileServiceLayer()->GetTitle());
      html += wxT("<tr><td><b>Title</b></td><td>") + str + wxT("</td></tr>\n");
      str = wxString::FromUTF8(Layer->GetTileServiceLayer()->GetAbstract());
      html +=
        wxT("<tr><td><b>Abstract</b></td><td>") + str + wxT("</td></tr>\n");
      str = wxString::FromUTF8(Layer->GetTileServiceLayer()->GetPad());
      html += wxT("<tr><td><b>Pad</b></td><td>") + str + wxT("</td></tr>\n");
      str = wxString::FromUTF8(Layer->GetTileServiceLayer()->GetBands());
      html += wxT("<tr><td><b>Bands</b></td><td>") + str + wxT("</td></tr>\n");
      str = wxString::FromUTF8(Layer->GetTileServiceLayer()->GetDataType());
      html +=
        wxT("<tr><td><b>DataType</b></td><td>") + str + wxT("</td></tr>\n");
      html +=
        wxT
        ("<tr><td colspan=\"2\" align=\"center\"><h3>LatLon Bounding Box</h3></td></tr>\n");
      sprintf(dummy, "%1.6f", Layer->GetTileServiceLayer()->GetMinLong());
      str = wxString::FromUTF8(dummy);
      html += wxT("<tr><td><b>MinX</b></td><td>") + str + wxT("</td></tr>\n");
      sprintf(dummy, "%1.6f", Layer->GetTileServiceLayer()->GetMinLat());
      str = wxString::FromUTF8(dummy);
      html += wxT("<tr><td><b>MinY</b></td><td>") + str + wxT("</td></tr>\n");
      sprintf(dummy, "%1.6f", Layer->GetTileServiceLayer()->GetMaxLong());
      str = wxString::FromUTF8(dummy);
      html += wxT("<tr><td><b>MaxX</b></td><td>") + str + wxT("</td></tr>\n");
      sprintf(dummy, "%1.6f", Layer->GetTileServiceLayer()->GetMaxLat());
      str = wxString::FromUTF8(dummy);
      html += wxT("<tr><td><b>MaxY</b></td><td>") + str + wxT("</td></tr>\n");
      html +=
        wxT
        ("<tr><td colspan=\"2\" align=\"center\"><h3>Tiled Patterns</h3></td></tr>\n");
      WmsTilePattern *pattern = Layer->GetTileServiceLayer()->GetFirst();
      int n = 1;
      while (pattern != NULL)
        {
          char xnum[64];
          sprintf(xnum, "%d", n++);
          wxString num = wxString::FromUTF8(xnum);
          pattern->GetSampleURL(str);
          html +=
            wxT("<tr><td align=\"right\"><b>") + num + wxT("</b></td><td>") +
            str + wxT("</td></tr>\n");
          pattern = pattern->GetNext();
        }
      html += wxT("</table>\n");
  } else
    {
      // ordinary WMS layer
      html += wxT("<hr><h2>Layer info</h2>\n");
      html += wxT("<table>\n");
      str = wxString::FromUTF8(Layer->GetName());
      html += wxT("<tr><td><b>Name</b></td><td>") + str + wxT("</td></tr>\n");
      str = wxString::FromUTF8(Layer->GetTitle());
      html += wxT("<tr><td><b>Title</b></td><td>") + str + wxT("</td></tr>\n");
      str = wxString::FromUTF8(Layer->GetAbstract());
      html +=
        wxT("<tr><td><b>Abstract</b></td><td>") + str + wxT("</td></tr>\n");
      if (Layer->IsOpaque() == 0)
        str = wxT("no");
      else
        str = wxT("yes");
      html += wxT("<tr><td><b>Opaque</b></td><td>") + str + wxT("</td></tr>\n");
      if (Layer->IsQueryable() == 0)
        str = wxT("no");
      else
        str = wxT("yes");
      html +=
        wxT("<tr><td><b>Queryable</b></td><td>") + str + wxT("</td></tr>\n");
      str = wxString::FromUTF8(Layer->GetFormat());
      html += wxT("<tr><td><b>Format</b></td><td>") + str + wxT("</td></tr>\n");
      if (Layer->IsTiled() == 0)
        html += wxT("<tr><td><b>Tiled</b></td><td>no</td></tr>\n");
      else
        {
          char tile[1024];
          sprintf(tile,
                  "<tr><td><b>Tile Width</b></td><td>%dpx</td></tr>\n<tr><td><b>Tile Height</b></td><td>%dpx</td></tr>\n",
                  Layer->GetTileWidth(), Layer->GetTileHeight());
          html += wxString::FromUTF8(tile);
        }
      html += wxT("<tr><td></td><td><hr></td></tr>\n");
      html +=
        wxT("<tr><td></td><td><b>Geographic Bounding Box</b></td></tr>\n");
      sprintf(dummy, "%1.6f", Layer->GetGeoMinX());
      str = wxString::FromUTF8(dummy);
      html +=
        wxT("<tr><td><b>West Longitude</b></td><td>") + str +
        wxT("</td></tr>\n");
      sprintf(dummy, "%1.6f", Layer->GetGeoMaxX());
      str = wxString::FromUTF8(dummy);
      html +=
        wxT("<tr><td><b>East Longitude</b></td><td>") + str +
        wxT("</td></tr>\n");
      sprintf(dummy, "%1.6f", Layer->GetGeoMaxY());
      str = wxString::FromUTF8(dummy);
      html +=
        wxT("<tr><td><b>North Latitude</b></td><td>") + str +
        wxT("</td></tr>\n");
      sprintf(dummy, "%1.6f", Layer->GetGeoMinY());
      str = wxString::FromUTF8(dummy);
      html +=
        wxT("<tr><td><b>South Latitude</b></td><td>") + str +
        wxT("</td></tr>\n");
      html += wxT("<tr><td></td><td><hr></td></tr>\n");
      html += wxT("<tr><td></td><td><b>CRS Bounding Box</b></td></tr>\n");
      str = wxString::FromUTF8(Layer->GetCRS());
      html += wxT("<tr><td><b>CRS</b></td><td>") + str + wxT("</td></tr>\n");
      sprintf(dummy, "%1.6f", Layer->GetMinX());
      str = wxString::FromUTF8(dummy);
      html += wxT("<tr><td><b>Min X</b></td><td>") + str + wxT("</td></tr>\n");
      sprintf(dummy, "%1.6f", Layer->GetMaxX());
      str = wxString::FromUTF8(dummy);
      html += wxT("<tr><td><b>Max X</b></td><td>") + str + wxT("</td></tr>\n");
      sprintf(dummy, "%1.6f", Layer->GetMinY());
      str = wxString::FromUTF8(dummy);
      html += wxT("<tr><td><b>Min Y</b></td><td>") + str + wxT("</td></tr>\n");
      sprintf(dummy, "%1.6f", Layer->GetMaxY());
      str = wxString::FromUTF8(dummy);
      html += wxT("<tr><td><b>Max Y</b></td><td>") + str + wxT("</td></tr>\n");
      html += wxT("<tr><td></td><td><hr></td></tr>\n");
      int denom = Layer->GetMinScaleDenominator();
      if (denom <= 0)
        str = wxT("");
      else
        {
          sprintf(dummy, "%d", denom);
          str = wxString::FromUTF8(dummy);
        }
      html +=
        wxT("<tr><td><b>Min Scale Denominator</b></td><td>") + str +
        wxT("</td></tr>\n");
      sprintf(dummy, "%1.6f", Layer->GetMaxY());
      denom = Layer->GetMinScaleDenominator();
      if (denom <= 0)
        str = wxT("");
      else
        {
          sprintf(dummy, "%d", denom);
          str = wxString::FromUTF8(dummy);
        }
      denom = Layer->GetMaxScaleDenominator();
      if (denom <= 0)
        str = wxT("");
      else
        {
          sprintf(dummy, "%d", denom);
          str = wxString::FromUTF8(dummy);
        }
      html +=
        wxT("<tr><td><b>Max Scale Denominator</b></td><td>") + str +
        wxT("</td></tr>\n");
      html += wxT("<tr><td></td><td><hr></td></tr>\n");
      WmsStyle *Style = Layer->GetFirstStyle();
      while (Style != NULL)
        {
          // reporting all Styles
          html += wxT("<tr><td></td><td><b>Style</b></td></tr>\n");
          str = wxString::FromUTF8(Style->GetName());
          html +=
            wxT("<tr><td><b>Name</b></td><td>") + str + wxT("</td></tr>\n");
          str = wxString::FromUTF8(Style->GetTitle());
          html +=
            wxT("<tr><td><b>Title</b></td><td>") + str + wxT("</td></tr>\n");
          str = wxString::FromUTF8(Style->GetAbstract());
          html +=
            wxT("<tr><td><b>Abstract</b></td><td>") + str + wxT("</td></tr>\n");
          Style = Style->GetNext();
        }
      html += wxT("</table>\n");
    }
  html += wxT("</body>");
  html += wxT("</html>");
  HtmlCtrl->SetPage(html);
// QUIT button
  wxBoxSizer *quitBox = new wxBoxSizer(wxHORIZONTAL);
  boxSizer->Add(quitBox, 0, wxALIGN_CENTER_HORIZONTAL | wxALL, 0);
  wxButton *quit = new wxButton(this, wxID_CANCEL, wxT("&Quit"));
  quitBox->Add(quit, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);
// setting up the event handlers
  Connect(wxID_ANY, wxEVT_SIZE,
          (wxObjectEventFunction) & WmsInfoDialog::OnSize);
}

void WmsInfoDialog::OnSize(wxSizeEvent & WXUNUSED(event))
{
//
// this window has changed its size
//
  wxSize sz = GetClientSize();
  wxButton *quit = (wxButton *) FindWindow(wxID_CANCEL);
  wxSize btnSz = quit->GetSize();
  HtmlCtrl->SetSize(sz.GetWidth() - 6, sz.GetHeight() - 20 - btnSz.GetHeight());
  int x = (sz.GetWidth() - btnSz.GetWidth()) / 2;
  int y = sz.GetHeight() - 6 - btnSz.GetHeight();
  quit->SetSize(x, y, btnSz.GetWidth(), btnSz.GetHeight());
}

WmsIdentifyPanel::WmsIdentifyPanel(MyMapView * parent, wxString & html,
                                   rl2WmsFeatureCollectionPtr coll,
                                   GeometryList * list, int x,
                                   int y):wxWindow(parent, wxID_ANY,
                                                   wxDefaultPosition,
                                                   wxDefaultSize,
                                                   wxBORDER_DOUBLE |
                                                   wxBORDER_SUNKEN)
{
//
// creating the dialog
//
  MapView = parent;
  Html = wxT("");
  Collection = NULL;
  List = NULL;
// populates individual controls
  CreateControls();
// sets dialog sizer
  GetSizer()->Fit(this);
  GetSizer()->SetSizeHints(this);
  Initialize(html, coll, list, x, y);
}

void WmsIdentifyPanel::Reset()
{
// clean up - resetting to an empty initial state
  if (List != NULL)
    delete List;
  if (Collection != NULL)
    destroy_wms_feature_collection(Collection);
  List = NULL;
  Collection = NULL;
  Hide();
}

void WmsIdentifyPanel::Initialize(wxString & html,
                                  rl2WmsFeatureCollectionPtr coll,
                                  GeometryList * list, int x, int y)
{
//
// initializing the dialog
//
  if (x == y)
    x = y;                      // silencing warning about unused args
  Html = html;
  HtmlCtrl->SetPage(Html);
  Reset();
  Collection = coll;
  List = list;
// positioning the dialog window
  wxRect rect = MapView->GetClientRect();
  if (y < rect.GetHeight() / 2)
    SetSize(rect.GetX(),
            rect.GetY() + (rect.GetHeight() - (rect.GetHeight() / 4)),
            rect.GetWidth(), rect.GetHeight() / 4);
  else
    SetSize(rect.GetX(), rect.GetY(), rect.GetWidth(), rect.GetHeight() / 4);
}

void WmsIdentifyPanel::CreateControls()
{
//
// creating individual control and setting initial values
//
  wxBoxSizer *topSizer = new wxBoxSizer(wxVERTICAL);
  this->SetSizer(topSizer);
  HtmlCtrl = new wxHtmlWindow(this, wxID_ANY, wxDefaultPosition, wxDefaultSize);
  topSizer->Add(HtmlCtrl, 0, wxALIGN_RIGHT | wxALL, 0);
  HtmlCtrl->SetPage(Html);
// QUIT button
  wxButton *quit =
    new wxButton(this, wxID_CANCEL, wxT("&Hide"), wxDefaultPosition,
                 wxSize(100, 20));
  topSizer->Add(quit, 0, wxALIGN_CENTER_VERTICAL | wxALL, 0);
// setting up the event handlers
  Connect(wxID_ANY, wxEVT_SIZE,
          (wxObjectEventFunction) & WmsIdentifyPanel::OnSize);
  Connect(wxID_ANY, wxEVT_COMMAND_HTML_LINK_CLICKED,
          (wxObjectEventFunction) & WmsIdentifyPanel::OnLinkClicked);
  Connect(wxID_CANCEL, wxEVT_COMMAND_BUTTON_CLICKED,
          (wxObjectEventFunction) & WmsIdentifyPanel::OnCancel);
}

void WmsIdentifyPanel::ParseHREF(wxString & href, int *layer_id, int *row_no,
                                 int *col_no)
{
// attempting to parse an HREF into an ID and a layer name
  *layer_id = -1;
  *row_no = -1;
  *col_no = -1;
  char *in = new char[href.Len() + 1];
  strcpy(in, href.ToUTF8());
  const char *p_in = in;
  char buf[1024];
  char *p_out = buf;
  int count = 0;
  while (1)
    {
      if (*p_in >= '0' && *p_in <= '9')
        *p_out++ = *p_in++;
      else if (*p_in == '_')
        {
          *p_out = '\0';
          if (count == 0)
            *layer_id = atoi(buf);
          else
            *row_no = atoi(buf);
          count++;
          p_in++;
          p_out = buf;
      } else
        {
          *p_out = '\0';
          *col_no = atoi(buf);
          break;
        }
    }
  delete[]in;
}

void WmsIdentifyPanel::OnLinkClicked(wxHtmlLinkEvent & event)
{
//
// some link was clicked
//
  wxHtmlLinkInfo lnk = event.GetLinkInfo();
  int layer_id;
  int row_no;
  int col_no;
  wxString href = lnk.GetHref();
  ParseHREF(href, &layer_id, &row_no, &col_no);
  gaiaGeomCollPtr geom = List->Find(layer_id, row_no, col_no);
  if (geom != NULL)
    MapView->MarkGeometry(geom);
}

void WmsIdentifyPanel::OnSize(wxSizeEvent & WXUNUSED(event))
{
//
// this window has changed its size
//
  wxSize sz = GetClientSize();
  wxButton *quit = (wxButton *) FindWindow(wxID_CANCEL);
  wxSize btnSz = quit->GetSize();
  HtmlCtrl->SetSize(sz.GetWidth() - 6,
                    sz.GetHeight() - (4 + btnSz.GetHeight()));
  int x = (sz.GetWidth() - btnSz.GetWidth()) / 2;
  int y = sz.GetHeight() - (2 + btnSz.GetHeight());
  quit->SetSize(x, y, btnSz.GetWidth(), btnSz.GetHeight());
}

void WmsIdentifyPanel::OnCancel(wxCommandEvent & WXUNUSED(event))
{
//
// all done: 
//
  MapView->ResetMarker();
  Reset();
}

bool WmsCacheDialog::Create(MyFrame * parent, rl2WmsCachePtr cache)
{
//
// creating the dialog
//
  MainFrame = parent;
  Cache = cache;
  if (wxDialog::Create(parent, wxID_ANY, wxT("WMS internal Cache")) == false)
    return false;
// populates individual controls
  CreateControls();
// sets dialog sizer
  GetSizer()->Fit(this);
  GetSizer()->SetSizeHints(this);
// centers the dialog window
  Centre();
  return true;
}

void WmsCacheDialog::CreateControls()
{
//
// creating individual control and setting initial values
//
  wxBoxSizer *topSizer = new wxBoxSizer(wxVERTICAL);
  this->SetSizer(topSizer);
  wxBoxSizer *boxSizer = new wxBoxSizer(wxVERTICAL);
  topSizer->Add(boxSizer, 0, wxALIGN_CENTER_HORIZONTAL | wxALL, 5);
// Cache Statistics
  wxBoxSizer *itemsSizer = new wxBoxSizer(wxHORIZONTAL);
  boxSizer->Add(itemsSizer, 0, wxALIGN_RIGHT | wxALL, 5);
  wxStaticText *itemsLabel =
    new wxStaticText(this, wxID_STATIC, wxT("&Cached Items:"));
  itemsSizer->Add(itemsLabel, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);
  int cnt = get_wms_cache_items_count(Cache);
  char dummy[128];
  sprintf(dummy, "%d", cnt);
  wxString count = wxString::FromUTF8(dummy);
  wxTextCtrl *itemsCtrl = new wxTextCtrl(this, ID_CACHE_ITEMS, count,
                                         wxDefaultPosition, wxSize(100, 22),
                                         wxTE_RIGHT | wxTE_READONLY);
  itemsSizer->Add(itemsCtrl, 0, wxALIGN_CENTER_VERTICAL | wxALL, 0);
  wxBoxSizer *flushSizer = new wxBoxSizer(wxHORIZONTAL);
  boxSizer->Add(flushSizer, 0, wxALIGN_RIGHT | wxALL, 5);
  wxStaticText *flushLabel =
    new wxStaticText(this, wxID_STATIC, wxT("&Flushed Items:"));
  flushSizer->Add(flushLabel, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);
  cnt = get_wms_cache_flushed_count(Cache);
  sprintf(dummy, "%d", cnt);
  count = wxString::FromUTF8(dummy);
  wxTextCtrl *flushCtrl = new wxTextCtrl(this, ID_CACHE_FLUSHED, count,
                                         wxDefaultPosition, wxSize(100, 22),
                                         wxTE_RIGHT | wxTE_READONLY);
  flushSizer->Add(flushCtrl, 0, wxALIGN_CENTER_VERTICAL | wxALL, 0);
  wxBoxSizer *hitSizer = new wxBoxSizer(wxHORIZONTAL);
  boxSizer->Add(hitSizer, 0, wxALIGN_RIGHT | wxALL, 5);
  wxStaticText *hitLabel =
    new wxStaticText(this, wxID_STATIC, wxT("Total Cache &Hits:"));
  hitSizer->Add(hitLabel, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);
  cnt = get_wms_cache_hit_count(Cache);
  sprintf(dummy, "%d", cnt);
  count = wxString::FromUTF8(dummy);
  wxTextCtrl *hitCtrl = new wxTextCtrl(this, ID_CACHE_HIT, count,
                                       wxDefaultPosition, wxSize(100, 22),
                                       wxTE_RIGHT | wxTE_READONLY);
  hitSizer->Add(hitCtrl, 0, wxALIGN_CENTER_VERTICAL | wxALL, 0);
  wxBoxSizer *missSizer = new wxBoxSizer(wxHORIZONTAL);
  boxSizer->Add(missSizer, 0, wxALIGN_RIGHT | wxALL, 5);
  wxStaticText *missLabel =
    new wxStaticText(this, wxID_STATIC, wxT("Total Cache &Misses:"));
  missSizer->Add(missLabel, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);
  cnt = get_wms_cache_miss_count(Cache);
  sprintf(dummy, "%d", cnt);
  count = wxString::FromUTF8(dummy);
  wxTextCtrl *missCtrl = new wxTextCtrl(this, ID_CACHE_MISS, count,
                                        wxDefaultPosition, wxSize(100, 22),
                                        wxTE_RIGHT | wxTE_READONLY);
  missSizer->Add(missCtrl, 0, wxALIGN_CENTER_VERTICAL | wxALL, 0);
  wxBoxSizer *szSizer = new wxBoxSizer(wxHORIZONTAL);
  boxSizer->Add(szSizer, 0, wxALIGN_RIGHT | wxALL, 5);
  wxStaticText *szLabel =
    new wxStaticText(this, wxID_STATIC, wxT("Current &Size (bytes):"));
  szSizer->Add(szLabel, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);
  cnt = get_wms_cache_current_size(Cache);
  sprintf(dummy, "%d", cnt);
  count = wxString::FromUTF8(dummy);
  wxTextCtrl *szCtrl = new wxTextCtrl(this, ID_CACHE_SIZE, count,
                                      wxDefaultPosition, wxSize(100, 22),
                                      wxTE_RIGHT | wxTE_READONLY);
  szSizer->Add(szCtrl, 0, wxALIGN_CENTER_VERTICAL | wxALL, 0);
  wxBoxSizer *downSizer = new wxBoxSizer(wxHORIZONTAL);
  boxSizer->Add(downSizer, 0, wxALIGN_RIGHT | wxALL, 5);
  wxStaticText *downLabel =
    new wxStaticText(this, wxID_STATIC, wxT("Total &Download Size:"));
  downSizer->Add(downLabel, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);
  double dblcnt = get_wms_total_download_size(Cache);
  if (dblcnt >= (double) (1024 * 1024 * 1024))
    sprintf(dummy, "%1.2f GB", dblcnt / (double) (1024 * 1024 * 1024));
  else if (dblcnt >= (double) (1024 * 1024))
    sprintf(dummy, "%1.2f MB", dblcnt / (double) (1024 * 1024));
  else
    sprintf(dummy, "%1.2f KB", dblcnt / (double) (1024));
  count = wxString::FromUTF8(dummy);
  wxTextCtrl *downCtrl = new wxTextCtrl(this, ID_CACHE_DOWNLOAD, count,
                                        wxDefaultPosition, wxSize(100, 22),
                                        wxTE_RIGHT | wxTE_READONLY);
  downSizer->Add(downCtrl, 0, wxALIGN_CENTER_VERTICAL | wxALL, 0);
// Reset button
  wxBoxSizer *resetSizer = new wxBoxSizer(wxHORIZONTAL);
  boxSizer->Add(resetSizer, 0, wxALIGN_CENTER_HORIZONTAL | wxALL, 0);
  wxButton *resetBtn =
    new wxButton(this, ID_CACHE_RESET, wxT("&Reset WMS Cache"));
  resetSizer->Add(resetBtn, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);
// max size slider
  wxBoxSizer *maxSizer = new wxBoxSizer(wxHORIZONTAL);
  boxSizer->Add(maxSizer, 0, wxALIGN_TOP | wxALL, 5);
  wxStaticBox *maxBox = new wxStaticBox(this, wxID_STATIC,
                                        wxT("Max WMS Cache Size"),
                                        wxDefaultPosition,
                                        wxDefaultSize);
  wxBoxSizer *maxBoxSizer = new wxStaticBoxSizer(maxBox, wxHORIZONTAL);
  maxSizer->Add(maxBoxSizer, 0, wxALIGN_CENTER_HORIZONTAL | wxALL, 0);
  cnt = get_wms_cache_max_size(Cache);
  int cur = cnt / (1024 * 1024);
  wxSlider *maxCtrl =
    new wxSlider(this, ID_CACHE_MAX, cur, 4, 256, wxDefaultPosition,
                 wxSize(252, 50),
                 wxSL_HORIZONTAL | wxSL_AUTOTICKS | wxSL_LABELS);
  maxBoxSizer->Add(maxCtrl, 0, wxALIGN_CENTER_VERTICAL | wxALL, 0);
// OK-Cancel buttons
  wxBoxSizer *btnSizer = new wxBoxSizer(wxHORIZONTAL);
  boxSizer->Add(btnSizer, 0, wxALIGN_CENTER_HORIZONTAL | wxALL, 0);
  wxButton *okBtn = new wxButton(this, wxID_OK, wxT("&OK"));
  btnSizer->Add(okBtn, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);
  wxButton *cancelBtn = new wxButton(this, wxID_CANCEL, wxT("&Cancel"));
  btnSizer->Add(cancelBtn, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);
// appends event handlers
  Connect(ID_CACHE_RESET, wxEVT_COMMAND_BUTTON_CLICKED,
          (wxObjectEventFunction) & WmsCacheDialog::OnCacheReset);
  Connect(wxID_OK, wxEVT_COMMAND_BUTTON_CLICKED,
          (wxObjectEventFunction) & WmsCacheDialog::OnOk);
}

void WmsCacheDialog::OnCacheReset(wxCommandEvent & WXUNUSED(event))
{
//
// resetting WMS Cache
//
  reset_wms_cache(Cache);
  int cnt = get_wms_cache_items_count(Cache);
  char dummy[128];
  sprintf(dummy, "%d", cnt);
  wxString count = wxString::FromUTF8(dummy);
  wxTextCtrl *itemsCtrl = (wxTextCtrl *) FindWindow(ID_CACHE_ITEMS);
  itemsCtrl->SetValue(count);
  cnt = get_wms_cache_flushed_count(Cache);
  count = wxString::FromUTF8(dummy);
  wxTextCtrl *flushCtrl = (wxTextCtrl *) FindWindow(ID_CACHE_FLUSHED);
  flushCtrl->SetValue(count);
  cnt = get_wms_cache_hit_count(Cache);
  count = wxString::FromUTF8(dummy);
  wxTextCtrl *hitCtrl = (wxTextCtrl *) FindWindow(ID_CACHE_HIT);
  hitCtrl->SetValue(count);
  cnt = get_wms_cache_miss_count(Cache);
  count = wxString::FromUTF8(dummy);
  wxTextCtrl *missCtrl = (wxTextCtrl *) FindWindow(ID_CACHE_MISS);
  missCtrl->SetValue(count);
  cnt = get_wms_cache_current_size(Cache);
  count = wxString::FromUTF8(dummy);
  wxTextCtrl *sizeCtrl = (wxTextCtrl *) FindWindow(ID_CACHE_SIZE);
  sizeCtrl->SetValue(count);
  return;
}

void WmsCacheDialog::OnOk(wxCommandEvent & WXUNUSED(event))
{
//
// all done: 
//
  wxSlider *maxCtrl = (wxSlider *) FindWindow(ID_CACHE_MAX);
  set_wms_cache_max_size(Cache, maxCtrl->GetValue() * 1024 * 1024);
  wxDialog::EndModal(wxID_OK);
}

bool WmsMapCrsDialog::Create(MyFrame * parent)
{
//
// creating the dialog
//
  MainFrame = parent;
  if (wxDialog::Create(parent, wxID_ANY, wxT("WMS Map CRS selection")) == false)
    return false;
// populates individual controls
  CreateControls();
// sets dialog sizer
  GetSizer()->Fit(this);
  GetSizer()->SetSizeHints(this);
// centers the dialog window
  Centre();
  return true;
}

void WmsMapCrsDialog::CreateControls()
{
//
// creating individual control and setting initial values
//
  wxBoxSizer *topSizer = new wxBoxSizer(wxVERTICAL);
  this->SetSizer(topSizer);
  wxBoxSizer *boxSizer = new wxBoxSizer(wxVERTICAL);
  topSizer->Add(boxSizer, 0, wxALIGN_CENTER_HORIZONTAL | wxALL, 5);
// CRS combo box
  wxBoxSizer *crsSizer = new wxBoxSizer(wxHORIZONTAL);
  boxSizer->Add(crsSizer, 0, wxALIGN_TOP | wxALL, 5);
  wxStaticBox *crsBox = new wxStaticBox(this, wxID_STATIC,
                                        wxT("Reference System"),
                                        wxDefaultPosition,
                                        wxDefaultSize);
  wxBoxSizer *crsBoxSizer = new wxStaticBoxSizer(crsBox, wxVERTICAL);
  crsSizer->Add(crsBoxSizer, 0, wxALIGN_CENTER_VERTICAL | wxALL, 2);
  wxComboBox *crsList =
    new wxComboBox(this, ID_WMS_CRS, wxT(""), wxDefaultPosition,
                   wxSize(150, 21), 0, NULL,
                   wxCB_DROPDOWN | wxCB_SORT | wxCB_READONLY);
  WmsCRS *crs = MainFrame->GetFirstMapCRS();
  while (crs != NULL)
    {
      wxString crs_def = wxString::FromUTF8(crs->GetCRS());
      crsList->Append(crs_def);
      crs = crs->GetNext();
    }
  wxString curCrs = wxString::FromUTF8(MainFrame->GetMapCRS());
  if (crsList->SetStringSelection(curCrs) == false)
    crsList->SetSelection(0);
  crsBoxSizer->Add(crsList, 0, wxALIGN_CENTER_VERTICAL | wxALL, 2);
// OK-Cancel buttons
  wxBoxSizer *btnSizer = new wxBoxSizer(wxHORIZONTAL);
  boxSizer->Add(btnSizer, 0, wxALIGN_CENTER_HORIZONTAL | wxALL, 0);
  wxButton *okBtn = new wxButton(this, wxID_OK, wxT("&OK"));
  btnSizer->Add(okBtn, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);
  wxButton *cancelBtn = new wxButton(this, wxID_CANCEL, wxT("&Cancel"));
  btnSizer->Add(cancelBtn, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);
// appends event handlers
  Connect(wxID_OK, wxEVT_COMMAND_BUTTON_CLICKED,
          (wxObjectEventFunction) & WmsMapCrsDialog::OnOk);
}

void WmsMapCrsDialog::OnOk(wxCommandEvent & WXUNUSED(event))
{
//
// all done: 
//
  wxComboBox *comboCtrl = (wxComboBox *) FindWindow(ID_WMS_CRS);
  wxString crs = comboCtrl->GetValue();
  if (crs.Len() == 0)
    {
      wxMessageBox(wxT("You must select some CRS !!!"),
                   wxT("LibreWMS"), wxOK | wxICON_WARNING, this);
      return;
    }
  CRS = crs;
  wxDialog::EndModal(wxID_OK);
}

bool UserScaleDialog::Create(MyFrame * parent, int scale, double pixel_ratio)
{
//
// creating the dialog
//
  MainFrame = parent;
  if (wxDialog::Create(parent, wxID_ANY, wxT("WMS Map User Scale selection")) ==
      false)
    return false;
  CurrentScale = scale;
  PixelRatio = pixel_ratio;
// populates individual controls
  CreateControls();
// sets dialog sizer
  GetSizer()->Fit(this);
  GetSizer()->SetSizeHints(this);
// centers the dialog window
  Centre();
  return true;
}

void UserScaleDialog::CreateControls()
{
//
// creating individual control and setting initial values
//
  wxBoxSizer *topSizer = new wxBoxSizer(wxVERTICAL);
  this->SetSizer(topSizer);
  wxBoxSizer *boxSizer = new wxBoxSizer(wxVERTICAL);
  topSizer->Add(boxSizer, 0, wxALIGN_CENTER_HORIZONTAL | wxALL, 5);
// User Selected Scale
  wxBoxSizer *scaleSizer = new wxBoxSizer(wxHORIZONTAL);
  boxSizer->Add(scaleSizer, 0, wxALIGN_TOP | wxALL, 5);
  wxStaticBox *scaleBox = new wxStaticBox(this, wxID_STATIC,
                                          wxT("User Selected Scale"),
                                          wxDefaultPosition,
                                          wxDefaultSize);
  wxBoxSizer *scaleBoxSizer = new wxStaticBoxSizer(scaleBox, wxHORIZONTAL);
  scaleSizer->Add(scaleBoxSizer, 0, wxALIGN_CENTER_VERTICAL | wxALL, 2);
  wxStaticText *scaleLabel = new wxStaticText(this, wxID_ANY, wxT("&1 :"),
                                              wxDefaultPosition, wxDefaultSize,
                                              wxALIGN_RIGHT);
  scaleBoxSizer->Add(scaleLabel, 0, wxALIGN_CENTER_VERTICAL | wxALL, 2);
  wxSpinCtrl *scaleCtrl = new wxSpinCtrl(this, ID_USER_SCALE, wxEmptyString,
                                         wxDefaultPosition, wxSize(120, 24),
                                         wxSP_ARROW_KEYS,
                                         1, 100000000, CurrentScale);
  scaleBoxSizer->Add(scaleCtrl, 0, wxALIGN_CENTER_VERTICAL | wxALL, 2);
// Pixel - MapUnits ratio
  wxBoxSizer *ratioSizer = new wxBoxSizer(wxHORIZONTAL);
  boxSizer->Add(ratioSizer, 0, wxALIGN_TOP | wxALL, 5);
  wxStaticBox *ratioBox = new wxStaticBox(this, wxID_STATIC,
                                          wxT("MapUnits per Pixel"),
                                          wxDefaultPosition,
                                          wxDefaultSize);
  wxBoxSizer *ratioBoxSizer = new wxStaticBoxSizer(ratioBox, wxHORIZONTAL);
  ratioSizer->Add(ratioBoxSizer, 0, wxALIGN_CENTER_VERTICAL | wxALL, 2);
  char buf[128];
  sprintf(buf, "%1.12f", PixelRatio);
  wxString ratio = wxString::FromUTF8(buf);
  wxTextCtrl *ratioCtrl = new wxTextCtrl(this, ID_USER_RATIO, ratio,
                                         wxDefaultPosition, wxSize(150, 22),
                                         wxTE_READONLY | wxTE_RIGHT);
  ratioCtrl->Enable(false);
  ratioBoxSizer->Add(ratioCtrl, 0, wxALIGN_CENTER_VERTICAL | wxALL, 2);
// OK-Cancel buttons
  wxBoxSizer *btnSizer = new wxBoxSizer(wxHORIZONTAL);
  boxSizer->Add(btnSizer, 0, wxALIGN_CENTER_HORIZONTAL | wxALL, 0);
  wxButton *okBtn = new wxButton(this, wxID_OK, wxT("&OK"));
  btnSizer->Add(okBtn, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);
  wxButton *cancelBtn = new wxButton(this, wxID_CANCEL, wxT("&Cancel"));
  btnSizer->Add(cancelBtn, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);
// appends event handlers
  Connect(wxID_OK, wxEVT_COMMAND_BUTTON_CLICKED,
          (wxObjectEventFunction) & UserScaleDialog::OnOk);
  Connect(ID_USER_SCALE, wxEVT_COMMAND_SPINCTRL_UPDATED,
          (wxObjectEventFunction) & UserScaleDialog::OnScaleChanged);
  Connect(ID_USER_SCALE, wxEVT_COMMAND_TEXT_UPDATED,
          (wxObjectEventFunction) & UserScaleDialog::OnScaleChanged);
}

void UserScaleDialog::OnScaleChanged(wxSpinEvent & WXUNUSED(event))
{
//
// updating the current MapUnits - Pixel ratio: 
//
  wxSpinCtrl *scaleCtrl = (wxSpinCtrl *) FindWindow(ID_USER_SCALE);
  int scale = scaleCtrl->GetValue();
  double current_ratio = (double) scale * PixelRatio / (double) CurrentScale;
  char buf[128];
  sprintf(buf, "%1.12f", current_ratio);
  wxString ratio = wxString::FromUTF8(buf);
  wxTextCtrl *ratioCtrl = (wxTextCtrl *) FindWindow(ID_USER_RATIO);
  ratioCtrl->SetValue(ratio);
}

void UserScaleDialog::OnOk(wxCommandEvent & WXUNUSED(event))
{
//
// all done: 
//
  wxSpinCtrl *scaleCtrl = (wxSpinCtrl *) FindWindow(ID_USER_SCALE);
  CurrentScale = scaleCtrl->GetValue();
  wxDialog::EndModal(wxID_OK);
}

bool PrinterDialog::Create(MyFrame * parent)
{
//
// creating the dialog
//
  MainFrame = parent;
  if (wxDialog::Create(parent, wxID_ANY, wxT("WMS Map Printer")) == false)
    return false;
  Width = 2048;
  Height = 2048;
  PDF = false;
  TIFF = true;
  GeoTiff = false;
  TileSize = 0;
  StripSize = 0;
  Compression = RL2_COMPRESSION_NONE;
// populates individual controls
  CreateControls();
// sets dialog sizer
  GetSizer()->Fit(this);
  GetSizer()->SetSizeHints(this);
// centers the dialog window
  Centre();
  return true;
}

void PrinterDialog::CreateControls()
{
//
// creating individual control and setting initial values
//
  wxBoxSizer *topSizer = new wxBoxSizer(wxVERTICAL);
  this->SetSizer(topSizer);
  wxBoxSizer *boxSizer = new wxBoxSizer(wxVERTICAL);
  topSizer->Add(boxSizer, 0, wxALIGN_CENTER_HORIZONTAL | wxALL, 5);
// general parameters
  wxBoxSizer *genSizer = new wxBoxSizer(wxHORIZONTAL);
  boxSizer->Add(genSizer, 0, wxALIGN_CENTER_HORIZONTAL | wxALL, 5);
  wxStaticBox *genBox = new wxStaticBox(this, wxID_STATIC,
                                        wxT("Export Mode Selection"),
                                        wxDefaultPosition,
                                        wxDefaultSize);
  wxBoxSizer *genBoxSizer = new wxStaticBoxSizer(genBox, wxHORIZONTAL);
  genSizer->Add(genBoxSizer, 0, wxALIGN_CENTER_HORIZONTAL | wxALL, 2);
  wxString format[2];
  format[0] = wxT("Export Map as PDF");
  format[1] = wxT("Export Map as TIFF");
  wxRadioBox *formatBox = new wxRadioBox(this, ID_PDF_TIFF,
                                         wxT("Export Format"),
                                         wxDefaultPosition,
                                         wxDefaultSize, 2,
                                         format, 2,
                                         wxRA_SPECIFY_ROWS);
  formatBox->SetSelection(1);
  genBoxSizer->Add(formatBox, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);
  wxString mode[3];
  mode[0] = wxT("Preserve Scale");
  mode[1] = wxT("Preserve Width");
  mode[2] = wxT("Preserve Height");
  wxRadioBox *modeBox = new wxRadioBox(this, ID_PRINT_MODE,
                                       wxT("Map Scale and Viewport"),
                                       wxDefaultPosition,
                                       wxDefaultSize, 3,
                                       mode, 3,
                                       wxRA_SPECIFY_ROWS);
  modeBox->SetSelection(2);
  genBoxSizer->Add(modeBox, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);
  wxBoxSizer *printSizer = new wxBoxSizer(wxHORIZONTAL);
  boxSizer->Add(printSizer, 0, wxALIGN_CENTER_HORIZONTAL | wxALL, 5);
// PDF parameters
  wxBoxSizer *pdfSizer = new wxBoxSizer(wxHORIZONTAL);
  printSizer->Add(pdfSizer, 0, wxALIGN_TOP | wxALL, 0);
  wxStaticBox *pdfBox = new wxStaticBox(this, wxID_STATIC,
                                        wxT("PDF Options"),
                                        wxDefaultPosition,
                                        wxDefaultSize);
  wxBoxSizer *pdfBoxSizer = new wxStaticBoxSizer(pdfBox, wxVERTICAL);
  pdfSizer->Add(pdfBoxSizer, 0, wxALIGN_CENTER_VERTICAL | wxALL, 2);
  wxBoxSizer *pdf1BoxSizer = new wxBoxSizer(wxHORIZONTAL);
  pdfBoxSizer->Add(pdf1BoxSizer, 0, wxALIGN_CENTER_VERTICAL | wxALL, 2);
  wxString page[5];
  page[0] = wxT("A4");
  page[1] = wxT("A3");
  page[2] = wxT("A2");
  page[3] = wxT("A1");
  page[4] = wxT("A0");
  wxRadioBox *pageBox = new wxRadioBox(this, ID_PDF_PAGE,
                                       wxT("Page Size"),
                                       wxDefaultPosition,
                                       wxDefaultSize, 5,
                                       page, 5,
                                       wxRA_SPECIFY_ROWS);
  pageBox->SetSelection(0);
  pageBox->Enable(false);
  pdf1BoxSizer->Add(pageBox, 0, wxALIGN_TOP | wxALL, 2);
  wxString dpi[5];
  dpi[0] = wxT("72 DPI");
  dpi[1] = wxT("150 DPI");
  dpi[2] = wxT("300 DPI");
  dpi[3] = wxT("600 DPI");
  wxRadioBox *dpiBox = new wxRadioBox(this, ID_PDF_DPI,
                                      wxT("Resolution"),
                                      wxDefaultPosition,
                                      wxDefaultSize, 4,
                                      dpi, 4,
                                      wxRA_SPECIFY_ROWS);
  dpiBox->SetSelection(2);
  dpiBox->Enable(false);
  pdf1BoxSizer->Add(dpiBox, 0, wxALIGN_TOP | wxALL, 2);
  wxBoxSizer *pdf2BoxSizer = new wxBoxSizer(wxVERTICAL);
  pdfBoxSizer->Add(pdf2BoxSizer, 0, wxALIGN_CENTER_VERTICAL | wxALL, 2);
  wxString orient[2];
  orient[0] = wxT("Landscape");
  orient[1] = wxT("Portrait");
  wxRadioBox *orientBox = new wxRadioBox(this, ID_PDF_ORIENTATION,
                                         wxT("Page Orientation"),
                                         wxDefaultPosition,
                                         wxDefaultSize, 2,
                                         orient, 2,
                                         wxRA_SPECIFY_COLS);
  orientBox->SetSelection(0);
  orientBox->Enable(false);
  pdf2BoxSizer->Add(orientBox, 0, wxALIGN_CENTER_HORIZONTAL | wxALL, 2);
// TIFF parameters
  wxBoxSizer *tiffSizer = new wxBoxSizer(wxVERTICAL);
  printSizer->Add(tiffSizer, 0, wxALIGN_TOP | wxALL, 0);
  wxStaticBox *tiffBox = new wxStaticBox(this, wxID_STATIC,
                                         wxT("TIFF Options"),
                                         wxDefaultPosition,
                                         wxDefaultSize);
  wxBoxSizer *tiffBoxSizer = new wxStaticBoxSizer(tiffBox, wxVERTICAL);
  tiffSizer->Add(tiffBoxSizer, 0, wxALIGN_CENTER_VERTICAL | wxALL, 2);
  wxBoxSizer *dimSizer = new wxBoxSizer(wxHORIZONTAL);
  tiffBoxSizer->Add(dimSizer, 0, wxALIGN_TOP | wxALL, 2);
  wxBoxSizer *dim1Sizer = new wxBoxSizer(wxVERTICAL);
  dimSizer->Add(dim1Sizer, 0, wxALIGN_TOP | wxALL, 2);
  wxBoxSizer *wSizer = new wxBoxSizer(wxHORIZONTAL);
  dim1Sizer->Add(wSizer, 0, wxALIGN_RIGHT | wxALL, 2);
  wxStaticText *wLabel = new wxStaticText(this, wxID_ANY, wxT("&Width:"),
                                          wxDefaultPosition, wxDefaultSize,
                                          wxALIGN_RIGHT);
  wSizer->Add(wLabel, 0, wxALIGN_CENTER_VERTICAL | wxALL, 2);
  wxSpinCtrl *wCtrl = new wxSpinCtrl(this, ID_TIFF_WIDTH, wxT("2048"),
                                     wxDefaultPosition, wxSize(80, 20),
                                     wxSP_ARROW_KEYS,
                                     512, 21600, 2048);
  wSizer->Add(wCtrl, 0, wxALIGN_CENTER_VERTICAL | wxALL, 2);
  wxBoxSizer *hSizer = new wxBoxSizer(wxHORIZONTAL);
  dim1Sizer->Add(hSizer, 0, wxALIGN_RIGHT | wxALL, 2);
  wxStaticText *hLabel = new wxStaticText(this, wxID_ANY, wxT("&Height:"),
                                          wxDefaultPosition, wxDefaultSize,
                                          wxALIGN_RIGHT);
  hSizer->Add(hLabel, 0, wxALIGN_CENTER_VERTICAL | wxALL, 2);
  wxSpinCtrl *hCtrl = new wxSpinCtrl(this, ID_TIFF_HEIGHT, wxT("2048"),
                                     wxDefaultPosition, wxSize(80, 20),
                                     wxSP_ARROW_KEYS,
                                     512, 21600, 2048);
  hSizer->Add(hCtrl, 0, wxALIGN_CENTER_VERTICAL | wxALL, 2);
  wxString geo[4];
  geo[0] = wxT("GeoTIFF");
  geo[1] = wxT("Worldfile");
  geo[2] = wxT("Both");
  geo[3] = wxT("None");
  wxRadioBox *geoBox = new wxRadioBox(this, ID_TIFF_GEO,
                                      wxT("Georeferencing"),
                                      wxDefaultPosition,
                                      wxDefaultSize, 4,
                                      geo, 2,
                                      wxRA_SPECIFY_COLS);
  geoBox->SetSelection(2);
  dimSizer->Add(geoBox, 0, wxALIGN_TOP | wxALL, 2);
  wxBoxSizer *dim2Sizer = new wxBoxSizer(wxHORIZONTAL);
  tiffBoxSizer->Add(dim2Sizer, 0, wxALIGN_RIGHT | wxALL, 2);
  wxString tile_sz[7];
  tile_sz[0] = wxT("64");
  tile_sz[1] = wxT("128");
  tile_sz[2] = wxT("256");
  tile_sz[3] = wxT("512");
  tile_sz[4] = wxT("1024");
  tile_sz[5] = wxT("2048");
  tile_sz[6] = wxT("by Strip");
  wxRadioBox *tileSzBox = new wxRadioBox(this, ID_TIFF_TILE_SZ,
                                         wxT("Tile Size"),
                                         wxDefaultPosition,
                                         wxDefaultSize, 7,
                                         tile_sz, 2,
                                         wxRA_SPECIFY_COLS);
  tileSzBox->SetSelection(2);
  dim2Sizer->Add(tileSzBox, 0, wxALIGN_TOP | wxALL, 2);
  wxString compression[5];
  compression[0] = wxT("None");
  compression[1] = wxT("LZW");
  compression[2] = wxT("DEFLATE");
  compression[3] = wxT("LZMA");
  compression[4] = wxT("JPEG");
  wxRadioBox *compressionBox = new wxRadioBox(this, ID_TIFF_COMPR,
                                              wxT("Compression"),
                                              wxDefaultPosition,
                                              wxDefaultSize, 5,
                                              compression, 2,
                                              wxRA_SPECIFY_COLS);
  compressionBox->SetSelection(0);
  dim2Sizer->Add(compressionBox, 0, wxALIGN_TOP | wxALL, 2);
// OK-Cancel buttons
  wxBoxSizer *btnSizer = new wxBoxSizer(wxHORIZONTAL);
  boxSizer->Add(btnSizer, 0, wxALIGN_CENTER_HORIZONTAL | wxALL, 0);
  wxButton *okBtn = new wxButton(this, wxID_OK, wxT("&OK"));
  btnSizer->Add(okBtn, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);
  wxButton *cancelBtn = new wxButton(this, wxID_CANCEL, wxT("&Cancel"));
  btnSizer->Add(cancelBtn, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);
// appends event handlers
  Connect(ID_PDF_TIFF, wxEVT_COMMAND_RADIOBOX_SELECTED,
          (wxObjectEventFunction) & PrinterDialog::OnFormatChanged);
  Connect(wxID_OK, wxEVT_COMMAND_BUTTON_CLICKED,
          (wxObjectEventFunction) & PrinterDialog::OnOk);
}

void PrinterDialog::OnFormatChanged(wxCommandEvent & WXUNUSED(event))
{
//
// Map Export Format changed
//
  wxRadioBox *modeBox = (wxRadioBox *) FindWindow(ID_PDF_TIFF);
  wxRadioBox *pageBox = (wxRadioBox *) FindWindow(ID_PDF_PAGE);
  wxRadioBox *dpiBox = (wxRadioBox *) FindWindow(ID_PDF_DPI);
  wxRadioBox *orientBox = (wxRadioBox *) FindWindow(ID_PDF_ORIENTATION);
  wxRadioBox *geoBox = (wxRadioBox *) FindWindow(ID_TIFF_GEO);
  wxRadioBox *compressionBox = (wxRadioBox *) FindWindow(ID_TIFF_COMPR);
  wxRadioBox *tileSzBox = (wxRadioBox *) FindWindow(ID_TIFF_TILE_SZ);
  wxSpinCtrl *wCtrl = (wxSpinCtrl *) FindWindow(ID_TIFF_WIDTH);
  wxSpinCtrl *hCtrl = (wxSpinCtrl *) FindWindow(ID_TIFF_HEIGHT);
  if (modeBox->GetSelection() == 0)
    {
      pageBox->Enable(true);
      dpiBox->Enable(true);
      orientBox->Enable(true);
      geoBox->Enable(false);
      compressionBox->Enable(false);
      tileSzBox->Enable(false);
      wCtrl->Enable(false);
      hCtrl->Enable(false);
  } else
    {
      pageBox->Enable(false);
      dpiBox->Enable(false);
      orientBox->Enable(false);
      geoBox->Enable(true);
      compressionBox->Enable(true);
      tileSzBox->Enable(true);
      wCtrl->Enable(true);
      hCtrl->Enable(true);
    }
}

void PrinterDialog::OnOk(wxCommandEvent & WXUNUSED(event))
{
//
// all done: 
//
  wxRadioBox *formatBox = (wxRadioBox *) FindWindow(ID_PDF_TIFF);
  wxRadioBox *modeBox = (wxRadioBox *) FindWindow(ID_PRINT_MODE);
  wxRadioBox *pageBox = (wxRadioBox *) FindWindow(ID_PDF_PAGE);
  wxRadioBox *dpiBox = (wxRadioBox *) FindWindow(ID_PDF_DPI);
  wxRadioBox *orientBox = (wxRadioBox *) FindWindow(ID_PDF_ORIENTATION);
  wxRadioBox *geoBox = (wxRadioBox *) FindWindow(ID_TIFF_GEO);
  wxRadioBox *compressionBox = (wxRadioBox *) FindWindow(ID_TIFF_COMPR);
  wxRadioBox *tileSzBox = (wxRadioBox *) FindWindow(ID_TIFF_TILE_SZ);
  wxSpinCtrl *wCtrl = (wxSpinCtrl *) FindWindow(ID_TIFF_WIDTH);
  wxSpinCtrl *hCtrl = (wxSpinCtrl *) FindWindow(ID_TIFF_HEIGHT);
  if (modeBox->GetSelection() == 2)
    {
      PreserveScale = false;
      PreserveWidth = false;
      PreserveHeight = true;
  } else if (modeBox->GetSelection() == 1)
    {
      PreserveScale = false;
      PreserveWidth = true;
      PreserveHeight = false;
  } else
    {
      PreserveScale = true;
      PreserveWidth = false;
      PreserveHeight = false;
    }
  if (formatBox->GetSelection() == 0)
    {
      // PDF options
      PDF = true;
      TIFF = false;
      GeoTiff = false;
      Compression = RL2_COMPRESSION_NONE;
      switch (dpiBox->GetSelection())
        {
          case 1:
            Dpi = 150;
            break;
          case 2:
            Dpi = 300;
            break;
          case 3:
            Dpi = 600;
            break;
          default:
            Dpi = 72;
            break;
        };
      switch (pageBox->GetSelection())
        {
          case 1:
            if (orientBox->GetSelection() == 0)
              {
                // A3 landscape
                PageWidth = 16.5;
                PageHeight = 11.7;
            } else
              {
                // A3 portrait
                PageWidth = 11.7;
                PageHeight = 16.5;
              }
            break;
          case 2:
            if (orientBox->GetSelection() == 0)
              {
                // A2 landscape
                PageWidth = 23.4;
                PageHeight = 16.5;
            } else
              {
                // A2 portrait
                PageWidth = 16.5;
                PageHeight = 23.4;
              }
            break;
          case 3:
            if (orientBox->GetSelection() == 0)
              {
                // A1 landscape
                PageWidth = 33.1;
                PageHeight = 23.4;
            } else
              {
                // A1 portrait
                PageWidth = 23.4;
                PageHeight = 33.1;
              }
            break;
          case 4:
            if (orientBox->GetSelection() == 0)
              {
                // A0 landscape
                PageWidth = 46.8;
                PageHeight = 33.1;
            } else
              {
                // A0 portrait
                PageWidth = 33.1;
                PageHeight = 46.8;
              }
            break;
          default:
            if (orientBox->GetSelection() == 0)
              {
                // A4 landscape
                PageWidth = 11.7;
                PageHeight = 8.3;
            } else
              {
                // A4 portrait
                PageWidth = 8.3;
                PageHeight = 11.7;
              }
            break;
        };
      HorzMarginSize = 1.0;
      VertMarginSize = 1.0;
  } else
    {
      // TIFF options
      PDF = false;
      TIFF = true;
      GeoTiff = false;
      Worldfile = false;
      if (geoBox->GetSelection() == 0 || geoBox->GetSelection() == 2)
        GeoTiff = true;
      if (geoBox->GetSelection() == 1 || geoBox->GetSelection() == 2)
        Worldfile = true;
      switch (compressionBox->GetSelection())
        {
          case 1:
            Compression = RL2_COMPRESSION_LZW;
            break;
          case 2:
            Compression = RL2_COMPRESSION_DEFLATE;
            break;
          case 3:
            Compression = RL2_COMPRESSION_LZMA;
            break;
          case 4:
            Compression = RL2_COMPRESSION_JPEG;
            break;
          default:
            Compression = RL2_COMPRESSION_NONE;
            break;
        };
      switch (tileSzBox->GetSelection())
        {
          case 0:
            TileSize = 64;
            break;
          case 1:
            TileSize = 128;
            break;
          case 2:
            TileSize = 256;
            break;
          case 3:
            TileSize = 512;
            break;
          case 4:
            TileSize = 1024;
            break;
          case 5:
            TileSize = 2048;
            break;
          default:
            StripSize = 1;
            break;
        }
      Width = wCtrl->GetValue();
      Height = hCtrl->GetValue();
    }
  wxDialog::EndModal(wxID_OK);
}

bool TiffMonochromeDialog::Create(MyMapView * parent)
{
//
// creating the dialog
//
  Parent = parent;
  if (wxDialog::Create(parent, wxID_ANY, wxT("Monochrome TIFF")) == false)
    return false;
// populates individual controls
  CreateControls();
// sets dialog sizer
  GetSizer()->Fit(this);
  GetSizer()->SetSizeHints(this);
// centers the dialog window
  Centre();
  return true;
}

void TiffMonochromeDialog::CreateControls()
{
//
// creating individual control and setting initial values
//
  wxBoxSizer *topSizer = new wxBoxSizer(wxVERTICAL);
  this->SetSizer(topSizer);
  wxBoxSizer *boxSizer = new wxBoxSizer(wxVERTICAL);
  topSizer->Add(boxSizer, 0, wxALIGN_CENTER_HORIZONTAL | wxALL, 5);
// message
  wxString msg = wxT("The current image could be safely exported using a\n");
  msg += wxT("Monochrome/Bilevel color space; a Monochrome image simply\n");
  msg += wxT("requires 1-bit for each pixel, thus requiring much less space\n");
  msg += wxT("than a 24-bit RGB image\n\n");
  msg += wxT("Exporting this image as Monochrome is strongly suggested:\n");
  msg += wxT("please confirm\n");
  wxStaticText *label = new wxStaticText(this, wxID_ANY, msg,
                                         wxDefaultPosition, wxDefaultSize,
                                         wxALIGN_LEFT);
  boxSizer->Add(label, 0, wxALIGN_CENTER_VERTICAL | wxALL, 2);
// general parameters
  wxString color[2];
  color[0] = wxT("Export as Monochrome");
  color[1] = wxT("Export as RGB");
  wxRadioBox *colorBox = new wxRadioBox(this, ID_TIFF_COLORSPACE,
                                        wxT("Colorspace Selection"),
                                        wxDefaultPosition,
                                        wxDefaultSize, 2,
                                        color, 1,
                                        wxRA_SPECIFY_ROWS);
  colorBox->SetSelection(0);
  boxSizer->Add(colorBox, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);
// TIFF parameters
  wxString compression[3];
  compression[0] = wxT("None");
  compression[1] = wxT("CCITT FAX3");
  compression[2] = wxT("CCITT FAX4");
  wxRadioBox *compressionBox = new wxRadioBox(this, ID_TIFF_COMPR,
                                              wxT("Compression"),
                                              wxDefaultPosition,
                                              wxDefaultSize, 3,
                                              compression, 1,
                                              wxRA_SPECIFY_ROWS);
  compressionBox->SetSelection(2);
  boxSizer->Add(compressionBox, 0, wxALIGN_CENTER | wxALL, 2);
// OK-Cancel buttons
  wxBoxSizer *btnSizer = new wxBoxSizer(wxHORIZONTAL);
  boxSizer->Add(btnSizer, 0, wxALIGN_CENTER_HORIZONTAL | wxALL, 0);
  wxButton *okBtn = new wxButton(this, wxID_OK, wxT("&OK"));
  btnSizer->Add(okBtn, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);
  wxButton *cancelBtn = new wxButton(this, wxID_CANCEL, wxT("&Abort"));
  btnSizer->Add(cancelBtn, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);
// appends event handlers
  Connect(ID_TIFF_COLORSPACE, wxEVT_COMMAND_RADIOBOX_SELECTED,
          (wxObjectEventFunction) & TiffMonochromeDialog::OnColorspaceChanged);
  Connect(wxID_OK, wxEVT_COMMAND_BUTTON_CLICKED,
          (wxObjectEventFunction) & TiffMonochromeDialog::OnOk);
}


void TiffMonochromeDialog::OnColorspaceChanged(wxCommandEvent & WXUNUSED(event))
{
//
// TIFF Colorspace changed
//
  wxRadioBox *colorBox = (wxRadioBox *) FindWindow(ID_TIFF_COLORSPACE);
  wxRadioBox *compressionBox = (wxRadioBox *) FindWindow(ID_TIFF_COMPR);
  if (colorBox->GetSelection() == 0)
    compressionBox->Enable(true);
  else
    compressionBox->Enable(false);
}

void TiffMonochromeDialog::OnOk(wxCommandEvent & WXUNUSED(event))
{
//
// all done: 
//
  wxRadioBox *colorBox = (wxRadioBox *) FindWindow(ID_TIFF_COLORSPACE);
  wxRadioBox *compressionBox = (wxRadioBox *) FindWindow(ID_TIFF_COMPR);
  Monochrome = false;
  if (colorBox->GetSelection() == 0)
    {
      Monochrome = true;
      switch (compressionBox->GetSelection())
        {
          case 1:
            Compression = RL2_COMPRESSION_CCITTFAX3;
            break;
          case 2:
            Compression = RL2_COMPRESSION_CCITTFAX4;
            break;
          default:
            Compression = RL2_COMPRESSION_NONE;
            break;
        };
    }
  wxDialog::EndModal(wxID_OK);
}

bool TiffGrayscaleDialog::Create(MyMapView * parent, unsigned char compression)
{
//
// creating the dialog
//
  Parent = parent;
  Compression = compression;
  if (wxDialog::Create(parent, wxID_ANY, wxT("Grayscale TIFF")) == false)
    return false;
// populates individual controls
  CreateControls();
// sets dialog sizer
  GetSizer()->Fit(this);
  GetSizer()->SetSizeHints(this);
// centers the dialog window
  Centre();
  return true;
}

void TiffGrayscaleDialog::CreateControls()
{
//
// creating individual control and setting initial values
//
  wxBoxSizer *topSizer = new wxBoxSizer(wxVERTICAL);
  this->SetSizer(topSizer);
  wxBoxSizer *boxSizer = new wxBoxSizer(wxVERTICAL);
  topSizer->Add(boxSizer, 0, wxALIGN_CENTER_HORIZONTAL | wxALL, 5);
// message
  wxString msg =
    wxT("The current image could be safely exported using a Grayscale \n");
  msg +=
    wxT("color space; a Grayscale image simply requires 8-bits for each\n");
  msg +=
    wxT("pixel, thus requiring much less space than a 24-bit RGB image\n\n");
  msg += wxT("Exporting this image as Grayscale is strongly suggested:\n");
  msg += wxT("please confirm\n");
  wxStaticText *label = new wxStaticText(this, wxID_ANY, msg,
                                         wxDefaultPosition, wxDefaultSize,
                                         wxALIGN_LEFT);
  boxSizer->Add(label, 0, wxALIGN_CENTER_VERTICAL | wxALL, 2);
// general parameters
  wxString color[2];
  color[0] = wxT("Export as Grayscale");
  color[1] = wxT("Export as RGB");
  wxRadioBox *colorBox = new wxRadioBox(this, ID_TIFF_COLORSPACE,
                                        wxT("Colorspace Selection"),
                                        wxDefaultPosition,
                                        wxDefaultSize, 2,
                                        color, 1,
                                        wxRA_SPECIFY_ROWS);
  colorBox->SetSelection(0);
  boxSizer->Add(colorBox, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);
// TIFF parameters
  wxString compression[5];
  compression[0] = wxT("None");
  compression[1] = wxT("LZW");
  compression[2] = wxT("DEFLATE");
  compression[3] = wxT("LZMA");
  compression[4] = wxT("JPEG");
  wxRadioBox *compressionBox = new wxRadioBox(this, ID_TIFF_COMPR,
                                              wxT("Compression"),
                                              wxDefaultPosition,
                                              wxDefaultSize, 5,
                                              compression, 2,
                                              wxRA_SPECIFY_ROWS);
  switch (Compression)
    {
      case RL2_COMPRESSION_LZW:
        compressionBox->SetSelection(1);
        break;
      case RL2_COMPRESSION_DEFLATE:
        compressionBox->SetSelection(2);
        break;
      case RL2_COMPRESSION_LZMA:
        compressionBox->SetSelection(3);
        break;
      case RL2_COMPRESSION_JPEG:
        compressionBox->SetSelection(4);
        break;
      default:
        compressionBox->SetSelection(0);
        break;
    };
  boxSizer->Add(compressionBox, 0, wxALIGN_CENTER | wxALL, 2);
// OK-Cancel buttons
  wxBoxSizer *btnSizer = new wxBoxSizer(wxHORIZONTAL);
  boxSizer->Add(btnSizer, 0, wxALIGN_CENTER_HORIZONTAL | wxALL, 0);
  wxButton *okBtn = new wxButton(this, wxID_OK, wxT("&OK"));
  btnSizer->Add(okBtn, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);
  wxButton *cancelBtn = new wxButton(this, wxID_CANCEL, wxT("&Abort"));
  btnSizer->Add(cancelBtn, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);
// appends event handlers
  Connect(ID_TIFF_COLORSPACE, wxEVT_COMMAND_RADIOBOX_SELECTED,
          (wxObjectEventFunction) & TiffGrayscaleDialog::OnColorspaceChanged);
  Connect(wxID_OK, wxEVT_COMMAND_BUTTON_CLICKED,
          (wxObjectEventFunction) & TiffGrayscaleDialog::OnOk);
}


void TiffGrayscaleDialog::OnColorspaceChanged(wxCommandEvent & WXUNUSED(event))
{
//
// TIFF Colorspace changed
//
  wxRadioBox *colorBox = (wxRadioBox *) FindWindow(ID_TIFF_COLORSPACE);
  wxRadioBox *compressionBox = (wxRadioBox *) FindWindow(ID_TIFF_COMPR);
  if (colorBox->GetSelection() == 0)
    compressionBox->Enable(true);
  else
    compressionBox->Enable(false);
}

void TiffGrayscaleDialog::OnOk(wxCommandEvent & WXUNUSED(event))
{
//
// all done: 
//
  wxRadioBox *colorBox = (wxRadioBox *) FindWindow(ID_TIFF_COLORSPACE);
  wxRadioBox *compressionBox = (wxRadioBox *) FindWindow(ID_TIFF_COMPR);
  Grayscale = false;
  if (colorBox->GetSelection() == 0)
    {
      Grayscale = true;
      switch (compressionBox->GetSelection())
        {
          case 1:
            Compression = RL2_COMPRESSION_LZW;
            break;
          case 2:
            Compression = RL2_COMPRESSION_DEFLATE;
            break;
          case 3:
            Compression = RL2_COMPRESSION_LZMA;
            break;
          case 4:
            Compression = RL2_COMPRESSION_JPEG;
            break;
          default:
            Compression = RL2_COMPRESSION_NONE;
            break;
        };
    }
  wxDialog::EndModal(wxID_OK);
}


bool TiffPaletteDialog::Create(MyMapView * parent, unsigned char compression)
{
//
// creating the dialog
//
  Parent = parent;
  Compression = compression;
  if (wxDialog::Create(parent, wxID_ANY, wxT("Palette TIFF")) == false)
    return false;
// populates individual controls
  CreateControls();
// sets dialog sizer
  GetSizer()->Fit(this);
  GetSizer()->SetSizeHints(this);
// centers the dialog window
  Centre();
  return true;
}

void TiffPaletteDialog::CreateControls()
{
//
// creating individual control and setting initial values
//
  wxBoxSizer *topSizer = new wxBoxSizer(wxVERTICAL);
  this->SetSizer(topSizer);
  wxBoxSizer *boxSizer = new wxBoxSizer(wxVERTICAL);
  topSizer->Add(boxSizer, 0, wxALIGN_CENTER_HORIZONTAL | wxALL, 5);
// message
  wxString msg =
    wxT("The current image could be safely exported using a Palette \n");
  msg += wxT("color space; a Palette image simply requires 8-bits for each\n");
  msg +=
    wxT("pixel, thus requiring much less space than a 24-bit RGB image\n\n");
  msg += wxT("Exporting this image as Palette is strongly suggested:\n");
  msg += wxT("please confirm\n");
  wxStaticText *label = new wxStaticText(this, wxID_ANY, msg,
                                         wxDefaultPosition, wxDefaultSize,
                                         wxALIGN_LEFT);
  boxSizer->Add(label, 0, wxALIGN_CENTER_VERTICAL | wxALL, 2);
// general parameters
  wxString color[2];
  color[0] = wxT("Export as Palette");
  color[1] = wxT("Export as RGB");
  wxRadioBox *colorBox = new wxRadioBox(this, ID_TIFF_COLORSPACE,
                                        wxT("Colorspace Selection"),
                                        wxDefaultPosition,
                                        wxDefaultSize, 2,
                                        color, 1,
                                        wxRA_SPECIFY_ROWS);
  colorBox->SetSelection(0);
  boxSizer->Add(colorBox, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);
// TIFF parameters
  wxString compression[4];
  compression[0] = wxT("None");
  compression[1] = wxT("LZW");
  compression[2] = wxT("DEFLATE");
  compression[3] = wxT("LZMA");
  wxRadioBox *compressionBox = new wxRadioBox(this, ID_TIFF_COMPR,
                                              wxT("Compression"),
                                              wxDefaultPosition,
                                              wxDefaultSize, 4,
                                              compression, 2,
                                              wxRA_SPECIFY_ROWS);
  switch (Compression)
    {
      case RL2_COMPRESSION_LZW:
        compressionBox->SetSelection(1);
        break;
      case RL2_COMPRESSION_DEFLATE:
        compressionBox->SetSelection(2);
        break;
      case RL2_COMPRESSION_LZMA:
        compressionBox->SetSelection(3);
        break;
      default:
        compressionBox->SetSelection(0);
        break;
    };
  boxSizer->Add(compressionBox, 0, wxALIGN_CENTER | wxALL, 2);
// OK-Cancel buttons
  wxBoxSizer *btnSizer = new wxBoxSizer(wxHORIZONTAL);
  boxSizer->Add(btnSizer, 0, wxALIGN_CENTER_HORIZONTAL | wxALL, 0);
  wxButton *okBtn = new wxButton(this, wxID_OK, wxT("&OK"));
  btnSizer->Add(okBtn, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);
  wxButton *cancelBtn = new wxButton(this, wxID_CANCEL, wxT("&Abort"));
  btnSizer->Add(cancelBtn, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);
// appends event handlers
  Connect(ID_TIFF_COLORSPACE, wxEVT_COMMAND_RADIOBOX_SELECTED,
          (wxObjectEventFunction) & TiffPaletteDialog::OnColorspaceChanged);
  Connect(wxID_OK, wxEVT_COMMAND_BUTTON_CLICKED,
          (wxObjectEventFunction) & TiffPaletteDialog::OnOk);
}


void TiffPaletteDialog::OnColorspaceChanged(wxCommandEvent & WXUNUSED(event))
{
//
// TIFF Colorspace changed
//
  wxRadioBox *colorBox = (wxRadioBox *) FindWindow(ID_TIFF_COLORSPACE);
  wxRadioBox *compressionBox = (wxRadioBox *) FindWindow(ID_TIFF_COMPR);
  if (colorBox->GetSelection() == 0)
    compressionBox->Enable(true);
  else
    compressionBox->Enable(false);
}

void TiffPaletteDialog::OnOk(wxCommandEvent & WXUNUSED(event))
{
//
// all done: 
//
  wxRadioBox *colorBox = (wxRadioBox *) FindWindow(ID_TIFF_COLORSPACE);
  wxRadioBox *compressionBox = (wxRadioBox *) FindWindow(ID_TIFF_COMPR);
  Palette = false;
  if (colorBox->GetSelection() == 0)
    {
      Palette = true;
      switch (compressionBox->GetSelection())
        {
          case 1:
            Compression = RL2_COMPRESSION_LZW;
            break;
          case 2:
            Compression = RL2_COMPRESSION_DEFLATE;
            break;
          case 3:
            Compression = RL2_COMPRESSION_LZMA;
            break;
          default:
            Compression = RL2_COMPRESSION_NONE;
            break;
        };
    }
  wxDialog::EndModal(wxID_OK);
}
