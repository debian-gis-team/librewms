/*
/ Classdef.h
/ class definitions for LibreWMS
/
/ version 1.0, 2013 July 28
/
/ Author: Sandro Furieri a-furieri@lqt.it
/
/ Copyright (C) 2013  Alessandro Furieri
/
/    This program is free software: you can redistribute it and/or modify
/    it under the terms of the GNU General Public License as published by
/    the Free Software Foundation, either version 3 of the License, or
/    (at your option) any later version.
/
/    This program is distributed in the hope that it will be useful,
/    but WITHOUT ANY WARRANTY; without even the implied warranty of
/    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/    GNU General Public License for more details.
/
/    You should have received a copy of the GNU General Public License
/    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/
*/

#include "wx/wx.h"
#include "wx/graphics.h"
#include "wx/aui/aui.h"
#include "wx/treectrl.h"
#include "wx/grid.h"
#include "wx/html/htmlwin.h"
#include "wx/spinctrl.h"

#include "config.h"

#include <sqlite3.h>
#include <spatialite.h>

#include <rasterlite2/rasterlite2.h>
#include <rasterlite2/rl2wms.h>
#include <rasterlite2/rl2tiff.h>
#include <rasterlite2/rl2graphics.h>

//
// functions for threaded queries
//
#ifdef _WIN32
DWORD WINAPI DoMapDownloadTilesThread(void *arg);
DWORD WINAPI DoPdfDownloadTilesThread(void *arg);
DWORD WINAPI DoTiffDownloadTilesThread(void *arg);
#else
void *DoMapDownloadTilesThread(void *arg);
void *DoPdfDownloadTilesThread(void *arg);
void *DoTiffDownloadTilesThread(void *arg);
#endif
enum
{
// control IDs for main window controls
  ID_Wms = 1,
  ID_Predefined,
  ID_Center,
  ID_Identify,
  ID_ZoomIn,
  ID_ZoomOut,
  ID_UserScale,
  ID_UserRatio,
  ID_Pan,
  ID_Cache,
  ID_WmsAbort,
  ID_Printer,
  Tree_CRS,
  Tree_RemoveAll,
  Tree_ShowAll,
  Tree_HideAll,
  Tree_Visible,
  Tree_RemoveLayer,
  Tree_LayerInfo,
  Tree_Configure,
  Wms_Copy,
  Wms_Layer,
  Wms_Datasource
};

enum
{
// control IDs for dialogs
  ID_WMS_URL = 10000,
  ID_WMS_CATALOG,
  ID_WMS_RESET,
  ID_WMS_VERSION,
  ID_WMS_TILED,
  ID_WMS_WIDTH,
  ID_WMS_HEIGHT,
  ID_WMS_NAME,
  ID_WMS_TITLE,
  ID_WMS_ABSTRACT,
  ID_WMS_CRS,
  ID_WMS_FORMAT,
  ID_WMS_STYLE,
  ID_WMS_TRANSPARENT,
  ID_WMS_SWAP,
  ID_WMS_OK,
  ID_WMS_COUNTRY,
  ID_WMS_STATE,
  ID_WMS_CATEGORY,
  ID_WMS_ENABLE_PROXY,
  ID_WMS_PROXY,
  ID_FILTER_COUNTRY,
  ID_FILTER_STATE,
  ID_FILTER_CATEGORY,
  ID_CACHE_ITEMS,
  ID_CACHE_FLUSHED,
  ID_CACHE_HIT,
  ID_CACHE_MISS,
  ID_CACHE_SIZE,
  ID_CACHE_DOWNLOAD,
  ID_CACHE_MAX,
  ID_CACHE_RESET,
  ID_USER_SCALE,
  ID_USER_RATIO,
  ID_TIFF_WIDTH,
  ID_TIFF_HEIGHT,
  ID_TIFF_GEO,
  ID_TIFF_COMPR,
  ID_TIFF_TILE_SZ,
  ID_TIFF_COLORSPACE,
  ID_PDF_PAGE,
  ID_PDF_DPI,
  ID_PDF_ORIENTATION,
  ID_PDF_TIFF,
  ID_PRINT_MODE,
  ID_MAP_WMS_THREAD_FINISHED,
  ID_PDF_WMS_THREAD_FINISHED,
  ID_TIFF_WMS_THREAD_FINISHED
};

enum
{
// control IDs for timers
  ID_WHEEL_TIMER = 20000,
  ID_UPDATE_TIMER,
  ID_PRINT_TIMER,
  ID_DYNAMIC_TIMER,
};

//
// functions for threaded WMS GetMap download
//
void WmsCallback(int rows, void *ptr);
#ifdef _WIN32
DWORD WINAPI DoExecuteWmsGetMap(void *arg);
#else
void *DoExecuteWmsGetMap(void *arg);
#endif

class MyApp:public wxApp
{
//
// the main APP
//
  virtual bool OnInit();
};

class WmsFormat
{
//
// a class wrapping a WMS format
//
private:
  char *Name;
  WmsFormat *Next;
public:
    WmsFormat(const char *name);
   ~WmsFormat();
  const char *GetName()
  {
    return Name;
  }
  void SetNext(WmsFormat * next)
  {
    Next = next;
  }
  WmsFormat *GetNext()
  {
    return Next;
  }
};

class WmsStyle
{
//
// a class wrapping a WMS style
//
private:
  char *Name;
  char *Title;
  char *Abstract;
  WmsStyle *Next;
public:
    WmsStyle(const char *name, const char *title, const char *abstract);
   ~WmsStyle();
  const char *GetName()
  {
    return Name;
  }
  const char *GetTitle()
  {
    return Title;
  }
  const char *GetAbstract()
  {
    return Abstract;
  }
  void SetNext(WmsStyle * next)
  {
    Next = next;
  }
  WmsStyle *GetNext()
  {
    return Next;
  }
};

class WmsBBox
{
//
// a class wrapping a WMS Bounding Box
//
private:
  double MinX;
  double MinY;
  double MaxX;
  double MaxY;
public:
    WmsBBox(double minx, double miny, double maxx, double maxy)
  {
    MinX = minx;
    MinY = miny;
    MaxX = maxx;
    MaxY = maxy;
  }
   ~WmsBBox()
  {;
  }
  double GetMinX()
  {
    return MinX;
  }
  double GetMinY()
  {
    return MinY;
  }
  double GetMaxX()
  {
    return MaxX;
  }
  double GetMaxY()
  {
    return MaxY;
  }
};

class WmsCRS
{
//
// a class wrapping a WMS CRS definition
//
private:
  char *CRS;
  WmsBBox *BBox;
  WmsCRS *Next;
public:
    WmsCRS(const char *crs);
   ~WmsCRS();
  const char *GetCRS()
  {
    return CRS;
  }
  void SetBBox(WmsBBox * bbox)
  {
    BBox = bbox;
  }
  WmsBBox *GetBBox()
  {
    return BBox;
  }
  void SetNext(WmsCRS * next)
  {
    Next = next;
  }
  WmsCRS *GetNext()
  {
    return Next;
  }
};

class WmsService
{
//
// a class corresponding to a WMS service
//
private:
  char *Version;
  char *Name;
  char *Title;
  char *Abstract;
  char *TileServiceName;
  char *TileServiceTitle;
  char *TileServiceAbstract;
  char *URL_GetMap_Get;
  char *URL_GetMap_Post;
  char *URL_GetTileService_Get;
  char *URL_GetTileService_Post;
  char *URL_GetFeatureInfo_Get;
  char *URL_GetFeatureInfo_Post;
  char *GmlMimeType;
  char *XmlMimeType;
  char *ContactPerson;
  char *ContactOrganization;
  char *ContactPosition;
  char *PostalAddress;
  char *City;
  char *StateProvince;
  char *PostCode;
  char *Country;
  char *VoiceTelephone;
  char *FaxTelephone;
  char *EMailAddress;
  char *Fees;
  char *AccessConstraints;
  int LayerLimit;
  int MaxWidth;
  int MaxHeight;
  int RefCount;
  WmsFormat *FirstFormat;
  WmsFormat *LastFormat;
  WmsService *Prev;
  WmsService *Next;
  bool FormatAlreadyDefined(const char *name);
public:
    WmsService(const char *version, const char *name, const char *title,
               const char *abstract, const char *ts_name,
               const char *ts_title, const char *ts_abstract,
               const char *url_GetMap_get, const char *url_GetMap_post,
               const char *url_GetTileService_get,
               const char *url_GetTileService_post,
               const char *url_GetFeatureInfo_get,
               const char *url_GetFeatureInfo_post,
               const char *gml_mime_type, const char *xml_mime_type,
               const char *contact_person, const char *contact_organization,
               const char *contact_position, const char *postal_address,
               const char *city, const char *state_province,
               const char *post_code, const char *country,
               const char *voice_telephone, const char *fax_telephone,
               const char *email_address, const char *fees,
               const char *access_constraints, int layer_limit,
               int max_width, int max_height);
   ~WmsService();
  const char *GetVersion()
  {
    return Version;
  }
  const char *GetName()
  {
    return Name;
  }
  const char *GetTitle()
  {
    return Title;
  }
  const char *GetAbstract()
  {
    return Abstract;
  }
  const char *GetTileServiceName()
  {
    return TileServiceName;
  }
  const char *GetTileServiceTitle()
  {
    return TileServiceTitle;
  }
  const char *GetTileServiceAbstract()
  {
    return TileServiceAbstract;
  }
  const char *GetURL_GetMap_Get()
  {
    return URL_GetMap_Get;
  }
  const char *GetURL_GetMap_Post()
  {
    return URL_GetMap_Post;
  }
  const char *GetURL_GetTileService_Get()
  {
    return URL_GetTileService_Get;
  }
  const char *GetURL_GetTileService_Post()
  {
    return URL_GetMap_Post;
  }
  const char *GetURL_GetFeatureInfo_Get()
  {
    return URL_GetFeatureInfo_Get;
  }
  const char *GetURL_GetFeatureInfo_Post()
  {
    return URL_GetFeatureInfo_Post;
  }
  const char *GetContactPerson()
  {
    return ContactPerson;
  }
  const char *GetContactOrganization()
  {
    return ContactOrganization;
  }
  const char *GetContactPosition()
  {
    return ContactPosition;
  }
  const char *GetPostalAddress()
  {
    return PostalAddress;
  }
  const char *GetCity()
  {
    return City;
  }
  const char *GetStateProvince()
  {
    return StateProvince;
  }
  const char *GetPostCode()
  {
    return PostCode;
  }
  const char *GetCountry()
  {
    return Country;
  }
  const char *GetVoiceTelephone()
  {
    return VoiceTelephone;
  }
  const char *GetFaxTelephone()
  {
    return FaxTelephone;
  }
  const char *GetEMailAddress()
  {
    return EMailAddress;
  }
  const char *GetFees()
  {
    return Fees;
  }
  const char *GetAccessConstraints()
  {
    return AccessConstraints;
  }
  int GetLayerLimit()
  {
    return LayerLimit;
  }
  int GetMaxWidth()
  {
    return MaxWidth;
  }
  int GetMaxHeight()
  {
    return MaxHeight;
  }
  int GetRefCount()
  {
    return RefCount;
  }
  void AddReference()
  {
    RefCount++;
  }
  void DeleteReference()
  {
    RefCount--;
  }
  void SetPrev(WmsService * prev)
  {
    Prev = prev;
  }
  WmsService *GetPrev()
  {
    return Prev;
  }
  void SetNext(WmsService * next)
  {
    Next = next;
  }
  WmsService *GetNext()
  {
    return Next;
  }
  bool Equals(WmsService * service);
  void AddFormat(const char *name);
  int CountFormats();
  const char *GetFormatByIndex(int idx);
  const char *GetGmlMimeType()
  {
    return GmlMimeType;
  }
  const char *GetXmlMimeType()
  {
    return XmlMimeType;
  }
};

class WmsQueryableChildLayer
{
//
// a class storing the name of some Child Layer
// ensured to be of the queryable type
//
private:
  char *Name;
  char *Title;
  WmsQueryableChildLayer *Next;
public:
    WmsQueryableChildLayer(const char *name, const char *title);
   ~WmsQueryableChildLayer();
  const char *GetName()
  {
    return Name;
  }
  const char *GetTitle()
  {
    return Title;
  }
  void SetNext(WmsQueryableChildLayer * next)
  {
    Next = next;
  }
  WmsQueryableChildLayer *GetNext()
  {
    return Next;
  }
};

class WmsCRScandidate
{
// utility class: common CRS wrapper
private:
  char *CRS;
  int Count;
  WmsCRScandidate *Next;
public:
    WmsCRScandidate(const char *crs);
   ~WmsCRScandidate();
  const char *GetCRS()
  {
    return CRS;
  }
  int GetCount()
  {
    return Count;
  }
  void Increment()
  {
    Count++;
  }
  void SetNext(WmsCRScandidate * next)
  {
    Next = next;
  }
  WmsCRScandidate *GetNext()
  {
    return Next;
  }
};

class WmsCRScandidates
{
// utility class: a list of CRSes candidates
private:
  WmsCRScandidate * First;
  WmsCRScandidate *Last;
  int Count;
public:
    WmsCRScandidates()
  {
    First = NULL;
    Last = NULL;
    Count = 0;
  }
   ~WmsCRScandidates();
  int GetCount()
  {
    return Count;
  }
  void Increment()
  {
    Count++;
  }
  void AddCandidate(const char *crs);
  WmsCRScandidate *GetFirst()
  {
    return First;
  }
};

class WmsTilePattern
{
//
// a class corresponding to a WMS TilePattern
//
private:
  rl2WmsTilePatternPtr Handle;
  char *SRS;
  int TileWidth;
  int TileHeight;
  double TileBaseX;
  double TileBaseY;
  double TileExtentX;
  double TileExtentY;
  WmsTilePattern *Next;
public:
    WmsTilePattern(rl2WmsTilePatternPtr handle, const char *srs, int width,
                   int height, double base_x, double base_y, double ext_x,
                   double ext_y);
   ~WmsTilePattern();
  rl2WmsTilePatternPtr GetHandle()
  {
    return Handle;
  }
  const char *GetSRS()
  {
    return SRS;
  }
  int GetTileWidth()
  {
    return TileWidth;
  }
  int GetTileHeight()
  {
    return TileHeight;
  }
  double GetTileBaseX()
  {
    return TileBaseX;
  }
  double GetTileBaseY()
  {
    return TileBaseY;
  }
  double GetTileExtentX()
  {
    return TileExtentX;
  }
  double GetTileExtentY()
  {
    return TileExtentY;
  }
  void GetSampleURL(wxString & url);
  void SetNext(WmsTilePattern * next)
  {
    Next = next;
  }
  WmsTilePattern *GetNext()
  {
    return Next;
  }
};

class WmsTileServiceLayer
{
//
// a class corresponding to a WMS TileService layer 
//
private:
  char *Name;
  char *Title;
  char *Abstract;
  double MinLat;
  double MaxLat;
  double MinLong;
  double MaxLong;
  char *Pad;
  char *Bands;
  char *DataType;
  int WmsFixedScalesCount;
  double *WmsFixedScales;
  WmsTilePattern *First;
  WmsTilePattern *Last;
public:
    WmsTileServiceLayer(const char *name, const char *title,
                        const char *abstract, double min_lat,
                        double min_long, double max_lat, double max_long,
                        const char *pad, const char *bands,
                        const char *data_type);
   ~WmsTileServiceLayer();
  const char *GetName()
  {
    return Name;
  }
  const char *GetTitle()
  {
    return Title;
  }
  const char *GetAbstract()
  {
    return Abstract;
  }
  double GetMinLat()
  {
    return MinLat;
  }
  double GetMinLong()
  {
    return MinLong;
  }
  double GetMaxLat()
  {
    return MaxLat;
  }
  double GetMaxLong()
  {
    return MaxLong;
  }
  const char *GetPad()
  {
    return Pad;
  }
  const char *GetBands()
  {
    return Bands;
  }
  const char *GetDataType()
  {
    return DataType;
  }
  void AddTilePattern(rl2WmsTilePatternPtr handle, const char *srs,
                      int width, int height, double base_x, double base_y,
                      double ext_x, double ext_y);
  WmsTilePattern *GetFirst()
  {
    return First;
  }
  WmsTileServiceLayer *Clone();
  void BuildFixedScales(void);
  int GetFixedScalesCount()
  {
    return WmsFixedScalesCount;
  }
  double *GetFixedScales()
  {
    return WmsFixedScales;
  }
  bool CompareFixedScales(WmsTileServiceLayer * other);
  bool Equals(WmsTileServiceLayer * other);
  bool GetTileParams(double scale, double frame_minx, double frame_miny,
                     int *tile_width, int *tile_height, double *tileW,
                     double *tileH, double *baseX, double *baseY,
                     WmsTilePattern ** tilepattern);
};

class WmsLayer
{
//
// a class corresponding to a WMS layer 
//
private:
  WmsService * Service;
  WmsTileServiceLayer *TileLayer;
  char *Name;
  char *Title;
  char *Abstract;
  int Tiled;
  int TileWidth;
  int TileHeight;
  int Queryable;
  int Opaque;
  char *Version;
  char *StyleName;
  char *StyleTitle;
  char *StyleAbstract;
  char *Format;
  char *CRS;
  int SwapXY;
  double MinX;
  double MaxX;
  double MinY;
  double MaxY;
  double GeoMinX;
  double GeoMaxX;
  double GeoMinY;
  double GeoMaxY;
  double MinScaleDenominator;
  double MaxScaleDenominator;
  int Visible;
  WmsStyle *FirstStyle;
  WmsStyle *LastStyle;
  WmsCRS *FirstCRS;
  WmsCRS *LastCRS;
  WmsQueryableChildLayer *FirstChild;
  WmsQueryableChildLayer *LastChild;
  WmsLayer *Prev;
  WmsLayer *Next;
  bool StyleAlreadyDefined(const char *name, const char *title,
                           const char *abstract);
  bool CrsAlreadyDefined(const char *crs);
public:
    WmsLayer(WmsService * service, const char *name, const char *title,
             const char *abstract, int tiled, int tile_width,
             int tile_height, int queryable, int opaque, const char *version,
             const char *style, const char *style_title,
             const char *style_abstract, const char *format, const char *crs,
             int swap_xy, double min_x, double max_x, double min_y,
             double max_y, double geo_min_x, double geo_max_x,
             double geo_min_y, double geo_max_y,
             double min_scale_denominator, double max_scale_denominator);
    WmsLayer(WmsService * service, const char *name, const char *title,
             const char *abstract, double min_long, double min_lat,
             double max_long, double max_lat, const char *pad,
             const char *bands, const char *data_type);
   ~WmsLayer();
  void Reconfigure(int tiled, int tile_width,
                   int tile_height, int opaque, const char *version,
                   const char *style, const char *style_title,
                   const char *style_abstract, const char *format,
                   const char *crs, int swap_xy, double min_x, double max_x,
                   double min_y, double max_y);
  WmsService *GetWmsService()
  {
    return Service;
  }
  WmsTileServiceLayer *GetTileServiceLayer()
  {
    return TileLayer;
  }
  bool IsTiled()
  {
    return Tiled;
  }
  int GetTileWidth()
  {
    return TileWidth;
  }
  int GetTileHeight()
  {
    return TileHeight;
  }
  bool IsQueryable()
  {
    if (Queryable)
      return true;
    else
      return false;
  }
  bool HasQueryableChildren()
  {
    if (FirstChild != NULL)
      return true;
    else
      return false;
  }
  bool IsOpaque()
  {
    return Opaque;
  }
  const char *GetName()
  {
    return Name;
  }
  const char *GetTitle()
  {
    return Title;
  }
  const char *GetAbstract()
  {
    return Abstract;
  }
  const char *GetVersion()
  {
    return Version;
  }
  const char *GetStyleName()
  {
    return StyleName;
  }
  const char *GetStyleTitle()
  {
    return StyleTitle;
  }
  const char *GetStyleAbstract()
  {
    return StyleAbstract;
  }
  const char *GetFormat()
  {
    return Format;
  }
  const char *GetCRS();
  bool ChangeCRS(class MyFrame * MainFrame, const char *crs);
  int IsSwapXY()
  {
    return SwapXY;
  }
  void ChangeSwapXY(void)
  {
    if (SwapXY == 0)
      SwapXY = 1;
    else
      SwapXY = 0;
  }
  double GetMinX()
  {
    return MinX;
  }
  double GetMaxX()
  {
    return MaxX;
  }
  double GetMinY()
  {
    return MinY;
  }
  double GetMaxY()
  {
    return MaxY;
  }
  double GetGeoMinX()
  {
    return GeoMinX;
  }
  double GetGeoMaxX()
  {
    return GeoMaxX;
  }
  double GetGeoMinY()
  {
    return GeoMinY;
  }
  double GetGeoMaxY()
  {
    return GeoMaxY;
  }
  double GetMinScaleDenominator()
  {
    return MinScaleDenominator;
  }
  double GetMaxScaleDenominator()
  {
    return MaxScaleDenominator;
  }
  int IsVisible()
  {
    return Visible;
  }
  bool IsCovered(double minx, double miny, double maxx, double maxy);
  void MaxLimits(double *maxx, double *maxy);
  void Hide()
  {
    Visible = 0;
  }
  void Show()
  {
    Visible = 1;
  }
  void SetPrev(WmsLayer * prev)
  {
    Prev = prev;
  }
  WmsLayer *GetPrev()
  {
    return Prev;
  }
  void SetNext(WmsLayer * next)
  {
    Next = next;
  }
  WmsLayer *GetNext()
  {
    return Next;
  }
  WmsLayer *Clone();
  bool Equals(WmsLayer * service);
  void AddStyle(const char *name, const char *title, const char *abstract);
  int CountStyles();
  bool GetStyleByIndex(int idx, const char **name, const char **title,
                       const char **abstract);
  WmsCRS *AddCRS(const char *name);
  int CountCRS();
  const char *GetCrsByIndex(int idx);
  bool HasBBox(const char *crs);
  void GetBBox(const char *crs, double *minx, double *miny, double *maxx,
               double *maxy);
  void AddQueryableChild(const char *name, const char *title);
  WmsQueryableChildLayer *GetFirstChild()
  {
    return FirstChild;
  }
  WmsStyle *GetFirstStyle()
  {
    return FirstStyle;
  }
};

class LayerObject:public wxTreeItemData
{
//
// a class to store TreeItemData - WMS Layer wrapper
//
private:
  WmsLayer * Layer;
public:
  LayerObject(WmsLayer * lyr)
  {
    Layer = lyr;
  }
  virtual ~ LayerObject()
  {;
  }
  WmsLayer *GetLayer()
  {
    return Layer;
  }
};

class MyLayerTree:public wxTreeCtrl
{
//
// a tree-control used for Map Layers
//
private:
  class MyFrame * MainFrame;
  bool Changed;
  wxTreeItemId Root;            // the root node
  wxImageList *Images;          // the images list
  wxTreeItemId CurrentItem;     // the tree item holding the current context menu
  wxTreeItemId DraggedItem;     // the tree item to be moved
  void AddOnBitmap(wxBitmap & bitmap);
  void AddOffBitmap(wxBitmap & bitmap);
public:
    MyLayerTree()
  {;
  }
  MyLayerTree(class MyFrame * parent, wxWindowID id = wxID_ANY);
  virtual ~ MyLayerTree();
  void FlushAll()
  {
    DeleteChildren(Root);
    Changed = true;
  }
  wxTreeItemId & GetRoot()
  {
    return Root;
  }
  void AddLayer(WmsLayer * layer);
  void SetLayerIcons();
  void MarkCurrentItem();
  void UpdateDefault(wxTreeItemId item, wxImageList * imageList);
  void OnSelChanged(wxTreeEvent & event);
  void OnItemActivated(wxTreeEvent & event);
  void OnRightClick(wxTreeEvent & event);
  void OnCmdMapCRS(wxCommandEvent & event);
  void OnCmdRemoveAll(wxCommandEvent & event);
  void OnCmdShowAll(wxCommandEvent & event);
  void OnCmdHideAll(wxCommandEvent & event);
  void OnCmdVisible(wxCommandEvent & event);
  void OnCmdConfigure(wxCommandEvent & event);
  void OnCmdRemoveLayer(wxCommandEvent & event);
  void OnCmdLayerInfo(wxCommandEvent & event);
  void OnDragStart(wxTreeEvent & event);
  void OnDragEnd(wxTreeEvent & event);
};

class CatalogObject:public wxTreeItemData
{
//
// a class to store TreeItemData - WMS Catalog wrapper
//
private:
  rl2WmsLayerPtr LayerHandle;
  rl2WmsTiledLayerPtr TiledLayerHandle;
  const char *LayerName;
public:
    CatalogObject(const char *lyr)
  {
    LayerHandle = NULL;
    TiledLayerHandle = NULL;
    LayerName = lyr;
  }
  CatalogObject(rl2WmsLayerPtr handle, const char *lyr)
  {
    LayerHandle = handle;
    TiledLayerHandle = NULL;
    LayerName = lyr;
  }
  CatalogObject(rl2WmsTiledLayerPtr handle, const char *lyr)
  {
    LayerHandle = NULL;
    TiledLayerHandle = handle;
    LayerName = lyr;
  }
  virtual ~ CatalogObject()
  {;
  }
  rl2WmsLayerPtr GetHandle()
  {
    return LayerHandle;
  }
  rl2WmsTiledLayerPtr GetTiledHandle()
  {
    return TiledLayerHandle;
  }
  const char *GetLayerName()
  {
    return LayerName;
  }
};

class MyCatalogTree:public wxTreeCtrl
{
//
// a tree-control used for Service Layers
//
private:
  class WmsDialog * MainDialog;
  wxTreeItemId Root;            // the root node
  wxImageList *Images;          // the images list
  wxTreeItemId CurrentItem;     // the tree item holding the current context menu
  void ExpandChildren(wxTreeItemId item, rl2WmsLayerPtr handle);
  void ExpandChildren(wxTreeItemId item, rl2WmsTiledLayerPtr handle);
public:
    MyCatalogTree()
  {;
  }
  MyCatalogTree(WmsDialog * parent, wxSize sz, wxWindowID id = wxID_ANY);
  virtual ~ MyCatalogTree();
  void FlushAll()
  {
    DeleteAllItems();
    Root = wxTreeItemId();
  }
  void AddLayer(rl2WmsLayerPtr handle, const char *layer);
  void AddTiledRoot(const char *name);
  void AddTiledLayer(rl2WmsTiledLayerPtr handle, const char *layer);
  void AddLayer(wxTreeItemId parent, rl2WmsLayerPtr handle, const char *layer);
  void AddTiledLayer(wxTreeItemId parent, rl2WmsTiledLayerPtr handle,
                     const char *layer);
  void SetLayerIcons();
  void MarkCurrentItem();
  void RootAutoSelect(void);
  void OnSelChanged(wxTreeEvent & event);
  void OnCmdShowAll(wxCommandEvent & event);
  void OnCmdHideAll(wxCommandEvent & event);
};

class WmsTileRequest
{
//
// a class wrapping a single Tile request
//
private:
  int Row;
  int Col;
  double MinX;
  double MinY;
  double MaxX;
  double MaxY;
  int ImageX;
  int ImageY;
  WmsTileRequest *Next;
public:
    WmsTileRequest(int row, int col, double minx, double miny, double maxx,
                   double maxy, int image_x, int image_y)
  {
    Row = row;
    Col = col;
    MinX = minx;
    MinY = miny;
    MaxX = maxx;
    MaxY = maxy;
    ImageX = image_x;
    ImageY = image_y;
    Next = NULL;
  }
   ~WmsTileRequest()
  {;
  }
  int GetRow()
  {
    return Row;
  }
  int GetCol()
  {
    return Col;
  }
  double GetMinX()
  {
    return MinX;
  }
  double GetMinY()
  {
    return MinY;
  }
  double GetMaxX()
  {
    return MaxX;
  }
  double GetMaxY()
  {
    return MaxY;
  }
  int GetImageX()
  {
    return ImageX;
  }
  int GetImageY()
  {
    return ImageY;
  }
  void SetNext(WmsTileRequest * next)
  {
    Next = next;
  }
  WmsTileRequest *GetNext()
  {
    return Next;
  }
};

class WmsTileSet
{
//
// a class wrapping a complex Tile-set
//
private:
  double FrameMinX;
  double FrameMinY;
  double FrameMaxX;
  double FrameMaxY;
  double BaseX;
  double BaseY;
  double TileHorzSize;
  double TileVertSize;
  int ImageOriginX;
  int ImageOriginY;
  int ImageWidth;
  int ImageHeight;
  int TileWidth;
  int TileHeight;
  WmsTileRequest *First;
  WmsTileRequest *Last;
public:
    WmsTileSet(double minx, double miny, double maxx, double maxy,
               double base_x, double base_y, double tile_horz,
               double tile_vert, int image_x, int image_y, int image_w,
               int image_h, int tile_w, int tile_h);
   ~WmsTileSet();
  int GetTileWidth()
  {
    return TileWidth;
  }
  int GetTileHeight()
  {
    return TileHeight;
  }
  void AddTile(double minx, double miny, int row, int col);
  WmsTileRequest *GetFirst()
  {
    return First;
  }
};

class WmsGetMapRequest
{
// parameters for WMS GetMap download
private:
  int PosX;
  int PosY;
  rl2WmsCachePtr CacheHandle;
  const char *Url;
  const char *Proxy;
  const char *Version;
  const char *Layer;
  const char *Crs;
  int SwapXY;
  double MinX;
  double MinY;
  double MaxX;
  double MaxY;
  int Width;
  int Height;
  const char *Style;
  const char *Format;
  int Opaque;
  WmsTilePattern *TilePattern;
  bool CacheHit;
  wxImage Image;
  unsigned char *RGBA;
  bool ImageReady;
  bool ImageDone;
  bool Ignore;
  WmsGetMapRequest *Next;
public:
    WmsGetMapRequest(int pos_x, int pos_y,
                     rl2WmsCachePtr cache_handle, const char *url,
                     const char *proxy, const char *version,
                     const char *layer, const char *crs, int swap_xy,
                     double minx, double miny, double maxx, double maxy,
                     int width, int height, const char *style,
                     const char *format, int opaque, int with_image);
    WmsGetMapRequest(int pos_x, int pos_y,
                     rl2WmsCachePtr cache_handle, const char *url,
                     const char *proxy, const char *layer, int swap_xy,
                     double minx, double miny, double maxx, double maxy,
                     int width, int height, WmsTilePattern * tilepattern,
                     int with_image);
   ~WmsGetMapRequest()
  {
    if (RGBA != NULL)
      free(RGBA);
  }
  bool ExecuteMap(int from_cache);
  bool ExecutePrint(int from_cache);
  const char *GetLayerName()
  {
    return Layer;
  }
  int GetPosX()
  {
    return PosX;
  }
  int GetPosY()
  {
    return PosY;
  }
  int GetWidth()
  {
    return Width;
  }
  int GetHeight()
  {
    return Height;
  }
  bool IsImageReady()
  {
    return ImageReady;
  }
  bool IsImageDone()
  {
    return ImageDone;
  }
  void Done();
  wxImage & GetImage()
  {
    return Image;
  }
  unsigned char *GetRGBA()
  {
    return RGBA;
  }
  bool IsCacheHit()
  {
    return CacheHit;
  }
  bool IsTileService()
  {
    if (TilePattern != NULL)
      return true;
    else
      return false;
  }
  bool ToBeIgnored()
  {
    return Ignore;
  }
  void Consider()
  {
    Ignore = false;
  }
  void SetNext(WmsGetMapRequest * next)
  {
    Next = next;
  }
  WmsGetMapRequest *GetNext()
  {
    return Next;
  }
};

class GeometryRef
{
// a reference to some Geometry
private:
  int LayerId;
  int LineNo;
  int ColNo;
  gaiaGeomCollPtr Geometry;
  GeometryRef *Next;
public:
    GeometryRef(int layer_id, int line_no, int col_no, gaiaGeomCollPtr geom)
  {
    LayerId = layer_id;
    LineNo = line_no;
    ColNo = col_no;
    Geometry = geom;
    Next = NULL;
  }
   ~GeometryRef()
  {;
  }
  int GetLayerId()
  {
    return LayerId;
  }
  int GetLineNo()
  {
    return LineNo;
  }
  int GetColNo()
  {
    return ColNo;
  }
  gaiaGeomCollPtr GetGeometry()
  {
    return Geometry;
  }
  void SetNext(GeometryRef * next)
  {
    Next = next;
  }
  GeometryRef *GetNext()
  {
    return Next;
  }
};

class GeometryList
{
// a container for GetFeatureInfo Geometries
private:
  GeometryRef * First;
  GeometryRef *Last;
public:
    GeometryList()
  {
    First = NULL;
    Last = NULL;
  }
   ~GeometryList();
  void Add(int layer_id, int line_no, int col_no, gaiaGeomCollPtr geom);
  gaiaGeomCollPtr Find(int layer_id, int line_no, int col_no);
};

class WmsIdentifyPanel:public wxWindow
{
//
// a panel for showing GetFeatureInfo returned by a WMS Layer
//
private:
  class MyMapView * MapView;
  rl2WmsFeatureCollectionPtr Collection;
  GeometryList *List;
  wxString Html;
  wxHtmlWindow *HtmlCtrl;
  void ParseHREF(wxString & href, int *layer_id, int *feature_id, int *col_no);
public:
    WmsIdentifyPanel(class MyMapView * map_view, wxString & html,
                     rl2WmsFeatureCollectionPtr coll, GeometryList * list,
                     int x, int y);
    virtual ~ WmsIdentifyPanel()
  {
    Reset();
  }
  void Reset();
  void Initialize(wxString & html, rl2WmsFeatureCollectionPtr coll,
                  GeometryList * list, int x, int y);
  void CreateControls();
  void Resize();
  void OnSize(wxSizeEvent & event);
  void OnLinkClicked(wxHtmlLinkEvent & event);
  void OnCancel(wxCommandEvent & event);
};

class MyMapView:public wxPanel
{
//
// a panel used  to show the Map
//
private:
  class MyFrame * MainFrame;
  WmsIdentifyPanel *IdentifyPanel;
  wxBitmap MapBitmap;
  wxBitmap ScreenBitmap;
  bool DynamicOddEven;
  int BitmapWidth;
  int BitmapHeight;
  wxCursor CursorCross;
  wxCursor CursorHand;
  bool ValidMap;
  double MapCX;
  double MapCY;
  double MapMinX;
  double MapMaxX;
  double MapMinY;
  double MapMaxY;
  double MapExtentX;
  double MapExtentY;
  int FrameWidth;
  int FrameHeight;
  double FrameCX;
  double FrameCY;
  double FrameExtentX;
  double FrameExtentY;
  double FrameMinX;
  double FrameMinY;
  double FrameMaxX;
  double FrameMaxY;
  double PixelRatio;
  int DragStartX;
  int DragStartY;
  int LastDragX;
  int LastDragY;
  int CurrentScale;
  WmsService *FirstService;
  WmsService *LastService;
  WmsLayer *FirstLayer;
  WmsLayer *LastLayer;
  WmsLayer *ActiveLayer;
  WmsCRS *FirstCRS;
  WmsCRS *LastCRS;
  int WmsFixedScalesCount;
  double *WmsFixedScales;
// Timers
  int MouseLastX;
  int MouseLastY;
  int WheelTics;
  wxTimer *TimerMouseWheel;
  int DynamicPhase;
  wxTimer *TimerDynamic;
  wxTimer *UpdateTimer;
  wxTimer *PrintTimer;
  WmsGetMapRequest *FirstRequest;
  WmsGetMapRequest *LastRequest;
  char *Proxy;
  void CleanHtml(const char *dirty, wxString & clean);
  wxGauge *ProgressControl;
  bool DownloadInProgress;
  bool PrintInProgress;
  bool PendingRefresh;
  bool PendingOnSize;
  bool PendingWmsAbort;
  void HtmlFromGetFeatureInfo(rl2WmsFeatureCollectionPtr coll, int idx,
                              wxString & layer_name, wxString & html);
  void AddGetMapRequest(int pos_x, int pos_y, const char *url,
                        const char *version,
                        const char *layer,
                        const char *crs, int swap_xy,
                        double minx, double miny,
                        double maxx, double maxy,
                        int width, int height,
                        const char *style, const char *format, int opaque);
  void AddGetMapRequest(int pos_x, int pos_y, const char *url,
                        const char *layer, int swap_xy,
                        double minx, double miny,
                        double maxx, double maxy, int width, int height,
                        WmsTilePattern * tilepattern);
  void ResetGetMapRequests();
  int CountGetMapRequests();
  void UpdatePendingTiles();
  void ResetCRSlist();
  class PdfPrinter *Pdf;
  class TiffWriter *Tiff;
  wxImage ImageMarkerOdd;
  wxImage ImageMarkerEven;
public:
    MyMapView()
  {;
  }
  MyMapView(MyFrame * parent, wxWindowID id = wxID_ANY);
  virtual ~ MyMapView();

  WmsService *InsertWmsService(const char *version, const char *name,
                               const char *title, const char *ts_name,
                               const char *ts_title, const char *ts_service,
                               const char *abstract,
                               const char *url_GetMap_get,
                               const char *url_GetMap_post,
                               const char *url_GetTileService_get,
                               const char *url_GetTileService_post,
                               const char *url_GetFeatureInfo_get,
                               const char *url_GetFeatureInfo_post,
                               const char *gml_mime_type,
                               const char *xml_mime_type,
                               const char *contact_person,
                               const char *contact_organization,
                               const char *contact_position,
                               const char *postal_address, const char *city,
                               const char *state_province,
                               const char *post_code, const char *country,
                               const char *voice_telephone,
                               const char *fax_telephone,
                               const char *email_address, const char *fees,
                               const char *access_constraints,
                               int layer_limit, int max_width, int max_height);
  void InsertWmsLayer(WmsLayer * layer);
  void RemoveWmsLayer(WmsLayer * layer);
  void RemoveWmsService(WmsService * service);
  WmsService *FindAlreadyDefined(WmsService * service);
  void CheckWmsLayer(WmsLayer * layer, bool * already_defined,
                     bool * mixed_crs, bool * multiple_fixed_scales);
  void ResetWmsLayers();
  void SetWmsAbort()
  {
    PendingWmsAbort = true;
  }
  void SetPrintInProgress(PdfPrinter * pdf);
  void SetPrintInProgress(TiffWriter * pdf);
  bool IsPendingWmsAbort()
  {
    return PendingWmsAbort;
  }
  bool IsDownloadInProgress()
  {
    return DownloadInProgress;
  }
  bool IsPrintInProgress()
  {
    return PrintInProgress;
  }
  void BuildCRSlist();
  int GetSridFromCRS(const char *crs);
  void SetMapCRS(const char *crs);
  WmsCRS *GetFirstCRS()
  {
    return FirstCRS;
  }
  void ReinsertWmsLayer(WmsLayer * layer);
  int CountWmsLayers();
  void BuildFixedScales();
  void ResetScreenBitmap();
  void UpdateTile(wxImage & Image, int pos_x, int pos_y, int width, int height);
  void PrepareMap();
  void SetFullExtent();
  void Invalidate();
  void SetActiveWmsLayer(WmsLayer * layer)
  {
    ActiveLayer = layer;
  }
  bool IsValidMap()
  {
    return ValidMap;
  }
  void DragMap(int x, int y);
  void DoIdentify(int x, int y);
  void DoIdentifyMultiLayer(int x, int y);
  bool CanIdentify();
  const char *GetMapCRS();
  int GetCurrentScale()
  {
    return CurrentScale;
  }
  void SetCurrentScale(int scale);
  WmsLayer *GetLastLayer()
  {
    return LastLayer;
  }
  WmsGetMapRequest *GetFirstRequest()
  {
    return FirstRequest;
  }
  void Consider(const char *layer_name);
  void Consider(void);
  double GetFrameCX()
  {
    return FrameCX;
  }
  double GetFrameCY()
  {
    return FrameCY;
  }
  double GetFrameMinX()
  {
    return FrameMinX;
  }
  double GetFrameMinY()
  {
    return FrameMinY;
  }
  double GetFrameMaxX()
  {
    return FrameMaxX;
  }
  double GetFrameMaxY()
  {
    return FrameMaxY;
  }
  double GetPixelRatio()
  {
    return PixelRatio;
  }
  bool IsWmsFixedScale(void)
  {
    if (WmsFixedScalesCount > 0)
      return true;
    return false;
  }
  double GetWmsBestResolution(double pixel_ratio);
  void GetTileOrigin(double minx, double miny, double start_x,
                     double start_y, double tile_width, double tile_height,
                     double *base_x, double *base_y);
  const char *GetProxy()
  {
    return Proxy;
  }
  void PopulateGeometryList(class GeometryList * list, int layer_no,
                            rl2WmsFeatureCollectionPtr coll);
  void MarkGeometry(gaiaGeomCollPtr geom);
  void ResetMarker();
  void BuildMarker(wxImage & image, bool odd_even, gaiaGeomCollPtr geom);

  void OnSize(wxSizeEvent & event);
  void OnPaint(wxPaintEvent & event);
  void OnMouseMove(wxMouseEvent & event);
  void OnMouseWheel(wxMouseEvent & event);
  void OnTimerMouseWheel(wxTimerEvent & event);
  void OnTimerDynamic(wxTimerEvent & event);
  void OnTimerUpdate(wxTimerEvent & event);
  void OnTimerPrint(wxTimerEvent & event);
  void OnMouseClick(wxMouseEvent & event);
  void OnMouseDoubleClick(wxMouseEvent & event);
  void OnMouseDragStop(wxMouseEvent & event);
  void OnEraseBackground(wxEraseEvent & WXUNUSED(event))
  {;
  }
  void OnRefreshTimer(wxTimerEvent & event);
  void OnMapWmsThreadFinished(wxCommandEvent & event);
  void OnPdfWmsThreadFinished(wxCommandEvent & event);
  void OnTiffWmsThreadFinished(wxCommandEvent & event);
};

class MyFrame:public wxFrame
{
//
// the main GUI frame
//
private:
  wxAuiManager Manager;         // the GUI manager
  wxString ConfigLayout;        // PERSISTENCY - the layout configuration
  int ConfigPaneX;              // PERSISTENCY - the main pane screen origin X
  int ConfigPaneY;              // PERSISTENCY - the main pane screen origin Y
  int ConfigPaneWidth;          // PERSISTENCY - the main pane screen width
  int ConfigPaneHeight;         // PERSISTENCY - the main pane screen height
  wxString HttpProxy;           // PERSISTENCY - the last used HTTP Proxy
  MyLayerTree *LayerTree;       // the layer tree list
  MyMapView *MapView;           // the map panel
  wxString LastDirectory;       // path of directory used  
  wxString CurrentWmsDatasource;
  sqlite3 *SQLiteHandle;        // handle for SQLite DB
  void *InternalCache;          // pointer to the InternalCache supporting the DB connection
  wxBitmap *BtnWms;             // button icon for WMS add
  wxBitmap *BtnCenter;          // button icon for Zoom to Full Extent
  wxBitmap *BtnIdentify;        // buttom icon for Identify
  wxBitmap *BtnZoomIn;          // button icon for Zoom In
  wxBitmap *BtnZoomOut;         // buttom icon for Zoom Out
  wxBitmap *BtnUserScale;       // buttom icon for User Scale
  wxBitmap *BtnPan;             // button icon for Pan
  wxBitmap *BtnCache;           // button icon for WMS Cache
  wxBitmap *BtnWmsAbort;        // button icon for WMS Abort
  wxBitmap *BtnPrinter;         // button icon for Printer
  wxBitmap *BtnAbout;           // button icon for ABOUT
  wxBitmap *BtnExit;            // button icon for EXIT
  bool IsIdentify;              // current map click is: Identify
  bool IsZoomIn;                // current map click is: ZoomIn
  bool IsZoomOut;               // current map click is: ZoomOut
  bool IsPan;                   // current map click is: Pan
  rl2WmsCachePtr WmsCache;      // internal WMS Cache
  void WmsSelection(bool immediate);
  void AddQueryableChildren(WmsLayer * lyr, rl2WmsLayerPtr handle);
public:
    MyFrame(const wxString & title, const wxPoint & pos, const wxSize & size);
    virtual ~ MyFrame();

  void SaveConfig();
  void LoadConfig();
  void CreateMemoryDB();
  void CloseMemoryDB();
    wxString & GetCurrentWmsDatasource()
  {
    return CurrentWmsDatasource;
  }
  void InitializeSpatialMetadata();
  void InitializeWmsList();
  sqlite3 *GetSQLite()
  {
    return SQLiteHandle;
  }
  void SetLastDirectory(wxString & path)
  {
    LastDirectory = path;
  }
  wxString & GetLastDirectory()
  {
    return LastDirectory;
  }
  void AddPredefinedWMS(const char *country, const char *state_province,
                        const char *category, const char *url);
  void UpdateTools();
  void UpdateWmsAbortTool(bool mode);
  void UpdatePrinterTool(bool mode);
  void ClearLayerTree();
  bool IsModeIdentify()
  {
    return IsIdentify;
  }
  bool IsModeZoomIn()
  {
    return IsZoomIn;
  }
  bool IsModeZoomOut()
  {
    return IsZoomOut;
  }
  bool IsModePan()
  {
    return IsPan;
  }
  bool IsGeographicCRS(const char *crs);
  bool IsSwapXYcrs(const char *crs);
  bool BBoxFromLongLat(const char *crs_str, double *minx, double *maxx,
                       double *miny, double *maxy);
  char *GetProjParams(int srid);
  wxString & GetHttpProxy()
  {
    return HttpProxy;
  }
  rl2WmsCachePtr GetWmsCache()
  {
    return WmsCache;
  }

  wxBitmap *GetBtnCenter()
  {
    return BtnCenter;
  }
  void SetFullExtent()
  {
    MapView->SetFullExtent();
  }
  void RefreshMap()
  {
    MapView->PrepareMap();
  }

  MyLayerTree *GetLayerTree()
  {
    return LayerTree;
  }
  MyMapView *GetMapView()
  {
    return MapView;
  }
  WmsCRS *GetFirstMapCRS()
  {
    return MapView->GetFirstCRS();
  }
  void SetActiveWmsLayer(WmsLayer * layer)
  {
    MapView->SetActiveWmsLayer(layer);
    UpdateTools();
  }

  void SetMapCRS(const char *crs)
  {
    MapView->SetMapCRS(crs);
  }
  void UpdateMapCRS();
  const char *GetMapCRS()
  {
    return MapView->GetMapCRS();
  }
  void UpdateMapScale();
  void UpdateMapCoords(wxString & coords);
  bool GetProgressRect(wxRect & rect);

  void OnWms(wxCommandEvent & event);
  void OnPredefinedWms(wxCommandEvent & event);
  void OnCenter(wxCommandEvent & event);
  void OnIdentify(wxCommandEvent & event);
  void OnZoomIn(wxCommandEvent & event);
  void OnZoomOut(wxCommandEvent & event);
  void OnUserScale(wxCommandEvent & event);
  void OnPan(wxCommandEvent & event);
  void OnWmsCache(wxCommandEvent & event);
  void OnWmsAbort(wxCommandEvent & event);
  void OnPrinter(wxCommandEvent & event);
  void OnQuit(wxCommandEvent & event);
  void OnAbout(wxCommandEvent & event);
  void OnClose(wxCloseEvent & event);
};

class PredefinedWmsDialog:public wxDialog
{
//
// a dialog for selecting a pre-defines WMS datasource
//
private:
  MyFrame * MainFrame;
  sqlite3 *SQLiteHandle;
  wxGrid *WmsView;
  int CurrentEvtRow;
  int CurrentEvtColumn;
  wxComboBox *CountryList;
  wxComboBox *StateList;
  wxComboBox *CategoryList;
  char *CountryFilter;
  char *StateFilter;
  char *CategoryFilter;
  wxString URL;
  void SelectWmsDatasource();
  void PopulateCountries();
  void PopulateStates();
  void PopulateCategories();
  void PopulateGrid();
public:
    PredefinedWmsDialog()
  {
    WmsView = NULL;
    CountryList = NULL;
    StateList = NULL;
    CategoryList = NULL;
    CountryFilter = NULL;
    StateFilter = NULL;
    CategoryFilter = NULL;
  }
  virtual ~ PredefinedWmsDialog()
  {
    if (CountryFilter)
      delete[]CountryFilter;
    if (StateFilter)
      delete[]StateFilter;
    if (CategoryFilter)
      delete[]CategoryFilter;
  }
  bool Create(MyFrame * parent, sqlite3 * sqlite);
  void CreateControls();
  wxString & GetURL()
  {
    return URL;
  }
  void OnLeftClick(wxGridEvent & event);
  void OnRightClick(wxGridEvent & event);
  void OnOk(wxCommandEvent & event);
  void OnCancel(wxCommandEvent & event);
  void OnCmdCopy(wxCommandEvent & event);
  void OnCmdSelectWmsDatasource(wxCommandEvent & event);
  void OnCountrySelected(wxCommandEvent & event);
  void OnStateSelected(wxCommandEvent & event);
  void OnCategorySelected(wxCommandEvent & event);
};

class WmsDialog:public wxDialog
{
//
// a dialog for selecting a WMS datasource
//
private:
  MyFrame * MainFrame;
  wxString URL;
  MyCatalogTree *WmsTree;
  rl2WmsCatalogPtr Catalog;
  rl2WmsLayerPtr CurrentLayer;
  rl2WmsTiledLayerPtr CurrentTiledLayer;
  int MaxWidth;
  int MaxHeight;
  char *Version;
  int SwapXY;
  bool ProxyEnabled;
  wxString HttpProxy;
public:
    WmsDialog()
  {
    Catalog = NULL;
    WmsTree = NULL;
    Version = NULL;
    CurrentLayer = NULL;
    CurrentTiledLayer = NULL;
    ProxyEnabled = false;
  }
  virtual ~ WmsDialog()
  {
    if (Catalog != NULL)
      destroy_wms_catalog(Catalog);
    if (Version != NULL)
      delete[]Version;
  }
  bool Create(MyFrame * parent, wxString & proxy, bool immediate);
  void CreateControls();
  void SelectLayer(void);
  void SelectLayer(rl2WmsLayerPtr layer);
  void SelectLayer(rl2WmsTiledLayerPtr layer);
  int IsTiled();
  int GetTileWidth();
  int GetTileHeight();
  rl2WmsLayerPtr GetLayerHandle()
  {
    return CurrentLayer;
  }
  rl2WmsTiledLayerPtr GetTiledLayerHandle()
  {
    return CurrentTiledLayer;
  }
  int IsQueryable()
  {
    if (is_wms_layer_queryable(CurrentLayer) > 0)
      return 1;
    return 0;
  }
  int IsOpaque();
  wxString & GetURL()
  {
    return URL;
  }
  const char *GetServiceVersion()
  {
    return get_wms_version(Catalog);
  }
  const char *GetServiceName()
  {
    return get_wms_name(Catalog);
  }
  const char *GetServiceTitle()
  {
    return get_wms_title(Catalog);
  }
  const char *GetServiceAbstract()
  {
    return get_wms_abstract(Catalog);
  }
  const char *GetTileServiceName()
  {
    return get_wms_tile_service_name(Catalog);
  }
  const char *GetTileServiceTitle()
  {
    return get_wms_tile_service_title(Catalog);
  }
  const char *GetTileServiceAbstract()
  {
    return get_wms_tile_service_abstract(Catalog);
  }
  const char *GetURL_GetMap_Get()
  {
    return get_wms_url_GetMap_get(Catalog);
  }
  const char *GetURL_GetMap_Post()
  {
    return get_wms_url_GetTileService_post(Catalog);
  }
  const char *GetURL_GetTileService_Get()
  {
    return get_wms_url_GetTileService_get(Catalog);
  }
  const char *GetURL_GetTileService_Post()
  {
    return get_wms_url_GetTileService_post(Catalog);
  }
  const char *GetURL_GetFeatureInfo_Get()
  {
    return get_wms_url_GetFeatureInfo_get(Catalog);
  }
  const char *GetURL_GetFeatureInfo_Post()
  {
    return get_wms_url_GetFeatureInfo_post(Catalog);
  }
  const char *GetGmlMimeType()
  {
    return get_wms_gml_mime_type(Catalog);
  }
  const char *GetXmlMimeType()
  {
    return get_wms_xml_mime_type(Catalog);
  }
  const char *GetContactPerson()
  {
    return get_wms_contact_person(Catalog);
  }
  const char *GetContactOrganization()
  {
    return get_wms_contact_organization(Catalog);
  }
  const char *GetContactPosition()
  {
    return get_wms_contact_position(Catalog);
  }
  const char *GetPostalAddress()
  {
    return get_wms_contact_postal_address(Catalog);
  }
  const char *GetCity()
  {
    return get_wms_contact_city(Catalog);
  }
  const char *GetStateProvince()
  {
    return get_wms_contact_state_province(Catalog);
  }
  const char *GetPostCode()
  {
    return get_wms_contact_post_code(Catalog);
  }
  const char *GetCountry()
  {
    return get_wms_contact_country(Catalog);
  }
  const char *GetVoiceTelephone()
  {
    return get_wms_contact_voice_telephone(Catalog);
  }
  const char *GetFaxTelephone()
  {
    return get_wms_contact_fax_telephone(Catalog);
  }
  const char *GetEMailAddress()
  {
    return get_wms_contact_email_address(Catalog);
  }
  const char *GetFees()
  {
    return get_wms_fees(Catalog);
  }
  const char *GetAccessConstraints()
  {
    return get_wms_access_constraints(Catalog);
  }
  int GetLayerLimit()
  {
    return get_wms_layer_limit(Catalog);
  }
  int GetMaxWidth()
  {
    return get_wms_max_width(Catalog);
  }
  int GetMaxHeight()
  {
    return get_wms_max_height(Catalog);
  }
  const char *GetName()
  {
    return get_wms_layer_name(CurrentLayer);
  }
  const char *GetTitle()
  {
    return get_wms_layer_title(CurrentLayer);
  }
  const char *GetAbstract()
  {
    return get_wms_layer_abstract(CurrentLayer);
  }
  const char *GetVersion();
  const char *GetStyleName();
  const char *GetStyleTitle();
  const char *GetStyleAbstract();
  const char *GetFormat();
  const char *GetCRS();
  int IsSwapXY()
  {
    return SwapXY;
  }
  double GetMinX();
  double GetMaxX();
  double GetMinY();
  double GetMaxY();
  double GetGeoMinX();
  double GetGeoMaxX();
  double GetGeoMinY();
  double GetGeoMaxY();
  double GetMinScaleDenominator()
  {
    return get_wms_layer_min_scale_denominator(CurrentLayer);
  }
  double GetMaxScaleDenominator()
  {
    return get_wms_layer_max_scale_denominator(CurrentLayer);
  }
  int CountFormats()
  {
    return get_wms_format_count(Catalog, 1);
  }
  const char *GetFormatByIndex(int idx)
  {
    return get_wms_format(Catalog, idx, 1);
  }
  int CountStyles()
  {
    return get_wms_layer_style_count(CurrentLayer);
  }
  bool GetStyleByIndex(int idx, const char **name, const char **title,
                       const char **abstract);
  int CountCRS()
  {
    return get_wms_layer_crs_count(CurrentLayer);
  }
  const char *GetCrsByIndex(int idx)
  {
    return get_wms_layer_crs(CurrentLayer, idx);
  }
  WmsBBox *GetBBoxByCRS(const char *crs);
  bool IsWmsTileService()
  {
    if (is_wms_tile_service(Catalog) == 1)
      return true;
    else
      return false;
  }
  const char *GetTiledLayerName()
  {
    return get_wms_tiled_layer_name(CurrentTiledLayer);
  }
  const char *GetTiledLayerTitle()
  {
    return get_wms_tiled_layer_title(CurrentTiledLayer);
  }
  const char *GetTiledLayerAbstract()
  {
    return get_wms_tiled_layer_abstract(CurrentTiledLayer);
  }
  const char *GetTiledLayerPad()
  {
    return get_wms_tiled_layer_pad(CurrentTiledLayer);
  }
  const char *GetTiledLayerBands()
  {
    return get_wms_tiled_layer_bands(CurrentTiledLayer);
  }
  const char *GetTiledLayerDataType()
  {
    return get_wms_tiled_layer_data_type(CurrentTiledLayer);
  }
  int GetTilePatternCount()
  {
    return get_wms_tile_pattern_count(CurrentTiledLayer);
  }
  rl2WmsTilePatternPtr GetTilePatternHandle(int i);
  const char *GetTilePatternSRS(int i);
  int GetTilePatternWidth(int i);
  int GetTilePatternHeight(int i);
  double GetTilePatternBaseX(int i);
  double GetTilePatternBaseY(int i);
  double GetTilePatternExtentX(int i);
  double GetTilePatternExtentY(int i);
  double GetTiledLayerMinLong();
  double GetTiledLayerMinLat();
  double GetTiledLayerMaxLong();
  double GetTiledLayerMaxLat();
  wxString & GetHttpProxy()
  {
    return HttpProxy;
  }
  void PrepareCatalog(void);
  void UpdateSwapXY(void);
  void OnProxy(wxCommandEvent & event);
  void OnCrsChanged(wxCommandEvent & event);
  void OnVersionChanged(wxCommandEvent & event);
  void OnSwapXYChanged(wxCommandEvent & event);
  void OnTiledChanged(wxCommandEvent & event);
  void OnCatalog(wxCommandEvent & event);
  void OnReset(wxCommandEvent & event);
  void OnWmsOk(wxCommandEvent & event);
  void OnQuit(wxCommandEvent & event);
};

class WmsLayerDialog:public wxDialog
{
//
// a dialog for configuring a WMS Layer
//
private:
  MyFrame * MainFrame;
  int MaxWidth;
  int MaxHeight;
  char *Version;
  int SwapXY;
  WmsLayer *Layer;
public:
    WmsLayerDialog()
  {
    Version = NULL;
  }
  virtual ~ WmsLayerDialog()
  {
    if (Version != NULL)
      delete[]Version;
  }
  bool Create(MyFrame * parent, WmsLayer * lyr);
  void CreateControls();
  int IsTiled();
  int GetTileWidth();
  int GetTileHeight();
  int IsOpaque();
  const char *GetVersion();
  const char *GetStyleName();
  const char *GetStyleTitle();
  const char *GetStyleAbstract();
  const char *GetFormat();
  const char *GetCRS();
  double GetMinX();
  double GetMaxX();
  double GetMinY();
  double GetMaxY();
  int IsSwapXY()
  {
    return SwapXY;
  }
  void UpdateSwapXY(void);
  void OnCrsChanged(wxCommandEvent & event);
  void OnVersionChanged(wxCommandEvent & event);
  void OnSwapXYChanged(wxCommandEvent & event);
  void OnTiledChanged(wxCommandEvent & event);
  void OnWmsOk(wxCommandEvent & event);
  void OnQuit(wxCommandEvent & event);
};

class WmsMapCrsDialog:public wxDialog
{
//
// a dialog for selecting the Map CRS
//
private:
  MyFrame * MainFrame;
  wxString CRS;
public:
    WmsMapCrsDialog()
  {;
  }
  virtual ~ WmsMapCrsDialog()
  {;
  }
  bool Create(MyFrame * parent);
  void CreateControls();
  wxString & GetCRS()
  {
    return CRS;
  }
  void OnOk(wxCommandEvent & event);
};

class WmsInfoDialog:public wxDialog
{
//
// a dialog for showing infos about a WMS Layer
//
private:
  MyFrame * MainFrame;
  WmsLayer *Layer;
  wxHtmlWindow *HtmlCtrl;
public:
    WmsInfoDialog()
  {
    Layer = NULL;
  }
  virtual ~ WmsInfoDialog()
  {;
  }
  bool Create(MyFrame * parent, WmsLayer * layer);
  void CreateControls();
  void OnSize(wxSizeEvent & event);
};

class WmsCacheDialog:public wxDialog
{
//
// a dialog for showing infos about the WMS internal Cache
//
private:
  MyFrame * MainFrame;
  rl2WmsCachePtr Cache;
public:
    WmsCacheDialog()
  {
    Cache = NULL;
  }
  virtual ~ WmsCacheDialog()
  {;
  }
  bool Create(MyFrame * parent, rl2WmsCachePtr cache);
  void CreateControls();
  void OnCacheReset(wxCommandEvent & event);
  void OnOk(wxCommandEvent & event);
};

class UserScaleDialog:public wxDialog
{
//
// a dialog for selecting a precise Scale
//
private:
  MyFrame * MainFrame;
  int CurrentScale;
  double PixelRatio;
public:
    UserScaleDialog()
  {;
  }
  virtual ~ UserScaleDialog()
  {;
  }
  bool Create(MyFrame * parent, int scale, double pixel_ratio);
  void CreateControls();
  int GetCurrentScale()
  {
    return CurrentScale;
  }
  void OnOk(wxCommandEvent & event);
  void OnScaleChanged(wxSpinEvent & event);
};

class PrinterDialog:public wxDialog
{
//
// a dialog for selecting a Printer
//
private:
  MyFrame * MainFrame;
  int Dpi;
  double PageWidth;
  double PageHeight;
  double HorzMarginSize;
  double VertMarginSize;
  int Width;
  int Height;
  bool PreserveScale;
  bool PreserveWidth;
  bool PreserveHeight;
  bool PDF;
  bool TIFF;
  bool GeoTiff;
  bool Worldfile;
  unsigned char Compression;
  int TileSize;
  int StripSize;
public:
    PrinterDialog()
  {;
  }
  virtual ~ PrinterDialog()
  {;
  }
  bool Create(MyFrame * parent);
  void CreateControls();
  int GetWidth()
  {
    return Width;
  }
  int GetHeight()
  {
    return Height;
  }
  int GetDPI()
  {
    return Dpi;
  }
  double GetPageWidth()
  {
    return PageWidth;
  }
  double GetPageHeight()
  {
    return PageHeight;
  }
  double GetHorzMarginSize()
  {
    return HorzMarginSize;
  }
  double GetVertMarginSize()
  {
    return VertMarginSize;
  }
  bool IsPreserveScale()
  {
    return PreserveScale;
  }
  bool IsPreserveWidth()
  {
    return PreserveWidth;
  }
  bool IsPreserveHeight()
  {
    return PreserveHeight;
  }
  bool IsPDF()
  {
    return PDF;
  }
  bool IsTIFF()
  {
    return TIFF;
  }
  bool IsGeoTiff()
  {
    return GeoTiff;
  }
  bool HasWorldfile()
  {
    return Worldfile;
  }
  unsigned char GetCompression()
  {
    return Compression;
  }
  bool IsTiled()
  {
    if (TileSize > 0)
      return true;
    else
      return false;
  }
  int GetTileSize()
  {
    return TileSize;
  }
  int GetStripSize()
  {
    return StripSize;
  }
  void OnFormatChanged(wxCommandEvent & event);
  void OnOk(wxCommandEvent & event);
};

class PdfPrinter
{
//
// a class implementing the Map PDF printer
//
private:
  MyFrame * MainFrame;
  MyMapView *MapView;
  char *Path;
  rl2GraphicsContextPtr Ctx;
  double FrameWidth;
  double FrameHeight;
  double FrameCX;
  double FrameCY;
  double FrameExtentX;
  double FrameExtentY;
  double FrameMinX;
  double FrameMinY;
  double FrameMaxX;
  double FrameMaxY;
  double PixelRatio;
  wxGauge *ProgressControl;
  WmsGetMapRequest *FirstRequest;
  WmsGetMapRequest *LastRequest;
  void InitPreserveScale();
  void InitPreserveWidth();
  void InitPreserveHeight();
  void AddGetMapRequest(int pos_x, int pos_y, const char *url,
                        const char *version,
                        const char *layer,
                        const char *crs, int swap_xy,
                        double minx, double miny,
                        double maxx, double maxy,
                        int width, int height,
                        const char *style, const char *format, int opaque);
  void AddGetMapRequest(int pos_x, int pos_y, const char *url,
                        const char *layer, int swap_xy,
                        double minx, double miny,
                        double maxx, double maxy, int width, int height,
                        WmsTilePattern * tilepattern);
  int CountGetMapRequests();
public:
    PdfPrinter(MyFrame * parent, const char *path, int dpi, double page_w,
               double page_h, double horz_margin_sz, double vert_margin_sz,
               bool preserve_scale, bool preserve_width, bool preserve_height);
   ~PdfPrinter();
  void Go();
  MyMapView *GetMapView()
  {
    return MapView;
  }
  bool IsValid()
  {
    if (Ctx == NULL)
      return false;
    return true;
  }
  void Consider(void);
  void UpdatePendingTiles();
  void Consider(const char *layer_name);
  void PrintTile(unsigned char *rgba, int pos_x, int pos_y, int width,
                 int height);
  WmsGetMapRequest *GetFirstRequest()
  {
    return FirstRequest;
  }
};

class TiffWriter
{
//
// a class implementing the Map TIFF printer
//
private:
  MyFrame * MainFrame;
  MyMapView *MapView;
  char *Path;
  int FrameWidth;
  int FrameHeight;
  unsigned char *Pixels;
  unsigned int TotPixels;
  unsigned int MonoPixels;
  unsigned int GrayPixels;
  unsigned char Red[256];
  unsigned char Green[256];
  unsigned char Blue[256];
  int MaxPalette;
  bool GeoTiff;
  bool Worldfile;
  unsigned char Compression;
  int IsTiled;
  int TileSize;
  unsigned char ColorSpace;
  double FrameCX;
  double FrameCY;
  double FrameExtentX;
  double FrameExtentY;
  double FrameMinX;
  double FrameMinY;
  double FrameMaxX;
  double FrameMaxY;
  double PixelRatio;
  wxGauge *ProgressControl;
  WmsGetMapRequest *FirstRequest;
  WmsGetMapRequest *LastRequest;
  void InitPreserveScale();
  void InitPreserveWidth();
  void InitPreserveHeight();
  void AddGetMapRequest(int pos_x, int pos_y, const char *url,
                        const char *version,
                        const char *layer,
                        const char *crs, int swap_xy,
                        double minx, double miny,
                        double maxx, double maxy,
                        int width, int height,
                        const char *style, const char *format, int opaque);
  void AddGetMapRequest(int pos_x, int pos_y, const char *url,
                        const char *layer, int swap_xy,
                        double minx, double miny,
                        double maxx, double maxy, int width, int height,
                        WmsTilePattern * tilepattern);
  int CountGetMapRequests();
public:
    TiffWriter(MyFrame * parent, const char *path, int width, int height,
               bool geo_tiff, bool has_worldfile, unsigned char compression,
               bool is_tiled, int tile_strip_size, bool preserve_scale,
               bool preserve_width, bool preserve_height);
   ~TiffWriter();
  bool IsValid()
  {
    if (Pixels == NULL)
      return false;
    return true;
  }
  bool IsMonochrome()
  {
    if (MonoPixels == TotPixels)
      return true;
    else
      return false;
  }
  bool IsGrayscale()
  {
    if (GrayPixels == TotPixels)
      return true;
    else
      return false;
  }
  bool IsPalette()
  {
    if (MaxPalette <= 256)
      return true;
    else
      return false;
  }
  unsigned char FindPaletteIndex(unsigned char red, unsigned char green,
                                 unsigned char blue);
  void SetColorSpace(unsigned char cs)
  {
    ColorSpace = cs;
  }
  unsigned char GetCompression()
  {
    return Compression;
  }
  void SetCompression(unsigned char compression)
  {
    Compression = compression;
  }
  int GetSrid();
  void Go();
  MyMapView *GetMapView()
  {
    return MapView;
  }
  void Consider(void);
  void UpdatePendingTiles();
  void Consider(const char *layer_name);
  void PrintTile(unsigned char *rgba, int pos_x, int pos_y, int width,
                 int height);
  WmsGetMapRequest *GetFirstRequest()
  {
    return FirstRequest;
  }
  rl2PalettePtr BuildPalette();
  void ExportImage();
};

class TiffMonochromeDialog:public wxDialog
{
//
// a dialog supporting Monochrome TIFF export
//
private:
  MyMapView * Parent;
  bool Monochrome;
  unsigned char Compression;
public:
    TiffMonochromeDialog()
  {;
  }
  virtual ~ TiffMonochromeDialog()
  {;
  }
  bool Create(MyMapView * parent);
  void CreateControls();
  bool IsMonochrome()
  {
    return Monochrome;
  }
  unsigned char GetCompression()
  {
    return Compression;
  }
  void OnColorspaceChanged(wxCommandEvent & event);
  void OnOk(wxCommandEvent & event);
};

class TiffGrayscaleDialog:public wxDialog
{
//
// a dialog supporting Grayscale TIFF export
//
private:
  MyMapView * Parent;
  bool Grayscale;
  unsigned char Compression;
public:
    TiffGrayscaleDialog()
  {;
  }
  virtual ~ TiffGrayscaleDialog()
  {;
  }
  bool Create(MyMapView * parent, unsigned char compression);
  void CreateControls();
  bool IsGrayscale()
  {
    return Grayscale;
  }
  unsigned char GetCompression()
  {
    return Compression;
  }
  void OnColorspaceChanged(wxCommandEvent & event);
  void OnOk(wxCommandEvent & event);
};

class TiffPaletteDialog:public wxDialog
{
//
// a dialog supporting Palette TIFF export
//
private:
  MyMapView * Parent;
  bool Palette;
  unsigned char Compression;
public:
    TiffPaletteDialog()
  {;
  }
  virtual ~ TiffPaletteDialog()
  {;
  }
  bool Create(MyMapView * parent, unsigned char compression);
  void CreateControls();
  bool IsPalette()
  {
    return Palette;
  }
  unsigned char GetCompression()
  {
    return Compression;
  }
  void OnColorspaceChanged(wxCommandEvent & event);
  void OnOk(wxCommandEvent & event);
};
